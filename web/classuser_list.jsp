<%-- 
    Document   : classuser_list
    Created on : Jun 27, 2022, 4:25:40 PM
    Author     : admin
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>

<t:wrapper title='Class User List'>
    <h1>
        Class User List
    </h1>
    <table id="table" data-toggle="table" data-search="true" data-filter-control="true" data-show-search-clear-button="true"  data-pagination="true" data-resizable="true"  data-page-list="[10 ,25 ,50 ,100 ,all]">
        <thead>
            <tr>
                <th data-sortable="true" data-field="class_code" data-filter-control="select">Class Code</th>
                <th data-sortable="true" data-field="team_code" data-filter-control="select">Team Code</th>
                <th data-sortable="true">Full Name</th>
                <th >Role ID</th>
                <th data-sortable="true" data-field="team_leader" data-filter-control="select">Team Leader</th>
                <th data-sortable="true" data-field="dropout_date" data-filter-control="select">Dropout Date</th>
                <th>Status</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            <c:forEach items="${requestScope.culist}" var="culist">
                <tr>
                    <td>${culist.classes.class_code} </td>
                    <td>${culist.team.team_code} </td>
                    <td>${culist.account.displayname}</td>
                    <td>${culist.account.role_id}</td>
                    <td><c:if test="${culist.team_leader ==1}">No</c:if>
                        <c:if test="${culist.team_leader ==2}">Yes</c:if>
                        </td>
                        <td><fmt:formatDate pattern="dd/MM/yyyy" value="${culist.dropout_date}"/></td>
                    <td><c:if test="${culist.status == 1}">Activate</c:if>
                        <c:if test="${culist.status == 2}">Deactivate</c:if>
                        </td>
                        <td><a href="classuserdetail?email=${culist.account.email}" class="zoom btn btn-success"><i class="bi bi-layout-text-sidebar-reverse"></i></a></td>
                </tr>
            </c:forEach>
        </tbody>
    </table>
    <form action="classuserlist" method="POST" class="w-50">
        <div  class="mb-3">
            <label class="form-label" for="filenameInput">File Name</label> 
            <input type="text" class="form-control" name="filename" id="filenameInput">
        </div>
        <button type="submit" class="btn btn-primary"><i class="bi bi-file-earmark-spreadsheet"></i></button>
    </form>
    <a class="btn btn-success" style="color: white" href="index"><i class="bi bi-house"></i></a>
    </t:wrapper>

