<%@tag description="layout" pageEncoding="UTF-8" %>

<%@attribute name="title" %>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<!DOCTYPE html>
<html>
    <head>
        <title>${title}</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" crossorigin="anonymous">
        <link href="https://unpkg.com/jquery-resizable-columns@0.2.3/dist/jquery.resizableColumns.css" rel="stylesheet">
        <link href="https://unpkg.com/bootstrap-table@1.20.2/dist/bootstrap-table.min.css" rel="stylesheet">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.3/font/bootstrap-icons.css">
        <link rel="stylesheet" href="https://unpkg.com/bootstrap-table@1.20.2/dist/bootstrap-table.min.css">
        <link href="https://unpkg.com/bootstrap-table@1.20.2/dist/bootstrap-table.min.css" rel="stylesheet">
        <link href="https://unpkg.com/bootstrap-table@1.20.2/dist/extensions/page-jump-to/bootstrap-table-page-jump-to.min.css" rel="stylesheet">
    </head>
    <style>
        .zoom {
            transition: transform .2s; 
            margin: 0 auto;
        }

        .zoom:hover {
            transform: scale(1.2); 
        }
    </style>
    <body>
        <header>
            <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
                <div class="container">
                    <a class="navbar-brand" href="index">SPM</a>

                    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent">
                        <span class="navbar-toggler-icon"></span>
                    </button>

                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav me-auto mb-2 mb-lg-0">


                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown"><i class="bi bi-menu-button"></i></a>
                                <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                                    <c:if test="${sessionScope.account != null}">
                                        <c:if test="${sessionScope.account.role_id ==1}">
                                            <li><a class="dropdown-item" href="featurelist">Feature List</a></li>
                                            <li><a class="dropdown-item" href="functionlist">Function List</a></li>
                                            <li><a class="dropdown-item" href="issuelist">Issue List</a></li>
                                            <li><a class="dropdown-item" href="trackinglist">Tracking List</a></li>
                                            <li><a class="dropdown-item" href="iterevalist">Iter Evaluation List</a></li>
                                            <li><a class="dropdown-item" href="teamevallist">Team Evaluation List</a></li>
                                            </c:if>
                                            <c:if test="${sessionScope.account.role_id == 2}">
                                            <li><a class="dropdown-item" href="settinglist">Setting List</a></li>
                                            <li><a class="dropdown-item" href="userlist">Display all user</a></li>
                                            <li><a class="dropdown-item" href="subjectlist">Subject List</a></li>
                                            <li><a class="dropdown-item" href="iterationlist">Iteration List</a></li>
                                            <li><a class="dropdown-item" href="evacriterialist">Evaluation critetia</a></li>
                                            <li><a class="dropdown-item" href="classlist">Class List</a></li>
                                            </c:if>
                                            <c:if test="${sessionScope.account.role_id == 3}">
                                            <li><a class="dropdown-item" href="iterationlist">Iteration List</a></li>
                                            <li><a class="dropdown-item" href="evacriterialist">Evaluation critetia</a></li>
                                            <li><a class="dropdown-item" href="classlist">Class List</a></li>
                                            <li><a class="dropdown-item" href="subjectsettinglist">Subject Setting List</a></li>
                                            </c:if>
                                            <c:if test="${sessionScope.account.role_id == 4}">
                                            <li><a class="dropdown-item" href="classlist">Class List</a></li>
                                            <li><a class="dropdown-item" href="milestonelist">Milestone List</a></li>
                                            <li><a class="dropdown-item" href="teamlist">Team List</a></li>
                                            <li><a class="dropdown-item" href="classuserlist">Class User List</a></li>
                                            <li><a class="dropdown-item" href="featurelist">Feature List</a></li>
                                            <li><a class="dropdown-item" href="functionlist">Function List</a></li>
                                            <li><a class="dropdown-item" href="issuelist">Issue List</a></li>
                                            <li><a class="dropdown-item" href="trackinglist">Tracking List</a></li>
                                            <li><a class="dropdown-item" href="iterevalist">Iter Evaluation List</a></li>
                                            <li><a class="dropdown-item" href="teamevallist">Team Evaluation List</a></li>
                                            </c:if>
                                        </c:if>
                                    <li><hr class="dropdown-divider"></li>
                                    <li >
                                        <a class="dropdown-item" href="index">Home</a>
                                    </li>
                                    <c:if test="${sessionScope.account != null}">
                                        <li >
                                            <a class="dropdown-item" href="https://gitlab.com/hieutvan/spm">Link</a>
                                        </li>
                                    </c:if>
                                </ul>
                            </li>

                        </ul>

                        <%-- <form class="d-flex" role="search">
                                <input class="form-control me-2" type="search" placeholder="Search">
                                <button class="btn btn-success" type="submit">Search</button>
                        </form> --%>

                        <ul class='navbar-nav ms-auto'>
                            <c:if test="${not empty sessionScope.account}">
                                <%--
                                        <li class="nav-item dropdown">
                                                <a class="nav-link dropdown-toggle" role="button" data-bs-toggle="dropdown">
                                                        <svg class="bi me-1" width="16" height="16" fill="currentColor">
                                                                <use xlink:href="{{ asset('img/bootstrap-icons.svg') }}#person-circle"/>
                                                        </svg>
                                                        {{ Auth::user()->username }}
                                                </a>
                                                <ul class="dropdown-menu">
                                                        <div class="dropdown-divider"></div>
                                                        <form method='post' action="{{ route('auth.logout') }}">
                                                                @csrf
                                                                <button type='submit' class="btn text-danger btn-link">{{ __('Đăng xuất') }}</button>
                                                        </form>
                                                </ul>
                                        </li>
                                --%>
                            </c:if>
                            <c:if test="${empty sessionScope.account}">
                                <li class="nav-item">
                                    <a class="nav-link" href="login">Login</a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link" href="register">Register</a>
                                </li>
                            </c:if>
                            <c:if test="${sessionScope.account != null}">
                                <li class="nav-item">
                                    <a class="nav-link" href="accountprofile?email=${sessionScope.account.email}"><i class="bi bi-person-circle"></i> ${sessionScope.account.username}</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="logout"><i class="bi bi-box-arrow-right"></i></a>
                                </li>

                            </c:if>
                        </ul>
                    </div>
                </div>
            </nav>
        </header>

        <div class="container py-3 min-vh-100">
            <jsp:doBody />
        </div>

        <footer class='text-bg-dark'>
            <div class="container py-3">
                <p>© 2022 G2 ISPorts. Cấm sao chép và tấu hề dưới mọi hình thức</p>
                <p>
                    This is a student project management of Group 2<br>
                    Members: 
                    <ul>
                        <li>Văn Trung Hiếu - HE160168</li>
                        <li>Thân Thu Hương - HE163472</li>
                        <li>Lưu Xuân Trường - HE161948</li>
                        <li>Lê Cao Khánh - HE163867</li>
                    </ul>
                </p>
            </div>
        </footer>

        <script src="https://cdn.jsdelivr.net/npm/jquery/dist/jquery.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
        <script src="https://unpkg.com/bootstrap-table@1.20.2/dist/bootstrap-table.min.js"></script>
        <script src="https://unpkg.com/bootstrap-table@1.20.2/dist/extensions/filter-control/bootstrap-table-filter-control.min.js"></script>
        <script src="https://unpkg.com/jquery-resizable-columns@0.2.3/dist/jquery.resizableColumns.min.js"></script>
        <script src="https://unpkg.com/bootstrap-table@1.20.2/dist/extensions/resizable/bootstrap-table-resizable.min.js"></script>
        <script src="https://unpkg.com/bootstrap-table@1.20.2/dist/extensions/page-jump-to/bootstrap-table-page-jump-to.min.js"></script>
    </body>
</html>
