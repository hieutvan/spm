<%-- 
    Document   : update_subject
    Created on : Jun 10, 2022, 10:11:51 PM
    Author     : admin
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<!DOCTYPE html>

<t:wrapper title="Update subject">
    <h1>
        Subject Detail
    </h1>
    <form action="updatesubject" class="w-50" method="POST">
        <input type="text" name="subject_id" value="${requestScope.subj.subject_id}" hidden>
        <div class="mb-3">
            <label for="subject_code" class="form-label">Subject Code</label>
            <input class="form-control" name="subject_code" type="text" id="subjectcodeInput" value="${requestScope.subj.subject_code}" required> 
        </div>
        <div class="mb-3">
            <label for="subject_name" class="form-label">Subject Name</label>
            <input class="form-control" name="subject_name" type="text" id="subjectnameInput" value="${requestScope.subj.subject_name}" required> 
        </div>
        <div class="mb-3">
            <label for="emailInput" class="form-label">Trainer</label>
            <select name="email" class="form-control" style="width: auto" required>

                <c:forEach items="${requestScope.trainerlist}" var="tlist">
                    <option ${requestScope.subj.email.email == tlist.email ? "selected=\"selected\"":""}  value="${tlist.email}">${tlist.displayname} - ${tlist.username}</option>
                </c:forEach>
            </select>
        </div>
        <div class="mb-3">
            <label class="form-label" for="Status">Status</label>
            <c:if test="${requestScope.subj.status == 1}">
                <input name="status" type="radio" id="status" value="1" checked="checked">Active
                <input name="status" type="radio" id="status" value="2">Inactive
            </c:if>
            <c:if test="${requestScope.subj.status == 2}">
                <input name="status" type="radio" id="status" value="1">Active
                <input name="status" type="radio" id="status" value="2" checked="checked">Inactive
            </c:if>
        </div>
        <input type="submit" value="DONE" class="btn btn-success">
        <a href="subjectlist" class="btn btn-success"><i class="bi bi-box-arrow-left"></i></a>
        <a class="btn btn-success" style="color: white" href="index"><i class="bi bi-house"></i></a>
    </form>
</t:wrapper>
