<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<t:wrapper title='Member Evaluation'>
    <h1>Member Evaluation</h1>
    
    <table data-toggle="table">
        <thead>
            <th>Evaluation ID</th>
            <th>Criteria ID</th>
            <th>Converted LOC</th>
            <th>Grade</th>
            <th>Notes</th>
        </thead>
        
        <tbody>
            <c:forEach items="${evals}" var="i">
                <tr>
                    <td>${i.evaluation.evaluation_id}</td>
                    <td>${i.criteria.criteria_id}</td>
                    <td>${i.converted_loc}</td>
                    <td>${i.grade}</td>
                    <td>${i.note}</td>
                </tr>
            </c:forEach>
        </tbody>
    </table>
</t:wrapper>