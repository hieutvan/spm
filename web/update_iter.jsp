<%-- 
    Document   : update_iter
    Created on : Jun 15, 2022, 10:38:13 PM
    Author     : admin
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<!DOCTYPE html>

<t:wrapper title="Update iter">
    <h1>
        Iteration Detail
    </h1>
    <form action="updateiter" class="w-50" method="POST">
        <input type="text" name="iteration_id" value="${requestScope.iter.iteration_id}" hidden>
        <input type="text" name="subject_id" value="${requestScope.iter.subject.subject_id}" hidden>
        <div class="mb-3">
            <label for="iternameInput" class="form-label">Subject Name</label>
            <input class="form-control" name="" type="text" id="iternameInput" value="${requestScope.subject.subject_name}" readonly>
        </div>
        <div class="mb-3">
            <label for="iternameInput" class="form-label">Iteration Name</label>
            <input class="form-control" name="iteration_name" type="text" id="iternameInput" value="${requestScope.iter.iteration_name}">
        </div>
        <div class="mb-3">
            <label for="durationInput" class="form-label">Duration</label>
            <input class="form-control" name="duration" type="text" id="durationInput" value="${requestScope.iter.duration}">
        </div>
        <div class="mb-3">
            <label class="form-label" for="StatusInput">Status</label>
            <c:if test="${requestScope.iter.status == 1}">
                <input name="status" type="radio" id="StatusInput" value="1" checked="checked">Activate
                <input name="status" type="radio" id="StatusInput" value="2">Deactivate
            </c:if>
            <c:if test="${requestScope.iter.status == 2}">
                <input name="status" type="radio" id="StatusInput" value="1">Activate
                <input name="status" type="radio" id="StatusInput" value="2" checked="checked">Deactivate
            </c:if>
        </div>
        <input class="btn btn-success" type="submit" value="DONE">
        <a href="iterationlist" class="btn btn-success"><i class="bi bi-box-arrow-left"></i></a>
        <a class="btn btn-success" style="color: white" href="index"><i class="bi bi-house"></i></a>
    </form>
</t:wrapper>
