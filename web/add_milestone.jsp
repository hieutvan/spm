<%-- 
    Document   : add_milestone
    Created on : Jun 22, 2022, 12:22:15 PM
    Author     : admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>

<t:wrapper title='Add milestone'>
    <h1>
        Add new milestone
    </h1>
    <form action="addmilestone" method="POST" class="w-50">
        <div class="row">
            <div class="col">
                <div class="mb-3">
                    <label class="form-label" for="classInput">Class <span style="color: red">*</span></label>
                    <select name="class_id" class="form-control" required>
                        <c:forEach items="${requestScope.classlist}" var="clist">
                            <option value="${clist.class_id}">${clist.class_id} - ${clist.class_code}</option>
                        </c:forEach>
                    </select>
                </div>
            </div>
            <div class="col">
                <div class="mb-3">
                    <label class="form-label" for="iterationInput">Iteration <span style="color: red">*</span></label>
                    <select name="iteration_id" class="form-control" required>
                        <c:forEach items="${requestScope.iterlist}" var="ilist">
                            <option value="${ilist.iteration_id}">${ilist.iteration_id} - ${ilist.iteration_name}</option>
                        </c:forEach>
                    </select>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <div class="mb-3"> 
                    <label class="form-label" for="fromdateInput">From Date <span style="color: red">*</span></label>
                    <input type="date" name="from_date" class="form-control" id="fromdateInput" placeholder="dd-MM-yyyy" >
                </div>
            </div>
            <div class="col">
                <div class="mb-3"> 
                    <label class="form-label" for="todateInput">To Date <span style="color: red">*</span></label>
                    <input type="date" name="to_date" class="form-control" id="todateInput"  placeholder="dd-MM-yyyy">
                </div>
            </div>
        </div>
        <div class="mb-3">
            <label for="status" class="form-label">Status <span style="color: red">*</span></label>
            <select name="status" class="form-control" style="width: auto">
                <option value="0">Cancel</option>
                <option value="1">Close</option>
                <option value="2">Open</option>
            </select>
        </div>
        <input type="submit" class="zoom btn btn-success" value="DONE">
    </form>
</t:wrapper>

