<%-- 
    Document   : add_criteria
    Created on : Jun 17, 2022, 12:18:09 AM
    Author     : admin
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<!DOCTYPE html>

<t:wrapper title='Add new criteria'>
    <h1>
        Add new criteria
    </h1>
    <form class="w-50" method="POST" action="addcrit">
        <div class="mb-3">
            <div class="row">
                <div class="col">
                    <label class="form-label" for="iterationInput">Iteration <span style="color: red">*</span></label>
                    <select name="iteration_id" class="form-control" required>
                        <c:forEach items="${requestScope.iterlist}" var="iter">
                            <option value="${iter.iteration_id}">${iter.iteration_name}</option>
                        </c:forEach>
                    </select>
                </div>
            </div>
        </div>
        <div class="mb-3">
            <label class="form-label" for="weightInput">Evaluation weight <span style="color: red">*</span></label>    
            <input class="form-control" name="evaluation_weight" type="number" id="weightInput" min="0" max="100" required>
            <p style="color: red" class="warning">${requestScope.error}</p>
        </div>
        <div class="mb-3">
            <label class="form-label" for="teamevaluateInput">Team evaluation <span style="color: red">*</span></label>
            <input type="radio" name="team_evaluation" id="teamevaluateInput" value="1">No
            <input type="radio" name="team_evaluation" id="teamevaluateInput" value="2">Yes
        </div>
        <div class="mb-3">
            <label class="form-label" for="orderinput">Criteria order <span style="color: red">*</span></label>
            <input type="text" name="criteria_order" class="form-control" id="orderinput" min="0" required>
        </div>
        <div class="mb-3">
            <label class="form-label" for="LOCInput">Max Loc <span style="color: red">*</span></label>
            <input type="number" name="max_loc" class="form-control" id="LOCInput" min="0" max="720" required>
        </div>
        <div class="mb-3">
            <label class="form-label" for="status">Status <span style="color: red">*</span></label>
            <input  type="radio" name="status" value="1" id="status">Deactivate
            <input  type="radio" name="status" value="2" id="status">Activate
        </div>

        <input class="xoom btn btn-success" value="DONE" type="submit">
    </form>
</t:wrapper>
