<%-- 
    Document   : issue_list
    Created on : Jul 9, 2022, 4:56:11 PM
    Author     : admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<t:wrapper title='Issue List'>
    <script>
        function submitForm() {
            document.getElementById("frmSearch").submit();
        }
        function updateStatus(issue_id, status) {
            var result = confirm("Do you want to update this issue status?");
            if (result) {
                window.location.href = 'updateisseustatus?issue_id=' + issue_id + '&status=' + status;
            }
        }
    </script>
    <h1>
        Issue List
    </h1>

    <table id="table" data-toggle="table" data-search="true" data-pagination="true" data-filter-control="true" data-show-search-clear-button="true" data-resizable="true"  data-page-list="[10,25,50,100,all]">
        <thead>
            <tr>
                <th data-sortable="true">Issue ID</th>
                <th data-field="issue_title" data-filter-control="select" data-sortable="true">Issue Title</th>
                <th>Issue Type</th>
                <th data-field="function" data-filter-control="select" data-sortable="true">Function</th>
                <th data-field="assignee" data-filter-control="select" data-sortable="true">Assignee</th>
                <th data-field="milestone_id" data-filter-control="select" data-sortable="true">Milestone</th>
                <th data-field="due_date" data-filter-control="select" data-sortable="true">Due Date</th>
                <th data-field="status" data-filter-control="select" data-sortable="true">Status</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            <c:forEach items="${requestScope.issuelist}" var="ilist">
                <tr>
                    <td>${ilist.issue_id}</td>
                    <td>${ilist.issue_title}</td>
                    <td>${ilist.issue_type}</td>
                    <td>${ilist.function.function_name}</td>
                    <td>${ilist.account.displayname}</td>
                    <td>${ilist.milestone.milestone_id}</td>
                    <td><fmt:formatDate pattern="dd/MM/yyyy" value="${ilist.due_date}"/></td>
                    <td>
                        <c:if test="${ilist.status == 2}">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown">Open</a>
                        </c:if>
                        <c:if test="${ilist.status == 1}">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown">Close</a>
                        </c:if>
                        <c:if test="${ilist.status == 0}">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown">Pending</a>
                        </c:if>
                        <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <li><a href="#" onclick="updateStatus(${ilist.issue_id}, 0)" class="dropdown-item">Pending</a></li>
                            <li><a href="#" onclick="updateStatus(${ilist.issue_id}, 1)" class="dropdown-item">Close</a></li>
                            <li><a href="#" onclick="updateStatus(${ilist.issue_id}, 2)" class="dropdown-item">Open</a></li>
                        </ul>
                    </td>
                    <td><a class="zoom btn btn-success" href="issuedetail?issue_id=${ilist.issue_id}"><i class="bi bi-layout-text-sidebar-reverse"></i></a></td>
                </tr>
            </c:forEach>
        </tbody>
    </table>
    <a class="btn btn-success" style="color: white" href="index"><i class="bi bi-house"></i></a>
    <a href="addissue" class="btn btn-success"><i class="bi bi-plus"></i></a>
    <span>
        <form action="issuelist" method="POST" class="w-50">
            <input type="text" value="${requestScope.team_id}" name="team_id" hidden>
            <div>
                <label class="form-label" for="filenameInput">File Name</label> 
                <input type="text" class="form-control" name="filename" id="filenameInput">
            </div>
            <button type="submit" class="btn btn-primary"><i class="bi bi-file-earmark-spreadsheet"></i></button>
        </form>
    </span>
</t:wrapper>

