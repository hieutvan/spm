
<%-- 
    Document   : userprofile
    Created on : May 30, 2022, 8:05:35 PM
    Author     : admin
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


<t:wrapper title="Account profile">
    <h1>
        Account Profile
    </h1>
    <form action="accountprofile" class="w-50" method="POST">
        <div class="mb-3">
            <label for="fullNameInput" class="form-label">Full Name</label>
            <input id="fullNameInput" class="form-control" name="displayname" value="${sessionScope.account.displayname}" ><br/>
        </div>
        <div class="mb-3">
            <label for="usernameInput" class="form-label">Username</label>
            <input id="usernameInput" class="form-control" name="username" value="${sessionScope.account.username}" ><br/>
        </div>
        <div class="mb-3">
            <label for="email" class="form-label">Email</label>
            <input id="emailInput" type="email" class="form-control" name="email" value="${sessionScope.account.email}" readonly><br/>
        </div>
        <div class="mb-3">
            <label for="phonenumber" class="form-label">Phone number</label>
            <input id="phoneInput" class="form-control" name="phonenumber" value="${sessionScope.account.phonenumber}"><br/>
        </div >
        <div class="mb-3">
            <c:if test="${sessionScope.account.status == 1}"><input name="status" type="radio" id="status" value="1" checked="checked">Activate
                <input name="status" type="radio" id="status" value="2"> Deactivate</c:if>
            <c:if test="${sessionScope.account.status == 2}">
                <input name="status" type="radio" id="status" value="1" checked="checked">Activate
                <input name="status" type="radio" id="status" value="2" checked="checked">Deactivate</c:if>
            </div>
            <div><input type="password" name="password" value="${sessionScope.account.password}" hidden></div>
        <input class="btn btn-success" type="submit" value="DONE">
        <a style="color: white" class="btn btn-primary" href="changepassword?email=${sessionScope.account.email}">Change password</a>
        <br/>

    </form>
</t:wrapper>


