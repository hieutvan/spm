<%-- 
    Document   : teameval_list
    Created on : Jul 21, 2022, 12:42:39 AM
    Author     : admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <t:wrapper title='Team Evaluation List'>
            <table id="table" data-toggle="table" data-search="true" data-pagination="true" data-filter-control="true" data-show-search-clear-button="true" data-resizable="true"  data-page-list="[10,25,50,100,all]">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Team</th>
                        <th>Class</th>
                        <th>Subject</th>
                        <th>Iteration</th>
                        <th>Criteria</th>
                        <th>Weight</th>
                        <th>Grade</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach items="${requestScope.teamevallist}" var="telist">
                        <tr>
                            <td>${telist.team_eval_id}</td>
                            <td>${telist.team.team_code}</td>
                            <td>${telist.team.classes.class_code}</td>
                            <td>${telist.criteria.iteration.subject.subject_name}</td>
                            <td>${telist.criteria.iteration.iteration_name}</td>
                            <td>${telist.criteria.criteria_id}</td>
                            <td>${telist.criteria.evaluation_weight}%</td>
                            <td>${telist.grade}</td>
                            <td><a href="teamevaldetail?team_eval_id=${telist.team_eval_id}"><i class="bi bi-layout-text-sidebar-reverse"></i></a></td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
        </t:wrapper>
    </body>
</html>
