<%-- 
    Document   : function_list
    Created on : Jul 8, 2022, 9:29:11 PM
    Author     : admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>

<t:wrapper title='Function List'>
    <h1>
        Function List
    </h1>
    <script>
        function submitForm() {
            document.getElementById("frmSearch").submit();
        }
        function updateFunctionStatus(function_id, status) {
            var result = confirm("Do you want to update this function status?");
            if (result) {
                window.location.href = 'updatefunctionstatus?function_id=' + function_id + '&status=' + status;
            }
        }
    </script>
    <table id="table" data-toggle="table" data-search="true" data-pagination="true" data-filter-control="true" data-show-search-clear-button="true" data-resizable="true"  data-page-list="[10,25,50,100,all]">
        <thead>
            <tr>
                <th data-sortable="true" data-field="function_name" data-filter-control="select">Function Name</th>
                <th data-sortable="true" data-field="team_code" data-filter-control="select">Team Code</th>
                <th data-sortable="true" data-field="feature_name" data-filter-control="select">Feature</th>
                <th data-sortable="true" >Owner</th>
                <th data-sortable="true" >Access Roles</th>
                <th data-sortable="true" >Complexity</th>
                <th data-sortable="true" data-field="status" data-filter-control="select">Status</th>
                <th data-sortable="true">Action</th>
            </tr>
        </thead>
        <tbody>
            <c:forEach items="${requestScope.functionlist}" var="flist">
                <tr>
                    <td>${flist.function_name}</td>
                    <td>${flist.team.team_code}</td>
                    <td>${flist.feature.feature_name}</td>
                    <td>${flist.account.displayname}</td>
                    <td>${flist.access_roles}</td>
                    <td><c:if test="${flist.complexity_id == 3}">
                            Complex
                        </c:if>
                        <c:if test="${flist.complexity_id == 2}">
                            Medium
                        </c:if>
                        <c:if test="${flist.complexity_id == 1}">
                            Simple
                        </c:if>
                    </td>
                    <td>
                            <c:if test="${flist.status == 1}">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown">Pending</a>
                            </c:if>
                            <c:if test="${flist.status == 2}">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown">Planned</a>
                            </c:if>
                            <c:if test="${flist.status == 3}">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown">Evaluated</a>
                            </c:if>
                            <c:if test="${flist.status == 4}">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown">Rejected</a>
                            </c:if>
                            <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <li><a href="#" onclick="updateFunctionStatus(${flist.function_id}, 1);" class="dropdown-item">Pending</a></li>
                                <li><a href="#" onclick="updateFunctionStatus(${flist.function_id}, 2);" class="dropdown-item">Planned</a></li>
                                <li><a href="#" onclick="updateFunctionStatus(${flist.function_id}, 3);" class="dropdown-item">Evaluated</a></li>
                                <li><a href="#" onclick="updateFunctionStatus(${flist.function_id}, 4);" class="dropdown-item">Rejected</a></li>
                            </ul>
                    </td>
                    <td><a class="zoom btn btn-success" href="functiondetail?function_id=${flist.function_id}"><i class="bi bi-layout-text-sidebar-reverse"></i></a></td>
                </tr>
            </c:forEach>
        </tbody>
    </table>
    <a class="btn btn-success" style="color: white" href="index"><i class="bi bi-house"></i></a>
    <a href="addfunction" class="btn btn-success"><i class="bi bi-plus"></i></a>
    <form action="functionlist" method="POST" class="w-50">
        <input type="text" value="${requestScope.team_id}" name="team_id" hidden>
        <div  class="mb-3">
            <label class="form-label" for="filenameInput">File Name</label> 
            <input type="text" class="form-control" name="filename" id="filenameInput">
        </div>
        <button type="submit" class="btn btn-primary"><i class="bi bi-file-earmark-spreadsheet"></i></button>
    </form>
</t:wrapper>

