<%-- 
    Document   : specificclassuser_list
    Created on : Jun 29, 2022, 12:23:49 AM
    Author     : admin
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <body>
        <t:wrapper title='Class User List'>
            <table id="table" data-toggle="table" data-search="true" data-pagination="true" data-page-list="[10 ,25 ,50 ,100 ,all]">
                <thead>
                <p style="text-align: right; font-size: 30px">Class code: ${requestScope.classes.class_code}</p>
                <tr>
                    <th>Class Code</th>
                    <th data-sortable="true">Team_id</th>
                    <th data-sortable="true">Full name</th>
                    <th>Role ID</th>
                    <th data-sortable="true">Team Leader</th>
                    <th data-sortable="true">Dropout Date</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>

                <c:forEach items="${requestScope.tculist}" var="tculist">
                    <tr>
                        <td>${tculist.classes.class_code}</td>
                        <td>${tculist.team.team_id}</td>
                        <td>${tculist.account.displayname} <a href="userprofile?email=${tculist.account.email}" class="btn btn-success"><i class="bi bi-layout-text-sidebar-reverse"></i></a></td>
                        <td>${tculist.account.role_id}</td>
                        <td><c:if test="${tculist.team_leader ==1}">No</c:if>
                            <c:if test="${tculist.team_leader ==2}">Yes</c:if>
                            </td>
                            <td><fmt:formatDate pattern="dd/MM/yyyy" value="${tculist.dropout_date}" /></td>
                        <td><c:if test="${tculist.status == 1}">Activate</c:if>
                            <c:if test="${tculist.status == 2}">Deactivate</c:if>
                            </td>
                            <td><a href="classuserdetail?email=${tculist.account.email}" class="btn btn-success"><i class="bi bi-layout-text-sidebar-reverse"></i></a></td>
                    </tr>
                </c:forEach>
            </tbody>
        </table>
        <form action="specificclassuserlist" method="POST" class="w-50">
            <div  class="mb-3">
                <input name="class_id" value="${requestScope.classes.class_id}" hidden>
                <label class="form-label" for="filenameInput">File Name</label> 
                <input type="text" class="form-control" name="filename" id="filenameInput">
            </div>
            <input type="submit" value="Export" class="btn btn-success">
        </form>
    </t:wrapper>
</body>
</html>
