<%-- 
    Document   : feature_list
    Created on : Jul 7, 2022, 9:29:23 PM
    Author     : admin
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<!DOCTYPE html>

<t:wrapper title='Feature List'>
    <h1>
        Feature List
    </h1>
    <script>
        function submitForm() {
            document.getElementById("frmSearch").submit();
        }
        function updateStatus(feature_id, status) {
            var result = confirm("Do you want to update this feature status?");
            if (result) {
                window.location.href = 'updatefeaturestatus?feature_id=' + feature_id + '&status=' + status;
            }
        }
    </script>
    <c:if test="${sessionScope.account.role_id == 4}">

    </c:if>
    <table id="table" data-toggle="table" data-search="true" data-filter-control="true" data-show-search-clear-button="true" data-pagination="true" data-resizable="true"  data-page-list="[10,25,50,100,all]">
        <thead>
            <tr>
                <th data-sortable="true" data-field="feature_name" data-filter-control="select">Feature name</th>
                <th data-sortable="true" data-field="team_code" data-filter-control="select">Team code</th>
                    <c:if test="${sessionScope.account.role_id == 4}">
                    <th data-field="class_code" data-filter-control="select">Class Code</th>
                    </c:if>
                <th data-sortable="true" data-field="status" data-filter-control="select">Status</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            <c:forEach items="${requestScope.featurelist}" var="flist">
                <tr>
                    <td>${flist.feature_name}</td>
                    <td>${flist.team.team_code}</td>
                    <c:if test="${sessionScope.account.role_id == 4}">
                        <td>${flist.team.classes.class_code}</td>
                    </c:if>
                    <td>
                        <c:if test="${flist.status == 1}">
                            Inactive
                            <c:if test="${sessionScope.account.role_id == 4}">
                                <a class="btn btn-success" onclick="updateStatus(${flist.feature_id}, 2)" href="#"><i class="bi bi-eye"></i></a>
                                </c:if>
                            </c:if>
                            <c:if test="${flist.status == 2}">
                            Active
                            <c:if test="${sessionScope.account.role_id == 4}">
                                <a class="btn btn-warning" onclick="updateStatus(${flist.feature_id}, 1)" href="#"><i class="bi bi-eye"></i></a>
                                </c:if>
                            </c:if>
                    </td>
                    <td><a class="zoom btn btn-success" href="featuredetail?feature_id=${flist.feature_id}"><i class="bi bi-layout-text-sidebar-reverse"></i></a></td>
                </tr>
            </c:forEach>
        </tbody>
    </table>
    <a class="btn btn-success" style="color: white" href="index"><i class="bi bi-house"></i></a>
    <c:if test="${sessionScope.account.role_id == 4}">
        <a href="addfeature" class="btn btn-success"><i class="bi bi-plus"></i></a>
        </c:if>
    </t:wrapper>

