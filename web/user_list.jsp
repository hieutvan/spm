<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<t:wrapper title='User List'>
    <script>
        function updateStatus(email, status) {
            var result = confirm("Do you want to update this user status?");
            if (result) {
                window.location.href = 'updateuserstatus?email=' + email + '&status=' + status;
            }
        }
    </script>
    <h1>
        User List
    </h1>
    <table id="table" data-toggle="table" data-filter-control="true" data-show-search-clear-button="true" data-search="true" data-pagination="true" data-page-list="[5,10, 25, 50, 100, all]">
        <thead>
            <tr>
                <th data-sortable="true">Full name</th>
                <th data-sortable="true">Email</th>
                <th data-field="role" data-sortable="true" data-filter-control="select">Role</th>
                <th data-searchable="false" data-searchable="false">Phone number</th>
                <th data-field="status" data-sortable="true" data-filter-control="select">Status</th>
                <th>View detail information</th>
            </tr>
        </thead>
        <tbody>
            <c:forEach items="${requestScope.userlist}" var="ulist">
                <tr>
                    <td>${ulist.displayname}</td>
                    <td>${ulist.email}</td>
                    <td><c:if test="${ulist.role_id ==1}">
                            <div class="btn btn-outline-danger btn-sm">
                                User
                            </div>
                        </c:if>
                        <c:if test="${ulist.role_id ==2}">
                            <div class="btn btn-outline-primary btn-sm">
                                Admin
                            </div>

                        </c:if>
                        <c:if test="${ulist.role_id ==3}">
                            <div class="btn btn-outline-secondary btn-sm">
                                Author
                            </div>

                        </c:if>
                        <c:if test="${ulist.role_id ==4}">
                            <div class="btn btn-outline-warning btn-sm">
                                Trainer
                            </div>

                        </c:if>
                    </td>
                    <td>${ulist.phonenumber}</td>
                    <td>
                        <c:if test="${ulist.status == 1}">
                            Deactive
                            <a href="#" onclick="updateStatus(${ulist.email}, 2)" class="btn btn-success "><i class="bi bi-eye-slash"></i></a>
                            </c:if>
                            <c:if test="${ulist.status == 2}">
                            Active
                            <a href="#" onclick="updateStatus(${ulist.email}, 1)"  class="btn btn-success "><i class="bi bi-eye"></i></a>
                            </c:if>
                    </td>
                    <td>
                        <div><a class="btn btn-success" style="color: white" href="userprofile?email=${ulist.email}"><i class="bi bi-person-lines-fill"></i></a></div>
                    </td>
                </tr>
            </c:forEach>
        </tbody>
    </table>
    <br/>
    <div>
        <a class="btn btn-success" style="color: white" href="adduser"><i class="bi bi-person-plus-fill"></i></a>
        <a class="btn btn-success" style="color: white" href="index"><i class="bi bi-house"></i></a>
    </div>

</t:wrapper>

