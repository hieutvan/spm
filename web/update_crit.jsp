<%-- 
    Document   : update_crit
    Created on : Jun 18, 2022, 6:02:17 PM
    Author     : admin
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<!DOCTYPE html>

<t:wrapper title="Criteria Detail">
    <h1>
        Criteria Detail
    </h1>
    <form method="POST" action="updatecrit" class="w-50">
        <div class="mb-3">
            <div class="row">
                <div class="col">
                    <label class="form-label"> Iteration id</label>
                    <input type="text" value="${requestScope.evacrit.iteration.iteration_id} - ${requestScope.evacrit.iteration.iteration_name}" class="form-control" readonly>
                </div>
                <div class="col">
                    <label class="form-label"> Subject Name</label>
                    <input type="text"  value="${requestScope.evacrit.iteration.subject.subject_name}" class="form-control" readonly>
                </div>
            </div>
        </div>
        <input type="text" name="criteria_id" value="${requestScope.evacrit.criteria_id}" hidden>
        <div class="mb-3">
            <label for="weightInput" class="form-label">Evaluation weight</label>
            <input class="form-control" name="evaluation_weight" type="text" id="weightInput" value="${requestScope.evacrit.evaluation_weight}">
            <p style="color: red" class="warning">${requestScope.error}</p>
        </div>
        <div class="mb-3">
            <label class="form-label" for="teamevaluateInput">Team evaluation</label>
            <c:if test="${requestScope.evacrit.team_evaluation == 1}">
                <input name="team_evaluation" type="radio" id="teamevaluateInput" value="1" checked="checked">No
                <input name="team_evaluation" type="radio" id="teamevaluateInput" value="2">Yes
            </c:if>
            <c:if test="${requestScope.evacrit.team_evaluation == 2}">
                <input name="team_evaluation" type="radio" id="teamevaluateInput" value="1">No
                <input name="team_evaluation" type="radio" id="teamevaluateInput" value="2" checked="checked">Yes
            </c:if>
        </div>
        <div class="mb-3">
            <label for="orderinput" class="form-label">Criteria order</label>
            <input class="form-control" name="criteria_order" type="text" id="orderinput" value="${requestScope.evacrit.criteria_order}" >
        </div>
        <div class="mb-3">
            <label class="form-label" for="LOCInput">Max Loc</label>
            <input type="text" name="max_loc" class="form-control" id="LOCInput" value="${requestScope.evacrit.max_loc}" >
            <c:if test="${requestScope.total < 750}">
                <div class="alert alert-info" role="alert">Total loc of this iteration is: ${requestScope.total}</div>
            </c:if>
            <c:if test="${requestScope.total >= 750}">
                <div class="alert alert-danger" role="alert">You have pass the total LOC (${requestScope.total})</div>
            </c:if>
        </div>
        <div class="mb-3">
            <label class="form-label" for="StatusInput">Status</label>
            <c:if test="${requestScope.evacrit.status == 1}">
                <input name="status" type="radio" id="StatusInput" value="1" checked="checked">Activate
                <input name="status" type="radio" id="StatusInput" value="2">Deactivate
            </c:if>
            <c:if test="${requestScope.evacrit.status == 2}">
                <input name="status" type="radio" id="StatusInput" value="1">Activate
                <input name="status" type="radio" id="StatusInput" value="2" checked="checked">Deactivate
            </c:if>
        </div>
        <input type="submit" value="DONE" class="btn btn-success">
        <a href="evacriterialist" class="btn btn-success"><i class="bi bi-box-arrow-left"></i></a>
        <a class="btn btn-success" style="color: white" href="index"><i class="bi bi-house"></i></a>
    </form>
</t:wrapper>
