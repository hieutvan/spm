<%-- 
    Document   : add_Loc_Eva
    Created on : Jul 20, 2022, 12:16:20 AM
    Author     : admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>

    <body>
        <t:wrapper title='LOC Evaluation'>
            <script>
                function updateLOC() {
                    var complex_value = parseInt(document.getElementById('complex_value').value);
                    var quality = parseInt(document.getElementById('quality_id').value);
                    var LOCOutput = document.getElementById('LOCOutput');

                    LOCOutput.value = complex_value * quality / 100;
                }
            </script> 
            <h1>
                Create new Loc Evaluation
            </h1>
            <a href="trackinglist" class="btn btn-success"><i class="bi bi-box-arrow-left"></i></a>
            <form action="addloceva" class="w-50" method="POST">
                <input type="text" name="tracking_id" value="${requestScope.tracking_id}" hidden="">
                <div class="row">
                    <div class="col">
                        <div class="mb-3">
                            <label class="form-label" for="EvaluationTimeInput">Evaluated At</label>
                            <span class="form-control" readonly> <fmt:formatDate pattern="dd/MM/yyyy" value="${requestScope.eva_at}"/> </span>
                        </div>
                    </div>
                </div>
                <div class="mb-3">
                    <label class="form-label" for="EvaluationNoteInput">Evaluated Note</label>
                    <textarea class="form-control" name="evaluation_note" type="text" id="EvaluationNoteInput" ></textarea>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="mb-3">
                            <label class="form-label" for="ComplexityIDInput">Complexity</label>
                            <select name="complex_value" class="form-control" id="complex_value" onchange="updateLOC();" >
                                <option ${requestScope.track.function.complexity_id == 1 ? "selected=\"selected\"":""} value="60">Simple</option>
                                <option ${requestScope.track.function.complexity_id == 2 ? "selected=\"selected\"":""} value="120">Medium</option>
                                <option ${requestScope.track.function.complexity_id == 3 ? "selected=\"selected\"":""} value="240">Complex</option>
                            </select>
                        </div>
                    </div>
                    <div class="col">
                        <div class="mb-3">
                            <label class="form-label" for="QualityIDInput">Quality</label>
                            <select name="quality_value" class="form-control" id="quality_id" onchange="updateLOC();">
                                <c:forEach items="${requestScope.sslist}" var="sslist">
                                    <option ${requestScope.loceva.quality_id == sslist.type_id ? "selected=\"selected\"":""} value="${sslist.setting_value}">${sslist.setting_title}</option>
                                </c:forEach>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="mb-3">
                    <label class="form-label" for="LOCOutput">LOC</label>
                    <input class="form-control" value="" name="LOC" type="text" id="LOCOutput" readonly>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="mb-3">
                            <label class="form-label" for="MilestoneOutput">
                                <a class="btn btn-primary" href="milestonedetail?milestone_id=${requestScope.loceva.tracking.milestone.milestone_id}">
                                    <i class="bi bi-info-square"></i>
                                    <span>
                                        Milestone
                                    </span>
                                </a>
                            </label>
                            <input class="form-control" value="${requestScope.loceva.tracking.milestone.milestone_id}" name="milestone_id" type="text" id="MilestoneOutput" hidden>
                        </div>
                    </div>
                    <div class="col">
                        <div class="mb-3">
                            <label class="form-label" for="TeamOutput">
                                <a class="btn btn-primary" href="teamdetail?team_id=${requestScope.loceva.tracking.team.team_id}">
                                    <i class="bi bi-info-square"></i>
                                    <span>
                                        Team
                                    </span>
                                </a>
                            </label>
                            <input class="form-control" value="${requestScope.loceva.tracking.team.team_id}" name="team_id" type="text" id="TeamOutput" hidden>
                        </div>
                    </div>
                    <div class="col">
                        <div class="mb-3">
                            <label class="form-label" for="FunctionOutput">
                                <a class="btn btn-primary" href="functiondetail?function_id=${requestScope.track.function.function_id}">
                                    <i class="bi bi-info-square"></i>
                                    <span>
                                        Function
                                    </span>
                                </a>
                            </label>
                            <input class="form-control" value="${requestScope.track.function.function_id}" name="function_id" type="text" id="FunctionOutput" hidden>
                        </div>
                    </div>
                </div>
                <input type="submit" name="DONE" class="btn btn-success">
            </form>
        </t:wrapper>
        <script>
            $(document).ready(function () {
                updateLOC();
            });
        </script>
