<%-- 
    Document   : add_setting
    Created on : Jul 17, 2022, 7:22:19 PM
    Author     : admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<!DOCTYPE html>

<t:wrapper title='Create new settings'>
    <h1 >
        Add new  
    </h1>
    <form acsettingtion="addsetting" class="w-50" method="POST"> 
        <div class="row">
            <div class="col">
                <div class="mb-3">
                    <label class="form-label" for="TitleInput">Setting title</label>
                    <input type="text" name="setting_title" class="form-control" >
                </div>   
            </div>
            <div class="col">
                <div class="mb-3">
                    <label class="form-label" for="valueInput">Setting value</label>
                    <input type="text" name="setting_value" class="form-control"  >
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <div class="mb-3">
                    <label class="form-label" for="OrderInput">Display Order</label>
                    <input type="text" name="display_order" min="0" class="form-control"  >
                </div>   
            </div>
            <div class="col">
                <div class="mb-3">
                    <label class="form-label" for="TypeInput">Type</label>
                    <input type="text" name="type_id" min="0" class="form-control"  >
                </div>   
            </div>
        </div>
        <div class="mb-3">
            <label class="form-label" for="status">Status <span style="color: red">*</span></label>
            <input  type="radio" name="status" value="1" id="status">Deactivate
            <input  type="radio" name="status" value="2" id="status">Activate
        </div> 
        <input type="submit" value="DONE" class="zoom btn btn-success">
    </form> 
</t:wrapper>
</body>
</html>
