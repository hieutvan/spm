<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:wrapper title="Home Page">
    <c:if test="${sessionScope.account == null}">
        <h1 style="text-align: center">
            WELCOME TO THE STUDENT PROJECT MANAGEMENT
        </h1>
        <div>
            <h2 >
                <a class="zoom btn btn-primary" href="login">Login</a>
                <a class="zoom btn btn-primary" href="register">Register</a>
            </h2>
        </div>
        <h2 style="font-size: 20px">
            GROUP 2 IS1601 PROJECT
        </h2>
    </c:if>
    <div>
        <c:if test="${sessionScope.account != null}">
            <p>
                <a href="logout" class="zoom btn btn-primary" >Logout</a>
            </p>
        </c:if>
    </div>
</t:wrapper>