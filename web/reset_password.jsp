<%-- 
    Document   : resetPassword
    Created on : Jun 5, 2022, 3:41:42 PM
    Author     : admin
--%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:wrapper title='Reset Password'>
    <h1>
        Reset Password
    </h1>
    <form action="resetpassword" method="POST">

        <div class="mb-3">
            <label for="inputemail" class="form-label">Email:</label>
            <input class="form-control" name="email" type="email" id="inputemail"  required >  
        </div>
        <div class="mb-3">
            <label for="inputpw" class="form-label">Password</label>
            <input class="form-control" name="newpass" type="password" id="newpass" pattern=".{8,20}" title="8 to 20 characters" required>  
        </div>
        <div class="mb-3">
            <label for="inputcfpw" class="form-label">Password</label>
            <input class="form-control" name="confirm" type="password" id="confirm" pattern=".{8,20}" title="8 to 20 characters" required>  
        </div>
        <p style="color: red">${requestScope.error}</p>
        <div class="mb-3">
            <input type="submit" class="btn btn-success" value="DONE">
        </div>
        <div><button class="btn btn-success"><a href="index" style="color: white">Back to home page</a></button></div><br/>
    </form>
    <script>
        var check = function () {
            if (document.getElementById('newpass').value ==
                    document.getElementById('confirm').value) {
                document.getElementById('message').style.color = 'green';
                document.getElementById('message').innerHTML = 'matching';
            } else {
                document.getElementById('message').style.color = 'red';
                document.getElementById('message').innerHTML = 'not matching';
            }
        }
    </script>
</t:wrapper>
