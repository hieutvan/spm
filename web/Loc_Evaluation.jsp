<%-- 
    Document   : Loc_Evaluation
    Created on : Jul 19, 2022, 12:54:01 PM
    Author     : admin
--%>

<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<t:wrapper title='LOC Evaluation'>
    <script>
        function updateLOC() {
            var complex_value = parseInt(document.getElementById('complex_value').value);
            var quality = parseInt(document.getElementById('quality_id').value);
            var LOCOutput = document.getElementById('LOCOutput');

            LOCOutput.value = complex_value * quality / 100;
        }
    </script> 
    <h1>
        Loc Evaluation
    </h1>

    <form action="locevafunction" class="w-50" method="POST">
        <input type="text" name="tracking_id" value="${requestScope.loceva.tracking.tracking_id}" hidden="">
        <input type="text" name="function_id" value="${requestScope.track.function.function_id}" hidden="">
        <div class="row">
            <div class="col">
                <div class="mb-3">
                    <label class="form-label" for="EvaluationIDInput">Evaluation ID</label>
                    <input class="form-control" value="${requestScope.loceva.evaluation_id}" name="evaluation_id" type="text" id="EvaluationIDInput" readonly>
                </div>
            </div>
            <div class="col">
                <div class="mb-3">
                    <label class="form-label" for="EvaluationTimeInput">Evaluated At</label>
                    <span class="form-control" readonly> <fmt:formatDate pattern="dd/MM/yyyy" value="${requestScope.loceva.evaluation_time}"/> </span>
                </div>
            </div>
        </div>
        <div class="mb-3">
            <label class="form-label" for="EvaluationNoteInput">Evaluated Note</label>
            <textarea class="form-control" name="evaluation_note" type="text" id="EvaluationNoteInput" <c:if test="${sessionScope.account.role_id == 1}">readonly</c:if> >${requestScope.loceva.evaluation_note}</textarea>
            </div>
            <div class="row">
                <div class="col">
                    <div class="mb-3">
                        <label class="form-label" for="ComplexityIDInput">Complexity</label>
                        <select name="complex_value" class="form-control" id="complex_value" onchange="updateLOC();" <c:if test="${sessionScope.account.role_id == 1}">disabled</c:if> >
                        <option ${requestScope.track.function.complexity_id == 1 ? "selected=\"selected\"":""} value="60">Simple</option>
                        <option ${requestScope.track.function.complexity_id == 2 ? "selected=\"selected\"":""} value="120">Medium</option>
                        <option ${requestScope.track.function.complexity_id == 3 ? "selected=\"selected\"":""} value="240">Complex</option>
                    </select>
                </div>
            </div>
            <div class="col">
                <div class="mb-3">
                    <label class="form-label" for="QualityIDInput">Quality</label>
                    <select  name="quality_value" class="form-control" id="quality_id" onchange="updateLOC();" <c:if test="${sessionScope.account.role_id == 1}">disabled</c:if>>
                        <c:forEach items="${requestScope.sslist}" var="sslist">
                            <option ${requestScope.loceva.quality_id == sslist.type_id ? "selected=\"selected\"":""} value="${sslist.setting_value}">${sslist.setting_title}</option>
                        </c:forEach>
                    </select>
                </div>
            </div>
            <div class="col">
                <div class="mb-3">
                    <label class="form-label" for="LOCOutput">LOC</label>
                    <input class="form-control" value="${requestScope.loceva.LOC}" name="LOC" type="text" id="LOCOutput" readonly>
                </div>
            </div>
        </div>
        <c:if test="${sessionScope.account.role_id == 4}">
            <div class="mb-3">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown">Direction</a>
                <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <li><a class="dropdown-item" href="milestonedetail?milestone_id=${requestScope.loceva.tracking.milestone.milestone_id}">Milestone</a></li>
                    <li><a class="dropdown-item" href="teamdetail?team_id=${requestScope.loceva.tracking.team.team_id}">Team</a></li>
                    <li><a class="dropdown-item" href="functiondetail?function_id=${requestScope.track.function.function_id}">Function</a></li>
                </ul>
            </div>

            <input type="submit" name="DONE" class="btn btn-success">
            <a href="trackinglist" class="btn btn-success"><i class="bi bi-box-arrow-left"></i></a>
            </c:if>
            <c:if test="${sessionScope.account.role_id == 1}">
            <a href="index" class="btn btn-success"><i class="bi bi-house"></i></a>

        </c:if>
    </form> 
</t:wrapper>

<script>
    $(document).ready(function () {
        updateLOC();
    });
</script>