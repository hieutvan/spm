<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:wrapper title="Subject List">
    <script>
        function submitForm() {
            document.getElementById("frmSearch").submit();
        }
        function updateStatus(subject_id, status) {
            var result = confirm("Do you want to update this subject status?");
            if (result) {
                window.location.href = 'updatesubjectstatus?subject_id=' + subject_id + '&status=' + status;
            }
        }
    </script>
    <h1>
        Subject List
    </h1>
    <table id="table" data-toggle="table" data-search="true" data-filter-control="true" data-show-search-clear-button="true" data-pagination="true" data-resizable="true"  data-page-list="[5, 10, 25, 50, 100, all]">
        <thead>
            <tr>
                <th data-sortable="true" >Subject code</th>
                <th data-sortable="true" >Subject Name</th>
                <th data-sortable="true" data-field="trainer" data-filter-control="select">Author</th>
                    <c:if test="${sessionScope.account.role_id ==2 || sessionScope.account.role_id == 3}">
                    <th data-field="status" data-filter-control="select" data-sortable="true">Status</th>
                    <th>Action</th>
                    </c:if>
            </tr>
        </thead>
        <tbody>
            <c:forEach items="${requestScope.sublist}" var="slist">
                <tr>
                    <c:if test="${sessionScope.account.role_id != 1}">
                        <td style="font-style: oblique">${slist.subject_code}</td>
                        <td>${slist.subject_name}</td>
                        <td>${slist.email.displayname}</td>
                        <td>
                            <c:if test="${slist.status == 2}">Active
                                <a class="btn btn-success" onclick="updateStatus(${slist.subject_id}, 1)" href="#"><i class="bi bi-eye-slash"></i></a>
                                </c:if>
                                <c:if test="${slist.status == 1}">Inactive
                                <a class="btn btn-warning" onclick="updateStatus(${slist.subject_id}, 2)" href="#"><i class="bi bi-eye"></i></a>
                                </c:if>
                        </td>
                        <td>
                            <a class="btn btn-success" href="updatesubject?subject_code=${slist.subject_code}"><i class="bi bi-layout-text-sidebar-reverse"></i></a>


                        </td>
                    </c:if>
                    <c:if test="${sessionScope.account.role_id == 1}">
                        <c:if test="${slist.status == 2}">
                            <td style="font-style: oblique">${slist.subject_code}</td>
                            <td>${slist.subject_name}</td>
                            <td>${slist.email.displayname}</td>
                            <td><a class="zoom btn btn-success" href="subjectdetail?subject_code=${slist.subject_code}"><i class="bi bi-layout-text-sidebar-reverse"></i></a>
                                </c:if>
                            </c:if>
                </tr>
            </c:forEach>
        </tbody>
    </table>
    <br/>
    <div class="ms-3">
        <c:if test="${sessionScope.account.role_id != 1}">
            <a class="btn btn-success" style="color: white" href="addsubject"><i class="bi bi-plus-lg"></i></a>
            </c:if>
        <a class="btn btn-success" href="subjectsettinglist"><i class="bi bi-gear-wide-connected"></i></a>
        <a class="btn btn-success" style="color: white" href="index"><i class="bi bi-house"></i></a>
    </div>
</t:wrapper>

