<%-- 
    Document   : issue_detail
    Created on : Jul 9, 2022, 6:56:30 PM
    Author     : admin
--%>

<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<t:wrapper title='Issue Detail'>
    <h1>
        Issue Detail
    </h1>
    <form action="issuedetail" class="w-50" method="POST">
        <input type="text" value="${requestScope.issue.issue_id}" name="issue_id" hidden="">
        <div class="row">
            <div class="col">
                <div class="mb-3">
                    <label class="form-label" for="emailInput">Assignee Name</label>
                    <input class="form-control" value="${requestScope.issue.account.email}" name="email" type="text" id="emailInput" hidden="">
                    <input class="form-control" value="${requestScope.issue.account.displayname}" name="email" type="text" id="emailInput" readonly="">
                </div>
            </div>
            <div class="col">
                <div class="mb-3">
                    <label class="form-label" for="issuetitleInput">Title</label>
                    <input class="form-control" value="${requestScope.issue.issue_title}" name="issue_title" type="text" id="issuetitleInput" >
                </div>
            </div>
            <div class="mb-3">
                <label class="form-label" for="DescriptionInput">Description</label>
                <textarea class="form-control" name="description" id="DescriptionInput" type="text">${requestScope.issue.description}</textarea>
            </div>
            <div class="mb-3">
                <div class="row">
                    <div class="col">
                        <label class="form-label" for="gitlabInput">Gitlab ID</label>
                        <input style="display: inline-block" class="form-control" value="${requestScope.issue.gitlab_id}" name="gitlab_id" type="text" id="gitlabInput" >
                    </div>

                    <div class="col">
                        <label class="form-label" for="gitlabInput">Gitlab URL</label>
                        <input style="display: inline-block" class="form-control" value="${requestScope.issue.gitlab_url}" name="gitlab_url" type="text" id="gitlabInput" >
                    </div>
                </div>
            </div>
            <div class="mb-3" >
                <div class="row g-3">
                    <div class="col">
                        <label class="form-label" for="CreatedAtInput">Created At</label>
                        <input style="display: inline-block" class="form-control" value="${requestScope.issue.created_at}" name="created_at" type="text" id="CreatedAtInput" readonly="">
                    </div>
                    <div class="col">
                        <label class="form-label" for="DuedateInput">Due Date</label>
                        <input style="display: inline-block" class="form-control" value="${requestScope.issue.due_date}" name="due_date" type="date" id="DuedateInput" >
                    </div>
                </div>
            </div>
            <div class="mb-3">
                <div class="row">
                    <div class="col">
                        <label class="form-label" for="TeamInput">Team Code</label>
                        <input class="form-control" value="${requestScope.issue.team.team_id}" name="team_id" type="text" id="TeamIDInput" hidden="">
                        <input class="form-control" value="${requestScope.issue.team.team_code}" name="team_code" type="text" id="teamcodeInput" readonly="">
                    </div>

                    <div class="col">
                        <div class="mb-3">
                            <label class="form-label" for="MilestoneInput">Milestone</label>
                            <input class="form-control" value="${requestScope.issue.milestone.milestone_id}" name="milestone_id" type="text" id="MilestoneInput" readonly="">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="mb-3">
                            <label class="form-label" for="FunctionIDinput">Function</label>
                            <select name="function_id" class="form-control" >
                                <c:forEach items="${requestScope.functionlist}" var="flist">
                                    <option ${flist.function_id == requestScope.issue.function.function_id ? "selected=\"selected\"":""} value="${flist.function_id}">${flist.function_name}</option>
                                </c:forEach>
                            </select>
                        </div>
                    </div>
                    <div class="col">
                        <div class="mb-3">
                            <label class="form-label" for="LabelInput">Label</label>
                            <input class="form-control" value="${requestScope.issue.labels}" name="labels" type="text" id="LabelInput" >
                        </div>
                    </div>
                    <div class="col">
                        <div class="mb-3">
                            <label class="form-label" for="TypeInput">Issue Type</label>
                            <select name="issue_type" class="form-control" >
                                <c:forEach items="${requestScope.issuetypelist}" var="itlist">
                                    <option ${itlist.issue_type == requestScope.issue.issue_type ? "selected=\"selected\"":""} value="${itlist.issue_type}">${itlist.issue_type}</option>
                                </c:forEach>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="mb-3">
                    <label class="form-label" for="Status">Status</label>
                    <c:if test="${requestScope.issue.status == 0}">
                        <input name="status" type="radio" id="status" value="0" checked="checked">Pending
                        <input name="status" type="radio" id="status" value="1" >Close
                        <input name="status" type="radio" id="status" value="2">Open
                    </c:if>
                    <c:if test="${requestScope.issue.status == 1}">
                        <input name="status" type="radio" id="status" value="0">Pending
                        <input name="status" type="radio" id="status" value="1" checked="checked">Close
                        <input name="status" type="radio" id="status" value="2">Open
                    </c:if>
                    <c:if test="${requestScope.issue.status == 2}">
                        <input name="status" type="radio" id="status" value="0">Pending
                        <input name="status" type="radio" id="status" value="1">Close
                        <input name="status" type="radio" id="status" value="2" checked="checked">Open
                    </c:if>
                </div>
            </div>
        </div>
        <input type="submit" value="DONE" class="btn btn-success">
        <a href="issuelist" class="btn btn-success"><i class="bi bi-box-arrow-left"></i></a>
        <a class="btn btn-success" style="color: white" href="index"><i class="bi bi-house"></i></a>
    </div>
</form>
</t:wrapper>

