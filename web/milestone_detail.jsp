<%--
    Document   : milestone_detail
    Created on : Jun 22, 2022, 12:42:26 AM
    Author     : admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>

<t:wrapper title='Milestone Details'>
    <h1>
        Milestone Detail
    </h1>
    <form class="w-50" method="POST" action="milestonedetail">
        <div class="mb-3">
            <input name="milestone_id" class="form-control" value="${requestScope.milestone.milestone_id}"　id="msIDInput" hidden>
        </div>
        <div class="row">
            <div class="col">
                <div class="mb-3">
                    <label class="form-label" for="classInput">Class</label>
                    <select name="class_id" class="form-control">
                        <c:forEach items="${requestScope.classlist}" var="clist">
                            <option ${clist.class_id == requestScope.milestone.classes.class_id ? "selected=\"selected\"":""} value="${clist.class_id}">${clist.class_id} - ${clist.class_code}</option>
                        </c:forEach>
                    </select>
                </div>
            </div>
            <div class="col">
                <div class="mb-3">
                    <label class="form-label" for="iterationInput">Iteration</label>
                    <select name="iteration_id" class="form-control">
                        <c:forEach items="${requestScope.iterlist}" var="ilist">
                            <option ${ilist.iteration_id==requestScope.milestone.iteration.iteration_id ? "selected=\"selected\"":""} value="${ilist.iteration_id}">${ilist.iteration_id} - ${ilist.iteration_name}</option>
                        </c:forEach>
                    </select>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <div class="mb-3">
                    <label class="form-label" for="fromdateInput">From Date</label>
                    <input type="date" name="from_date" class="form-control" value="${requestScope.milestone.from_date}">
                </div>
            </div>
            <div class="col">
                <div class="mb-3">
                    <label class="form-label" for="todateInput">To Date</label>
                    <input type="date" name="to_date" class="form-control" value="${requestScope.milestone.to_date}">
                </div>
            </div>
        </div>
        <div class="mb-3">
            <label class="form-label" for="status">Status</label>
            <c:if test="${requestScope.milestone.status == 0}">
                <input name="status" type="radio" id="status" value="0" checked="checked">Cancelled
                <input name="status" type="radio" id="status" value="1" >Close
                <input name="status" type="radio" id="status" value="2">Open
            </c:if>
            <c:if test="${requestScope.milestone.status == 1}">
                <input name="status" type="radio" id="status" value="0">Cancel
                <input name="status" type="radio" id="status" value="1" checked="checked">Closed
                <input name="status" type="radio" id="status" value="2">Open
            </c:if>
            <c:if test="${requestScope.milestone.status == 2}">
                <input name="status" type="radio" id="status" value="0">Cancel
                <input name="status" type="radio" id="status" value="1">Close
                <input name="status" type="radio" id="status" value="2" checked="checked">Opened
            </c:if>
        </div>
        <input class="btn btn-success" value="DONE" type="submit">
        <a href="milestonelist" class="btn btn-success"><i class="bi bi-box-arrow-left"></i></a>
        <a class="btn btn-success" style="color: white" href="index"><i class="bi bi-house"></i></a>
    </form>
</t:wrapper>
