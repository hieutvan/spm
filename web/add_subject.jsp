<%-- 
    Document   : add_subject
    Created on : Jun 10, 2022, 9:19:09 PM
    Author     : admin
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<t:wrapper title='Register'>
    <h1>
        Add new subject
    </h1>
    <form action="addsubject" method="POST" class="w-50">
        <div class="mb-3">
            <label for="subject_code" class="form-label">Subject Code <span style="color: red">*</span></label>
            <input class="form-control" name="subject_code" type="text" id="subjectcodeInput" required>
        </div>
        <div class="mb-3">
            <label for="subject_name" class="form-label">Subject Name <span style="color: red">*</span></label>
            <input class="form-control" name="subject_name" type="text" id="subjectnameInput" required>
        </div>
        <div class="mb-3">
            <label for="emailInput" class="form-label">Author <span style="color: red">*</span></label>
            <select name="email" class="form-control" style="width: auto" required>
                <c:forEach items="${requestScope.trainerlist}" var="tlist">
                    <option value="${tlist.email}">${tlist.displayname} - ${tlist.username}</option>
                </c:forEach>
            </select>
        </div>
        <div class="mb-3">
            <label for="status" class="form-label">Status <span style="color: red">*</span></label>
            <input name="status" type="radio" id="status" value="1">Hide
            <input name="status" type="radio" id="status" value="2">Display
        </div>
        <div class="md-3"><input type="submit" class="zoom btn btn-success" value="DONE"></div>

    </form>
</t:wrapper>