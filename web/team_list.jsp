<%-- 
    Document   : team_list
    Created on : Jun 23, 2022, 8:28:35 PM
    Author     : admin
--%>

<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<t:wrapper title='Team List'>
    <script>
        function submitForm() {
            document.getElementById("frmSearch").submit();
        }
        function updateStatus(team_id, status) {
            var result = confirm("Do you want to update this team status?");
            if (result) {
                window.location.href = 'updateteamstatus?team_id=' + team_id + '&status=' + status;
            }
        }
    </script>
    <h1>
        Team List
    </h1>
    <table id="table" data-toggle="table" data-search="true" data-pagination="true" data-filter-control="true" data-show-search-clear-button="true" data-resizable="true"  date-page-list="{10,25,50,100,all}">
        
        <thead>
            <tr>
                <th data-sortable="true" data-field="class_code" data-filter-control="select">Class code</th>
                <th data-sortable="true" data-field="topic_code" data-filter-control="select">Topic code</th>
                <th data-sortable="true">Topic Name</th>
                <th data-sortable="true" data-field="status" data-filter-control="select">Status</th>
                <th data-searchable="false">Action</th>
            </tr>
        </thead>
        <tbody>
            <c:forEach items="${requestScope.teamlist}" var="tlist">
                <tr>
                    <td>${tlist.classes.class_code}</td>
                    <td>${tlist.topic_code}</td>
                    <td>${tlist.topic_name}</td>
                    <td><c:if test="${tlist.status == 1}">Inactive
                            <a href="#" onclick="updateStatus(${tlist.team_id},2)" class="btn btn-success"><i class="bi bi-eye"></i></a>
                            </c:if>
                            <c:if test="${tlist.status == 2}">Active
                            <a href="#" onclick="updateStatus(${tlist.team_id},1)" class="btn btn-warning"><i class="bi bi-eye-slash"></i></a></c:if>
                        </td>
                        <td><a href="teamdetail?team_id=${tlist.team_id}" class="zoom btn btn-success"><i class="bi bi-layout-text-sidebar-reverse"></i></a>
                        <a href="${tlist.gitlab_url}" class="btn btn-success"><i class="bi bi-git"></i></a>
                    </td>
                </tr>
            </c:forEach>
        </tbody>
    </table>
    <a href="index" class="btn btn-success"><i class="bi bi-house"></i></a>
    <a href="addteam" class="btn btn-success"><i class="bi bi-plus"></i></a>
    </t:wrapper>

