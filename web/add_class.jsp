<%-- 
    Document   : add_class
    Created on : Jun 19, 2022, 10:08:11 PM
    Author     : admin
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<!DOCTYPE html>

        <t:wrapper title='Add class'>
            <h1>
                Add new class
            </h1>
            <form class="w-50" method="POST" action="addclass">
                <div class="mb-3">
                    <label class="form-label" for="ClassCodeInput">Class Code <span style="color: red">*</span></label>
                    <input name="class_code" type="text" class="form-control" id="ClassCodeInput">
                </div>
                <div class="mb-3">
                    <label class="form-label" for="subjectNameInput">Subject Name <span style="color: red">*</span></label>
                    <select name="subject_id" class="form-control" style="width: auto" >
                        <c:forEach items="${requestScope.sub}" var="slist">
                            <option value="${slist.subject_id}">${slist.subject_code}-${slist.subject_name}</option>
                        </c:forEach>
                    </select>
                </div>
                <div class="mb-3">
                    <label class="form-label" for="emailInput">Manager <span style="color: red">*</span></label>
                    <select name="email" class="form-control" style="width: auto" >
                        <c:forEach items="${requestScope.acc}" var="alist">
                            <option value="${alist.email}">${alist.displayname}-${alist.email}</option>
                        </c:forEach>
                    </select>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="mb-3">
                            <label class="form-label" for="classyearInput">Class Year <span style="color: red">*</span></label>
                            <input class="form-control" name="class_year" type="text" id="classyearInput" required>
                        </div>
                    </div>
                    <div class="col">
                        <div class="mb-3">
                            <label class="form-label" for="classtermInput">Class term <span style="color: red">*</span></label>
                            <select name="class_term" class="form-control">
                                <option value="Spring">Spring</option>
                                <option value="Summer">Summer</option>
                                <option value="Fall">Fall</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="mb-3">
                    <label class="form-label" for="bl5Input">Block 5 class <span style="color: red">*</span></label>
                    <input  type="radio" name="block5_class" value="1" id="bl5Input">Deactivate
                    <input  type="radio" name="block5_class" value="2" id="bl5Input">Activate
                </div>
                <div class="mb-3">
                    <label class="form-label" for="status">Status <span style="color: red">*</span></label>
                    <input  type="radio" name="status" value="1" id="status">Deactivate
                    <input  type="radio" name="status" value="2" id="status">Activate
                </div>
                <input class="zoom btn btn-success" type="submit" value="DONE">
            </form>
        </t:wrapper>

