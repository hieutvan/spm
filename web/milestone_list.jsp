<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:wrapper title='Milestone List'>

    <script>
        function submitForm() {
            document.getElementById("frmSearch").submit();
        }
        function updateStatus(milestone_id, status) {
            var result = confirm("Do you want to update this milestone status?");
            if (result) {
                window.location.href = 'updatemilestonestatus?milestone_id=' + milestone_id + '&status=' + status;
            }
        }
    </script>
    <h1>
        Milestone List
    </h1>
    <table id="table" data-toggle="table" data-search="true" data-pagination="true" data-filter-control="true" data-show-search-clear-button="true" data-resizable="true"  data-page-list="[10, 25, 50, 100, all]">
        <thead>
            <tr>
                <th data-field="iteration_name" data-filter-control="select" data-sortable="true">Iteration Name</th>
                <th data-field="class_code" data-filter-control="select" data-sortable="true">Class Code</th>
                <th data-field="start_date" data-filter-control="select" data-sortable="true">Start Date</th>
                <th data-field="end_date" data-filter-control="select" data-sortable="true">End Date</th>
                <th data-sortable="true">Status</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            <c:forEach items="${requestScope.mslist}" var="mlist">
                <tr>
                    <td>${mlist.iteration.iteration_name}</td>
                    <td>${mlist.classes.class_code}</td>
                    <td><fmt:formatDate pattern="dd/MM/yyyy" value="${mlist.from_date}"/></td>
                    <td><fmt:formatDate pattern="dd/MM/yyyy" value="${mlist.to_date}"/></td>
                    <td>
                        <c:if test="${mlist.status == 0}">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown">Canceled</a>
                        </c:if>
                        <c:if test="${mlist.status == 1}">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown">Closed</a>
                        </c:if>
                        <c:if test="${mlist.status == 2}">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown">Opened</a>
                        </c:if>
                        <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <li><a class="dropdown-item" onclick="updateStatus(${mlist.milestone_id}, 1)" href="#">Close</a></li>
                            <li><a class="dropdown-item" onclick="updateStatus(${mlist.milestone_id}, 2)" href="#">Open</a></li>
                            <li><a class="dropdown-item" onclick="updateStatus(${mlist.milestone_id}, 0)" href="#">Cancel</a></li>
                        </ul>
                    </td>
                    <td><a class="btn btn-success" href="milestonedetail?milestone_id=${mlist.milestone_id}"><i class="bi bi-layout-text-sidebar-reverse"></i></a></td>
                </tr>
            </c:forEach>
        </tbody>
    </table>
    <a href="index" class="btn btn-success"><i class="bi bi-house"></i></a>
    <a href="addmilestone" class="btn btn-success"><i class="bi bi-plus"></i></a>
</t:wrapper>