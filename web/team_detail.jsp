<%-- 
    Document   : team_detail
    Created on : Jun 25, 2022, 9:29:45 PM
    Author     : admin
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<!DOCTYPE html>
        <t:wrapper title='Team Detail'>
            <h1>
                TEAM DETAILS
            </h1>
            
            <form class="w-50" method="POST" action="teamdetail">
                <div class="mb-3">
                    <input name="team_id" class="form-control" value="${requestScope.team.team_id}" hidden>
                </div>   
                <div class="row">
                    <div class="col">
                        <div class="mb-3">
                            <label class="form-label" for="teamcodeInput">Team code</label>
                            <input class="form-control" name="team_code" type="text" id="teamcodeInput" value="${requestScope.team.team_code}">
                        </div>
                    </div>
                    <div class="col">
                        <div class="mb-3">
                            <label class="form-label" for="classInput">Class Code</label>
                            <select name="class_id" class="form-control" >
                                <c:forEach items="${requestScope.classlist}" var="clist">
                                    <option ${clist.class_id == requestScope.team.classes.class_id ? "selected=\"selected\"":""} value="${clist.class_id}">
                                        ${clist.class_id} - ${clist.class_code}
                                    </option>
                                </c:forEach>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="mb-3">
                            <label class="form-label" for="topiccodeInput">Topic code</label>
                            <input class="form-control" name="topic_code" type="text" id="topicocdeInput" value="${requestScope.team.topic_code}">
                        </div></div>
                    <div class="col">
                        <div class="mb-3">
                            <label class="form-label" for="topicnameInput">Topic name</label>
                            <input class="form-control" name="topic_name" type="text" id="topicnameInput" value="${requestScope.team.topic_name}">
                        </div>
                    </div>
                </div>
                <div class="mb-3">
                    <label class="form-label" for="gitlbaInput">GitLab Link</label>
                    <input class="form-control" name="gitlab_url" type="text" id="gitlbaInput" value="${requestScope.team.gitlab_url}">
                </div>
                <div class="mb-3">
                    <label class="form-label" for="status">Status</label>
                    <c:if test="${requestScope.team.status == 1}">
                        <input name="status" type="radio" id="status" value="1" checked="checked">Inactive
                        <input name="status" type="radio" id="status" value="2">Active
                    </c:if>
                    <c:if test="${requestScope.team.status == 2}">
                        <input name="status" type="radio" id="status" value="1">Inactive
                        <input name="status" type="radio" id="status" value="2" checked="checked">Active
                    </c:if>
                </div>
                <input type="submit" class="btn btn-success" value="DONE">
                <a href="milestonelist" class="btn btn-success"><i class="bi bi-box-arrow-left"></i></a>
                <a class="btn btn-success" style="color: white" href="index"><i class="bi bi-house"></i></a>
            </form>
        </t:wrapper>
    </body>
</html>
