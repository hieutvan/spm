<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:wrapper title='Settings List'>
    <script>
        function submitForm() {
            document.getElementById("frmSearch").submit();
        }
        function updateStatus(setting_id, status) {
            var result = confirm("Do you want to update this setting status?");
            if (result) {
                window.location.href = 'updatesettingstatus?setting_id=' + setting_id + '&status=' + status;
            }
        }
    </script>
    <h1>
        Setting List
    </h1>

    <table id="table" data-toggle="table" data-search="true" data-pagination="true" data-filter-control="true" data-show-search-clear-button="true" data-resizable="true" data-show-jump-to="true" data-page-list="[10, 25, 50, 100, all]">
        <thead>
        <th data-sortable="true" >Setting</th>
        <th data-sortable="true" data-field="type" data-filter-control="select"data-searchable="false">Type</th>
        <th data-sortable="true" data-field="value" data-filter-control="select"data-searchable="false">Value</th>
        <th data-sortable="true" data-field="status" data-filter-control="select">Status</th>
        <th data-searchable="false">Action</th>
    </thead>
    <tbody>
        <c:forEach items="${requestScope.settinglist}" var="slist">
            <tr>
                <td>${slist.setting_title}</td>
                <td>${slist.type_id}</td>
                <td>${slist.setting_value}</td>
                <td>
                    <c:if test="${slist.status == 1}">Deactivate
                        <a href="#" onclick="updateStatus(${slist.setting_id}, 2)" class="btn btn-success"><i class="bi bi-eye"></i></a>
                        </c:if>
                        <c:if test="${slist.status == 2}">Activate
                        <a href="#" onclick="updateStatus(${slist.setting_id}, 1)" class="btn btn-warning"><i class="bi bi-eye-slash"></i></a>
                        </c:if>
                </td>
                <td><a href="settingdetail?setting_id=${slist.setting_id}" class="zoom btn btn-success"><i class="bi bi-layout-text-sidebar-reverse"></i></a></td>
            </tr>
        </c:forEach>
    </tbody>
</table>
<a class="btn btn-success" style="color: white" href="index"><i class="bi bi-house"></i></a>
<a href="addsetting" class="btn btn-success"><i class="bi bi-plus"></i></a>
</t:wrapper>