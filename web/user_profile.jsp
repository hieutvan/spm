<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<t:wrapper title='User Profile'>
    <h1>
        User Profile
    </h1>
    <table border="2px" class="table" style="text-align: center">
        <thead lass="thead-dark">
            <tr style="border-color: red">
                <th>Display name</th>
                <th>Username</th>
                <th>Email</th>
                <th>Role</th>
                <th>Phone number</th>
                <th>Status</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>${requestScope.acc.displayname}</td>
                <td>${requestScope.acc.username}</td>
                <td>${requestScope.acc.email}</td>
                <td><c:if test="${requestScope.acc.role_id == 1}">User</c:if>
                    <c:if test="${requestScope.acc.role_id == 2}">Admin</c:if>
                    <c:if test="${requestScope.acc.role_id == 3}">Author</c:if>
                    <c:if test="${requestScope.acc.role_id == 4}">Trainer</c:if>
                    </td>
                <td>${requestScope.acc.phonenumber}</td>
                <td><c:if test="${requestScope.acc.status == 1}">Deactivate</c:if>
                    <c:if test="${requestScope.acc.status == 2}">Activate</c:if>
                </td>
            </tr>
        </tbody>
    </table>
    <br/>
     <a href="userlist" class="btn btn-success"><i class="bi bi-box-arrow-left"></i></a>
     <a class="btn btn-success" style="color: white" href="index"><i class="bi bi-house"></i></a>
</t:wrapper>