<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<t:wrapper title='Tracking List'>
    <script>
        function submitForm() {
            document.getElementById("frmSearch").submit();
        }
        function updateStatus(tracking_id, status) {
            var result = confirm("Do you want to update this tracking status?");
            if (result) {
                window.location.href = 'updatetrackingstatus?tracking_id=' + tracking_id + '&status=' + status;
            }
        }
    </script>
    <h1>
        Tracking List
    </h1>
    <table id="table" data-toggle="table" data-search="true" data-pagination="true" data-filter-control="true" data-show-search-clear-button="true" data-resizable="true"  data-resizable="true" data-page-list="[10, 25, 50, 100, all]">
        <thead>
            <tr>
                <th data-sortable="true" data-field="assignee_email" data-filter-control="select">Assignee</th>
                <th data-sortable="true">Tracking Note</th>
                <th data-sortable="true" <c:if test="${sessionScope.account.role_id == 4}">data-field="team_code" data-filter-control="select"</c:if>>Team Code</th>
                <th data-sortable="true">Milestone</th>
                <th data-sortable="true">Function</th>
                <th data-sortable="true" data-field="status" data-filter-control="select">Status</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            <c:forEach items="${requestScope.trlist}" var="trlist">
                <tr>
                    <td>${trlist.assignee_email.displayname}</td>
                    <td>${trlist.updates}</td>
                    <td>${trlist.team.team_code}</td>
                    <td>${trlist.milestone.milestone_id}</td>
                    <td>${trlist.function.function_name}</td>
                    <td><c:if test="${sessionScope.account.role_id == 1}">
                            <c:if test="${trlist.status == 1}">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown">Planned</a>
                            </c:if>
                            <c:if test="${trlist.status == 2}">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown">Analyzed</a>
                            </c:if>
                            <c:if test="${trlist.status == 3}">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown">Designed</a>
                            </c:if>
                            <c:if test="${trlist.status == 4}">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown">Coded</a>
                            </c:if>
                            <c:if test="${trlist.status == 5}">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown">Integrated</a>
                            </c:if>
                            <c:if test="${trlist.status == 6}">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown">Submitted</a>
                            </c:if>
                            <c:if test="${trlist.status == 7}">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown">Evaluated</a>
                            </c:if>
                            <c:if test="${trlist.status == 8}">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown">Rejected</a>
                            </c:if>
                        </c:if>
                        <c:if test="${sessionScope.account.role_id == 4}">
                            <c:if test="${trlist.status == 1}">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown">Planned</a>
                            </c:if>
                            <c:if test="${trlist.status == 2}">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown">Analyzed</a>
                            </c:if>
                            <c:if test="${trlist.status == 3}">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown">Designed</a>
                            </c:if>
                            <c:if test="${trlist.status == 4}">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown">Coded</a>
                            </c:if>
                            <c:if test="${trlist.status == 5}">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown">Integrated</a>
                            </c:if>
                            <c:if test="${trlist.status == 6}">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown">Submitted</a>
                            </c:if>
                            <c:if test="${trlist.status == 7}">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown">Evaluated</a>
                            </c:if>
                            <c:if test="${trlist.status == 8}">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown">Rejected</a>
                            </c:if>
                            <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <li><a class="dropdown-item" onclick="updateStatus(${trlist.tracking_id}, 1)" href="#">Planned${trlist.tracking_id}</a></li>
                                <li><a class="dropdown-item" onclick="updateStatus(${trlist.tracking_id}, 2)" href="#">Analyzed${trlist.tracking_id}</a></li>
                                <li><a class="dropdown-item" onclick="updateStatus(${trlist.tracking_id}, 3)" href="#">Designed${trlist.tracking_id}</a></li>
                                <li><a class="dropdown-item" onclick="updateStatus(${trlist.tracking_id}, 4)" href="#">Coded${trlist.tracking_id}</a></li>
                                <li><a class="dropdown-item" onclick="updateStatus(${trlist.tracking_id}, 5)" href="#">Integrated${trlist.tracking_id}</a></li>
                                <li><a class="dropdown-item" onclick="updateStatus(${trlist.tracking_id}, 6)" href="#">Submitted${trlist.tracking_id}</a></li>
                                <li><a class="dropdown-item" onclick="updateStatus(${trlist.tracking_id}, 7)" href="#">Evaluated${trlist.tracking_id}</a></li>
                                <li><a class="dropdown-item" onclick="updateStatus(${trlist.tracking_id}, 8)" href="#">Rejected${trlist.tracking_id}</a></li>
                            </ul>
                        </c:if>
                    </td>
                    <td>
                        <a class="btn btn-success" href="trackingdetail?tracking_id=${trlist.tracking_id}"><i class="bi bi-layout-text-sidebar-reverse"></i></a>
                            <c:if test="${ trlist.status !=7 }">
                            <a class="btn btn-primary" href="locevafunction?tracking_id=${trlist.tracking_id}"><i class="bi bi-hand-index-thumb"></i></a>
                            </c:if>
                            <c:if test="${sessionScope.account.role_id == 4}">
                                <c:if test="${ trlist.status == 7}">
                                <a class="zoom btn btn-primary" href="addloceva?tracking_id=${trlist.tracking_id}"><i class="bi bi-plus-circle-fill"></i></a>
                                </c:if>
                            </c:if>
                    </td>
                </tr>
            </c:forEach>
        </tbody>
    </table>
    <a href="index" class="btn btn-success"><i class="bi bi-house"></i></a>
    </t:wrapper>
