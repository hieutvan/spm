<%-- 
    Document   : add_user
    Created on : Jun 12, 2022, 9:14:15 PM
    Author     : admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<!DOCTYPE html>

<t:wrapper title='Add user'>
    <h1>
        Add new user
    </h1>
    <form action="adduser" method="POST" class="w-50">
        <div class="mb-3">
            <label for="fullNameInput" class="form-label">Full name <span style="color: red">*</span></label>
            <input type="text" class="form-control" pattern=".{5,}"  name="name" id="fullNameInput" required title="5 characters minimum">
        </div>
        <div class="mb-3">
            <label for="emailInput" class="form-label">Email address <span style="color: red">*</span></label>
            <input type="email" name="email" class="form-control" id="emailInput" required>
        </div>
        <div class="mb-3">
            <label for="exampleInputEmail1" class="form-label">Phone number</label>
            <input name="phonenumber" class="form-control" id="emailInput">
        </div>
        <div class="mb-3">
            <label for="roleInput" class="form-label">Role <span style="color: red">*</span></label>
            <select class="form-control" name="role_id">
                <option value="1">User</option>
                <option value="2">Admin</option>
                <option value="3">Author</option>
                <option value="4">Trainer</option>
            </select>
        </div>
        <div class="mb-3">
            <label for="usernameInput" class="form-label">Username <span style="color: red">*</span></label>
            <input class="form-control" type="text" name='username' pattern=".{5,}" id="usernameInput" required title="5 characters minimum">
        </div>
        <div class="mb-3">
            <label for="pwInput" class="form-label">Password <span style="color: red">*</span></label>
            <input type="text" name='password' pattern=".{8,20}" class="form-control" id="pwInput" value="123a123@" title="8 to 20 characters" required>
        </div>
        <div class="mb-3">
            <label for="status" class="form-label">Status <span style="color: red">*</span></label>
            <input type="radio" name="status" value="1" id="status">Activate
            <input type="radio" name="status" value="2" id="status">Deactivate
        </div>
        <div class="mb-3">
            <input type="submit" class="zoom btn btn-success" value="DONE">
        </div>
    </form>
</t:wrapper>

