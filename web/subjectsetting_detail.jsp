<%-- 
    Document   : subjectsetting_detail
    Created on : Jul 23, 2022, 12:27:46 PM
    Author     : admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<!DOCTYPE html>

<t:wrapper title='Subject Setting Detail'>
    <h1>
        Subject Setting Detail
    </h1>
    <form action="subjectsettingdetail" class="w-50" method="POST">
        <input name="setting_id" value="${subset.SSsetting_id}" hidden>

        <div class="row">
            <div class="col">
                <div class="mb-3">
                    <label class="form-label" for="issuetitleInput">Subject Code</label>
                    <input class="form-control" value="${subset.subject.subject_code}"  type="text" id="issuetitleInput" disabled="" >
                    <input class="form-control" value="${subset.subject.subject_id}" name="subject_id" type="text" id="issuetitleInput" hidden="" >
                </div>
            </div>
            <div class="col">
                <div class="mb-3">
                    <label class="form-label" for="issuetitleInput">Subject Name</label>
                    <input class="form-control" value="${subset.subject.subject_name}"  type="text" id="issuetitleInput" disabled="" >
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <div class="mb-3">
                    <label class="form-label" for="issuetitleInput">Title</label>
                    <input class="form-control" value="${subset.setting_title}" name="setting_title" type="text" id="issuetitleInput" >
                </div>
            </div>
            <div class="col">
                <div class="mb-3">
                    <label class="form-label" for="issuetitleInput">Value</label>
                    <input class="form-control" value="${subset.setting_value}" name="setting_value" type="text" id="issuetitleInput" >
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col">                
                <div class="mb-3">
                    <label class="form-label" for="issuetitleInput">Type</label>
                    <input class="form-control" value="${subset.type_id}" name="type_id" type="text" id="issuetitleInput" >
                </div></div>
            <div class="col">
                <div class="mb-3">
                    <label class="form-label" for="issuetitleInput">Display order</label>
                    <input class="form-control" value="${subset.display_order}" name="display_order" type="text" id="issuetitleInput" >
                </div>
            </div>
        </div>
        <div class="mb-3">
            <label class="form-label" for="Status">Status</label>
            <c:if test="${subset.status == 1}">
                <input name="status" type="radio" id="status" value="1" checked="checked">Inactive
                <input name="status" type="radio" id="status" value="2">Active
            </c:if>
            <c:if test="${subset.status == 2}">
                <input name="status" type="radio" id="status" value="1">Inactive
                <input name="status" type="radio" id="status" value="2" checked="checked">Active
            </c:if>
        </div>
        <input type="submit" value="DONE" class="btn btn-success">
        <a href="subjectsettinglist" class="btn btn-success"><i class="bi bi-box-arrow-left"></i></a>
        <a class="btn btn-success" style="color: white" href="index"><i class="bi bi-house"></i></a>
    </form>
</t:wrapper>