<%-- 
    Document   : add_issue
    Created on : Jul 10, 2022, 4:24:35 PM
    Author     : admin
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<!DOCTYPE html>

<t:wrapper title='Creat issue'>
    <h1>
        Add new issue
    </h1>
    <form action="addissue" method="POST" class="w-50"> 
        <div class="row">
            <div class="col">
                <div class="mb-3">
                    <label class="form-label" for="FunctionIDinput">Assignee</label>
                    <select name="email" class="form-control" required="">
                        <c:forEach items="${requestScope.teamuserlist}" var="ulist">
                            <option value="${ulist.account.email}">${ulist.account.displayname}</option>
                        </c:forEach>
                    </select>
                </div>
            </div>
            <div class="col">
                <div class="mb-3">
                    <label class="form-label" for="issuetitleInput">Title</label>
                    <input class="form-control" name="issue_title" type="text" id="issuetitleInput" required>
                </div>
            </div>
        </div>
        <div class="mb-3">
            <label class="form-label" for="descriptionInput">Description</label>
            <textarea class="form-control" name="description" id="descriptionInput" type="text"></textarea>
        </div>
        <div class="mb-3">
            <div class="row">
                <div class="col">
                    <label class="form-label" for="gitlabInput">Gitlab ID</label>
                    <input style="display: inline-block" class="form-control" name="gitlab_id" type="text" id="gitlabInput" required="" >
                </div>
                <div class="col">
                    <label class="form-label" for="gitlabInput">Gitlab URL</label>
                    <input style="display: inline-block" class="form-control" name="gitlab_url" type="text" id="gitlabInput" required="">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <div class="mb-3">
                    <label class="form-label" for="FunctionIDinput">Milestone</label>
                    <select name="milestone_id" class="form-control">
                        <c:forEach items="${requestScope.milestonelist}" var="mlist">
                            <option value="${mlist.milestone_id}">${mlist.milestone_id}</option>
                        </c:forEach>
                    </select>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <div class="mb-3" >
                    <label class="form-label" for="DuedateInput">Due Date</label>
                    <input style="display: inline-block" class="form-control" name="due_date" type="date" id="DuedateInput"placeholder="dd-MM-yyyy" >
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <div class="mb-3">
                    <label class="form-label" for="FunctionIDinput">Function</label>
                    <select name="function_id" class="form-control" style="width: auto" >
                        <c:forEach items="${requestScope.functionlist}" var="flist">
                            <option value="${flist.function_id}">${flist.function_name}</option>
                        </c:forEach>
                    </select>
                </div>
            </div>
            <div class="col">
                <div class="mb-3">
                    <label class="form-label" for="LabelInput">Label</label>
                    <input class="form-control" name="labels" type="text" id="LabelInput" required="">
                </div>
            </div>
        </div>
        <div class="mb-3">
            <label class="form-label" for="Status">Status</label>
            <input  type="radio" name="status" value="0" id="status">Pending
            <input  type="radio" name="status" value="1" id="status">Close
            <input  type="radio" name="status" value="2" id="status">Open
        </div>
        <input type="submit" class="zoom btn btn-success" value="DONE">
    </form>
</t:wrapper>

