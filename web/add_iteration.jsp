<%-- 
    Document   : add_iteration
    Created on : Jun 13, 2022, 5:58:57 PM
    Author     : admin
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<!DOCTYPE html>

<t:wrapper title='Add iteration'>
    <h1>
        Add new iteration
    </h1>
    <form action="additeration" method="POST" class="w-50">
        <div class="mb-3">
            <label class="form-label" for="iternameInput">Iteration Name <span style="color: red">*</span></label>
            <input type="text" class="form-control" name="iteration_name" id="iternameInput" required>
        </div>
        <div class="mb-3">
            <label class="form-label" for="DurationInput">Duration <span style="color: red">*</span></label>
            <input type="number" class="form-control" name="duration" id="DurationInput" required>
        </div>
        <div class="mb-3">
            <label class="form-label" for="subjectidInput">Subject <span style="color: red">*</span></label>
            <select name="subject_id" class="form-control" required>
                <c:forEach items="${requestScope.sublist}" var="slist">
                    <option value="${slist.subject_id}">${slist.subject_code} - ${slist.subject_name}</option>
                </c:forEach>
            </select>
        </div>
        <div class="mb-3">
            <label class="form-label" for="statusInput">Status <span style="color: red">*</span></label>
            <input name="status" type="radio" id="status" value="1">Hide
            <input name="status" type="radio" id="status" value="2">Display
        </div>

        <input type="submit" class="zoom btn btn-success" value="DONE">
    </form>
</t:wrapper>
