<%-- 
    Document   : add_feature
    Created on : Jul 8, 2022, 5:14:11 PM
    Author     : admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<!DOCTYPE html>
        <t:wrapper title='Add Feature'>
            <h1>
                Add new feature
            </h1>
            <form action="addfeature" class="w-50" method="POST">
                <div class="mb-3">
                    <label class="form-label" for="TeamIDInput">Team List <span style="color: red">*</span></label>
                    <select name="team_id" class="form-control" style="width: auto" >
                        <c:forEach items="${requestScope.teamlist}" var="tlist">
                            <option value="${tlist.team_id}">${tlist.team_code} - ${tlist.classes.class_code}</option>
                        </c:forEach>
                    </select>
                </div>
                <div  class="mb-3">
                    <label class="form-label" for="FeatureNameInput">Feature Name <span style="color: red">*</span></label>
                    <input class="form-control" name="feature_name" type="text" id="FeatureNameInput" required>
                </div>
                <div class="mb-3">
                    <label class="form-label" for="status">Status <span style="color: red">*</span></label>
                    <input  type="radio" name="status" value="1" id="status">Inactive
                    <input  type="radio" name="status" value="2" id="status">Active
                </div>
                <input type="submit" class="zoom btn btn-success" value="DONE">
            </form>
        </t:wrapper>
