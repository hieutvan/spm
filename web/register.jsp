<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:wrapper title='Register'>
    <form action="register" class='w-50' method="POST">
        <h1>
            REGISTER FORM
        </h1>
        <div class="mb-3">
            <label for="fullNameInput" class="form-label">Full name</label>
            <input class="form-control" name='name' id="fullNameInput" required>
        </div>

        <div class="mb-3">
            <label for="emailInput" class="form-label">Email address</label>
            <input type="email" name='email' class="form-control" id="emailInput" required>
        </div>

        <div class="mb-3">
            <label for="exampleInputEmail1" class="form-label">Phone</label>
            <input class="form-control" name='phonenumber' id="phoneInput" required>
        </div>

        <div class="mb-3">
            <label for="usernameInput" class="form-label">Username</label>
            <input class="form-control" name='username' id="usernameInput" required>
        </div>

        <div class="mb-3">
            <label for="pwInput" class="form-label">Password</label>
            <input type="password" name='password' class="form-control" id="pwInput" required>
        </div>

        <div class="mb-3">
            <label for="repPwInput" class="form-label">Repeat password</label>
            <input type="password" name='repassword' class="form-control" id="repPwInput" required>
        </div>

        <div class='mb-3'>
            <input type="submit" class='btn btn-success' value="Register">
            <a class="btn btn-primary" href="login" style="color: white">Already have an account?</a>
        </div>


    </form>
</t:wrapper>
