<%-- 
    Document   : add_subjectsetting
    Created on : Jul 23, 2022, 1:08:37 PM
    Author     : admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<!DOCTYPE html>
<t:wrapper title='Add Subject Setting'>
    <h1>
        Add new subject setting
    </h1>
    <form action="addsubjectsetting" class="w-50" method="POST">
        <div class="mb-3">
            <label class="form-label" for="FunctionIDinput">Assignee</label>
            <select name="subject_id" class="form-control" required="">
                <c:forEach items="${requestScope.sublist}" var="slist">
                    <option value="${slist.subject_id}">${slist.subject_code} - ${slist.subject_name}</option>
                </c:forEach>
            </select>
        </div>
        <div class="row">
            <div class="col">
                <div class="mb-3">
                    <label class="form-label" for="issuetitleInput">Title</label>
                    <input class="form-control"  name="setting_title" type="text" id="issuetitleInput" >
                </div>
            </div>
            <div class="col">
                <div class="mb-3">
                    <label class="form-label" for="issuetitleInput">Value</label>
                    <input class="form-control"  name="setting_value" type="text" id="issuetitleInput" >
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col">                
                <div class="mb-3">
                    <label class="form-label" for="issuetitleInput">Type</label>
                    <input class="form-control" name="type_id" type="text" id="issuetitleInput" >
                </div></div>
            <div class="col">
                <div class="mb-3">
                    <label class="form-label" for="issuetitleInput">Display order</label>
                    <input class="form-control" name="display_order" type="text" id="issuetitleInput" >
                </div>
            </div>
        </div>
        <div class="mb-3">
            <label class="form-label" for="Status">Status</label>
            <input  type="radio" name="status" value="1" id="status">Inactive
            <input  type="radio" name="status" value="2" id="status">Active
        </div>
        <input type="submit" class="zoom btn btn-success" value="DONE">
    </form>
</t:wrapper>