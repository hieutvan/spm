<%-- 
    Document   : teameval_detail
    Created on : Jul 21, 2022, 1:10:47 AM
    Author     : admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <t:wrapper title='Team Evaluation Detail'>
            <a href="teamevallist" class="btn btn-success"><i class="bi bi-box-arrow-left"></i></a>
            <script>
                function updateLOC() {
                    var MaxLoc = parseInt(document.getElementById('MaxLoc').value);
                    var Weight = parseInt(document.getElementById('Weight').value);
                    var LOCtograde = document.getElementById('LOCtograde');

                    LOCtograde.value = MaxLoc * Weight / 1000;
                }
            </script> 
            <form action="teamevaldetail" class="w-50" method="POST">
                <input name="team_eval_id" value="${teameval.team_eval_id}" hidden="">
                <input name="criteria_id" value="${teameval.criteria.criteria_id}" hidden>
                <div class="row">
                    <div class="col">
                        <div class="mb-3">
                            <label class="form-label" for="EvaluationIDInput">Class</label>
                            <input class="form-control" value="${teameval.team.classes.class_id}" name="team_id" type="text" id="EvaluationIDInput" hidden="">
                            <input class="form-control" value="${teameval.team.classes.class_code}" name="team_code" type="text" id="EvaluationIDInput" disabled>
                        </div>
                    </div>
                    <div class="col">
                        <div class="mb-3">
                            <label class="form-label" for="EvaluationIDInput">Team</label>
                            <input class="form-control" value="${teameval.team.team_id}" name="team_id" type="text" id="EvaluationIDInput" hidden="">
                            <input class="form-control" value="${teameval.team.team_code}" name="team_code" type="text" id="EvaluationIDInput" disabled>
                        </div>
                    </div>
                    <div class="col">
                        <div class="mb-3">
                            <label class="form-label" for="EvaluationIDInput">Subject</label>
                            <input class="form-control" value="${teameval.criteria.iteration.subject.subject_id}" name="team_id" type="text" id="EvaluationIDInput" hidden="">
                            <input class="form-control" value="${teameval.criteria.iteration.subject.subject_name}" name="team_code" type="text" id="EvaluationIDInput" disabled>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col">
                        <div class="mb-3">
                            <label class="form-label" for="EvaluationIDInput">Max Loc</label>
                            <input class="form-control" value="${teameval.criteria.max_loc}" name="max_loc" type="text" id="MaxLoc" onchange="updateLOC();" <c:if test="${sessionScope.account.role_id == 1}">disabled</c:if>>
                            </div>
                        </div>
                        <div class="col">
                            <div class="mb-3">
                                <label class="form-label" for="EvaluationIDInput">Weight</label>
                                <input class="form-control" value="${teameval.criteria.evaluation_weight}" name="weight" type="text" id="Weight" onchange="updateLOC();" <c:if test="${sessionScope.account.role_id == 1}">disabled</c:if>>
                            </div>
                        </div>
                        <div class="col">

                        </div>
                    </div>
                    <div class="mb-3">
                        <label class="form-label" for="LOCOutput">Grade form LOC</label>
                        <input class="form-control" value="" name="LOC" type="text" id="LOCtograde" readonly>
                    </div>
                    <div class="mb-3">
                        <label class="form-label" for="EvaluationIDInput">Grade</label>
                        <input class="form-control" value="${teameval.grade}" name="grade" type="text" id="gradeInput" <c:if test="${sessionScope.account.role_id == 1}">readonly</c:if>>
                    </div>
                    <div class="mb-3">
                        <label class="form-label" for="EvaluationIDInput">Note</label>
                        <textarea class="form-control" name="note" type="text" id="gradeInput" <c:if test="${sessionScope.account.role_id == 1}">readonly</c:if>>${teameval.note}</textarea>
                    </div>
                <c:if test="${sessionScope.account.role_id == 4}">
                    <input type="submit" value="DONE" class="btn btn-success">
                </c:if>
            </form>

        </t:wrapper>
        <script>
            $(document).ready(function () {
                updateLOC();
            });
        </script>
    </body>
</html>
