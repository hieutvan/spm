<%-- 
    Document   : class_detail
    Created on : Jun 19, 2022, 2:17:49 PM
    Author     : admin
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<!DOCTYPE html>

<t:wrapper title='Class details'>
    <h1>
        Class detail
    </h1>
    <form class="w-50" method="POST" action="classdetail">
        <div class="mb-3">
            <label class="form-label" for="ClassCodeInput">Class Code</label>
            <input name="class_id" value="${requestScope.classes.class_id}" hidden>
            <input name="class_code" class="form-control" value="${requestScope.classes.class_code}" id="ClassCodeInput"<c:if test="${sessionScope.account.role_id == 4}">readonly</c:if>>
            </div>
            <div class="mb-3">
                <label class="form-label" for="subjectNameInput">Subject Name</label>
                <select name="subject_id" class="form-control" style="width: auto" <c:if test="${sessionScope.account.role_id == 4}">readonly</c:if>>
                <c:forEach items="${requestScope.sub}" var="slist">
                    <option ${slist.subject_id == requestScope.classes.subject.subject_id ? "selected=\"selected\"":""} value="${slist.subject_id}">${slist.subject_code}-${slist.subject_name}</option>
                </c:forEach>
            </select>
        </div>
        <div class="mb-3">
            <label class="form-label" for="emailInput">Author</label>
            <select name="email" class="form-control" style="width: auto" <c:if test="${sessionScope.account.role_id == 4}">disabled</c:if>>
                <c:forEach items="${requestScope.acc}" var="alist">
                    <option ${alist.email == requestScope.classes.account.email ? "selected=\"selected\"":""} value="${alist.email}">${alist.displayname}-${alist.email}</option>
                </c:forEach>
            </select>
        </div>
        <div class="row">
            <div class="col">
                <div class="mb-3">
                    <label class="form-label" for="classyearInput">Class Year</label>
                    <input class="form-control" name="class_year" type="text" id="classyearInput" value="${requestScope.classes.class_year}" <c:if test="${sessionScope.account.role_id == 4}">readonly</c:if>>
                    </div>
                </div>
                <div class="col">
                    <div class="mb-3">
                        <label class="form-label" for="classtermInput">Class term</label>
                        <select class="form-control" name="class_term" id="classtermInput" <c:if test="${sessionScope.account.role_id == 4}">disabled</c:if>>
                        <option ${requestScope.classes.class_term eq 'Spring'? "selected=\"selected\"":""}>Spring</option>
                        <option ${requestScope.classes.class_term eq 'Summer'? "selected=\"selected\"":""}>Summer</option>
                        <option ${requestScope.classes.class_term eq 'Fall'? "selected=\"selected\"":""}>Fall</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="mb-3">
            <label class="form-label" for="bl5Input">Block 5 class</label>
            <c:if test="${requestScope.classes.block5_class == 1}">
                <input name="block5_class" type="radio" id="bl5Input" value="1" checked="checked" >Deactivated
                <input name="block5_class" type="radio" id="bl5Input" value="2" >Activate
            </c:if>
            <c:if test="${requestScope.classes.block5_class == 2}">
                <input name="block5_class" type="radio" id="bl5Input" value="1">Deactivate
                <input name="block5_class" type="radio" id="bl5Input" value="2" checked="checked">Activated
            </c:if>
        </div>
        <div class="mb-3">
            <label class="form-label" for="status">Status</label>
            <c:if test="${requestScope.classes.status == 0}">
                <input name="status" type="radio" id="status" value="0" checked="checked">Cancel
                <input name="status" type="radio" id="status" value="1" >Deactive
                <input name="status" type="radio" id="status" value="2">Activate
            </c:if>
            <c:if test="${requestScope.classes.status == 1}">
                <input name="status" type="radio" id="status" value="0">Cancel
                <input name="status" type="radio" id="status" value="1" checked="checked">Deactive
                <input name="status" type="radio" id="status" value="2">Activate
            </c:if>
            <c:if test="${requestScope.classes.status == 2}">
                <input name="status" type="radio" id="status" value="0">Cancel
                <input name="status" type="radio" id="status" value="1">Deactive
                <input name="status" type="radio" id="status" value="2" checked="checked">Activate
            </c:if>
        </div>
        <input class="btn btn-success" type="submit" value="DONE">
        <a href="classlist" class="btn btn-success"><i class="bi bi-box-arrow-left"></i></a>
        <a class="btn btn-success" style="color: white" href="index"><i class="bi bi-house"></i></a>
    </form>
</t:wrapper>
