<%-- 
    Document   : function_detail
    Created on : Jul 9, 2022, 2:45:42 PM
    Author     : admin
--%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<t:wrapper title='Function Detail'>
    <h1>
        Function Detail
    </h1>
    <form action="functiondetail" class="w-50" method="POST">
        <input type="text" value="${requestScope.function.function_id}" name="function_id" hidden="">
        <div class="mb-3">
            <label class="form-label" for="FunctionNameInput">Function Name</label>
            <input class="form-control" value="${requestScope.function.function_name}" name="function_name" type="text" id="FunctionNameInput">
        </div>
        <c:if test="${sessionScope.account.role_id ==1}">
            <div class="mb-3" hidden>
                <label class="form-label" for="FeatureNameInput">Feature Name</label>
                <input class="form-control" value="${requestScope.function.feature.feature_name}" name="feature_name" type="text" id="FeatureNameInput" hidden="">
                <input class="form-control" value="${requestScope.function.feature.feature_id}" name="feature_id" type="text" id="FeatureNameInput" hidden="">
            </div>
        </c:if>
        <div class="row">
            <div class="col">
                <div class="mb-3">
                    <label class="form-label" for="TeamIDInput">Team Code</label>

                    <input class="form-control" value="${requestScope.function.team.team_id}" name="team_id" type="text" id="TeamIDInput" hidden="">
                    <input class="form-control" value="${requestScope.function.team.team_code}" name="team_code" type="text" id="TeamIDInput" readonly="">
                </div>
            </div>
            <div class="col">
                <div class="mb-3">
                    <label class="form-label" for="emailInput">Owner</label>
                    <input class="form-control" value="${requestScope.function.account.displayname}" name="displayname" type="text" id="emailInput" readonly="">
                    <input class="form-control" value="${requestScope.function.account.email}" name="email" type="text" id="emailInput" hidden="">
                </div>
            </div>
        </div>
        <c:if test="${sessionScope.account.role_id == 4}">
            <div class="mb-3">
                <label class="form-label" for="FeatureNameInput">Feature Name</label>
                <select name="feature_id" class="form-control" style="width: auto" >
                    <c:forEach items="${requestScope.list}" var="list">
                        <option ${list.feature.feature_id==requestScope.function.feature.feature_id ? "selected=\"selected\"":""} value="${list.feature.feature_id}">${list.feature.feature_id} - ${list.feature.feature_name}</option>
                    </c:forEach>
                </select>
            </div>
        </c:if>

        <div class="mb-3">
            <label class="form-label" for="AccessRoleInput">Access Role</label>
            <input class="form-control" value="${requestScope.function.access_roles}" name="access_roles" type="text" id="AccessRoleInput">
        </div>
        <div class="mb-3">
            <label class="form-label" for="DescriptionInput">Description</label>
            <textarea class="form-control" name="description" id="descriptionInput" type="text">${requestScope.function.description}</textarea>
        </div>
        <div class="row">
            <div class="col">
                <div class="mb-3">
                    <label class="form-label" for="ComplexityInput">Complexity</label>
                    <c:if test="${requestScope.function.complexity_id == 1}">
                        <input name="complexity_id" type="radio" id="complexity_id" value="1" checked="checked">Simple
                        <input name="complexity_id" type="radio" id="complexity_id" value="2" >Medium
                        <input name="complexity_id" type="radio" id="complexity_id" value="3">Complex
                    </c:if>
                    <c:if test="${requestScope.function.complexity_id == 2}">
                        <input name="complexity_id" type="radio" id="complexity_id" value="1" >Simple
                        <input name="complexity_id" type="radio" id="complexity_id" value="2" checked="checked">Medium
                        <input name="complexity_id" type="radio" id="complexity_id" value="3">Complex
                    </c:if>
                    <c:if test="${requestScope.function.complexity_id == 3}">
                        <input name="complexity_id" type="radio" id="complexity_id" value="1" >Simple
                        <input name="complexity_id" type="radio" id="complexity_id" value="2" >Medium
                        <input name="complexity_id" type="radio" id="complexity_id" value="3" checked="checked">Complex
                    </c:if>
                </div>
            </div>
            <div class="col">
                <div class="mb-3">
                    <label class="form-label" for="PriorityInput">Priority</label>
                    <input class="form-control" value="${requestScope.function.priority}" name="priority" type="text" id="PriorityInput">
                </div>
            </div>
        </div>
        <div class="mb-3" style="border: 2px">
            <label class="form-label" for="status">Status</label>
            <c:if test="${requestScope.function.status == 1}">
                <input name="status" type="radio" id="status" value="1" checked="checked">Pending
                <input name="status" type="radio" id="status" value="2" >Planned
                <input name="status" type="radio" id="status" value="3">Evaluated
                <input name="status" type="radio" id="status" value="4">Rejected
            </c:if>
            <c:if test="${requestScope.function.status == 2}">
                <input name="status" type="radio" id="status" value="1">Pending
                <input name="status" type="radio" id="status" value="2" checked="checked">Planned
                <input name="status" type="radio" id="status" value="3">Evaluated
                <input name="status" type="radio" id="status" value="4">Rejected
            </c:if>
            <c:if test="${requestScope.function.status == 3}">
                <input name="status" type="radio" id="status" value="1">Pending
                <input name="status" type="radio" id="status" value="2">Planned
                <input name="status" type="radio" id="status" value="3" checked="checked">Evaluated
                <input name="status" type="radio" id="status" value="4">Rejected
            </c:if>
            <c:if test="${requestScope.function.status == 4}">
                <input name="status" type="radio" id="status" value="1">Pending
                <input name="status" type="radio" id="status" value="2">Planned
                <input name="status" type="radio" id="status" value="3" >Evaluated
                <input name="status" type="radio" id="status" value="4" checked="checked">Rejected
            </c:if>
        </div>
        <input type="submit" class="btn btn-success" value="DONE">
        <a href="functionlist" class="btn btn-success"><i class="bi bi-box-arrow-left"></i></a>
        <a class="btn btn-success" style="color: white" href="index"><i class="bi bi-house"></i></a>
    </form>
</t:wrapper>
