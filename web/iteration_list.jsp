<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:wrapper title='Iteration list'>
    <script>
        function submitForm() {
            document.getElementById("frmSearch").submit();
        }
        function updateStatus(iteration_id, status) {
            var result = confirm("Do you want to update this iteration status?");
            if (result) {
                window.location.href = 'updateiterstatus?iteration_id=' + iteration_id + '&status=' + status;
            }
        }
    </script>
    <h1>
        Iteration List
    </h1>
    <table id="table" data-toggle="table" data-search="true" data-pagination="true" data-filter-control="true" data-show-search-clear-button="true" data-resizable="true"  data-page-list="[10, 25, 50, 100, all]">
        <thead>
            <tr style="font-size: 35px">
                <th data-sortable="true">Iteration name</th>
                <th data-sortable="true"data-searchable="false">Duration</th>
                <th data-field="subject_name" data-filter-control="select" data-sortable="true">Subject</th>
                    <c:if test="${sessionScope.account.role_id != 1}">
                    <th data-sortable="true" data-field="status" data-filter-control="select">Status</th>
                    </c:if>
                <th data-searchable="false">Action</th>
            </tr>
        </thead>
        <tbody>
            <c:forEach items="${requestScope.iterlist}" var="iterlist">
                <tr><c:if test="${sessionScope.account.role_id != 1}">
                        <td style="font-style: oblique">${iterlist.iteration_name}</td>
                        <td>${iterlist.duration} days</td>
                        <td>${iterlist.subject.subject_name}</td>
                        <td>
                            <c:if test="${iterlist.status == 1}">Deactive
                                <button class="btn btn-success"><a href="#" onclick="updateStatus(${iterlist.iteration_id},2)" style="color: white"><i class="bi bi-eye"></i></a></button>
                                    </c:if>
                                    <c:if test="${iterlist.status == 2}">Active
                                <button class="btn btn-warning"><a href="#" onclick="updateStatus(${iterlist.iteration_id},1)" style="color: white"><i class="bi bi-eye-slash"></i></a></button>
                                    </c:if>
                        </td>
                    </c:if>
                    <c:if test="${sessionScope.account.role_id == 1}">
                        <c:if test="${iterlist.status == 2}">
                            <td style="font-style: oblique">${iterlist.iteration_name}</td>
                            <td><fmt:formatDate pattern ="dd/MMM/yyyy" value="${iterlist.date}"/></td>
                        </c:if>
                    </c:if>
                    <td><a href="updateiter?iteration_id=${iterlist.iteration_id}" class="zoom btn btn-success"><i class="bi bi-layout-text-sidebar-reverse"></i></a></td>
                </tr>
            </c:forEach>
        </tbody>
    </table>
    <c:if test="${sessionScope.account.role_id != 1}">
        <button class="btn btn-success"><a href="additeration" style="color: white"><i class="bi bi-plus-lg"></i></a></button>
            </c:if>
        <a class="btn btn-success" style="color: white" href="index"><i class="bi bi-house"></i></a>
</t:wrapper>