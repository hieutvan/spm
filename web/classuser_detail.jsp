<%-- 
    Document   : classuser_detail
    Created on : Jun 27, 2022, 10:11:40 PM
    Author     : admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>

<t:wrapper title='Class User Detail'>
    <h1>
        Class User Detail
    </h1>
    <form class="w-50">
        <div class="row">
            <div class="col">
                <div class="mb-3">
                    <label for="className" class="form-label">Class Name</label>
                    <input type="text" class="form-control" name="class_id" id="className" value="${requestScope.classes.class_code}" readonly="">
                </div>
            </div>
            <div class="col">
                <div class="mb-3">
                    <label for="className" class="form-label">Team ID</label>
                    <input type="text" class="form-control" name="class_id" id="className" value="${requestScope.team.team_code}" readonly="">
                </div>
            </div>
        </div>
        <div class="mb-3">
            <label for="className" class="form-label">Full name</label>
            <input type="text" class="form-control" name="class_id" id="className" value="${requestScope.account.displayname}" readonly="">
        </div>
        <div class="row">
            <div class="col">
                <div class="mb-3">
                    <label for="className" class="form-label">Ongoing evaluation</label>
                    <input type="text" class="form-control" name="class_id" id="className" value="${requestScope.user.ongoing_eval}" readonly="">
                </div>
            </div>
            <div class="col">
                <div class="mb-3">
                    <label for="className" class="form-label">Final presentation evaluation</label>
                    <input type="text" class="form-control" name="class_id" id="className" value="${requestScope.user.final_pres_eval}" readonly="">
                </div>
            </div>
        </div>
        <div class="col">
            <div class="mb-3">
                <label for="className" class="form-label">Final topic evaluation</label>
                <input type="text" class="form-control" name="class_id" id="className" value="${requestScope.user.final_topic_eval}" readonly="">
            </div>
        </div>
        <div class="mb-3">
            <label class="form-label" for="status">Status</label>
            <c:if test="${requestScope.user.status == 2}">
                <input  type="radio" name="status" value="1" id="status">Inactive
                <input  type="radio" name="status" value="2" id="status" checked="checked">Active
            </c:if>
            <c:if test="${requestScope.user.status == 1}">
                <input  type="radio" name="status" value="1" id="status" checked="checked">Inactive
                <input  type="radio" name="status" value="2" id="status">Active
            </c:if>
        </div>
    </form>
    <a href="classuserlist" class="btn btn-success"><i class="bi bi-box-arrow-left"></i></a>
    <a class="btn btn-success" style="color: white" href="index"><i class="bi bi-house"></i></a>
    </t:wrapper>

