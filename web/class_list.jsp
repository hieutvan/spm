<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:wrapper title='Class List'>
    <h1>
        Class List
    </h1>
    <c:if test="${sessionScope.account.role_id ==2 || sessionScope.account.role_id == 3}">
        
        </c:if>
    <script>
        function submitForm() {
            document.getElementById("frmSearch").submit();
        }
        function updateStatus(class_id, status) {
            var result = confirm("Do you want to update this class status?");
            if (result) {
                window.location.href = 'updateclassstatus?class_id=' + class_id + '&status=' + status;
            }
        }
    </script>
    <table id="table" data-toggle="table" data-search="true" data-filter-control="true" data-show-search-clear-button="true" data-pagination="true" data-resizable="true"  data-page-list="[10, 25, 50, 100, all]">
        <thead>
            <tr>
                <th data-field="class_code" data-sortable="true" data-filter-control="select" >Class Code</th>
                <th data-field="trainer" data-searchable="false">Trainer</th>
                <th data-field="subject_name" data-filter-control="select">Subject Name</th>
                    <c:if test="${sessionScope.account.role_id == 2 || sessionScope.account.role_id == 3}">
                    <th data-sortable="true" data-searchable="false">Status</th>
                    <th data-sortable="true" data-searchable="false">Block 5 class</th>
                    </c:if>
                <th data-field="class_term" data-filter-control="select">Class term</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            <c:forEach items="${requestScope.classlist}" var="clist">
                <tr>
                    <td>${clist.class_code} <a href="specificclassuserlist?class_id=${clist.class_id}" class="btn btn-success"><i class="bi bi-list"></i></a></td>
                    <td>${clist.account.displayname}</td>
                    <td>${clist.subject.subject_name}</td>
                    <c:if test="${sessionScope.account.role_id == 2 || sessionScope.account.role_id == 3}">
                        <td>
                            <c:if test="${clist.status == 0}">
                                Cancelled
                                <a href="#" onclick="updateStatus(${clist.class_id}, 2)" class="btn btn-success"><i class="bi bi-eye"></i></a>
                                </c:if>
                                <c:if test="${clist.status == 1}">
                                Deactivated
                                <a href="#" onclick="updateStatus(${clist.class_id}, 0)" class="btn btn-danger"><i class="bi bi-x-circle-fill"></i></a>
                                </c:if>
                                <c:if test="${clist.status == 2}">
                                Activated
                                <a href="#" onclick="updateStatus(${clist.class_id}, 1)" class="btn btn-warning"><i class="bi bi-eye-slash"></i></a>
                                </c:if>
                        </td>
                        <td>
                            <c:if test="${clist.block5_class == 1}">
                                NO
                                <a href="updatebl?class_id=${clist.class_id}&block5_class=2" class="btn btn-success"><i class="bi bi-eye"></i></a>
                                </c:if>
                                <c:if test="${clist.block5_class == 2}">
                                YES
                                <a href="updatebl?class_id=${clist.class_id}&block5_class=1" class="btn btn-warning"><i class="bi bi-eye-slash"></i></a>
                                </c:if>
                        </td>
                    </c:if>
                    <td>${clist.class_term} ${clist.class_year}</td>
                    <td>
                        <a class="zoom btn btn-success" href="classdetail?class_id=${clist.class_id}"><i class="bi bi-layout-text-sidebar-reverse"></i></a></td>
                </tr>
            </c:forEach>
        </tbody>
    </table>
    <a href="index" class="btn btn-success"><i class="bi bi-house"></i></a>
    <a href="addclass" class="btn btn-success"><i class="bi bi-plus"></i></a>
</t:wrapper>