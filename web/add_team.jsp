<%-- 
    Document   : add_team
    Created on : Jun 26, 2022, 1:12:50 AM
    Author     : admin
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<!DOCTYPE html>
<t:wrapper title='Add teams'>
    <h1>
        Add new team
    </h1>
    <form class="w-50" method="POST" action="addteam">
        <div class="row">
            <div class="col">
                <div class="mb-3">
                    <label class="form-label" for="teamcodeInput">Team code <span style="color: red">*</span></label>
                    <input class="form-control" name="team_code" type="text" id="teamcodeInput">
                </div>
            </div>
            <div class="col">
                <div class="mb-3">
                    <label class="form-label" for="classInput">Class Code <span style="color: red">*</span></label>
                    <select name="class_id" class="form-control" style="width: auto">
                        <c:forEach items="${requestScope.classlist}" var="clist">
                            <option value="${clist.class_id}">
                                ${clist.class_id} - ${clist.class_code}
                            </option>
                        </c:forEach>
                    </select>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <div class="mb-3">
                    <label class="form-label" for="topiccodeInput">Topic code <span style="color: red">*</span></label>
                    <input class="form-control" name="topic_code" type="text" id="topicocdeInput">
                </div>
            </div>
            <div class="col">
                <div class="mb-3">
                    <label class="form-label" for="topicnameInput">Topic name <span style="color: red">*</span></label>
                    <input class="form-control" name="topic_name" type="text" id="topicnameInput">
                </div>
            </div>
        </div>
        <div class="mb-3">
            <label class="form-label" for="gitlbaInput">GitLab Link <span style="color: red">*</span></label>
            <input class="form-control" name="gitlab_url" type="text" id="gitlbaInput">
        </div>
        <div class="mb-3">
            <label class="form-label" for="status">Status <span style="color: red">*</span></label>
            <input name="status" type="radio" id="status" value="1">Deactivated
            <input name="status" type="radio" id="status" value="2">Activate
        </div>
        <input type="submit" class="zoom btn btn-success" value="DONE">
    </form>
</t:wrapper>

