<%-- 
    Document   : subject_detail
    Created on : Jun 8, 2022, 11:31:55 PM
    Author     : admin
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<!DOCTYPE html>

<t:wrapper title="Subject Detail">
    <h1>
        Subject Detail
    </h1>
    <table class="table" border="2px">
        <thead>
            <tr>
                <th>Subject code</th>
                <th>Subject name</th>
                <th>Manager</th>
                <th>Email</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>${requestScope.subject.subject_code}</td>
                <td>${requestScope.subject.subject_name}</td>
                <td>${requestScope.acc.displayname}</td>
                <td>${requestScope.acc.email}</td>
            </tr>
        </tbody>
    </table>
    <a href="subjectlist" class="btn btn-success" style="color: white">Back to subject list</a>
    <c:if test="${sessionScope.account.role_id == 2}">
        <a href="updatesubject?subject_code=${requestScope.subject.subject_code}" class="btn btn-success" style="color: white">Update subject</a>
    </c:if>
</t:wrapper>

