<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<t:wrapper title='Tracking Details'>
    <h1>
        Tracking Detail
    </h1>
    <form class="w-50" method="POST" action="trackingdetail">
        <input name="tracking_id" class="form-control" value="${requestScope.tracking.tracking_id}" hidden>

        <div class='row mb-3'>
            <div class='col'>
                <label class="form-label">Team</label>
                <input name="team_id" class="form-control" value="${requestScope.tracking.team.team_id}" hidden>
                <input name="team" class="form-control" disabled value="${requestScope.tracking.team.team_code}">
            </div>

            <div class="col">
                <label class="form-label">Milestone</label>
                <input name="milestone_id" class="form-control" value="${requestScope.tracking.milestone.milestone_id}" hidden>
                <input name="milestone" class="form-control" disabled value="${requestScope.tracking.milestone.milestone_id}">
            </div>
            
            <div class="col">
                <label class="form-label">Function</label>
                <input name="function_id" class="form-control" value="${requestScope.tracking.function.function_id}" hidden>
                <input name="function" class="form-control" disabled value="${requestScope.tracking.function.function_name}">
            </div>
        </div>

        <div class="row mb-3">
            <div class="col">
                <label class="form-label">Assigner</label>
                <select name='assigner_email' class='form-control'>
                    <c:forEach items='${requestScope.peopleList}' var='pp'>
                        <option ${pp.email == requestScope.tracking.assigner_email.email ? "selected" : ""} value='${pp.email}'>${pp.displayname}</option>
                    </c:forEach>
                </select>
            </div>
                       
            <div class="col">
                <label class="form-label">Assignee</label>
                
                <select name='assignee_email' class='form-control'>
                    <c:forEach items='${requestScope.peopleList}' var='pp'>
                        <option ${pp.email == requestScope.tracking.assignee_email.email ? "selected" : ""} value='${pp.email}'>${pp.displayname}</option>
                    </c:forEach>
                </select>
            </div>
        </div>

        <div class='row mb-3'>
            <div class='col'>
                <label class='form-label' for='trackingNotesInput'>Tracking Notes</label>
                <input type='text' id='trackingNotesInput' name='tracking_notes' class='form-control' value="${requestScope.tracking.tracking_note}">
            </div>
        
            <div class='col'>
                <label class='form-label'>Updates</label>
                <textarea type='text' id='trackingNoteInput' name='updates' class='form-control'>${requestScope.tracking.updates}</textarea>
            </div>
        </div>
        
        <div class="row mb-3">
            <div class='col'>
                <label class="form-label">Status</label>
                <select class="form-select" name="status">
                    <option ${0 == requestScope.tracking.status ? "selected" : ""} value="0">Planned</option>
                    <option ${1 == requestScope.tracking.status ? "selected" : ""} value="1">Analyzed</option>
                    <option ${2 == requestScope.tracking.status ? "selected" : ""} value="2">Designed</option>
                    <option ${3 == requestScope.tracking.status ? "selected" : ""} value="3">Coded</option>
                    <option ${4 == requestScope.tracking.status ? "selected" : ""} value="4">Integrated</option>
                    <option ${5 == requestScope.tracking.status ? "selected" : ""} value="5">Submitted</option>
                    <option ${6 == requestScope.tracking.status ? "selected" : ""} value="6">Evaluated</option>
                    <option ${7 == requestScope.tracking.status ? "selected" : ""} value="7">Rejected</option>
                </select>
            </div>
        </div>

        <div class='row mb-3'>
            <input class="btn btn-success" value="DONE" type="submit">
        </div>
    </form>
</t:wrapper>
