<%-- 
    Document   : subjectsetting_list
    Created on : Jul 17, 2022, 8:43:13 PM
    Author     : admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <body>
        <t:wrapper title='Subject Setting List'>
            <h1>
                Subject Setting List
            </h1>
            <script>
                function submitForm() {
                    document.getElementById("frmSearch").submit();
                }
                function updateStatus(SSsetting_id, status) {
                    var result = confirm("Do you want to update this subject setting status?");
                    if (result) {
                        window.location.href = 'updatesubsetting?SSsetting_id=' + SSsetting_id + '&status=' + status;
                    }
                }
            </script>

            <table id="table" data-toggle="table" data-search="true" data-pagination="true" data-filter-control="true" data-show-search-clear-button="true" data-resizable="true"  date-page-list="{10,25,50,100,all}">
                <thead>
                <th data-sortable="true">Subject code</th>
                <th data-sortable="true">Subject Name</th>
                <th data-sortable="true">Setting title</th>
                <th data-sortable="true" data-field="status" data-filter-control="select">Status</th>
                <th>Action</th>
            </thead>
            <tbody>
                <c:forEach items="${requestScope.sslist}" var="sslist">
                    <tr>
                        <td>${sslist.subject.subject_code}</td>
                        <td>${sslist.subject.subject_name}</td>
                        <td>${sslist.setting_title}</td>
                        <td><c:if test="${sslist.status == 1}">Inactive
                                <a href="#" onclick="updateStatus(${sslist.SSsetting_id}, 2)" class="btn btn-success"><i class="bi bi-eye"></i></a>
                                </c:if>
                                <c:if test="${sslist.status == 2}">Active
                                <a href="#" onclick="updateStatus(${sslist.SSsetting_id}, 1)" class="btn btn-warning"><i class="bi bi-eye-slash"></i></a></c:if></td>
                        <td><a href="subjectsettingdetail?setting_id=${sslist.SSsetting_id}" class="zoom btn btn-success"><i class="bi bi-layout-text-sidebar-reverse"></i></a></td>
                    </tr>
                </c:forEach>
            </tbody>
        </table>
        <a class="btn btn-success" style="color: white" href="index"><i class="bi bi-house"></i></a>
        <a href="addsubjectsetting" class="btn btn-success"><i class="bi bi-plus"></i></a>
        </t:wrapper>
</body>
</html>
