<%-- 
    Document   : additereval
    Created on : Jul 20, 2022, 5:48:20 PM
    Author     : admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <t:wrapper title='Iteration Evaluation'>
            <script>
                function submitForm() {
                    document.getElementById("frmSearch").submit();
                }
                function updateLOC() {
                    var grade = parseInt(document.getElementById('gradeInput').value);
                    var bonus = parseInt(document.getElementById('bonusInput').value);
                    var TotalOutput = document.getElementById('TotalOutput');

                    TotalOutput.value = grade + bonus;
                }
            </script>
            <form action="additereval" class="w-50" method="POST" id="frmSearch">

                <div class="mb-3">
                    <label class="form-label" for="QualityIDInput">Student</label>
                    <select name="email" class="form-control"  >
                        <c:forEach items="${requestScope.assigneelist}" var="alist">
                            <option value="${alist.email}">${alist.displayname}</option>
                        </c:forEach>
                    </select>
                </div>

                <div class="row">
                    <div class="col">
                        <div class="mb-3">
                            <label class="form-label" for="gradeInput">Grade</label>
                            <input class="form-control" value="" <c:if test="${sessionScope.account.role_id == 1}">readonly</c:if> oninput="updateLOC();" name="grade" step="0.1" type="text" id="gradeInput" >
                            </div>
                        </div>
                        <div class="col">
                            <div class="mb-3">
                                <label class="form-label" for="bonusInput">Bonus</label>
                                <input class="form-control" value="" <c:if test="${sessionScope.account.role_id == 1}">readonly</c:if> oninput="updateLOC();" name="bonus" step="0.1" type="text" id="bonusInput" >
                            </div>
                        </div>
                        <div class="col">
                            <div class="mb-3">
                                <label class="form-label" for="LOCOutput">Total grade</label>
                                <input class="form-control" value="" name="LOC" type="output" id="TotalOutput" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="mb-3">
                                <label class="form-label" for="QualityIDInput">Subject</label>
                                <select name="subject_id" class="form-control"  >
                                    <option value="-1">Select Subject</option>
                                <c:forEach items="${requestScope.sublist}" var="slist">
                                    <option ${requestScope.subject_id == slist.subject_id ? "selected=\"selected\"":""} value="${slist.subject_id}">${slist.subject_name}</option>
                                </c:forEach>
                            </select>
                        </div>
                    </div>
                    <div class="col">
                        <div class="mb-3">
                            <label class="form-label" for="QualityIDInput">Iteration</label>
                            <select name="iteration_id" class="form-control" >
                                <c:forEach items="${requestScope.iterlist}" var="ilist">
                                    <option value="${ilist.iteration_id}">${ilist.iteration_name}---${ilist.subject.subject_name}</option>
                                </c:forEach>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="mb-3">
                    <label class="form-label" for="gradeInput">Note</label>
                    <textarea class="form-control" name="note" type="text" id="EvaluationNoteInput" <c:if test="${sessionScope.account.role_id == 1}">readonly</c:if> ></textarea>
                    </div>
                    <input type="submit" value="DONE" class="btn btn-success">
                </form>
        </t:wrapper>
        $(document).ready(function () {
        updateLOC();
        });
    </body>
</html>
