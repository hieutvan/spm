<%-- 
    Document   : itereval_detail
    Created on : Jul 20, 2022, 2:48:36 PM
    Author     : admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<t:wrapper title='Iteration Evaluation'>
    <script>
        function updateLOC() {
            var grade = parseInt(document.getElementById('gradeInput').value);
            var bonus = parseInt(document.getElementById('bonusInput').value);
            var TotalOutput = document.getElementById('TotalOutput');

            TotalOutput.value = grade + bonus;
        }
    </script> 
    <a href="iterevalist" class="btn btn-success"><i class="bi bi-box-arrow-left"></i></a>
    <form action="iterevadetail" class="w-50" method="POST">
        <div class="mb-3">
            <label class="form-label" for="EvaluationIDInput">Evaluation ID</label>
            <input class="form-control" value="${requestScope.iedetail.evaluation_id}" name="evaluation_id" type="text" id="EvaluationIDInput" readonly>
        </div>
        <div class="mb-3">
            <label class="form-label" for="EvaluationIDInput">Iteration</label>
            <input class="form-control" value="${requestScope.iedetail.iteration.iteration_id}" name="iteration_id" type="text" id="EvaluationIDInput" hidden>
            <input class="form-control" value="${requestScope.iedetail.iteration.iteration_name}" name="iteration_name" type="text" id="EvaluationIDInput" readonly>
        </div>

        <c:if test="${sessionScope.account.role_id == 4}">
            <div class="mb-3">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown">Direction</a>
                <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <li><a class="dropdown-item" href="updateiter?iteration_id=${requestScope.iedetail.iteration.iteration_id}">Iteration</a></li>
                    <li><a class="dropdown-item" href="teamdetail?team_id=${requestScope.iedetail.team.team.team_id}">Team</a></li>
                    <li><a class="dropdown-item" href="classdetail?class_id=${requestScope.iedetail.classes.classes.class_id}">Function</a></li>
                </ul>
            </div>
        </c:if>
        <div class="row">
            <div class="col">
                <div class="mb-3">
                    <label class="form-label" for="gradeInput">Grade</label>
                    <input class="form-control" value="${requestScope.iedetail.grade}" <c:if test="${sessionScope.account.role_id == 1}">readonly</c:if> oninput="updateLOC();" name="grade" step="0.1" type="text" id="gradeInput" >
                    </div>
                </div>
                <div class="col">
                    <div class="mb-3">
                        <label class="form-label" for="bonusInput">Bonus</label>
                        <input class="form-control" value="${requestScope.iedetail.bonus}" <c:if test="${sessionScope.account.role_id == 1}">readonly</c:if> oninput="updateLOC();" name="bonus" step="0.1" type="text" id="bonusInput" >
                    </div>
                </div>
                <div class="col">
                    <div class="mb-3">
                        <label class="form-label" for="LOCOutput">Total grade</label>
                        <input class="form-control" value="" name="LOC" type="output" id="TotalOutput" readonly>
                    </div>
                </div>
            </div>
            <div class="mb-3">
                <label class="form-label" for="gradeInput">Note</label>
                <textarea class="form-control" name="note" type="text" id="EvaluationNoteInput" <c:if test="${sessionScope.account.role_id == 1}">readonly</c:if> >${requestScope.iedetail.note}</textarea>
            </div>
            <input type="submit" name="DONE" class="btn btn-success">
        </form>
</t:wrapper>
<script>
    $(document).ready(function () {
        updateLOC();
    });
</script>
