<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:wrapper title='Criteria List'>
    <c:if test="${sessionScope.account.role_id == 3}">
        <a href="addcrit" class="btn btn-success"><i class="bi bi-plus"></i></a>
        </c:if>
    <script>
        function submitForm() {
            document.getElementById("frmSearch").submit();
        }
        function updateStatus(criteria_id, status) {
            var result = confirm("Do you want to update this criteria status?");
            if (result) {
                window.location.href = 'updatecritstatus?criteria_id=' + criteria_id + '&status=' + status;
            }
        }
    </script>
    <h1>
        Evaluation Criteria List
    </h1>
    <table id="table" data-toggle="table" data-search="true" data-pagination="true" data-filter-control="true" data-show-search-clear-button="true" data-resizable="true" data-show-jump-to="true" data-page-list="[10, 25, 50, 100, all]">
        <thead>
            <tr>
                <th data-sortable="true" data-field="iteration_id" data-filter-control="select">Iteration</th>
                <th data-sortable="true" data-searchable="false">Weight</th>
                <th data-sortable="true" data-field="subject" data-filter-control="select">Subject</th>
                <th data-sortable="true" data-searchable="false">Team evaluated</th>
                <th data-sortable="true" data-searchable="false">Max LOC (Line of Code)</th>
                <th data-sortable="true" data-searchable="false" data-field="status" data-filter-control="select">Status</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            <c:forEach items="${requestScope.eclist}" var="eclist">
                <tr>
                    <td>${eclist.iteration.iteration_name}</td>
                    <td>${eclist.evaluation_weight}</td>
                    <td>${eclist.iteration.subject.subject_name}</td>
                    <td>
                        <c:if test="${eclist.team_evaluation == 1}">False
                            <a href="updateteameva?criteria_id=${eclist.criteria_id}&team_evaluation=2" class="btn btn-success"><i class="bi bi-eye"></i></a>
                            </c:if>
                            <c:if test="${eclist.team_evaluation == 2}">True
                            <a href="updateteameva?criteria_id=${eclist.criteria_id}&team_evaluation=1" class="btn btn-success"><i class="bi bi-eye-slash"></i></a>
                            </c:if>
                    </td>
                    <td>${eclist.max_loc}</td>
                    <td>
                        <c:if test="${eclist.status == 1}">Deactivate
                            <a href="#" onclick="updateStatus(${eclist.criteria_id}, 2)" class="btn btn-success"><i class="bi bi-eye"></i></a>
                            </c:if>
                            <c:if test="${eclist.status == 2}">Activate
                            <a href="#" onclick="updateStatus(${eclist.criteria_id}, 1)" class="btn btn-warning"><i class="bi bi-eye-slash"></i></a>
                            </c:if>
                    </td>
                    <td><a href="updatecrit?criteria_id=${eclist.criteria_id}" class="zoom btn btn-success"><i class="bi bi-layout-text-sidebar-reverse"></i></a></td>   
                </tr>
            </c:forEach>
        </tbody>
    </table>
    <a class="btn btn-success" style="color: white" href="addcrit"><i class="bi bi-plus-lg"></i></a>
    <a href="index" class="btn btn-success"><i class="bi bi-house"></i></a>
</t:wrapper>