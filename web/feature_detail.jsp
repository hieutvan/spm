<%-- 
    Document   : feature_detail
    Created on : Jul 8, 2022, 7:43:16 PM
    Author     : admin
--%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>

<t:wrapper title='Feature Detail'>
    <h1>
        Feature Detail
    </h1>
    <form action="featuredetail" class="w-50" method="POST">
        
        <input type="text" value="${requestScope.feature.feature_id}" name="feature_id" hidden="">
        <c:if test="${sessionScope.account.role_id == 1}">
            <div class="mb-3">
                <label class="form-label" for="TeamIDInput">Team ID</label>
                <input class="form-control" value="${requestScope.feature.team.team_id}" name="team_id" type="text" id="TeamIDInput" readonly>
            </div>
        </c:if>
        <c:if test="${sessionScope.account.role_id == 4}">
            <div>
                <label class="form-label" for="TeamIDInput">Team ID</label>
                <select name="team_id" class="form-control" style="width: auto" >
                    <c:forEach items="${requestScope.teamlist}" var="tlist">
                        <option ${tlist.team_id==requestScope.feature.team.team_id ? "selected=\"selected\"":""} value="${tlist.team_id}">${tlist.team_id} - ${tlist.team_code}</option>
                    </c:forEach>
                </select>
            </div>
        </c:if>
        <div  class="mb-3">
            <label class="form-label" for="FeatureNameInput">Feature Name</label>
            <input class="form-control" value="${requestScope.feature.feature_name}" name="feature_name" type="text" id="FeatureNameInput" required>
        </div>
        <div class="mb-3">
            <label class="form-label" for="status">Status</label>
            <c:if test="${requestScope.feature.status == 2}">
                <input  type="radio" name="status" value="1" id="status">Inactive
                <input  type="radio" name="status" value="2" id="status" checked="checked">Active
            </c:if>
            <c:if test="${requestScope.feature.status == 1}">
                <input  type="radio" name="status" value="1" id="status" checked="checked">Inactive
                <input  type="radio" name="status" value="2" id="status">Active
            </c:if>
        </div>
        <input type="submit" class="btn btn-success" value="DONE">
        <a href="featurelist" class="btn btn-success"><i class="bi bi-box-arrow-left"></i></a>
        <a class="btn btn-success" style="color: white" href="index"><i class="bi bi-house"></i></a>
    </form>
</t:wrapper>

