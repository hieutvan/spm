<%-- 
    Document   : add_function
    Created on : Jul 27, 2022, 11:01:39 PM
    Author     : admin
--%>

<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<t:wrapper title='Add Function'>
    <h1>
        Add Function
    </h1>
    <form action="addfunction" class="w-50" method="POST">
        <div class="mb-3">
            <label class="form-label" for="FunctionNameInput">Function Name</label>
            <input class="form-control" name="function_name" type="text" id="FunctionNameInput">
        </div>
        <c:if test="${sessionScope.account.role_id == 1}">
            <div class="mb-3">
                <label class="form-label" for="classInput">Feature</label>
                <select name="feature_id" class="form-control" >
                    <c:forEach items="${requestScope.featurelist}" var="flist">
                        <option value="${flist.feature_id}">
                            ${flist.feature_name} 
                        </option>
                    </c:forEach>
                </select>
            </div>
        </c:if>
        <c:if test="${sessionScope.account.role_id == 4}">
            <div class="row">
                <div class="col">
                    <div class="mb-3">
                        <label class="form-label" for="classInput">Feature</label>
                        <select name="feature_id" class="form-control" >
                            <c:forEach items="${requestScope.featurelist}" var="flist">
                                <option value="${flist.feature_id}">
                                    ${flist.feature_name} - ${flist.team.team_code}
                                </option>
                            </c:forEach>
                        </select>
                    </div>
                </div>
                <div class="col">
                    <div class="mb-3">
                        <label class="form-label" for="emailInput">Owner</label>
                        <select name="email" class="form-control" >
                            <c:forEach items="${requestScope.accountlist}" var="alist">
                                <option value="${alist.email}">
                                    ${alist.displayname}
                                </option>
                            </c:forEach>
                        </select>
                    </div>
                </div>
            </div>
        </c:if>
        <div class="mb-3">
            <label class="form-label" for="AccessRoleInput">Access Role</label>
            <input class="form-control"  name="access_roles" type="text" id="AccessRoleInput">
        </div>
        <div class="mb-3">
            <label class="form-label" for="DescriptionInput">Description</label>
            <textarea class="form-control" name="description" id="descriptionInput" type="text"></textarea>
        </div>
        <div class="mb-3">
            <label class="form-label" for="AccessRoleInput">Access Role</label>
            <input name="complexity_id" type="radio" id="complexity_id" value="1">Simple
            <input name="complexity_id" type="radio" id="complexity_id" value="2">Medium
            <input name="complexity_id" type="radio" id="complexity_id" value="3">Complex
        </div>
        <div class="mb-3">
            <label class="form-label" for="PriorityInput">Priority</label>
            <input class="form-control"  name="priority" type="text" id="PriorityInput">
        </div>
        <div class="mb-3" style="border: 2px">
            <label class="form-label" for="status">Status</label>
            <input name="status" type="radio" id="status" value="1" >Pending
            <input name="status" type="radio" id="status" value="2" >Planned
            <input name="status" type="radio" id="status" value="3">Evaluated
            <input name="status" type="radio" id="status" value="4">Rejected
        </div>
        <input type="submit" name="Done" class="btn btn-success">
    </form>
</t:wrapper>
