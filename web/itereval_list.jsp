<%-- 
    Document   : itereval_list
    Created on : Jul 20, 2022, 1:46:42 PM
    Author     : admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <t:wrapper title='Issue List'>
            <a class="btn-primary btn" href="additereval"><i class="bi bi-plus-circle-fill"></i></a>
            <table id="table" data-toggle="table" data-search="true" data-pagination="true" data-filter-control="true" data-show-search-clear-button="true" data-resizable="true"  data-page-list="[10,25,50,100,all]">
                <thead>
                    <tr>
                        <th>Student name</th>
                        <th data-sortable="true" data-field="iteration_name" data-filter-control="select"> Iteration Name</th>
                        <th>Grade</th>
                        <th>Bonus</th>
                        <th>Detail</th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach items="${requestScope.iterevallist}" var="ielist">
                        <tr>
                            <td>${ielist.email.account.displayname}</td>
                            <td>${ielist.iteration.iteration_name}</td>
                            <td>${ielist.grade}</td>
                            <td>${ielist.bonus}</td>
                            <td><c:if test="${sessionScope.account.role_id == 4}">
                                    <a class="btn btn-primary" href="iterevadetail?evaluation_id=${ielist.evaluation_id}">
                                        <i class="bi bi-hand-index-thumb"></i>
                                    </c:if><c:if test="${sessionScope.account.role_id == 1}">
                                        <a href="iterevadetail?evaluation_id=${ielist.evaluation_id}">
                                            <i class="bi bi-hand-index-thumb"></i>
                                        </a>
                                    </c:if>

                            </td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>

        </t:wrapper>
    </body>
</html>
