<%-- 
    Document   : changePassword
    Created on : Jun 5, 2022, 3:02:35 PM
    Author     : admin
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:wrapper title='Change Password'>
    <h1>
        Change password form
    </h1>
    <form action="changepassword" class="w-50" method="POST">
        <div class="mb-3">
            <label for="PwInput" class="form-label">New Password</label>
            <input id="newpass" type="password" class="form-control" name="newpass" pattern=".{8,20}" onkeyup='check();' title="8 to 20 characters"  required><br/>
        </div>
        <div class="mb-3">
            <label for="repPwInput" class="form-label">Confirm Password</label>
            <input id="confirm" type="password" class="form-control" name="confirm" pattern=".{8,20}" onkeyup='check();' title="8 to 20 characters" required><br/>
        </div>
        <div class="alert alert-danger" id="message">
        </div>
        <input class="btn btn-success" type="submit" value="DONE">
    </form>
    <script>
        var check = function () {
            if (document.getElementById('newpass').value ==
                    document.getElementById('confirm').value) {
                document.getElementById('message').style.color = 'green';
                document.getElementById('message').innerHTML = 'matching';
            } else {
                document.getElementById('message').style.color = 'red';
                document.getElementById('message').innerHTML = 'not matching';
            }
        }
    </script>
</t:wrapper>
