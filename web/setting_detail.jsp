<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:wrapper title='Setting'>
    <h1>
        SETTING DETAILS
    </h1>
    <form action="settingdetail" class="w-50" method="POST">
        <div class="mb-3">
            <input name="setting_id" class="form-control" value="${requestScope.setting.setting_id}" hidden>
        </div> 
        <div class="row">
            <div class="col">
                <div class="mb-3">
                    <label class="form-label" for="TitleInput">Setting title</label>
                    <input type="text" name="setting_title" class="form-control" value="${requestScope.setting.setting_title}" >
                </div>   
            </div>
            <div class="col">
                <div class="mb-3">
                    <label class="form-label" for="valueInput">Setting value</label>
                    <input type="text" name="setting_value" class="form-control" value="${requestScope.setting.setting_value}" >
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <div class="mb-3">
                    <label class="form-label" for="OrderInput">Display Order</label>
                    <input type="text" name="display_order" class="form-control" value="${requestScope.setting.display_order}" >
                </div>   
            </div>
            <div class="col">
                <div class="mb-3">
                    <label class="form-label" for="TypeInput">Type</label>
                    <input type="text" name="type_id" class="form-control" value="${requestScope.setting.type_id}" >
                </div>   
            </div>
        </div>
        <div class="mb-3">
            <label class="form-label" for="status">Status</label>
            <c:if test="${requestScope.setting.status == 1}">
                <input name="status" type="radio" id="status" value="1" checked="checked">Inactive
                <input name="status" type="radio" id="status" value="2">Active
            </c:if>
            <c:if test="${requestScope.setting.status == 2}">
                <input name="status" type="radio" id="status" value="1">Inactive
                <input name="status" type="radio" id="status" value="2" checked="checked">Active
            </c:if>
        </div>   
        <div>
            <input type="submit" value="DONE" class="btn btn-success">
            <a href="settinglist" class="btn btn-success"><i class="bi bi-box-arrow-left"></i></a>
            <a class="btn btn-success" style="color: white" href="index"><i class="bi bi-house"></i></a>
        </div>
    </form>  

</t:wrapper>