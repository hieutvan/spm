<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:wrapper title="Home">
    <form action="login" class='w-50' method="POST">
        <h1>
            LOGIN FORM
        </h1>
        <div class="mb-3">
            <label for="usernameInput" class="form-label">Email</label>
            <input class="form-control" name="email" id="usernameInput" autocomplete="on" required>
        </div>

        <div class="mb-3">
            <label for="pwInput" class="form-label">Password</label>
            <input type="password" name="password" class="form-control" id="pwInput" required>
        </div>

        <div class='mb-3'>
            <input type="submit" class='btn btn-success' value="Login">

            <a class="btn btn-primary" href="register" style="color: white">Don't have an account?</a>
            <a class="btn btn-primary" href="resetpassword" style="color: white">Forget password?</a>
        </div>


    </form>
</t:wrapper>