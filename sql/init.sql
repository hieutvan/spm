-- MariaDB dump 10.19  Distrib 10.6.8-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: spm_project
-- ------------------------------------------------------
-- Server version	8.0.29

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `account`
--

DROP TABLE IF EXISTS `account`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `account` (
  `username` varchar(150) CHARACTER SET utf8mb3 COLLATE utf8_bin NOT NULL,
  `password` varchar(150) CHARACTER SET utf8mb3 COLLATE utf8_bin NOT NULL,
  `displayname` varchar(150) CHARACTER SET utf8mb3 COLLATE utf8_bin NOT NULL,
  `email` varchar(45) CHARACTER SET utf8mb3 COLLATE utf8_bin NOT NULL,
  `phonenumber` varchar(45) CHARACTER SET utf8mb3 COLLATE utf8_bin DEFAULT NULL,
  `role_id` int DEFAULT NULL,
  `status` int DEFAULT NULL,
  PRIMARY KEY (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `account`
--

LOCK TABLES `account` WRITE;
/*!40000 ALTER TABLE `account` DISABLE KEYS */;
INSERT INTO `account` VALUES ('CuongND','64cf112dac08eea4d7af7bf48ec701ce','Nguyen Dinh Cuong','CuongND@gmail.com','1221',4,2),('MinhNH','004fd6f81a4c659b400b5af265a29335','Nguyen Hoang Minh','MinhNH@gmail.com','124',1,1),('QuanNA','8a6e5b51e2493d93bfac9d777a9c4fe9','Nguyen Anh Quan','QuanNA@gmail.com','122',3,1),('ThuNT','fa338187c9225a04ef3e78f67daa211c','Nguyen Ha Thu','ThuNT@gmail.com','1442',3,1),('TungDQ','2c95a44f8755c2281739b956db750cdc','Dinh Quoc Tung','TungDQ@gmail.com','53',3,1),('VuongNV','353f5ad2bcee2204543d524bf33f69f8','Nguyen Van Vuong','VuongNV@gmail.com','121',3,1),('admin1','e00cf25ad42683b3df678c61f42c6bda','admin1','ad1@gmail.com','121',2,2),('admin2','c84258e9c39059a89ab77d846ddab909','admin2','ad2@gmail.com','1331',2,2),('KhanhLe','e5cc26d648b73b29b7480c17ea96690e','Le Cao Khanh','caokhanhle145@gmail.com','1354 34422',1,2),('Hieu Van','16d6aeaef65892d7c7cdef1f3b54a74a','Van Trung Hieu','hieuvt@gmail.com','1241',1,2),('Huong Thu','9ea32ce40cf1c50cb93be12f8fd2dc82','Than Thu Huong','huongtt@gmail.com','343',1,2),('KhanhLC','e5cc26d648b73b29b7480c17ea96690e','KhanhLC','khanhlche163867@fpt.edu.vn','1234',1,2),('truonglx','276c113542ba535bbe7f4141b8ed25cf','Luu Xuan Truong','truonglx@gmail.com','1122',3,2);
/*!40000 ALTER TABLE `account` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `class`
--

DROP TABLE IF EXISTS `class`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `class` (
  `class_id` int NOT NULL AUTO_INCREMENT,
  `class_code` varchar(45) CHARACTER SET utf8mb3 COLLATE utf8_bin NOT NULL,
  `email` varchar(45) CHARACTER SET utf8mb3 COLLATE utf8_bin DEFAULT NULL,
  `subject_id` int NOT NULL,
  `class_year` int NOT NULL,
  `class_term` varchar(45) CHARACTER SET utf8mb3 COLLATE utf8_bin NOT NULL,
  `block5_class` int NOT NULL,
  `status` tinyint NOT NULL,
  PRIMARY KEY (`class_id`),
  KEY `email` (`email`),
  KEY `subject_id` (`subject_id`),
  CONSTRAINT `class_ibfk_1` FOREIGN KEY (`email`) REFERENCES `account` (`email`),
  CONSTRAINT `class_ibfk_2` FOREIGN KEY (`subject_id`) REFERENCES `subject` (`subject_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb3 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `class`
--

LOCK TABLES `class` WRITE;
/*!40000 ALTER TABLE `class` DISABLE KEYS */;
INSERT INTO `class` VALUES (1,'SE1601','CuongND@gmail.com',2,2021,'Spring',2,1),(2,'SE1602','CuongND@gmail.com',2,2022,'Summer',2,2),(3,'SE1603','CuongND@gmail.com',3,2021,'Fall',2,2),(4,'Se1605','CuongND@gmail.com',8,2022,'Spring',1,0),(5,'SE1609','CuongND@gmail.com',3,2024,'Summer',1,2);
/*!40000 ALTER TABLE `class` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `class_user`
--

DROP TABLE IF EXISTS `class_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `class_user` (
  `class_id` int NOT NULL,
  `team_id` int NOT NULL,
  `email` varchar(45) CHARACTER SET utf8mb3 COLLATE utf8_bin NOT NULL,
  `team_leader` tinyint NOT NULL,
  `dropout_date` date DEFAULT NULL,
  `user_notes` varchar(45) CHARACTER SET utf8mb3 COLLATE utf8_bin DEFAULT NULL,
  `ongoing_eval` varchar(45) CHARACTER SET utf8mb3 COLLATE utf8_bin DEFAULT NULL,
  `final_pres_eval` varchar(45) CHARACTER SET utf8mb3 COLLATE utf8_bin DEFAULT NULL,
  `final_topic_eval` varchar(45) CHARACTER SET utf8mb3 COLLATE utf8_bin DEFAULT NULL,
  `status` tinyint NOT NULL,
  KEY `class_id` (`class_id`),
  KEY `team_id` (`team_id`),
  KEY `email` (`email`),
  CONSTRAINT `class_user_ibfk_1` FOREIGN KEY (`class_id`) REFERENCES `class` (`class_id`),
  CONSTRAINT `class_user_ibfk_2` FOREIGN KEY (`team_id`) REFERENCES `team` (`team_id`),
  CONSTRAINT `class_user_ibfk_3` FOREIGN KEY (`email`) REFERENCES `account` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `class_user`
--

LOCK TABLES `class_user` WRITE;
/*!40000 ALTER TABLE `class_user` DISABLE KEYS */;
INSERT INTO `class_user` VALUES (1,1,'khanhlche163867@fpt.edu.vn',2,'2022-11-11','Aca','720','720','720',1),(1,2,'caokhanhle145@gmail.com',2,'2022-11-11','ACC','720','720','720',1),(2,3,'MinhNH@gmail.com',1,'2022-11-11','ACC','720','720','720',1),(1,1,'hieuvt@gmail.com',1,'2022-11-11','ACC','720','720','720',1);
/*!40000 ALTER TABLE `class_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `evaluation_criteria`
--

DROP TABLE IF EXISTS `evaluation_criteria`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `evaluation_criteria` (
  `criteria_id` int NOT NULL AUTO_INCREMENT,
  `iteration_id` int NOT NULL,
  `evaluation_weight` int NOT NULL,
  `team_evaluation` tinyint NOT NULL,
  `criteria_order` int NOT NULL,
  `max_loc` int NOT NULL,
  `status` tinyint NOT NULL,
  PRIMARY KEY (`criteria_id`),
  KEY `iteration_id` (`iteration_id`),
  CONSTRAINT `evaluation_criteria_ibfk_1` FOREIGN KEY (`iteration_id`) REFERENCES `iteration` (`iteration_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb3 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `evaluation_criteria`
--

LOCK TABLES `evaluation_criteria` WRITE;
/*!40000 ALTER TABLE `evaluation_criteria` DISABLE KEYS */;
INSERT INTO `evaluation_criteria` VALUES (2,4,10,1,2,180,1),(3,4,10,2,1,200,2),(4,7,10,1,2,110,1),(5,6,10,2,3,125,2),(6,10,10,2,3,350,1),(7,11,10,2,4,123,2),(8,4,10,1,2,200,1);
/*!40000 ALTER TABLE `evaluation_criteria` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `feature`
--

DROP TABLE IF EXISTS `feature`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `feature` (
  `feature_id` int NOT NULL AUTO_INCREMENT,
  `team_id` int NOT NULL,
  `feature_name` varchar(45) CHARACTER SET utf8mb3 COLLATE utf8_bin NOT NULL,
  `status` int NOT NULL,
  PRIMARY KEY (`feature_id`),
  KEY `team_id` (`team_id`),
  CONSTRAINT `feature_ibfk_1` FOREIGN KEY (`team_id`) REFERENCES `team` (`team_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `feature`
--

LOCK TABLES `feature` WRITE;
/*!40000 ALTER TABLE `feature` DISABLE KEYS */;
/*!40000 ALTER TABLE `feature` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `features`
--

DROP TABLE IF EXISTS `features`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `features` (
  `feature_id` int NOT NULL AUTO_INCREMENT,
  `team_id` int NOT NULL,
  `feature_name` varchar(45) CHARACTER SET utf8mb3 COLLATE utf8_bin NOT NULL,
  `status` int NOT NULL,
  PRIMARY KEY (`feature_id`),
  KEY `team_id` (`team_id`),
  CONSTRAINT `features_ibfk_1` FOREIGN KEY (`team_id`) REFERENCES `team` (`team_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb3 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `features`
--

LOCK TABLES `features` WRITE;
/*!40000 ALTER TABLE `features` DISABLE KEYS */;
INSERT INTO `features` VALUES (1,1,'Display data',2),(2,1,'Insert data',1),(3,1,'Update data',1),(4,1,'Delete data',1),(5,2,'Display data',2),(6,2,'Insert data',1),(7,1,'Auto Commit',2);
/*!40000 ALTER TABLE `features` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `function`
--

DROP TABLE IF EXISTS `function`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `function` (
  `function_id` int NOT NULL AUTO_INCREMENT,
  `team_id` int NOT NULL,
  `function_name` varchar(45) CHARACTER SET utf8mb3 COLLATE utf8_bin NOT NULL,
  `feature_id` int NOT NULL,
  `access_roles` varchar(45) CHARACTER SET utf8mb3 COLLATE utf8_bin NOT NULL,
  `description` varchar(45) CHARACTER SET utf8mb3 COLLATE utf8_bin NOT NULL,
  `complexity_id` int NOT NULL,
  `email` varchar(45) CHARACTER SET utf8mb3 COLLATE utf8_bin NOT NULL,
  `priority` int NOT NULL,
  `status` tinyint NOT NULL,
  PRIMARY KEY (`function_id`),
  KEY `team_id` (`team_id`),
  KEY `feature_id` (`feature_id`),
  KEY `email` (`email`),
  CONSTRAINT `function_ibfk_1` FOREIGN KEY (`team_id`) REFERENCES `team` (`team_id`),
  CONSTRAINT `function_ibfk_2` FOREIGN KEY (`feature_id`) REFERENCES `features` (`feature_id`),
  CONSTRAINT `function_ibfk_3` FOREIGN KEY (`email`) REFERENCES `account` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb3 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `function`
--

LOCK TABLES `function` WRITE;
/*!40000 ALTER TABLE `function` DISABLE KEYS */;
INSERT INTO `function` VALUES (1,1,'Login',1,'1','Login with email and password',1,'khanhlche163867@fpt.edu.vn',1,2),(2,1,'Logout',1,'1','Logout of session ',1,'hieuvt@gmail.com',1,1),(3,2,'Login',1,'1','Login with email and password',1,'caokhanhle145@gmail.com',1,4),(4,1,'Add',2,'2','Add data to database',3,'khanhlche163867@fpt.edu.vn',1,2);
/*!40000 ALTER TABLE `function` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `issue`
--

DROP TABLE IF EXISTS `issue`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `issue` (
  `issue_id` int NOT NULL AUTO_INCREMENT,
  `assignee_email` varchar(45) CHARACTER SET utf8mb3 COLLATE utf8_bin NOT NULL,
  `issue_title` varchar(45) CHARACTER SET utf8mb3 COLLATE utf8_bin NOT NULL,
  `description` varchar(45) CHARACTER SET utf8mb3 COLLATE utf8_bin DEFAULT NULL,
  `gitlab_id` int NOT NULL,
  `gitlab_url` varchar(45) CHARACTER SET utf8mb3 COLLATE utf8_bin NOT NULL,
  `created_at` date NOT NULL,
  `due_date` date DEFAULT NULL,
  `team_id` int NOT NULL,
  `milestone_id` int DEFAULT NULL,
  `function_id` int DEFAULT NULL,
  `labels` varchar(45) CHARACTER SET utf8mb3 COLLATE utf8_bin NOT NULL,
  `issue_type` varchar(45) CHARACTER SET utf8mb3 COLLATE utf8_bin DEFAULT NULL,
  `status` tinyint NOT NULL,
  PRIMARY KEY (`issue_id`),
  KEY `assignee_email` (`assignee_email`),
  KEY `team_id` (`team_id`),
  KEY `issue_ibfk_3` (`milestone_id`),
  KEY `issue_ibfk_4` (`function_id`),
  CONSTRAINT `issue_ibfk_1` FOREIGN KEY (`assignee_email`) REFERENCES `account` (`email`),
  CONSTRAINT `issue_ibfk_2` FOREIGN KEY (`team_id`) REFERENCES `team` (`team_id`),
  CONSTRAINT `issue_ibfk_3` FOREIGN KEY (`milestone_id`) REFERENCES `milestone` (`milestone_id`),
  CONSTRAINT `issue_ibfk_4` FOREIGN KEY (`function_id`) REFERENCES `function` (`function_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb3 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `issue`
--

LOCK TABLES `issue` WRITE;
/*!40000 ALTER TABLE `issue` DISABLE KEYS */;
INSERT INTO `issue` VALUES (1,'khanhlche163867@fpt.edu.vn','1','Password encoding',1,'asc','2022-07-09','2022-07-09',1,1,1,'acs','WP',1),(2,'hieuvt@gmail.com','2','Add new functions',3,'asc','2022-07-09','2022-07-09',1,1,1,'acs','Q&A',2),(3,'caokhanhle145@gmail.com','1','Password decoding',3,'asc','2022-07-09','2022-07-09',2,1,1,'asc','Task',1),(4,'khanhlche163867@fpt.edu.vn','Update',NULL,1,'a','2022-07-10','2022-07-30',1,1,1,'Update','Defect',1),(5,'khanhlche163867@fpt.edu.vn','Update',NULL,1,'BBBB','2022-07-11','2022-07-28',1,1,1,'Update','Leakage',1),(6,'hieuvt@gmail.com','database design',NULL,2,'https://gitlab.com/hieutvan/spm/-/issues/2','2022-07-12','2022-07-15',1,1,1,'New','WP',1);
/*!40000 ALTER TABLE `issue` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `iteration`
--

DROP TABLE IF EXISTS `iteration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `iteration` (
  `iteration_id` int NOT NULL AUTO_INCREMENT,
  `subject_id` int NOT NULL,
  `iteration_name` varchar(45) CHARACTER SET utf8mb3 COLLATE utf8_bin NOT NULL,
  `duration` int NOT NULL,
  `status` tinyint DEFAULT NULL,
  PRIMARY KEY (`iteration_id`),
  KEY `subject_id` (`subject_id`),
  CONSTRAINT `iteration_ibfk_1` FOREIGN KEY (`subject_id`) REFERENCES `subject` (`subject_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb3 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `iteration`
--

LOCK TABLES `iteration` WRITE;
/*!40000 ALTER TABLE `iteration` DISABLE KEYS */;
INSERT INTO `iteration` VALUES (4,2,'Iter11',121,1),(5,3,'Iter2',12,2),(6,2,'Iteration 3',14,1),(7,11,'Iteration 1',12,2),(8,2,'Iteration 44',12,2),(9,2,'Iteration 5',12,2),(10,2,'iteration 6',12,1),(11,14,'iteration1',12,1);
/*!40000 ALTER TABLE `iteration` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `iteration_evaluation`
--

DROP TABLE IF EXISTS `iteration_evaluation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `iteration_evaluation` (
  `evaluation_id` int NOT NULL AUTO_INCREMENT,
  `iteration_id` int NOT NULL,
  `class_id` int NOT NULL,
  `team_id` int NOT NULL,
  `email` varchar(45) CHARACTER SET utf8mb3 COLLATE utf8_bin NOT NULL,
  `bonus` float DEFAULT NULL,
  `grade` float DEFAULT NULL,
  `note` varchar(45) CHARACTER SET utf8mb3 COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`evaluation_id`),
  KEY `iteration_id` (`iteration_id`),
  KEY `class_id` (`class_id`),
  KEY `team_id` (`team_id`),
  KEY `email` (`email`),
  CONSTRAINT `iteration_evaluation_ibfk_1` FOREIGN KEY (`iteration_id`) REFERENCES `iteration` (`iteration_id`),
  CONSTRAINT `iteration_evaluation_ibfk_2` FOREIGN KEY (`class_id`) REFERENCES `class_user` (`class_id`),
  CONSTRAINT `iteration_evaluation_ibfk_3` FOREIGN KEY (`team_id`) REFERENCES `class_user` (`team_id`),
  CONSTRAINT `iteration_evaluation_ibfk_4` FOREIGN KEY (`email`) REFERENCES `class_user` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8mb3 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `iteration_evaluation`
--

LOCK TABLES `iteration_evaluation` WRITE;
/*!40000 ALTER TABLE `iteration_evaluation` DISABLE KEYS */;
INSERT INTO `iteration_evaluation` VALUES (1,4,1,1,'khanhlche163867@fpt.edu.vn',8,1.1,'NOa'),(9,4,1,1,'hieuvt@gmail.com',1,1,'No'),(35,4,1,1,'khanhlche163867@fpt.edu.vn',0,0,'aaaa'),(36,4,1,1,'khanhlche163867@fpt.edu.vn',0,0,'aaa'),(37,4,1,1,'khanhlche163867@fpt.edu.vn',8,1,'AAAA'),(38,4,1,1,'hieuvt@gmail.com',0,0,''),(39,4,1,1,'hieuvt@gmail.com',0,0,'wtfggggg');
/*!40000 ALTER TABLE `iteration_evaluation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `loc_evaluation`
--

DROP TABLE IF EXISTS `loc_evaluation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `loc_evaluation` (
  `evaluation_id` int NOT NULL AUTO_INCREMENT,
  `evaluation_time` date NOT NULL,
  `evaluation_note` varchar(45) CHARACTER SET utf8mb3 COLLATE utf8_bin NOT NULL,
  `complexity_id` int DEFAULT NULL,
  `quality_id` int NOT NULL,
  `LOC` int NOT NULL,
  `tracking_id` int NOT NULL,
  PRIMARY KEY (`evaluation_id`),
  KEY `tracking_id` (`tracking_id`),
  CONSTRAINT `loc_evaluation_ibfk_1` FOREIGN KEY (`tracking_id`) REFERENCES `tracking` (`tracking_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb3 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `loc_evaluation`
--

LOCK TABLES `loc_evaluation` WRITE;
/*!40000 ALTER TABLE `loc_evaluation` DISABLE KEYS */;
INSERT INTO `loc_evaluation` VALUES (1,'2022-07-21','Noaa',1,1,60,1),(2,'2022-07-20','aa',1,1,60,2),(3,'2022-07-21','ascas',1,4,0,3),(5,'2022-07-21','C/M',3,2,180,4),(6,'2022-07-21','AAAAA',3,3,120,4);
/*!40000 ALTER TABLE `loc_evaluation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `member_evaluation`
--

DROP TABLE IF EXISTS `member_evaluation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `member_evaluation` (
  `member_eval_id` int NOT NULL AUTO_INCREMENT,
  `evaluation_id` int NOT NULL,
  `criteria_id` int NOT NULL,
  `converted_loc` int NOT NULL,
  `grade` float NOT NULL,
  `note` varchar(45) CHARACTER SET utf8mb3 COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`member_eval_id`),
  KEY `evaluation_id` (`evaluation_id`),
  KEY `criteria_id` (`criteria_id`),
  CONSTRAINT `member_evaluation_ibfk_1` FOREIGN KEY (`evaluation_id`) REFERENCES `iteration_evaluation` (`evaluation_id`),
  CONSTRAINT `member_evaluation_ibfk_2` FOREIGN KEY (`criteria_id`) REFERENCES `evaluation_criteria` (`criteria_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb3 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `member_evaluation`
--

LOCK TABLES `member_evaluation` WRITE;
/*!40000 ALTER TABLE `member_evaluation` DISABLE KEYS */;
INSERT INTO `member_evaluation` VALUES (1,1,2,60,6,'No'),(2,38,3,3,2,''),(3,39,3,3,5,'wtfggggg');
/*!40000 ALTER TABLE `member_evaluation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `milestone`
--

DROP TABLE IF EXISTS `milestone`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `milestone` (
  `milestone_id` int NOT NULL AUTO_INCREMENT,
  `iteration_id` int NOT NULL,
  `class_id` int NOT NULL,
  `from_date` date NOT NULL,
  `to_date` date NOT NULL,
  `status` int NOT NULL,
  PRIMARY KEY (`milestone_id`),
  KEY `iteration_id` (`iteration_id`),
  KEY `class_id` (`class_id`),
  CONSTRAINT `milestone_ibfk_1` FOREIGN KEY (`iteration_id`) REFERENCES `iteration` (`iteration_id`),
  CONSTRAINT `milestone_ibfk_2` FOREIGN KEY (`class_id`) REFERENCES `class` (`class_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb3 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `milestone`
--

LOCK TABLES `milestone` WRITE;
/*!40000 ALTER TABLE `milestone` DISABLE KEYS */;
INSERT INTO `milestone` VALUES (1,4,1,'2022-07-26','2022-06-30',1),(2,6,2,'2022-06-25','2022-07-21',2),(3,5,1,'2022-06-25','2022-07-21',1),(4,4,1,'2022-06-22','2022-07-21',1),(5,4,1,'2022-06-22','2022-10-29',2);
/*!40000 ALTER TABLE `milestone` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `setting`
--

DROP TABLE IF EXISTS `setting`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `setting` (
  `setting_id` int NOT NULL AUTO_INCREMENT,
  `setting_title` varchar(150) CHARACTER SET utf8mb3 COLLATE utf8_bin NOT NULL,
  `type_id` int NOT NULL,
  `display_order` int NOT NULL,
  `setting_value` varchar(45) CHARACTER SET utf8mb3 COLLATE utf8_bin NOT NULL,
  `status` tinyint NOT NULL,
  PRIMARY KEY (`setting_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb3 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `setting`
--

LOCK TABLES `setting` WRITE;
/*!40000 ALTER TABLE `setting` DISABLE KEYS */;
INSERT INTO `setting` VALUES (1,'New a',1,1,'ABCS',1),(2,'New a2',3,2,'ABSSS',2),(3,'a',1,2,'210',1);
/*!40000 ALTER TABLE `setting` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `subject`
--

DROP TABLE IF EXISTS `subject`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subject` (
  `subject_id` int NOT NULL AUTO_INCREMENT,
  `subject_code` varchar(45) CHARACTER SET utf8mb3 COLLATE utf8_bin NOT NULL,
  `subject_name` varchar(45) CHARACTER SET utf8mb3 COLLATE utf8_bin NOT NULL,
  `email` varchar(45) CHARACTER SET utf8mb3 COLLATE utf8_bin NOT NULL,
  `status` tinyint NOT NULL,
  PRIMARY KEY (`subject_id`),
  KEY `email` (`email`),
  CONSTRAINT `subject_ibfk_1` FOREIGN KEY (`email`) REFERENCES `account` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb3 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `subject`
--

LOCK TABLES `subject` WRITE;
/*!40000 ALTER TABLE `subject` DISABLE KEYS */;
INSERT INTO `subject` VALUES (2,'PRJ301','JAVA WEB APPLICATION','ThuNT@gmail.com',2),(3,'PRO201','OOP PROGRAM','ThuNT@gmail.com',1),(6,'ISP392','IS PROJECT','TungDQ@gmail.com',1),(8,'CSD201','DATA ALGORITHM','truonglx@gmail.com',1),(9,'DBI202','DATABASE','truonglx@gmail.com',1),(10,'ITA301','SYSTEM DESIGN','truonglx@gmail.com',2),(11,'ISC302','E-COMMERCE','TungDQ@gmail.com',2),(12,'PRC201C','CLOUD COMPUTING','ThuNT@gmail.com',2),(14,'WED201C','WEB DESIGN ','truonglx@gmail.com',2);
/*!40000 ALTER TABLE `subject` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `subject_setting`
--

DROP TABLE IF EXISTS `subject_setting`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subject_setting` (
  `setting_id` int NOT NULL AUTO_INCREMENT,
  `subject_id` int NOT NULL,
  `type_id` int NOT NULL,
  `setting_title` varchar(45) CHARACTER SET utf8mb3 COLLATE utf8_bin NOT NULL,
  `setting_value` varchar(45) CHARACTER SET utf8mb3 COLLATE utf8_bin NOT NULL,
  `display_order` int NOT NULL,
  `status` tinyint NOT NULL,
  PRIMARY KEY (`setting_id`),
  KEY `subject_id` (`subject_id`),
  CONSTRAINT `subject_setting_ibfk_1` FOREIGN KEY (`subject_id`) REFERENCES `subject` (`subject_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb3 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `subject_setting`
--

LOCK TABLES `subject_setting` WRITE;
/*!40000 ALTER TABLE `subject_setting` DISABLE KEYS */;
INSERT INTO `subject_setting` VALUES (1,2,1,'High','100',1,1),(2,2,2,'Medium','75',1,2),(3,2,3,'Low','50',1,1),(4,2,4,'Zero','0',1,2),(5,3,1,'Complex','240',2,1),(6,6,1,'Complex','240',1,2);
/*!40000 ALTER TABLE `subject_setting` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `team`
--

DROP TABLE IF EXISTS `team`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `team` (
  `team_id` int NOT NULL AUTO_INCREMENT,
  `class_id` int NOT NULL,
  `topic_code` varchar(45) CHARACTER SET utf8mb3 COLLATE utf8_bin NOT NULL,
  `topic_name` varchar(45) CHARACTER SET utf8mb3 COLLATE utf8_bin NOT NULL,
  `gitlab_url` varchar(45) CHARACTER SET utf8mb3 COLLATE utf8_bin NOT NULL,
  `status` tinyint NOT NULL,
  `team_code` varchar(45) CHARACTER SET utf8mb3 COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`team_id`),
  KEY `class_id` (`class_id`),
  CONSTRAINT `team_ibfk_1` FOREIGN KEY (`class_id`) REFERENCES `class` (`class_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb3 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `team`
--

LOCK TABLES `team` WRITE;
/*!40000 ALTER TABLE `team` DISABLE KEYS */;
INSERT INTO `team` VALUES (1,1,'A','AAAQA','https://github.com/highhigh234/PRJPROJECT',2,'A'),(2,1,'B','B','BBBBB',1,'B'),(3,2,'A','AAAA','https://github.com/highhigh234/CSD201',2,'AB');
/*!40000 ALTER TABLE `team` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `team_evaluation`
--

DROP TABLE IF EXISTS `team_evaluation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `team_evaluation` (
  `team_eval_id` int NOT NULL AUTO_INCREMENT,
  `evaluation_id` int NOT NULL,
  `criteria_id` int NOT NULL,
  `team_id` int NOT NULL,
  `grade` float DEFAULT NULL,
  `note` varchar(45) CHARACTER SET utf8mb3 COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`team_eval_id`),
  KEY `criteria_id` (`criteria_id`),
  KEY `team_id` (`team_id`),
  KEY `evaluation_id` (`evaluation_id`),
  CONSTRAINT `team_evaluation_ibfk_2` FOREIGN KEY (`criteria_id`) REFERENCES `evaluation_criteria` (`criteria_id`),
  CONSTRAINT `team_evaluation_ibfk_3` FOREIGN KEY (`team_id`) REFERENCES `team` (`team_id`),
  CONSTRAINT `team_evaluation_ibfk_4` FOREIGN KEY (`evaluation_id`) REFERENCES `iteration_evaluation` (`evaluation_id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb3 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `team_evaluation`
--

LOCK TABLES `team_evaluation` WRITE;
/*!40000 ALTER TABLE `team_evaluation` DISABLE KEYS */;
INSERT INTO `team_evaluation` VALUES (13,38,3,1,0,''),(14,39,3,1,0,'wtfggggg');
/*!40000 ALTER TABLE `team_evaluation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tracking`
--

DROP TABLE IF EXISTS `tracking`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tracking` (
  `tracking_id` int NOT NULL AUTO_INCREMENT,
  `team_id` int NOT NULL,
  `milestone_id` int NOT NULL,
  `function_id` int NOT NULL,
  `assigner_email` varchar(45) CHARACTER SET utf8mb3 COLLATE utf8_bin NOT NULL,
  `assignee_email` varchar(45) CHARACTER SET utf8mb3 COLLATE utf8_bin NOT NULL,
  `tracking_note` varchar(45) CHARACTER SET utf8mb3 COLLATE utf8_bin NOT NULL,
  `updates` varchar(45) CHARACTER SET utf8mb3 COLLATE utf8_bin NOT NULL,
  `status` tinyint NOT NULL,
  PRIMARY KEY (`tracking_id`),
  KEY `team_id` (`team_id`),
  KEY `milestone_id` (`milestone_id`),
  KEY `function_id` (`function_id`),
  CONSTRAINT `tracking_ibfk_1` FOREIGN KEY (`team_id`) REFERENCES `team` (`team_id`),
  CONSTRAINT `tracking_ibfk_2` FOREIGN KEY (`milestone_id`) REFERENCES `milestone` (`milestone_id`),
  CONSTRAINT `tracking_ibfk_3` FOREIGN KEY (`function_id`) REFERENCES `function` (`function_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb3 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tracking`
--

LOCK TABLES `tracking` WRITE;
/*!40000 ALTER TABLE `tracking` DISABLE KEYS */;
INSERT INTO `tracking` VALUES (1,1,1,1,'CuongND@gmail.com','khanhlche163867@fpt.edu.vn','New update function','Fix login',3),(2,2,1,1,'CuongND@gmail.com','caokhanhle145@gmail.com','Add new','1',6),(3,1,1,1,'khanhlche163867@fpt.edu.vn','hieuvt@gmail.com','Add new','1',4),(4,1,1,4,'CuongND@gmail.com','khanhlche163867@fpt.edu.vn','AAAAA','213',3);
/*!40000 ALTER TABLE `tracking` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-07-28  0:35:30
