/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author admin
 */
public class Team {
    private int team_id;
    private String team_code;
    private Classes classes;
    private String topic_code;
    private String topic_name;
    private String gitlab_url;
    private int status;

    public Team() {
    }

    public Team(int team_id,String team_code, Classes classes, String topic_code, String topic_name, String gitlab_url, int status) {
        this.team_id = team_id;
        this.team_code = team_code;
        this.classes = classes;
        this.topic_code = topic_code;
        this.topic_name = topic_name;
        this.gitlab_url = gitlab_url;
        this.status = status;
    }

    public Team(int team_id) {
        this.team_id = team_id;
    }

    public int getTeam_id() {
        return team_id;
    }

    public void setTeam_id(int team_id) {
        this.team_id = team_id;
    }

    public String getTeam_code() {
        return team_code;
    }

    public void setTeam_code(String team_code) {
        this.team_code = team_code;
    }

    public Classes getClasses() {
        return classes;
    }

    public void setClasses(Classes classes) {
        this.classes = classes;
    }

    public String getTopic_code() {
        return topic_code;
    }

    public void setTopic_code(String topic_code) {
        this.topic_code = topic_code;
    }

    public String getTopic_name() {
        return topic_name;
    }

    public void setTopic_name(String topic_name) {
        this.topic_name = topic_name;
    }

    public String getGitlab_url() {
        return gitlab_url;
    }

    public void setGitlab_url(String gitlab_url) {
        this.gitlab_url = gitlab_url;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Team{" + "team_id=" + team_id + ", team_code=" + team_code + ", classes=" + classes + ", topic_code=" + topic_code + ", topic_name=" + topic_name + ", gitlab_url=" + gitlab_url + ", status=" + status + '}';
    }


    
    
}
