/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.sql.Date;
import java.sql.Timestamp;


/**
 *
 * @author admin
 */
public class LOC_Evaluation {
    private int evaluation_id;
    private Date evaluation_time;
    private String evaluation_note;
    private int complexity_id;
    private int quality_id;
    private int LOC;
    private Tracking tracking;

    public LOC_Evaluation() {
    }

    public LOC_Evaluation(int evaluation_id, Date evaluation_time, String evaluation_note, int complexity_id, int quality_id, int LOC, Tracking tracking) {
        this.evaluation_id = evaluation_id;
        this.evaluation_time = evaluation_time;
        this.evaluation_note = evaluation_note;
        this.complexity_id = complexity_id;
        this.quality_id = quality_id;
        this.LOC = LOC;
        this.tracking = tracking;
    }

    

    public int getEvaluation_id() {
        return evaluation_id;
    }

    public void setEvaluation_id(int evaluation_id) {
        this.evaluation_id = evaluation_id;
    }

    public Date getEvaluation_time() {
        return evaluation_time;
    }

    public void setEvaluation_time(Date evaluation_time) {
        this.evaluation_time = evaluation_time;
    }

    public String getEvaluation_note() {
        return evaluation_note;
    }

    public void setEvaluation_note(String evaluation_note) {
        this.evaluation_note = evaluation_note;
    }

    public int getComplexity_id() {
        return complexity_id;
    }

    public void setComplexity_id(int complexity_id) {
        this.complexity_id = complexity_id;
    }

    public int getQuality_id() {
        return quality_id;
    }

    public void setQuality_id(int quality_id) {
        this.quality_id = quality_id;
    }

    public Tracking getTracking() {
        return tracking;
    }

    public void setTracking(Tracking tracking) {
        this.tracking = tracking;
    }

    public int getLOC() {
        return LOC;
    }

    public void setLOC(int LOC) {
        this.LOC = LOC;
    }

    @Override
    public String toString() {
        return "LOC_Evaluation{" + "evaluation_id=" + evaluation_id + ", evaluation_time=" + evaluation_time + ", evaluation_note=" + evaluation_note + ", complexity_id=" + complexity_id + ", quality_id=" + quality_id + ", LOC=" + LOC + ", tracking=" + tracking + '}';
    }

    
}
