/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author admin
 */
public class Tracking {
    private int tracking_id;
    private Team team;
    private Milestone milestone;
    private Function function;
    private Account assigner_email;
    private Account assignee_email;
    private String updates;
    private String tracking_note;
    private int status;

    public Tracking() {
    }

    public Tracking(int tracking_id, Team team, Milestone milestone, Function function, Account assigner_email, Account assignee_email, String tracking_note, String updates, int status) {
        this.tracking_id = tracking_id;
        this.team = team;
        this.milestone = milestone;
        this.function = function;
        this.assigner_email = assigner_email;
        this.assignee_email = assignee_email;
        this.tracking_note = tracking_note;
        this.updates = updates;
        this.status = status;
    }

    public int getTracking_id() {
        return tracking_id;
    }

    public void setTracking_id(int tracking_id) {
        this.tracking_id = tracking_id;
    }

    public Team getTeam() {
        return team;
    }

    public void setTeam(Team team) {
        this.team = team;
    }

    public Milestone getMilestone() {
        return milestone;
    }

    public void setMilestone(Milestone milestone) {
        this.milestone = milestone;
    }

    public Function getFunction() {
        return function;
    }

    public void setFunction(Function function) {
        this.function = function;
    }

    public Account getAssigner_email() {
        return assigner_email;
    }

    public void setAssigner_email(Account assigner_email) {
        this.assigner_email = assigner_email;
    }

    public Account getAssignee_email() {
        return assignee_email;
    }

    public void setAssignee_email(Account assignee_email) {
        this.assignee_email = assignee_email;
    }

    public String getUpdates() {
        return updates;
    }

    public void setUpdates(String updates) {
        this.updates = updates;
    }

    public String getTracking_note() {
        return tracking_note;
    }

    public void setTracking_note(String tracking_note) {
        this.tracking_note = tracking_note;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Tracking{" + "tracking_id=" + tracking_id + ", team=" + team + ", milestone=" + milestone + ", function=" + function + ", assigner_email=" + assigner_email + ", assignee_email=" + assignee_email + ", updates=" + updates + ", tracking_note=" + tracking_note + ", status=" + status + '}';
    }




}
