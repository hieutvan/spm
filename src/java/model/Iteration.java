/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author admin
 */
public class Iteration {
    private int iteration_id;
    private Subject subject;
    private String iteration_name;
    private int duration;
    private int status;

    public Iteration() {
    }

    public Iteration(String iteration_name) {
        this.iteration_name = iteration_name;
    }

    public Iteration(int iteration_id, Subject subject_id) {
        this.iteration_id = iteration_id;
        this.subject = subject_id;
    }
    
    
    
    public Iteration(int iteration_id, Subject subject_id, String iteration_name, int duration, int status) {
        this.iteration_id = iteration_id;
        this.subject = subject_id;
        this.iteration_name = iteration_name;
        this.duration = duration;
        this.status = status;
    }

    public Iteration(int iteration_id) {
        this.iteration_id = iteration_id;
    }

    public int getIteration_id() {
        return iteration_id;
    }

    public void setIteration_id(int iteration_id) {
        this.iteration_id = iteration_id;
    }

    public Subject getSubject() {
        return subject;
    }

    public void setSubject(Subject subject) {
        this.subject = subject;
    }

    public String getIteration_name() {
        return iteration_name;
    }

    public void setIteration_name(String iteration_name) {
        this.iteration_name = iteration_name;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Iteration{" + "iteration_id=" + iteration_id + ", subject_id=" + subject + ", iteration_name=" + iteration_name + ", duration=" + duration + ", status=" + status + '}';
    }
    
    
}
