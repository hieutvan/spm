/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author admin
 */
public class Feature {
    private int feature_id;
    private Team team;
    private String feature_name;
    private int status;

    public Feature() {
    }

    public Feature(int feature_id, Team team, String feature_name, int status) {
        this.feature_id = feature_id;
        this.team = team;
        this.feature_name = feature_name;
        this.status = status;
    }

    public Feature(int feature_id) {
        this.feature_id = feature_id;
    }

    public int getFeature_id() {
        return feature_id;
    }

    public void setFeature_id(int feature_id) {
        this.feature_id = feature_id;
    }

    public Team getTeam() {
        return team;
    }

    public void setTeam(Team team) {
        this.team = team;
    }

    public String getFeature_name() {
        return feature_name;
    }

    public void setFeature_name(String feature_name) {
        this.feature_name = feature_name;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Feature{" + "feature_id=" + feature_id + ", team_id=" + team + ", feature_name=" + feature_name + ", status=" + status + '}';
    }
    
}
