/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author admin
 */
public class EvaluationCriteria {
    private int criteria_id;
    private Iteration iteration;
    private int evaluation_weight;
    private int team_evaluation;
    private int criteria_order;
    private int max_loc;
    private int status;

    public EvaluationCriteria() {
    }

    public EvaluationCriteria(int criteria_id, Iteration iteration, int evaluation_weight, int team_evaluation, int criteria_order, int max_loc, int status) {
        this.criteria_id = criteria_id;
        this.iteration = iteration;
        this.evaluation_weight = evaluation_weight;
        this.team_evaluation = team_evaluation;
        this.criteria_order = criteria_order;
        this.max_loc = max_loc;
        this.status = status;
    }

    public EvaluationCriteria(int criteria_id) {
        this.criteria_id = criteria_id;
    }

    public int getCriteria_id() {
        return criteria_id;
    }

    public void setCriteria_id(int criteria_id) {
        this.criteria_id = criteria_id;
    }

    public Iteration getIteration() {
        return iteration;
    }

    public void setIteration(Iteration iteration) {
        this.iteration = iteration;
    }

    public int getEvaluation_weight() {
        return evaluation_weight;
    }

    public void setEvaluation_weight(int evaluation_weight) {
        this.evaluation_weight = evaluation_weight;
    }

    public int getTeam_evaluation() {
        return team_evaluation;
    }

    public void setTeam_evaluation(int team_evaluation) {
        this.team_evaluation = team_evaluation;
    }

    public int getCriteria_order() {
        return criteria_order;
    }

    public void setCriteria_order(int criteria_order) {
        this.criteria_order = criteria_order;
    }

    public int getMax_loc() {
        return max_loc;
    }

    public void setMax_loc(int max_loc) {
        this.max_loc = max_loc;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Evaluation_Criteria{" + "criteria_id=" + criteria_id + ", iteration_id=" + iteration + ", evaluation_weight=" + evaluation_weight + ", team_evaluation=" + team_evaluation + ", criteria_order=" + criteria_order + ", max_loc=" + max_loc + ", status=" + status + '}';
    }
            
}
