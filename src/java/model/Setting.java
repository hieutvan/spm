/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author admin
 */
public class Setting {

    private String setting_title;
    private int setting_id;
    private int type_id;
    private int display_order;
    private String setting_value;
    private int status;

    public Setting() {
    }

    public Setting(String setting_title, int setting_id, int type_id, int displayorder, String setting_value, int status) {
        this.setting_title = setting_title;
        this.setting_id = setting_id;
        this.type_id = type_id;
        this.display_order = displayorder;
        this.setting_value = setting_value;
        this.status = status;
    }

    public Setting(String setting_title, int type_id, int display_order, String setting_value, int status) {
        this.setting_title = setting_title;
        this.type_id = type_id;
        this.display_order = display_order;
        this.setting_value = setting_value;
        this.status = status;
    }

    public String getSetting_title() {
        return setting_title;
    }

    public int getSetting_id() {
        return setting_id;
    }

    public void setSetting_title(String setting_title) {
        this.setting_title = setting_title;
    }

    public void setSetting_id(int setting_id) {
        this.setting_id = setting_id;
    }

    public int getType_id() {
        return type_id;
    }

    public void setType_id(int type_id) {
        this.type_id = type_id;
    }

    public int getDisplay_order() {
        return display_order;
    }

    public void setDisplay_order(int display_order) {
        this.display_order = display_order;
    }

    public String getSetting_value() {
        return setting_value;
    }

    public void setSetting_value(String setting_value) {
        this.setting_value = setting_value;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
    
    @Override
    public String toString() {
        return "Setting{" + "setting_title=" + setting_title + ", setting_id=" + setting_id + ", type_id=" + type_id + ", displayorder=" + display_order + ", setting_value=" + setting_value + ", status=" + status + '}';
    }

}
