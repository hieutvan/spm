/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.sql.Date;

/**
 *
 * @author admin
 */
public class Issue {
    private int issue_id;
    private Account account;
    private String issue_title;
    private String description;
    private int gitlab_id;
    private String gitlab_url;
    private Date created_at;
    private Date due_date;
    private Team team;
    private Milestone milestone;
    private Function function;
    private String labels;
    private String issue_type;
    private int status;

    public Issue() {
    }

    public Issue(int issue_id, Account account, String issue_title, String description, int gitlab_id, String gitlab_url, Date created_at, Date due_date, Team team, Milestone milestone, Function function, String labels, String issue_type, int status) {
        this.issue_id = issue_id;
        this.account = account;
        this.issue_title = issue_title;
        this.description = description;
        this.gitlab_id = gitlab_id;
        this.gitlab_url = gitlab_url;
        this.created_at = created_at;
        this.due_date = due_date;
        this.team = team;
        this.milestone = milestone;
        this.function = function;
        this.labels = labels;
        this.issue_type = issue_type;
        this.status = status;
    }

    public int getIssue_id() {
        return issue_id;
    }

    public void setIssue_id(int issue_id) {
        this.issue_id = issue_id;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public String getIssue_title() {
        return issue_title;
    }

    public void setIssue_title(String issue_title) {
        this.issue_title = issue_title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getGitlab_id() {
        return gitlab_id;
    }

    public void setGitlab_id(int gitlab_id) {
        this.gitlab_id = gitlab_id;
    }

    public String getGitlab_url() {
        return gitlab_url;
    }

    public void setGitlab_url(String gitlab_url) {
        this.gitlab_url = gitlab_url;
    }

    public Date getCreated_at() {
        return created_at;
    }

    public void setCreated_at(Date created_at) {
        this.created_at = created_at;
    }

    public Date getDue_date() {
        return due_date;
    }

    public void setDue_date(Date due_date) {
        this.due_date = due_date;
    }

    public Team getTeam() {
        return team;
    }

    public void setTeam(Team team) {
        this.team = team;
    }

    public Milestone getMilestone() {
        return milestone;
    }

    public void setMilestone(Milestone milestone) {
        this.milestone = milestone;
    }

    public Function getFunction() {
        return function;
    }

    public void setFunction(Function function) {
        this.function = function;
    }

    public String getLabels() {
        return labels;
    }

    public void setLabels(String labels) {
        this.labels = labels;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getIssue_type() {
        return issue_type;
    }

    public void setIssue_type(String issue_type) {
        this.issue_type = issue_type;
    }

    @Override
    public String toString() {
        return "Issue{" + "issue_id=" + issue_id + ", account=" + account + ", issue_title=" + issue_title + ", description=" + description + ", gitlab_id=" + gitlab_id + ", gitlab_url=" + gitlab_url + ", created_at=" + created_at + ", due_date=" + due_date + ", team=" + team + ", milestone=" + milestone + ", function=" + function + ", labels=" + labels + ", issue_type=" + issue_type + ", status=" + status + '}';
    }
    
    
    
}
