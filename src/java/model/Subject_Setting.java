/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author admin
 */
public class Subject_Setting {
    private int SSsetting_id;
    private Subject subject;
    private int type_id;
    private String setting_title;
    private String setting_value;
    private int display_order;
    private int status;

    public Subject_Setting() {
    }

    public Subject_Setting(int setting_id, Subject subject, int type_id, String setting_title, String setting_value, int display_order, int status) {
        this.SSsetting_id = setting_id;
        this.subject = subject;
        this.type_id = type_id;
        this.setting_title = setting_title;
        this.setting_value = setting_value;
        this.display_order = display_order;
        this.status = status;
    }

    public int getSSsetting_id() {
        return SSsetting_id;
    }

    public void setSSsetting_id(int SSsetting_id) {
        this.SSsetting_id = SSsetting_id;
    }

    public Subject getSubject() {
        return subject;
    }

    public void setSubject(Subject subject) {
        this.subject = subject;
    }

    public int getType_id() {
        return type_id;
    }

    public void setType_id(int type_id) {
        this.type_id = type_id;
    }

    public String getSetting_title() {
        return setting_title;
    }

    public void setSetting_title(String setting_title) {
        this.setting_title = setting_title;
    }

    public String getSetting_value() {
        return setting_value;
    }

    public void setSetting_value(String setting_value) {
        this.setting_value = setting_value;
    }

    public int getDisplay_order() {
        return display_order;
    }

    public void setDisplay_order(int display_order) {
        this.display_order = display_order;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Subject_Setting{" + "setting_id=" + SSsetting_id + ", subject=" + subject + ", type_id=" + type_id + ", setting_title=" + setting_title + ", setting_value=" + setting_value + ", display_order=" + display_order + ", status=" + status + '}';
    }
    
    
}
