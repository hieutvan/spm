/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.util.ArrayList;

/**
 *
 * @author admin
 */
public class Classes {
    private int class_id;
    private String class_code;
    private Account account;
    private Subject subject;
    private int class_year;
    private String class_term;
    private int block5_class;
    private int status;

    public Classes() {
    }

    public Classes(int class_id, String class_code, Account account, Subject subject, int class_year, String class_term, int block5_class, int status) {
        this.class_id = class_id;
        this.class_code = class_code;
        this.account = account;
        this.subject = subject;
        this.class_year = class_year;
        this.class_term = class_term;
        this.block5_class = block5_class;
        this.status = status;
    }

    public Classes(int class_id) {
        this.class_id = class_id;
    }

    public Classes(String class_code) {
        this.class_code = class_code;
    }

    public int getClass_id() {
        return class_id;
    }

    public void setClass_id(int class_id) {
        this.class_id = class_id;
    }

    public String getClass_code() {
        return class_code;
    }

    public void setClass_code(String class_code) {
        this.class_code = class_code;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public Subject getSubject() {
        return subject;
    }

    public void setSubject(Subject subject) {
        this.subject = subject;
    }


    public int getClass_year() {
        return class_year;
    }

    public void setClass_year(int class_year) {
        this.class_year = class_year;
    }

    public String getClass_term() {
        return class_term;
    }

    public void setClass_term(String class_term) {
        this.class_term = class_term;
    }

    public int getBlock5_class() {
        return block5_class;
    }

    public void setBlock5_class(int block5_class) {
        this.block5_class = block5_class;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Class{" + "class_id=" + class_id + ", class_code=" + class_code + ", email=" + account + ", subject_id=" + subject + ", class_year=" + class_year + ", class_term=" + class_term + ", block5_class=" + block5_class + ", status=" + status + '}';
    }
    
    
}
