/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.sql.Date;

/**
 *
 * @author admin
 */
public class Class_User {
    private Classes classes;
    private Team team;
    private Account account;
    private int team_leader;
    private Date dropout_date;
    private String user_notes;
    private String ongoing_eval;
    private String final_pres_eval;
    private String final_topic_eval;
    private int status;

    public Class_User() {
    }

    public Class_User(Classes classes, Team team, Account account, int team_leader, Date dropout_date, String user_notes, String ongoing_eval, String final_pres_eval, String final_topic_eval, int status) {
        this.classes = classes;
        this.team = team;
        this.account = account;
        this.team_leader = team_leader;
        this.dropout_date = dropout_date;
        this.user_notes = user_notes;
        this.ongoing_eval = ongoing_eval;
        this.final_pres_eval = final_pres_eval;
        this.final_topic_eval = final_topic_eval;
        this.status = status;
    }

    public Class_User(Classes classes) {
        this.classes = classes;
    }

    public Class_User(Account account) {
        this.account = account;
    }

    public Class_User(Team team) {
        this.team = team;
    }

    public Classes getClasses() {
        return classes;
    }

    public void setClasses(Classes classes) {
        this.classes = classes;
    }

    public Team getTeam() {
        return team;
    }

    public void setTeam(Team team) {
        this.team = team;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }



    public int getTeam_leader() {
        return team_leader;
    }

    public void setTeam_leader(int team_leader) {
        this.team_leader = team_leader;
    }

    public Date getDropout_date() {
        return dropout_date;
    }

    public void setDropout_date(Date dropout_date) {
        this.dropout_date = dropout_date;
    }

    public String getUser_notes() {
        return user_notes;
    }

    public void setUser_notes(String user_notes) {
        this.user_notes = user_notes;
    }

    public String getOngoing_eval() {
        return ongoing_eval;
    }

    public void setOngoing_eval(String ongoing_eval) {
        this.ongoing_eval = ongoing_eval;
    }

    public String getFinal_pres_eval() {
        return final_pres_eval;
    }

    public void setFinal_pres_eval(String final_pres_eval) {
        this.final_pres_eval = final_pres_eval;
    }

    public String getFinal_topic_eval() {
        return final_topic_eval;
    }

    public void setFinal_topic_eval(String final_topic_eval) {
        this.final_topic_eval = final_topic_eval;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Class_User{" + "class_id=" + classes + ", team_id=" + team + ", email=" + account + ", team_leader=" + team_leader + ", dropout_date=" + dropout_date + ", user_notes=" + user_notes + ", ongoing_eval=" + ongoing_eval + ", final_pres_eval=" + final_pres_eval + ", final_topic_eval=" + final_topic_eval + ", status=" + status + '}';
    }
    
    
    
}
