/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.sql.Date;

/**
 *
 * @author admin
 */
public class Milestone {
    private int milestone_id;
    private Iteration iteration;
    private Classes classes;
    private Date from_date;
    private Date to_date;
    private int status;

    public Milestone() {
    }

    public Milestone(int milestone_id, Iteration iteration, Classes classes, Date from_date, Date to_date, int status) {
        this.milestone_id = milestone_id;
        this.iteration = iteration;
        this.classes = classes;
        this.from_date = from_date;
        this.to_date = to_date;
        this.status = status;
    }

    public Milestone(int milestone_id) {
        this.milestone_id = milestone_id;
    }

    public int getMilestone_id() {
        return milestone_id;
    }

    public void setMilestone_id(int milestone_id) {
        this.milestone_id = milestone_id;
    }

    public Iteration getIteration() {
        return iteration;
    }

    public void setIteration(Iteration iteration) {
        this.iteration = iteration;
    }

    public Classes getClasses() {
        return classes;
    }

    public void setClasses(Classes classes) {
        this.classes = classes;
    }

    public Date getFrom_date() {
        return from_date;
    }

    public void setFrom_date(Date from_date) {
        this.from_date = from_date;
    }

    public Date getTo_date() {
        return to_date;
    }

    public void setTo_date(Date to_date) {
        this.to_date = to_date;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Milestone{" + "milestone_id=" + milestone_id + ", iteration_id=" + iteration + ", class_id=" + classes + ", from_date=" + from_date + ", to_date=" + to_date + ", status=" + status + '}';
    }
    
    
}
