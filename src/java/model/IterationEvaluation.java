/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author admin
 */
public class IterationEvaluation {
    private int evaluation_id;
    private Iteration iteration;
    private Class_User classes;
    private Class_User team;
    private Class_User email;
    private float bonus;
    private float grade;
    private String note;

    public IterationEvaluation(int evaluation_id, Iteration iteration, Class_User classes, Class_User team, Class_User email, float bonus, float grade, String note) {
        this.evaluation_id = evaluation_id;
        this.iteration = iteration;
        this.classes = classes;
        this.team = team;
        this.email = email;
        this.bonus = bonus;
        this.grade = grade;
        this.note = note;
    }

    public IterationEvaluation() {
    }

    public IterationEvaluation(int evaluation_id, Iteration iteration, float bonus, float grade, String note) {
        this.evaluation_id = evaluation_id;
        this.iteration = iteration;
        this.bonus = bonus;
        this.grade = grade;
        this.note = note;
    }

   

    public int getEvaluation_id() {
        return evaluation_id;
    }

    public void setEvaluation_id(int evaluation_id) {
        this.evaluation_id = evaluation_id;
    }

    public Iteration getIteration() {
        return iteration;
    }

    public void setIteration(Iteration iteration) {
        this.iteration = iteration;
    }

    public float getBonus() {
        return bonus;
    }

    public void setBonus(float bonus) {
        this.bonus = bonus;
    }

    public float getGrade() {
        return grade;
    }

    public void setGrade(float grade) {
        this.grade = grade;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Class_User getClasses() {
        return classes;
    }

    public void setClasses(Class_User classes) {
        this.classes = classes;
    }

    public Class_User getTeam() {
        return team;
    }

    public void setTeam(Class_User team) {
        this.team = team;
    }

    public Class_User getEmail() {
        return email;
    }

    public void setEmail(Class_User email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "Iteration_Evaluation{" + "evaluation_id=" + evaluation_id + ", iteration=" + iteration + ", classes=" + classes + ", team=" + team + ", email=" + email + ", bonus=" + bonus + ", grade=" + grade + ", note=" + note + '}';
    }
    
    
}
