package model;

public class MemberEvaluation {
    private int member_eval_id;
    private IterationEvaluation evaluation;
    private EvaluationCriteria criteria;
    private int converted_loc;
    private float grade;
    private String note;

    public MemberEvaluation() {
    }

    public MemberEvaluation(int member_eval_id, IterationEvaluation evaluation, EvaluationCriteria criteria, int converted_loc, float grade, String note) {
        this.member_eval_id = member_eval_id;
        this.evaluation = evaluation;
        this.criteria = criteria;
        this.converted_loc = converted_loc;
        this.grade = grade;
        this.note = note;
    }

    public int getMember_eval_id() {
        return member_eval_id;
    }

    public void setMember_eval_id(int member_eval_id) {
        this.member_eval_id = member_eval_id;
    }

    public IterationEvaluation getEvaluation() {
        return evaluation;
    }

    public void setEvaluation(IterationEvaluation evaluation) {
        this.evaluation = evaluation;
    }

    public EvaluationCriteria getCriteria() {
        return criteria;
    }

    public void setCriteria(EvaluationCriteria criteria) {
        this.criteria = criteria;
    }

    public int getConverted_loc() {
        return converted_loc;
    }

    public void setConverted_loc(int converted_loc) {
        this.converted_loc = converted_loc;
    }

    public float getGrade() {
        return grade;
    }

    public void setGrade(float grade) {
        this.grade = grade;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }
}
