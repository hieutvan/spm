/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author admin
 */
public class Team_Evaluation {
    private int team_eval_id;
    private IterationEvaluation evaluation;
    private EvaluationCriteria criteria;
    private Team team;
    private float grade;
    private String note;

    public Team_Evaluation() {
    }

    public Team_Evaluation(int team_eval_id, IterationEvaluation evaluation, EvaluationCriteria criteria, Team team, float grade, String note) {
        this.team_eval_id = team_eval_id;
        this.evaluation = evaluation;
        this.criteria = criteria;
        this.team = team;
        this.grade = grade;
        this.note = note;
    }

    public int getTeam_eval_id() {
        return team_eval_id;
    }

    public void setTeam_eval_id(int team_eval_id) {
        this.team_eval_id = team_eval_id;
    }

    public IterationEvaluation getEvaluation() {
        return evaluation;
    }

    public void setEvaluation(IterationEvaluation evaluation) {
        this.evaluation = evaluation;
    }

    public EvaluationCriteria getCriteria() {
        return criteria;
    }

    public void setCriteria(EvaluationCriteria criteria) {
        this.criteria = criteria;
    }

    public Team getTeam() {
        return team;
    }

    public void setTeam(Team team) {
        this.team = team;
    }

    public float getGrade() {
        return grade;
    }

    public void setGrade(float grade) {
        this.grade = grade;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    @Override
    public String toString() {
        return "Team_Evaluation{" + "team_eval_id=" + team_eval_id + ", evaluation=" + evaluation + ", criteria=" + criteria + ", team=" + team + ", grade=" + grade + ", note=" + note + '}';
    }
    
}
