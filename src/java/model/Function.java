/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author admin
 */
public class Function {

    private int function_id;
    private Team team;
    private String function_name;
    private Feature feature;
    private String access_roles;
    private String description;
    private int complexity_id;
    private Account account;
    private int priority;
    private int status;

    public Function(int function_id, Team team, String function_name, Feature feature, String access_roles, String description, int complexity_id, Account account, int priority, int status) {
        this.function_id = function_id;
        this.team = team;
        this.function_name = function_name;
        this.feature = feature;
        this.access_roles = access_roles;
        this.description = description;
        this.complexity_id = complexity_id;
        this.account = account;
        this.priority = priority;
        this.status = status;
    }

    public Function() {
    }

    public Function(int function_id) {
        this.function_id = function_id;
    }

    public int getFunction_id() {
        return function_id;
    }

    public void setFunction_id(int function_id) {
        this.function_id = function_id;
    }

    public Team getTeam() {
        return team;
    }

    public void setTeam(Team team) {
        this.team = team;
    }

    public String getFunction_name() {
        return function_name;
    }

    public void setFunction_name(String function_name) {
        this.function_name = function_name;
    }

    public Feature getFeature() {
        return feature;
    }

    public void setFeature(Feature feature) {
        this.feature = feature;
    }

    public String getAccess_roles() {
        return access_roles;
    }

    public void setAccess_roles(String access_roles) {
        this.access_roles = access_roles;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getComplexity_id() {
        return complexity_id;
    }

    public void setComplexity_id(int complexity_id) {
        this.complexity_id = complexity_id;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Function{" + "function_id=" + function_id + ", team=" + team + ", function_name=" + function_name + ", feature=" + feature + ", access_roles=" + access_roles + ", description=" + description + ", complexity_id=" + complexity_id + ", email=" + account + ", priority=" + priority + ", status=" + status + '}';
    }
    
    

}
