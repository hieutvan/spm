/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author admin
 */
public class Account {

    private String displayname;
    private String username;
    private String password;
    private String email;
    private String phonenumber;
    private int role_id;
    private int status;
    
    public Account(String displayname, String username, String password, String email, String phonenumber, int role_id, int status) {
        this.displayname = displayname;
        this.username = username;
        this.password = password;
        this.email = email;
        this.phonenumber = phonenumber;
        this.role_id = role_id;
        this.status = status;
    }

    public Account() {
    }

    public Account(String displayname, int role_id) {
        this.displayname = displayname;
        this.role_id = role_id;
    }

    public Account(String email) {
        this.email = email;
    }



    
    public String getDisplayname() {
        return displayname;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getEmail() {
        return email;
    }

    public String getPhonenumber() {
        return phonenumber;
    }

    public void setDisplayname(String displayname) {
        this.displayname = displayname;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPhonenumber(String phonenumber) {
        this.phonenumber = phonenumber;
    }

    public int getRole_id() {
        return role_id;
    }

    public void setRole_id(int role_id) {
        this.role_id = role_id;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Account{" + "displayname=" + displayname + ", username=" + username + ", password=" + password + ", email=" + email + ", phonenumber=" + phonenumber + ", role_id=" + role_id + ", status=" + status + '}';
    }


}
