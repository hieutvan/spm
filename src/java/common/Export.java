/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package common;

import dao.AccountDAO;
import dao.ClassDAO;
import dao.Class_UserDAO;
import dao.FunctionDAO;
import dao.SubjectDAO;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import model.Account;
import model.Class_User;
import model.Classes;
import model.Function;
import model.Issue;
import model.Subject;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 *
 * @author admin
 */
public class Export {
    
    
    public static void main(String[] args) {
        FunctionDAO fd = new FunctionDAO();
        ArrayList<Function> functionlist = fd.getAllbyTeam("khanhlche163867@fpt.edu.vn");
        exportfunction(functionlist, "functionlist");
    }
    public static String exportclassuser(ArrayList<Class_User> classuser, String raw_filename) {

        Class_UserDAO cud = new Class_UserDAO();
//        ArrayList<Class_User> culist = cud.getAllUserbyTrainer(traineremail);
        String file_name = raw_filename + ".xlsx";
        XSSFWorkbook class_user_book = new XSSFWorkbook();
        XSSFSheet sheet = class_user_book.createSheet("class_user");
        XSSFRow row;
        CellStyle style = class_user_book.createCellStyle();
        style.setAlignment(HorizontalAlignment.CENTER);
        style.setVerticalAlignment(VerticalAlignment.CENTER);

        row = sheet.createRow(0);
        Cell class_id = row.createCell(1);
        class_id.setCellValue("class_id");
        class_id.setCellStyle(style);
        Cell team_id = row.createCell(2);
        team_id.setCellValue("Team_code");
        team_id.setCellStyle(style);
        Cell email = row.createCell(3);
        email.setCellValue("email");
        email.setCellStyle(style);
        Cell team_leader = row.createCell(4);
        team_leader.setCellValue("Leader");
        team_leader.setCellStyle(style);
        Cell dropoutDate = row.createCell(5);
        dropoutDate.setCellValue("dropout date");
        dropoutDate.setCellStyle(style);
        Cell user_notes = row.createCell(6);
        user_notes.setCellValue("User Note");
        user_notes.setCellStyle(style);
        Cell ongoing_eval = row.createCell(7);
        ongoing_eval.setCellValue("Ongoing evaluation");
        ongoing_eval.setCellStyle(style);
        Cell final_pres_eval = row.createCell(8);
        final_pres_eval.setCellValue("Final Presentation Evaluation");
        final_pres_eval.setCellStyle(style);
        Cell final_topic_eval = row.createCell(9);
        final_topic_eval.setCellValue("Final topic evaluation");
        final_topic_eval.setCellStyle(style);
        Cell status = row.createCell(10);
        status.setCellValue("status");

        for (int i = 0; i < classuser.size(); i++) {
            row = sheet.createRow(i + 1);
            for (int j = 0; j < 11; j++) {
                Cell cell = row.createCell(j);
                cell.setCellStyle(style);
                if (cell.getColumnIndex() == 0) {
                    cell.setCellValue(i + 1);
                } else if (cell.getColumnIndex() == 1) {
                    cell.setCellValue(classuser.get(i).getClasses().getClass_code());
                } else if (cell.getColumnIndex() == 2) {
                    cell.setCellValue(classuser.get(i).getTeam().getTeam_code());
                } else if (cell.getColumnIndex() == 3) {
                    cell.setCellValue(classuser.get(i).getAccount().getEmail());
                } else if (cell.getColumnIndex() == 4) {
                    if (classuser.get(i).getTeam_leader() == 2) {
                        cell.setCellValue("True");
                    } else if (classuser.get(i).getTeam_leader() != 2) {
                        cell.setCellValue("");
                    }
                } else if (cell.getColumnIndex() == 5) {
                    Date date = classuser.get(i).getDropout_date();
                    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                    String strDate = sdf.format(date);
                    cell.setCellValue(strDate);
                } else if (cell.getColumnIndex() == 6) {
                    cell.setCellValue(classuser.get(i).getUser_notes());
                } else if (cell.getColumnIndex() == 7) {
                    cell.setCellValue(classuser.get(i).getOngoing_eval());
                } else if (cell.getColumnIndex() == 8) {
                    cell.setCellValue(classuser.get(i).getFinal_pres_eval());
                } else if (cell.getColumnIndex() == 9) {
                    cell.setCellValue(classuser.get(i).getFinal_topic_eval());
                } else if (cell.getColumnIndex() == 10) {
                    if(classuser.get(i).getStatus() == 2){
                        cell.setCellValue("Active");
                    }else if(classuser.get(i).getStatus() == 1){
                        cell.setCellValue("Inactive");
                    }
                }
            }
        }

        for (int i = 0; i < 11; i++) {
            sheet.autoSizeColumn(i);
        }

        try {
            String tmpDir = Files.createTempDirectory("tmpDirPrefix").toFile().getAbsolutePath();
            FileOutputStream output = new FileOutputStream(new File(tmpDir + file_name));
            class_user_book.write(output);
            output.close();
            System.out.println(tmpDir + file_name);
            System.out.println("Done EXPORT");

            return tmpDir + file_name;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String exportfunction(ArrayList<Function> functionlist, String raw_filename) {
        FunctionDAO fd = new FunctionDAO();
//        ArrayList<Class_User> culist = cud.getAllUserbyTrainer(traineremail);
        String file_name = raw_filename + ".xlsx";
        XSSFWorkbook class_user_book = new XSSFWorkbook();
        XSSFSheet sheet = class_user_book.createSheet("function");
        XSSFRow row;
        CellStyle style = class_user_book.createCellStyle();
        style.setAlignment(HorizontalAlignment.CENTER);
        style.setVerticalAlignment(VerticalAlignment.CENTER);

        row = sheet.createRow(0);
        Cell class_id = row.createCell(1);
        class_id.setCellValue("function_id");
        class_id.setCellStyle(style);
        Cell team_id = row.createCell(2);
        team_id.setCellValue("Team_code");
        team_id.setCellStyle(style);
        Cell email = row.createCell(3);
        email.setCellValue("function_name");
        email.setCellStyle(style);
        Cell team_leader = row.createCell(4);
        team_leader.setCellValue("feature_name");
        team_leader.setCellStyle(style);
        Cell dropoutDate = row.createCell(5);
        dropoutDate.setCellValue("access_roles");
        dropoutDate.setCellStyle(style);
        Cell user_notes = row.createCell(6);
        user_notes.setCellValue("description");
        user_notes.setCellStyle(style);
        Cell ongoing_eval = row.createCell(7);
        ongoing_eval.setCellValue("complexity_id");
        ongoing_eval.setCellStyle(style);
        Cell final_pres_eval = row.createCell(8);
        final_pres_eval.setCellValue("Owner");
        final_pres_eval.setCellStyle(style);
        Cell final_topic_eval = row.createCell(9);
        final_topic_eval.setCellValue("priority");
        final_topic_eval.setCellStyle(style);
        Cell status = row.createCell(10);
        status.setCellValue("status");

        for (int i = 0; i < functionlist.size(); i++) {
            row = sheet.createRow(i + 1);
            for (int j = 0; j < 11; j++) {
                Cell cell = row.createCell(j);
                cell.setCellStyle(style);
                if (cell.getColumnIndex() == 0) {
                    cell.setCellValue(i + 1);
                } else if (cell.getColumnIndex() == 1) {
                    cell.setCellValue(functionlist.get(i).getFunction_id());
                } else if (cell.getColumnIndex() == 2) {
                    cell.setCellValue(functionlist.get(i).getTeam().getTeam_code());
                } else if (cell.getColumnIndex() == 3) {
                    cell.setCellValue(functionlist.get(i).getFunction_name());
                } else if (cell.getColumnIndex() == 4) {
                    cell.setCellValue(functionlist.get(i).getFeature().getFeature_name());
                } else if (cell.getColumnIndex() == 5) {
                    cell.setCellValue(functionlist.get(i).getAccess_roles());
                } else if (cell.getColumnIndex() == 6) {
                    cell.setCellValue(functionlist.get(i).getDescription());
                } else if (cell.getColumnIndex() == 7) {
                    if(functionlist.get(i).getComplexity_id() == 0){
                        cell.setCellValue("Complex");
                    }else if(functionlist.get(i).getComplexity_id() == 1){
                        cell.setCellValue("Medium");
                    }else if(functionlist.get(i).getComplexity_id() == 2){
                        cell.setCellValue("Simple");
                    }
                } else if (cell.getColumnIndex() == 8) {
                    cell.setCellValue(functionlist.get(i).getAccount().getDisplayname());
                } else if (cell.getColumnIndex() == 9) {
                    cell.setCellValue(functionlist.get(i).getPriority());
                } else if (cell.getColumnIndex() == 10) {
                    if(functionlist.get(i).getStatus() ==1){
                        cell.setCellValue("Pending");
                    }else if(functionlist.get(i).getStatus() ==2){
                        cell.setCellValue("Planned");
                    }else if(functionlist.get(i).getStatus() ==3){
                        cell.setCellValue("Evaluated");
                    }else if(functionlist.get(i).getStatus() ==4){
                        cell.setCellValue("Rejected");
                    }
                }
            }
        }

        for (int i = 0; i < 11; i++) {
            sheet.autoSizeColumn(i);
        }

        try {
            String tmpDir = Files.createTempDirectory("tmpDirPrefix").toFile().getAbsolutePath();
            FileOutputStream output = new FileOutputStream(new File(tmpDir + file_name));
            class_user_book.write(output);
            output.close();
            System.out.println(tmpDir + file_name);
            System.out.println("Done EXPORT");

            return tmpDir + file_name;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
    
    public static String exportissue(ArrayList<Issue> issuelist, String raw_filename) {
        FunctionDAO fd = new FunctionDAO();
//        ArrayList<Class_User> culist = cud.getAllUserbyTrainer(traineremail);
        String file_name = raw_filename + ".xlsx";
        XSSFWorkbook class_user_book = new XSSFWorkbook();
        XSSFSheet sheet = class_user_book.createSheet("class_user");
        XSSFRow row;
        CellStyle style = class_user_book.createCellStyle();
        style.setAlignment(HorizontalAlignment.CENTER);
        style.setVerticalAlignment(VerticalAlignment.CENTER);

        row = sheet.createRow(0);
        Cell class_id = row.createCell(1);
        class_id.setCellValue("Title");
        class_id.setCellStyle(style);
        Cell team_id = row.createCell(2);
        team_id.setCellValue("Description");
        team_id.setCellStyle(style);
        Cell email = row.createCell(3);
        email.setCellValue("IssueID");
        email.setCellStyle(style);
        Cell team_leader = row.createCell(4);
        team_leader.setCellValue("URL");
        team_leader.setCellStyle(style);
        Cell dropoutDate = row.createCell(5);
        dropoutDate.setCellValue("State");
        dropoutDate.setCellStyle(style);
        Cell user_notes = row.createCell(6);
        user_notes.setCellValue("Assginee");
        user_notes.setCellStyle(style);
        Cell ongoing_eval = row.createCell(7);
        ongoing_eval.setCellValue("Created At");
        ongoing_eval.setCellStyle(style);
        Cell final_pres_eval = row.createCell(8);
        final_pres_eval.setCellValue("Due Date");
        final_pres_eval.setCellStyle(style);
        Cell final_topic_eval = row.createCell(9);
        final_topic_eval.setCellValue("Milestone");
        final_topic_eval.setCellStyle(style);
        Cell status = row.createCell(10);
        status.setCellValue("Labels");
        Cell Functions = row.createCell(11);
        Functions.setCellValue("Functions");
        Functions.setCellStyle(style);
        Cell IssueType = row.createCell(12);
        IssueType.setCellValue("Type");
        IssueType.setCellStyle(style);

        for (int i = 0; i < issuelist.size(); i++) {
            row = sheet.createRow(i + 1);
            for (int j = 0; j < 13; j++) {
                Cell cell = row.createCell(j);
                cell.setCellStyle(style);
                if (cell.getColumnIndex() == 0) {
                    cell.setCellValue(i + 1);
                } else if (cell.getColumnIndex() == 1) {
                    cell.setCellValue(issuelist.get(i).getIssue_title());
                } else if (cell.getColumnIndex() == 2) {
                    cell.setCellValue(issuelist.get(i).getDescription());
                } else if (cell.getColumnIndex() == 3) {
                    cell.setCellValue(issuelist.get(i).getIssue_id());
                } else if (cell.getColumnIndex() == 4) {
                    cell.setCellValue(issuelist.get(i).getGitlab_url());
                } else if (cell.getColumnIndex() == 5) {
                    if(issuelist.get(i).getStatus() == 1){
                        cell.setCellValue("Closed");
                    }else if(issuelist.get(i).getStatus() == 2){
                        cell.setCellValue("Open");
                    }else if(issuelist.get(i).getStatus() == 0){
                        cell.setCellValue("Pending");
                    }
                   
                }else if (cell.getColumnIndex() == 6) {
                    cell.setCellValue(issuelist.get(i).getAccount().getDisplayname());
                }else if (cell.getColumnIndex() == 7) {
                    Date date = issuelist.get(i).getCreated_at();
                    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                    String strDate = sdf.format(date);
                    cell.setCellValue(strDate);
                } else if (cell.getColumnIndex() == 8) {
                    Date date = issuelist.get(i).getDue_date();
                    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                    String strDate = sdf.format(date);
                    cell.setCellValue(strDate);
                } else if (cell.getColumnIndex() == 9) {
                    cell.setCellValue(issuelist.get(i).getMilestone().getMilestone_id());
                } else if (cell.getColumnIndex() == 10) {
                    cell.setCellValue(issuelist.get(i).getLabels());
                } else if (cell.getColumnIndex() == 11) {
                    cell.setCellValue(issuelist.get(i).getFunction().getFunction_name());
                } else if (cell.getColumnIndex() == 12){
                    cell.setCellValue(issuelist.get(i).getIssue_type());
                }
            }
        }

        for (int i = 0; i < 13; i++) {
            sheet.autoSizeColumn(i);
        }

        try {
            String tmpDir = Files.createTempDirectory("tmpDirPrefix").toFile().getAbsolutePath();
            FileOutputStream output = new FileOutputStream(new File(tmpDir + file_name));
            class_user_book.write(output);
            output.close();
            System.out.println(tmpDir + file_name);
            System.out.println("Done EXPORT");

            return tmpDir + file_name;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
