/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package common;

import dao.Class_UserDAO;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Class_User;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 *
 * @author admin
 */
public class Import {

    public static void main(String[] args) {
        Class_UserDAO cud = new Class_UserDAO();
        Class_User cus = new Class_User();
        readfile();
    }

    private static void readfile() {
        Class_User cus = new Class_User();
        try {
            XSSFWorkbook workbook = new XSSFWorkbook(new FileInputStream("class_user.xlsx"));
            XSSFSheet sheet = workbook.getSheet("class_user");

            XSSFRow row  = null;
            
            int i = 0;
            while((row = sheet.getRow(i))!= null){
                System.out.println(row.getCell(0).getNumericCellValue());
                i++;
            }
            
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Import.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Import.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
}
