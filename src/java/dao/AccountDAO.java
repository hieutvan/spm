package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Account;
import org.apache.commons.codec.digest.DigestUtils;

public class AccountDAO extends DBContext {

	public Account getAccount(String email, String password) {
		try {
			String sql = "SELECT * FROM `spm_project`.`account` WHERE `email` = ? AND `password` = ?";
			PreparedStatement stm = connection.prepareStatement(sql);
			stm.setString(1, email);
			stm.setString(2, DigestUtils.md5Hex(password));

			ResultSet rs = stm.executeQuery();
			while (rs.next()) {
				Account account = new Account();
				account.setUsername(rs.getString("username"));
				account.setPassword(rs.getString("password"));
				account.setDisplayname(rs.getString("displayname"));
				account.setEmail(rs.getString("email"));
				account.setPhonenumber(rs.getString("phonenumber"));
				account.setRole_id(rs.getInt("role_id"));
                                account.setStatus(rs.getInt("status"));
				return account;
			}
		} catch (SQLException ex) {
			//
		}

		return null;
	}

	public ArrayList<Account> getAllAccounts() {
		ArrayList<Account> accs = new ArrayList<>();

		try {
			String sql = "SELECT * FROM `spm_project`.`account`";
			PreparedStatement stm = connection.prepareStatement(sql);
			ResultSet rs = stm.executeQuery();
			while (rs.next()) {
				Account acc = new Account();
				acc.setUsername(rs.getString("username"));
				acc.setPassword(rs.getString("password"));
				acc.setDisplayname(rs.getString("displayname"));
				acc.setEmail(rs.getString("email"));
				acc.setPhonenumber(rs.getString("phonenumber"));
				acc.setRole_id(rs.getInt("role_id"));
                acc.setStatus(rs.getInt("status"));
				accs.add(acc);
			}
		} catch (SQLException ex) {
			Logger.getLogger(AccountDAO.class.getName()).log(Level.SEVERE, null, ex);
		}

		return accs;
	}

	public void insertAccount(Account acc) {
		String sql = "INSERT INTO `spm_project`.`account`\n"
				+ "(`username`,\n"
				+ "`password`,\n"
				+ "`displayname`,\n"
				+ "`email`,\n"
				+ "`phonenumber`,\n"
				+ "`role_id`,\n"
                                + "`status`)\n"
				+ "VALUES\n"
				+ "(?,\n"
				+ "?,\n"
				+ "?,\n"
				+ "?,\n"
				+ "?,\n"
				+ "?,\n"
                                + "?);";
		PreparedStatement stm = null;

		try {
			stm = connection.prepareStatement(sql);
			stm.setString(1, acc.getUsername());
			stm.setString(2, DigestUtils.md5Hex(acc.getPassword()));
			stm.setString(3, acc.getDisplayname());
			stm.setString(4, acc.getEmail());
			stm.setString(5, acc.getPhonenumber());
                        stm.setInt(6, acc.getRole_id());
                        stm.setInt(7, acc.getStatus());
			stm.executeUpdate();
		} catch (SQLException e) {
			//
		} finally {
			if (stm != null) {
				try {
					stm.close();
				} catch (SQLException ex) {
					Logger.getLogger(AccountDAO.class.getName()).log(Level.SEVERE, null, ex);
				}
			}
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException ex) {
					Logger.getLogger(AccountDAO.class.getName()).log(Level.SEVERE, null, ex);
				}
			}
		}
	}

	public boolean isExistedemail(String email) {
		try {
			String sql = "SELECT * FROM `spm_project`.`account` WHERE `email` = ?";
			PreparedStatement stm = connection.prepareStatement(sql);
			stm.setString(1, email);
			ResultSet rs = stm.executeQuery();
			while (rs.next()) {
				Account acc = new Account(rs.getString("displayname"), rs.getString("username"), rs.getString("password"), rs.getString("email"), rs.getString("phonenumber"), rs.getInt("role_id"), rs.getInt("status"));
				stm.close();
				connection.close();
				if (acc != null) {
					return true;
				}
			}
			stm.close();
			connection.close();
		} catch (SQLException ex) {
			Logger.getLogger(AccountDAO.class.getName()).log(Level.SEVERE, null, ex);
		}
		return false;
	}

	public Account getAccount(String email) {
		try {
			String sql = "select * from `spm_project`.`account` where `email` = ?";
			PreparedStatement stm = connection.prepareStatement(sql);
			stm.setString(1, email);
			ResultSet rs = stm.executeQuery();
			if (rs.next()) {
				Account a = new Account();
				a.setUsername(rs.getString("username"));
				a.setDisplayname(rs.getString("displayname"));
				a.setPassword(rs.getString("password"));
				a.setEmail(rs.getString("email"));
				a.setPhonenumber(rs.getString("phonenumber"));
				a.setRole_id(rs.getInt("role_id"));
                                a.setStatus(rs.getInt("status"));
				return a;
			}
		} catch (SQLException ex) {
			Logger.getLogger(AccountDAO.class.getName()).log(Level.SEVERE, null, ex);
		}
		return null;
	}

	public void updateAccount(Account a) {
        String sql = "UPDATE `spm_project`.`account`\n" +
				 "SET\n"+
				 "`password` = ?,\n"+
				 "`displayname` = ?,\n"+
				 "`username` = ?,\n"+
				 "`phonenumber` = ?,\n"+
                                 "`status` = ?\n"+
				 "WHERE `email` = ?";

		PreparedStatement stm = null;
		try {
			stm = connection.prepareStatement(sql);
			stm.setString(4, a.getPhonenumber());
			stm.setString(1, a.getPassword());
			stm.setString(2, a.getDisplayname());
                        stm.setString(3, a.getUsername());
			stm.setString(6, a.getEmail());
                        stm.setInt(5, a.getStatus());
			stm.executeUpdate();
		} catch (SQLException ex) {
			Logger.getLogger(AccountDAO.class.getName()).log(Level.SEVERE, null, ex);
		} finally {
			if (stm != null) {
				try {
					stm.close();
				} catch (SQLException ex) {
					Logger.getLogger(AccountDAO.class.getName()).log(Level.SEVERE, null, ex);
				}
			}
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException ex) {
					Logger.getLogger(AccountDAO.class.getName()).log(Level.SEVERE, null, ex);
				}
			}
		}

	}

	public void resetPassword(String email, String password) {
		String sql = "UPDATE `spm_project`.`account` set `password`=? where email=?;";
		PreparedStatement stm = null;

		try {
			stm = connection.prepareStatement(sql);
			stm.setString(1, DigestUtils.md5Hex(password));
			stm.setString(2, email);
			stm.executeUpdate();
			stm.close();
		} catch (SQLException e) {
		}
	}
    
    public void updatestatus(int status, String email){
        String sql = "UPDATE account \n" +
            "SET `status` = ? \n" +
            "WHERE `email` = ?";
        PreparedStatement stm = null;
        try {
            stm = connection.prepareStatement(sql);
            stm.setInt(1, status);
            stm.setString(2, email);
            stm.executeUpdate();
        } catch (SQLException e) {
            if (stm != null) {
                try {
			stm.close();
		} catch (SQLException ex) {
		Logger.getLogger(AccountDAO.class.getName()).log(Level.SEVERE, null, ex);
		}
            }
            if (connection != null) {
		try {
			connection.close();
		} catch (SQLException ex) {
		Logger.getLogger(AccountDAO.class.getName()).log(Level.SEVERE, null, ex);
		}
            }
        }
    }

    public ArrayList<Account> getAllTrainer(){
        ArrayList<Account> trainerlist = new ArrayList<>();
        try {
            String sql = "SELECT * FROM `account` where `role_id` = 4";
            PreparedStatement stm = connection.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();
            while(rs.next()){
                Account acc = new Account();
                acc.setUsername(rs.getString("username"));
                acc.setPassword(rs.getString("password"));
                acc.setDisplayname(rs.getString("displayname"));
                acc.setEmail(rs.getString("email"));
                acc.setPhonenumber(rs.getString("phonenumber"));
                acc.setRole_id(rs.getInt("role_id"));
                acc.setStatus(rs.getInt("status"));
                trainerlist.add(acc);
            }
        } catch (Exception e) {
        }
        return trainerlist;
    }

    public ArrayList<Account> getAllAuthor() {
        ArrayList<Account> authorlist = new ArrayList<>();
        try {
            String sql = "select * from account where role_id = 3:";
            PreparedStatement stm = connection.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Account acc = new Account();
                acc.setUsername(rs.getString("username"));
                acc.setPassword(rs.getString("password"));
                acc.setDisplayname(rs.getString("displayname"));
                acc.setEmail(rs.getString("email"));
                acc.setPhonenumber(rs.getString("phonenumber"));
                acc.setRole_id(rs.getInt("role_id"));
                acc.setStatus(rs.getInt("status"));
                authorlist.add(acc);
                
                return authorlist;
            }
        } catch (Exception e) {
        }
        
        return null;
    }
        
    public ArrayList<Account> getAllAssigneebyAssingee(String email){
        ArrayList<Account> assigneelist = new ArrayList<>();
        try {
            String sql = "select a.displayname, a.email from tracking tr inner join team t\n" +
                            "on tr.team_id = t.team_id\n" +
                            "inner join class_user cu on t.team_id = cu.team_id\n" +
                            "inner join account a on tr.assignee_email = a.email\n" +
                            "where cu.email =? group by a.displayname";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, email);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {                    
                Account a = new Account();
                a.setDisplayname(rs.getString("displayname"));
                a.setEmail(rs.getString("email"));
                assigneelist.add(a);
            }
        } catch (Exception e) {
        }
        return assigneelist;
    }
               
    public ArrayList<Account> getAllStudentbyTrainer(String email){
        ArrayList<Account> assigneelist = new ArrayList<>();
        try {
            String sql = "select a.displayname, a.email from tracking tr inner join team t\n" +
                            "on tr.team_id = t.team_id\n" +
                            "inner join class c on t.class_id = c.class_id\n" +
                            "inner join class_user cu on tr.assignee_email = cu.email\n" +
                            "inner join account a on cu.email = a.email\n" +
                            "where c.email = ? group by a.displayname, a.email";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, email);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {                    
                Account a = new Account();
                a.setDisplayname(rs.getString("displayname"));
                a.setEmail(rs.getString("email"));
                assigneelist.add(a);
            }
        } catch (Exception e) {
        }
        return assigneelist;
    }
    
    public ArrayList<Account> getAllAssigneebyTrainer(String email, int team_id){
        ArrayList<Account> assigneelist = new ArrayList<>();
        try {
            String sql = "select a.displayname, a.email from tracking tr inner join team t\n" +
                            "on tr.team_id = t.team_id\n" +
                            "inner join class c on t.class_id = c.class_id\n" +
                            "inner join class_user cu on tr.assignee_email = cu.email\n" +
                            "inner join account a on cu.email = a.email\n" +
                            "where c.email = ?and t.team_id = ? group by a.displayname, a.email";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, email);
            stm.setInt(2, team_id);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {                    
                Account a = new Account();
                a.setDisplayname(rs.getString("displayname"));
                a.setEmail(rs.getString("email"));
                assigneelist.add(a);
            }
        } catch (Exception e) {
        }
        return assigneelist;
    }
}
