/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Classes;
import model.Team;

/**
 *
 * @author admin
 */
public class TeamDAO extends DBContext {

    public ArrayList<Team> getAllTeambyTrainer(String email) {
        ArrayList<Team> teamlist = new ArrayList<>();
        try {
            String sql = "select t.team_id,t.team_code, c.class_code, c.class_id, t.topic_code, t.topic_name, t.gitlab_url, t.status from team t inner join class c \n" +
                        "on t.class_id = c.class_id\n" +
                        "where c.email = ? and c.status = 2";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, email);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Team t = new Team();
                Classes c = new Classes();
                t.setTeam_id(rs.getInt("team_id"));
                t.setTeam_code(rs.getString("team_code"));
                c.setClass_code(rs.getString("class_code"));
                c.setClass_id(rs.getInt("class_id"));
                t.setClasses(c);
                t.setTopic_code(rs.getString("topic_code"));
                t.setTopic_name(rs.getString("topic_name"));
                t.setGitlab_url(rs.getString("gitlab_url"));
                t.setStatus(rs.getInt("status"));
                teamlist.add(t);
            }
        } catch (SQLException e) {
        }
        return teamlist;
    }
    
    public ArrayList<Team> getAllTeambyTrainerwithStatus(String email) {
        ArrayList<Team> teamlist = new ArrayList<>();
        try {
            String sql = "select t.team_id,t.team_code, c.class_code, c.class_id, t.topic_code, t.topic_name, t.gitlab_url, t.status from team t inner join class c\n" +
                            "on t.class_id = c.class_id\n" +
                            "where c.email = ? and t.status = 2";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, email);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Team t = new Team();
                Classes c = new Classes();
                t.setTeam_id(rs.getInt("team_id"));
                t.setTeam_code(rs.getString("team_code"));
                c.setClass_code(rs.getString("class_code"));
                c.setClass_id(rs.getInt("class_id"));
                t.setClasses(c);
                t.setTopic_code(rs.getString("topic_code"));
                t.setTopic_name(rs.getString("topic_name"));
                t.setGitlab_url(rs.getString("gitlab_url"));
                t.setStatus(rs.getInt("status"));
                teamlist.add(t);
            }
        } catch (SQLException e) {
        }
        return teamlist;
    }
    
    public ArrayList<Team> getAllTeambyTrainerwithfunction(String email) {
        ArrayList<Team> teamlist = new ArrayList<>();
        try {
            String sql = "select t.team_id, t.team_code from team t inner join `function` f\n" +
                        "on t.team_id = f.team_id\n" +
                        "inner join class c on t.class_id = c.class_id\n" +
                        "where c.email = ? group by t.team_id";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, email);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Team t = new Team();
                Classes c = new Classes();
                t.setTeam_id(rs.getInt("team_id"));
                t.setTeam_code(rs.getString("team_code"));
                teamlist.add(t);
            }
        } catch (SQLException e) {
        }
        return teamlist;
    }
        
    public ArrayList<Team> getAllTeambyTrainerwithissue(String email) {
        ArrayList<Team> teamlist = new ArrayList<>();
        try {
            String sql = "select t.team_id, t.team_code from issue i inner join team t\n" +
                    "on i.team_id = t.team_id\n" +
                    "inner join class c on t.class_id = c.class_id\n" +
                    "where c.email = ? group by t.team_id";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, email);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Team t = new Team();
                Classes c = new Classes();
                t.setTeam_id(rs.getInt("team_id"));
                t.setTeam_code(rs.getString("team_code"));
                teamlist.add(t);
            }
        } catch (SQLException e) {
        }
        return teamlist;
    }
        
    public Team getAllTeambyUser(String email) {
        try {
            String sql = "select t.team_id, t.team_code from team t inner join class_user cu\n" +
                            "on t.team_id = cu.team_id\n" +
                            "where cu.email = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, email);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Team t = new Team();
                t.setTeam_id(rs.getInt("team_id"));
                t.setTeam_code(rs.getString("team_code"));
                return t;
            }
        } catch (SQLException e) {
        }
        return null;
    }
    
    public Team getTeamDetail(int team_id) {
        try {
            String sql = "SELECT t.* FROM team t inner join class c on t.class_id = c.class_id where t.team_id = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, team_id);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Team t = new Team();
                Classes c = new Classes();
                t.setTeam_id(rs.getInt("team_id"));
                t.setTeam_code(rs.getString("team_code"));
                c.setClass_id(rs.getInt("class_id"));
                t.setClasses(c);
                t.setTopic_code(rs.getString("topic_code"));
                t.setTopic_name(rs.getString("topic_name"));
                t.setGitlab_url(rs.getString("gitlab_url"));
                t.setStatus(rs.getInt("status"));
                return t;
            }
        } catch (SQLException e) {
        }
        return null;
    }
    
    public void updateTeam(Team t){
        String sql = "UPDATE `spm_project`.`team`\n" +
                    "SET\n" +
                    "`class_id` = ?,\n" +
                    "`topic_code` = ?,\n" +
                    "`topic_name` = ?,\n" +
                    "`gitlab_url` = ?,\n" +
                    "`status` = ?,\n" +
                    "`team_code` = ?\n" +
                    "WHERE `team_id` = ?;";
        PreparedStatement stm = null;
        try {
            stm = connection.prepareStatement(sql);
            stm.setInt(1, t.getClasses().getClass_id());
            stm.setString(2, t.getTopic_code());
            stm.setString(3, t.getTopic_name());
            stm.setString(4, t.getGitlab_url());
            stm.setInt(5, t.getStatus());
            stm.setString(6, t.getTeam_code());
            stm.setInt(7, t.getTeam_id());
            stm.executeUpdate();
        } catch (SQLException e) {
        } finally {
            if(stm != null){
                try {
                    stm.close();
                } catch (SQLException ex) {
                    Logger.getLogger(TeamDAO.class.getName()).log(Level.SEVERE, null, ex);
                }
            }if(connection != null)
                try {
                    connection.close();
            } catch (SQLException ex) {
                Logger.getLogger(TeamDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    public void updateStatus(int team_id, int status){
        String sql = "UPDATE `spm_project`.`team`\n" +
                    "SET\n" +
                    "`status` = ?\n" +
                    "WHERE `team_id` = ?;";
        PreparedStatement stm = null;
        try {
            stm = connection.prepareStatement(sql);
            stm.setInt(1, status);
            stm.setInt(2, team_id);
            stm.executeUpdate();
        } catch (SQLException e) {
        } finally {
            if(stm != null){
                try {
                    stm.close();
                } catch (SQLException ex) {
                    Logger.getLogger(TeamDAO.class.getName()).log(Level.SEVERE, null, ex);
                }
            }if(connection != null){
                try {
                    connection.close();
                } catch (SQLException ex) {
                    Logger.getLogger(TeamDAO.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
    
    public Team getStudentTeam(String email){
        try {
            String sql = "select i.team_id from team t inner join issue i\n" +
                        "on t.team_id = i.team_id\n" +
                        "inner join account a on a.email = i.assignee_email\n" +
                        "where a.email = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, email);
            ResultSet rs = stm.executeQuery();
            while(rs.next()){
                Team t = new Team();
                t.setTeam_id(rs.getInt("team_id"));
                return t;
            }
        } catch (Exception e) {
        }
        return null;
    }
    
    public void addTeam(Team t){
        String sql = "INSERT INTO `spm_project`.`team`\n" +
            "(`class_id`,\n" +
            "`topic_code`,\n" +
            "`topic_name`,\n" +
            "`gitlab_url`,\n" +
            "`status`,\n" +
            "`team_code`)\n" +
            "VALUES\n" +
            "(?,\n" +
            "?,\n" +
            "?,\n" +
            "?,\n" +
            "?,\n" +
            "?);";
                    PreparedStatement stm = null;
        try {
            stm = connection.prepareStatement(sql);
            stm.setInt(1, t.getClasses().getClass_id());
            stm.setString(2, t.getTopic_code());
            stm.setString(3, t.getTopic_name());
            stm.setString(4, t.getGitlab_url());
            stm.setInt(5, t.getStatus());
            stm.setString(6, t.getTeam_code());
            stm.executeUpdate();
        } catch (Exception e) {
        } finally {
            if(stm != null){
                try {
                    stm.close();
                } catch (SQLException ex) {
                    Logger.getLogger(TeamDAO.class.getName()).log(Level.SEVERE, null, ex);
                }
            }if(connection != null){
                try {
                    connection.close();
                } catch (SQLException ex) {
                    Logger.getLogger(TeamDAO.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
}
