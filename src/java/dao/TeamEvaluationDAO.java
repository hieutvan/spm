package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Classes;
import model.EvaluationCriteria;
import model.Iteration;
import model.Subject;
import model.Team;
import model.Team_Evaluation;

public class TeamEvaluationDAO extends DBContext {
    
    public ArrayList<Team_Evaluation> getTeamEvalsByTrainer(String email) {
        ArrayList<Team_Evaluation> teamevalist = new ArrayList<>();
        try {
            String sql = "SELECT te.team_eval_id, t.team_id, t.team_code,c.class_id, c.class_code, s.subject_id, s.subject_name, i.iteration_id, i.iteration_name, ec.evaluation_weight,ec.max_loc, ec.criteria_id, te.grade, te.note\n" +
                                "FROM team_evaluation te inner join team t \n" +
                                "on te.team_id = t.team_id\n" +
                                "inner join evaluation_criteria ec on te.criteria_id = ec.criteria_id\n" +
                                "inner join iteration i on ec.iteration_id = i.iteration_id\n" +
                                "inner join subject s on i.subject_id = s.subject_id\n" +
                                "inner join class c on s.subject_id = c.subject_id\n" +
                                "where c.email = ? and ec.team_evaluation = 2 \n" +
                                "group by te.team_eval_id";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, email);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Team_Evaluation te = new Team_Evaluation();
                te.setTeam_eval_id(rs.getInt("team_eval_id"));
                Team t = new Team();
                t.setTeam_id(rs.getInt("team_id"));
                t.setTeam_code(rs.getString("team_code"));
                Classes c = new Classes();
                c.setClass_id(rs.getInt("class_id"));
                c.setClass_code(rs.getString("class_code"));
                t.setClasses(c);
                Subject s = new Subject();
                s.setSubject_id(rs.getInt("subject_id"));
                s.setSubject_name(rs.getString("subject_name"));
                Iteration i = new Iteration();
                i.setIteration_id(rs.getInt("iteration_id"));
                i.setIteration_name(rs.getString("iteration_name"));
                i.setSubject(s);
                EvaluationCriteria ec = new EvaluationCriteria();
                ec.setEvaluation_weight(rs.getInt("evaluation_weight"));
                ec.setMax_loc(rs.getInt("max_loc"));
                ec.setCriteria_id(rs.getInt("criteria_id"));
                ec.setIteration(i);
                te.setCriteria(ec);
                te.setTeam(t);
                te.setGrade(rs.getFloat("grade"));
                te.setNote(rs.getString("note"));
                teamevalist.add(te);
            }
        } catch (Exception e) {
        }
        
        return teamevalist;
    }
    
    public ArrayList<Team_Evaluation> getTeamEvalsByUser(String email) {
        ArrayList<Team_Evaluation> teamevalist = new ArrayList<>();
        try {
            String sql = "SELECT te.team_eval_id, t.team_id, t.team_code,c.class_id, c.class_code, s.subject_id, s.subject_name, i.iteration_id, i.iteration_name, ec.evaluation_weight, ec.criteria_id, ec.max_loc, te.grade, te.note\n" +
                            "FROM team_evaluation te inner join team t \n" +
                            "on te.team_id = t.team_id\n" +
                            "inner join evaluation_criteria ec on te.criteria_id = ec.criteria_id\n" +
                            "inner join iteration i on ec.iteration_id = i.iteration_id\n" +
                            "inner join subject s on i.subject_id = s.subject_id\n" +
                            "inner join class c on s.subject_id = c.subject_id\n" +
                            "inner join class_user cu on t.team_id = cu.team_id\n" +
                            "where cu.email = ?\n" +
                            "group by te.team_eval_id";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, email);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Team_Evaluation te = new Team_Evaluation();
                te.setTeam_eval_id(rs.getInt("team_eval_id"));
                Team t = new Team();
                t.setTeam_id(rs.getInt("team_id"));
                t.setTeam_code(rs.getString("team_code"));
                Classes c = new Classes();
                c.setClass_id(rs.getInt("class_id"));
                c.setClass_code(rs.getString("class_code"));
                t.setClasses(c);
                Subject s = new Subject();
                s.setSubject_id(rs.getInt("subject_id"));
                s.setSubject_name(rs.getString("subject_name"));
                Iteration i = new Iteration();
                i.setIteration_id(rs.getInt("iteration_id"));
                i.setIteration_name(rs.getString("iteration_name"));
                i.setSubject(s);
                EvaluationCriteria ec = new EvaluationCriteria();
                ec.setEvaluation_weight(rs.getInt("evaluation_weight"));
                ec.setMax_loc(rs.getInt("max_loc"));
                ec.setCriteria_id(rs.getInt("criteria_id"));
                ec.setIteration(i);
                te.setCriteria(ec);
                te.setTeam(t);
                te.setGrade(rs.getFloat("grade"));
                te.setNote(rs.getString("note"));
                teamevalist.add(te);
            }
        } catch (Exception e) {
        }
        return teamevalist;
    }
    
    public Team_Evaluation getTeamEvalByID(int team_eval_id) {
        try {
            String sql = "SELECT te.team_eval_id, t.team_id, t.team_code,c.class_id, c.class_code, s.subject_id, s.subject_name, i.iteration_id, i.iteration_name, ec.evaluation_weight, ec.criteria_id, ec.max_loc, te.grade, te.note\n" +
                                "FROM team_evaluation te inner join team t \n" +
                                "on te.team_id = t.team_id\n" +
                                "inner join evaluation_criteria ec on te.criteria_id = ec.criteria_id\n" +
                                "inner join iteration i on ec.iteration_id = i.iteration_id\n" +
                                "inner join subject s on i.subject_id = s.subject_id\n" +
                                "inner join class c on s.subject_id = c.subject_id\n" +
                                "where te.team_eval_id = ?\n" +
                                "group by te.team_eval_id";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, team_eval_id);
            ResultSet rs = stm.executeQuery();
            if (rs.next()) {
                Team_Evaluation te = new Team_Evaluation();
                te.setTeam_eval_id(rs.getInt("team_eval_id"));
                Team t = new Team();
                t.setTeam_id(rs.getInt("team_id"));
                t.setTeam_code(rs.getString("team_code"));
                Classes c = new Classes();
                c.setClass_id(rs.getInt("class_id"));
                c.setClass_code(rs.getString("class_code"));
                t.setClasses(c);
                Subject s = new Subject();
                s.setSubject_id(rs.getInt("subject_id"));
                s.setSubject_name(rs.getString("subject_name"));
                Iteration i = new Iteration();
                i.setIteration_id(rs.getInt("iteration_id"));
                i.setIteration_name(rs.getString("iteration_name"));
                i.setSubject(s);
                EvaluationCriteria ec = new EvaluationCriteria();
                ec.setEvaluation_weight(rs.getInt("evaluation_weight"));
                ec.setMax_loc(rs.getInt("max_loc"));
                ec.setCriteria_id(rs.getInt("criteria_id"));
                ec.setIteration(i);
                te.setCriteria(ec);
                te.setTeam(t);
                te.setGrade(rs.getFloat("grade"));
                te.setNote(rs.getString("note"));
                return te;
            }
        } catch (Exception e) {
        }
        return null;
    }
    
    public void updateTeamEval(Team_Evaluation te) {
        String sql = "UPDATE `spm_project`.`team_evaluation`\n" +
                        "SET\n" +
                        "`criteria_id` = ?,\n" +
                        "`team_id` = ?,\n" +
                        "`grade` = ?,\n" +
                        "`note` = ?\n" +
                        "WHERE `team_eval_id` = ?;";
        PreparedStatement stm = null;
        try {
            stm = connection.prepareStatement(sql);
            stm.setInt(1, te.getCriteria().getCriteria_id());
            stm.setInt(2, te.getTeam().getTeam_id());
            stm.setFloat(3, te.getGrade());
            stm.setString(4, te.getNote());
            stm.setInt(5, te.getTeam_eval_id());
            stm.executeUpdate();
        } catch (Exception e) {
        } finally {
            if(stm != null)
                try {
                    stm.close();
            } catch (SQLException ex) {
                Logger.getLogger(TeamEvaluationDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
            if(connection != null)
                try {
                    connection.close();
            } catch (SQLException ex) {
                Logger.getLogger(TeamEvaluationDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    public void addTeamEval(Team_Evaluation te) {
        String sql = "INSERT INTO `spm_project`.`team_evaluation`\n" +
                        "(`evaluation_id`,\n" +
                        "`criteria_id`,\n" +
                        "`team_id`,\n" +
                        "`grade`,\n" +
                        "`note`)\n" +
                        "VALUES\n" +
                        "(?,\n" +
                        "?,\n" +
                        "?,\n" +
                        "?,\n" +
                        "?);";
        
        PreparedStatement stm = null;
        
        try {
            stm = connection.prepareStatement(sql);
            
            stm.setInt(1, te.getEvaluation().getEvaluation_id());
            stm.setInt(2, te.getCriteria().getCriteria_id());
            stm.setInt(3, te.getTeam().getTeam_id());
            stm.setFloat(4, te.getGrade());
            stm.setString(5, te.getNote());
            
            stm.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace(System.err);
            System.err.println("SQLState: " + e.getSQLState());
            System.err.println("Error Code: " + e.getErrorCode());
            System.err.println("Message: " + e.getMessage());
        } finally {
            if (stm != null)
                try {
                    stm.close();
                } catch (SQLException ex) {
                    Logger.getLogger(TeamEvaluationDAO.class.getName()).log(Level.SEVERE, null, ex);
                }
            if (connection != null)
                try {
                    connection.close();
                } catch (SQLException ex) {
                    Logger.getLogger(TeamEvaluationDAO.class.getName()).log(Level.SEVERE, null, ex);
                }
        }
    }
}
