/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Iteration;
import model.Subject;

/**
 *
 * @author admin
 */
public class IterationDAO extends DBContext{
    public void additeration(Iteration i){
        String sql = "INSERT INTO `spm_project`.`iteration`\n" +
                    "(`subject_id`,\n" +
                    "`iteration_name`,\n" +
                    "`duration`,\n" +
                    "`status`)\n" +
                    "VALUES\n" +
                    "(?,\n" +
                    "?,\n" +
                    "?,\n" +
                    "?);";
        
        PreparedStatement stm = null;
        try {
            stm = connection.prepareStatement(sql);
            stm.setInt(1, i.getSubject().getSubject_id());
            stm.setString(2, i.getIteration_name());
            stm.setInt(3, i.getDuration());
            stm.setInt(4, i.getStatus());
            stm.executeUpdate();
        } catch (Exception e) {
        }finally{
            if(stm != null){
                try {
                    stm.close();
                } catch (SQLException ex) {
                    Logger.getLogger(IterationDAO.class.getName()).log(Level.SEVERE, null, ex);
                }
        }if(connection != null){
                try {
                    connection.close();
                } catch (SQLException ex) {
                    Logger.getLogger(IterationDAO.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
    public ArrayList<Iteration> getAllIterbyClass(int class_id){
        ArrayList<Iteration> iterlist = new ArrayList<>();
        try {
            String sql= "select i.iteration_id, i.iteration_name from iteration i inner join milestone m\n" +
                        "on i.iteration_id = m.iteration_id\n" +
                        "inner join class c on m.class_id = c.class_id\n" +
                        "where c.class_id = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, class_id);
            ResultSet rs = stm.executeQuery();
            while(rs.next()){
                Iteration iter = new Iteration();
                iter.setIteration_id(rs.getInt("iteration_id"));
                iter.setIteration_name(rs.getString("iteration_name"));
                iterlist.add(iter);
            }
        } catch (Exception e) {
        }
        return iterlist;
    }
    public ArrayList<Iteration> getalliter(int subject_id){
        ArrayList<Iteration> iterlist = new ArrayList<>();
        try {
            String sql = "SELECT * FROM `spm_project`.`iteration` WHERE `subject_id` = ?;";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, subject_id);
            ResultSet rs = stm.executeQuery();
            while(rs.next()){
                Iteration iter = new Iteration();
                Subject sub = new Subject();
                iter.setIteration_id(rs.getInt("iteration_id"));
                sub.setSubject_id(rs.getInt("subject_id"));
                iter.setIteration_name(rs.getString("iteration_name"));
                iter.setSubject(sub);
                iter.setDuration(rs.getInt("duration"));
                iter.setStatus(rs.getInt("status"));
                iterlist.add(iter);
            }
        } catch (SQLException ex) {
            Logger.getLogger(IterationDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return iterlist;
    }
    
    public ArrayList<Iteration> getAll(){
        ArrayList<Iteration> iterlist = new ArrayList<>();
        try {
            String sql = "select i.iteration_id, i.subject_id, s.subject_code, s.subject_name, i.iteration_name, i.duration, i.status from iteration i inner join subject s\n" +
                            "on i.subject_id = s.subject_id and s.status = 2";
            PreparedStatement stm = connection.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();
            while(rs.next()){
                Iteration iter = new Iteration();
                Subject sub = new Subject();
                iter.setIteration_id(rs.getInt("iteration_id"));
                sub.setSubject_id(rs.getInt("subject_id"));
                sub.setSubject_code(rs.getString("subject_code"));
                sub.setSubject_name(rs.getString("subject_name"));
                iter.setIteration_name(rs.getString("iteration_name"));
                iter.setSubject(sub);
                iter.setDuration(rs.getInt("duration"));
                iter.setStatus(rs.getInt("status"));
                iterlist.add(iter);
            }
        } catch (Exception e) {
        }
        return iterlist;
    }
    public ArrayList<Iteration> getall(){
        ArrayList<Iteration> iterlist = new ArrayList<>();
        try {
            String sql = "SELECT s.`subject_id`, i.`iteration_id`, i.`iteration_name` FROM `subject` s INNER JOIN `iteration` i ON s.`subject_id` = i.`subject_id`;";
            PreparedStatement stm= connection.prepareStatement(sql);
            ResultSet rs= stm.executeQuery();
            while(rs.next()){
                Iteration iter = new Iteration();
                Subject sub = new Subject();
                iter.setIteration_id(rs.getInt("iteration_id"));
                sub.setSubject_id(rs.getInt("subject_id"));
                iter.setIteration_name(rs.getString("iteration_name"));
                iter.setSubject(sub);
                iterlist.add(iter);
            }
        } catch (Exception e) {
        } 
        return iterlist;
    }
    
    public ArrayList<Iteration> getAlliterbyemail(String email){
        ArrayList<Iteration> iterlist = new ArrayList<>();
        try {
            String sql = "Select i.iteration_id, i.subject_id, s.subject_code, s.subject_name, i.iteration_name, i.duration, i.status from iteration i inner join subject s on i.subject_id = s.subject_id where s.`email` = ? and s.status = 2;";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, email);
            ResultSet rs = stm.executeQuery();
            while(rs.next()){
                Iteration iter = new Iteration();
                Subject sub = new Subject();
                iter.setIteration_id(rs.getInt("iteration_id"));
                sub.setSubject_id(rs.getInt("subject_id"));
                sub.setSubject_code(rs.getString("subject_code"));
                sub.setSubject_name(rs.getString("subject_name"));
                iter.setIteration_name(rs.getString("iteration_name"));
                iter.setSubject(sub);
                iter.setDuration(rs.getInt("duration"));
                iter.setStatus(rs.getInt("status"));
                iterlist.add(iter);
            }
        } catch (Exception e) {
        }
        return iterlist;
    }
    
    
    public void deleteiter(int iteration_id){
        String sql = "DELETE FROM `spm_project`.`iteration`\n" +
                    "WHERE iteration_id = ?;";
        PreparedStatement stm = null;
        
        try {
            stm = connection.prepareStatement(sql);
            stm.setInt(1, iteration_id);
            stm.executeUpdate();
        } catch (Exception e) {
        }finally{
            if(stm!= null){
                try {
                    stm.close();
                } catch (SQLException ex) {
                    Logger.getLogger(IterationDAO.class.getName()).log(Level.SEVERE, null, ex);
                }
            if(connection != null){
                    try {
                        connection.close();
                    } catch (SQLException ex) {
                        Logger.getLogger(IterationDAO.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        }
    }
    
    
    public void displayiteration(Iteration i){
        String sql = "UPDATE `spm_project`.`iteration`\n" +
                    "SET\n" +
                    "`status` = ?\n" +
                    "WHERE `iteration_id` = ?;";
        PreparedStatement stm = null;
        try {
            stm = connection.prepareStatement(sql);
            stm.setInt(1, i.getStatus());
            stm.setInt(2, i.getIteration_id());
            stm.executeUpdate();
        } catch (Exception e) {
        }finally{
            if(stm!=null){
                try {
                    stm.close();
                } catch (SQLException ex) {
                    Logger.getLogger(IterationDAO.class.getName()).log(Level.SEVERE, null, ex);
                }
            }if(connection != null){
                try {
                    connection.close();
                } catch (SQLException ex) {
                    Logger.getLogger(IterationDAO.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
    
    public void updatestatus(int status, int iteration_id){
        String sql = "UPDATE `spm_project`.`iteration`\n" +
                    "SET\n" +
                    "`status` = ?\n" +
                    "WHERE `iteration_id` = ?;";
        PreparedStatement stm = null;
        try {
            stm = connection.prepareStatement(sql);
            stm.setInt(1, status);
            stm.setInt(2, iteration_id);
            stm.executeUpdate();
        } catch (Exception e) {
        }finally{
            if(stm!= null){
                try {
                    stm.close();
                } catch (SQLException ex) {
                    Logger.getLogger(IterationDAO.class.getName()).log(Level.SEVERE, null, ex);
                }
            }if(connection != null){
                try {
                    connection.close();
                } catch (SQLException ex) {
                    Logger.getLogger(IterationDAO.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
    public Iteration getiteration(int iteration_id){
        try {
            String sql = "SELECT * FROM `spm_project`.`iteration` WHERE `iteration_id` = ?;";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, iteration_id);
            ResultSet rs = stm.executeQuery();
            while(rs.next()){
                Iteration iter = new Iteration();
                Subject sub = new Subject();
                iter.setIteration_id(rs.getInt("iteration_id"));
                sub.setSubject_id(rs.getInt("subject_id"));
                iter.setSubject(sub);
                iter.setIteration_name(rs.getString("iteration_name"));
                iter.setDuration(rs.getInt("duration"));
                iter.setStatus(rs.getInt("status"));
                return iter;
            }
            } catch (SQLException ex) {
            Logger.getLogger(IterationDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public void updateiteration(Iteration iteration){
        String sql = "UPDATE `spm_project`.`iteration`\n" +
                    "SET\n" +
                    "`iteration_name` = ?,\n" +
                    "`duration` = ?,\n" +
                    "`status` = ?\n" +
                    "WHERE `iteration_id` = ?;";
        PreparedStatement stm = null;
        try {
            stm = connection.prepareStatement(sql);
            stm.setInt(4, iteration.getIteration_id());
            stm.setString(1, iteration.getIteration_name());
            stm.setInt(2, iteration.getDuration());
            stm.setInt(3, iteration.getStatus());
            stm.executeUpdate();
        } catch (Exception e) {
        }finally{
            if(stm!= null){
                try {
                    stm.close();
                } catch (SQLException ex) {
                    Logger.getLogger(IterationDAO.class.getName()).log(Level.SEVERE, null, ex);
                }
            }if(connection != null){
                try {
                    connection.close();
                } catch (SQLException ex) {
                    Logger.getLogger(IterationDAO.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
    
    public ArrayList<Iteration> getallIterbySubject(String email, int subject_id ){
        ArrayList<Iteration> iterlist = new ArrayList<>();
        try {
            String sql = "select distinct i.iteration_id, i.iteration_name,s.subject_id, s.subject_name from iteration i inner join subject s \n" +
                        "on i.subject_id = s.subject_id\n" +
                        "inner join class c on s.subject_id = c.subject_id\n" +
                        "where c.email = ?";
            if(subject_id >-1){
                sql+= "and s.subject_id = ?";
            }
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, email);
            if(subject_id >-1){
                stm.setInt(2, subject_id);
            }
            ResultSet rs = stm.executeQuery();
            while(rs.next()){
                Iteration i = new Iteration();
                Subject s = new Subject();
                i.setIteration_id(rs.getInt("iteration_id"));
                i.setIteration_name(rs.getString("iteration_name"));
                s.setSubject_id(rs.getInt("subject_id"));
                s.setSubject_name(rs.getString("subject_name"));
                i.setSubject(s);
                iterlist.add(i);
            }
                            
        } catch (Exception e) {
        }
        return iterlist;
    }
}
