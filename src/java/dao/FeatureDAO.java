/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Classes;
import model.Feature;
import model.Team;

/**
 *
 * @author admin
 */
public class FeatureDAO extends DBContext{
    public ArrayList<Feature> getallfeaturebyteam(String email){
        ArrayList<Feature> featurelist = new ArrayList<>();
        try {
            String sql = "select f.feature_id, t.team_id, t.team_code, f.feature_name, f.status from features f inner join team t\n" +
                    "on f.team_id = t.team_id\n" +
                    "inner join class_user cu on \n" +
                    "t.team_id = cu.team_id\n" +
                    "where cu.email = ? and f.status = 2 order by t.team_code";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, email);
            ResultSet rs = stm.executeQuery();
            while(rs.next()){
                Feature f = new Feature();
                Team t = new Team();
                f.setFeature_id(rs.getInt("feature_id"));
                t.setTeam_id(rs.getInt("team_id"));
                t.setTeam_code(rs.getString("team_code"));
                f.setTeam(t);
                f.setFeature_name(rs.getString("feature_name"));
                f.setStatus(rs.getInt("status"));
                featurelist.add(f);
            }
        } catch (Exception e) {
        }
        return featurelist;
    }
    public ArrayList<Feature> getAllFeaturebyTrainer(String email){
        ArrayList<Feature> featurelist = new ArrayList<>();
        try {
            String sql = "select f.feature_id, t.team_id, t.team_code,c.class_code, f.feature_name, f.status from features f inner join team t\n" +
                        "on f.team_id = t.team_id\n" +
                        "inner join class c on t.class_id = c.class_id\n" +
                        "where c.email = ? order by t.team_code";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, email);
            ResultSet rs = stm.executeQuery();
            while(rs.next()){
                Feature f = new Feature();
                Team t = new Team();
                Classes c = new Classes();
                f.setFeature_id(rs.getInt("feature_id"));
                t.setTeam_id(rs.getInt("team_id"));
                c.setClass_code(rs.getString("class_code"));
                t.setClasses(c);
                t.setTeam_code(rs.getString("team_code"));
                f.setTeam(t);
                f.setFeature_name(rs.getString("feature_name"));
                f.setStatus(rs.getInt("status"));
                featurelist.add(f);
            }
        } catch (Exception e) {
        }
        return featurelist;
    }
    public ArrayList<Feature> getAllFeaturebyTrainerwithfunction(String email){
        ArrayList<Feature> featurelist = new ArrayList<>();
        try {
            String sql = "select f.feature_id, f.feature_name from features f inner join `function` fu\n" +
                            "on f.feature_id = fu.feature_id\n" +
                            "inner join team t on f.team_id = t.team_id\n" +
                            "inner join class c on t.class_id = c.class_id\n" +
                            "where c.email = ? group by f.feature_id";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, email);
            ResultSet rs = stm.executeQuery();
            while(rs.next()){
                Feature f = new Feature();
                f.setFeature_id(rs.getInt("feature_id"));
                f.setFeature_name(rs.getString("feature_name"));
                featurelist.add(f);
            }
        } catch (Exception e) {
        }
        return featurelist;
    }
    public void addFeature(Feature f){
        String sql = "INSERT INTO `spm_project`.`features`\n" +
                "(`team_id`,\n" +
                "`feature_name`,\n" +
                "`status`)\n" +
                "VALUES\n" +
                "(?,\n" +
                "?,\n" +
                "?);";
        PreparedStatement stm = null;
        try {
            stm = connection.prepareStatement(sql);
            stm.setInt(1, f.getTeam().getTeam_id());
            stm.setString(2, f.getFeature_name());
            stm.setInt(3, f.getStatus());
            stm.executeUpdate();
        } catch (Exception e) {
        } finally {
            if(stm != null){
                try {
                    stm.close();
                } catch (SQLException ex) {
                    Logger.getLogger(FeatureDAO.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            if(connection != null){
                try {
                    connection.close();
                } catch (SQLException ex) {
                    Logger.getLogger(FeatureDAO.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
    
    public void updateStatus(int status, int feature_id){
        String sql = "UPDATE `spm_project`.`features`\n" +
                        "SET`status` = ?\n" +
                        "WHERE `feature_id` = ?;";
        PreparedStatement stm = null;
        try {
            stm = connection.prepareStatement(sql);
            stm.setInt(1, status);
            stm.setInt(2, feature_id);
            stm.executeUpdate();
        } catch (Exception e) {
        }finally{
            if(stm != null){
                try {
                    stm.close();
                } catch (SQLException ex) {
                    Logger.getLogger(FeatureDAO.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            if(connection != null){
                try {
                    connection.close();
                } catch (SQLException ex) {
                    Logger.getLogger(FeatureDAO.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
    
    public Feature getFeatureDetail(int feature_id){
        try {
            String sql = "select f.feature_id,t.team_id, t.team_code, f.feature_name, f.status from features f inner join team t\n" +
                    "on f.team_id = t.team_id\n" +
                    "where f.feature_id = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, feature_id);
            ResultSet rs = stm.executeQuery();
            if(rs.next()){
                Feature f = new Feature();
                Team t = new Team();
                f.setFeature_id(rs.getInt("feature_id"));
                t.setTeam_code(rs.getString("team_code"));
                t.setTeam_id(rs.getInt("team_id"));
                f.setTeam(t);
                f.setFeature_name(rs.getString("feature_name"));
                f.setStatus(rs.getInt("status"));
                return f;
            }
        } catch (SQLException ex) {
            Logger.getLogger(FeatureDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public void updateFeature(Feature f){
        String sql = "UPDATE `spm_project`.`features`\n" +
                "SET\n" +
                "`team_id` = ?,\n" +
                "`feature_name` = ?,\n" +
                "`status` = ?\n" +
                "WHERE `feature_id` = ?;";
        PreparedStatement stm = null;
        try {
            stm = connection.prepareStatement(sql);
            stm.setInt(1, f.getTeam().getTeam_id());
            stm.setString(2, f.getFeature_name());
            stm.setInt(3, f.getStatus());
            stm.setInt(4, f.getFeature_id());
            stm.executeUpdate();
        } catch (Exception e) {
        } finally {
            try {
                if(stm != null){
                    try {
                        stm.close();
                    } catch (SQLException ex) {
                        Logger.getLogger(FeatureDAO.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }if(connection != null)
                    connection.close();
            } catch (SQLException ex) {
                Logger.getLogger(FeatureDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
