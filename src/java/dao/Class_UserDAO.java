/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Account;
import model.Class_User;
import model.Classes;
import model.Team;

/**
 *
 * @author admin
 */
public class Class_UserDAO extends DBContext{
    public void addusertoclass(Class_User c){
        String sql = "INSERT INTO `spm_project`.`class_user`\n" +
                    "(`class_id`,\n" +
                    "`team_id`,\n" +
                    "`email`,\n" +
                    "`team_leader`,\n" +
                    "`dropout_date`,\n" +
                    "`user_notes`,\n" +
                    "`ongoing_eval`,\n" +
                    "`final_pres_eval`,\n" +
                    "`final_topic_eval`,\n" +
                    "`status`)\n" +
                    "VALUES\n" +
                    "(?,\n" +
                    "?,\n" +
                    "?,\n" +
                    "?,\n" +
                    "?,\n" +
                    "?,\n" +
                    "?,\n" +
                    "?,\n" +
                    "?,\n" +
                    "?);";
        PreparedStatement stm = null;
        try {
            stm = connection.prepareStatement(sql);
            stm.setInt(1, c.getClasses().getClass_id());
            stm.setInt(2, c.getTeam().getTeam_id());
            stm.setString(3, c.getAccount().getEmail());
            stm.setInt(4, c.getTeam_leader());
            stm.setDate(5, c.getDropout_date());
            stm.setString(6, c.getUser_notes());
            stm.setString(7, c.getOngoing_eval());
            stm.setString(8, c.getFinal_pres_eval());
            stm.setString(9, c.getFinal_topic_eval());
            stm.setInt(10, c.getStatus());
            stm.executeUpdate();
            
        } catch (Exception e) {
        } finally {
            if(stm != null)
                try {
                    stm.close();
            } catch (SQLException ex) {
                Logger.getLogger(Class_UserDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }if(connection != null)
            try {
                connection.close();
        } catch (SQLException ex) {
            Logger.getLogger(Class_UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public ArrayList<Class_User> getAllUserbyTrainer(String email){
        ArrayList<Class_User> cuslist = new ArrayList<>();
        try {
            String sql = "SELECT c.class_code,c.class_id, t.team_id,t.team_code, a.displayname,a.role_id,a.email, cu.team_leader, cu.dropout_date, cu.user_notes,cu.ongoing_eval, cu.final_pres_eval, cu.final_topic_eval, cu.status FROM class_user cu inner join class c on cu.class_id = c.class_id\n" +
                            "inner join team t on cu.team_id = t.team_id\n" +
                            "inner join account a on cu.email = a.email\n" +
                            "where c.email = ? and c.status = 2";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, email);
            ResultSet rs = stm.executeQuery();
            while(rs.next()){
                Class_User cus = new Class_User();
                Account a = new Account();
                Team t = new Team();
                Classes c = new Classes();
                c.setClass_code(rs.getString("class_code"));
                c.setClass_id(rs.getInt("class_id"));
                t.setTeam_id(rs.getInt("team_id"));
                t.setTeam_code(rs.getString("team_code"));
                a.setDisplayname(rs.getString("displayname"));
                a.setRole_id(rs.getInt("role_id"));
                a.setEmail(rs.getString("email"));
                cus.setClasses(c);
                cus.setTeam(t);
                cus.setAccount(a);
                cus.setTeam_leader(rs.getInt("team_leader"));
                cus.setDropout_date(rs.getDate("dropout_date"));
                cus.setUser_notes(rs.getString("user_notes"));
                cus.setOngoing_eval(rs.getString("ongoing_eval"));
                cus.setFinal_pres_eval(rs.getString("final_pres_eval"));
                cus.setFinal_topic_eval(rs.getString("final_topic_eval"));

                cus.setStatus(rs.getInt("status"));
                cuslist.add(cus);
            }
        } catch (Exception e) {
        }
        return cuslist;
    }
    
        public ArrayList<Class_User> getAllUserbyTeam(int team_id){
        ArrayList<Class_User> cuslist = new ArrayList<>();
        try {
            String sql = "SELECT cu.class_id, cu.team_id, cu.email, a.displayname, cu.team_leader, cu.dropout_date, cu.user_notes, cu.ongoing_eval, cu.final_pres_eval, cu.final_topic_eval, cu.status\n" +
                                "FROM class_user cu inner join account a\n" +
                                "on cu.email = a.email where team_id = ?;";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, team_id);
            ResultSet rs = stm.executeQuery();
            while(rs.next()){
                Class_User cus = new Class_User();
                Account a = new Account();
                Team t = new Team();
                Classes c = new Classes();
                c.setClass_id(rs.getInt("class_id"));
                cus.setClasses(c);
                t.setTeam_id(rs.getInt("team_id"));
                cus.setTeam(t);
                a.setEmail(rs.getString("email"));
                a.setDisplayname(rs.getString("displayname"));
                cus.setAccount(a);
                cus.setTeam_leader(rs.getInt("team_leader"));
                cus.setDropout_date(rs.getDate("dropout_date"));
                cus.setUser_notes(rs.getString("user_notes"));
                cus.setOngoing_eval(rs.getString("ongoing_eval"));
                cus.setFinal_pres_eval(rs.getString("final_pres_eval"));
                cus.setFinal_topic_eval(rs.getString("final_topic_eval"));
                cus.setStatus(rs.getInt("status"));
                cuslist.add(cus);
            }
        } catch (Exception e) {
        }
        return cuslist;
    }
        public ArrayList<Class_User> getAllUserbyClass(int class_id){
        ArrayList<Class_User> cuslist = new ArrayList<>();
        try {
            String sql = "select c.class_code,c.class_id, t.team_id, a.displayname, a.role_id, a.email, cu.team_leader, cu.dropout_date,cu.user_notes,cu.ongoing_eval, cu.final_pres_eval, cu.final_topic_eval, cu.status\n" +
                        "from class_user cu inner join account a\n" +
                        "on cu.email = a.email\n" +
                        "inner join team t \n" +
                        "on cu.team_id = t.team_id \n" +
                        "inner join class c\n" +
                        "on cu.class_id = c.class_id\n" +
                        "where c.class_id = ?\n" +
                        "order by t.team_id ASC";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, class_id);
            ResultSet rs = stm.executeQuery();
            while(rs.next()){
                Class_User cus = new Class_User();
                Account a = new Account();
                Team t = new Team();
                Classes c = new Classes();
                c.setClass_code(rs.getString("class_code"));
                c.setClass_id(rs.getInt("class_id"));
                t.setTeam_id(rs.getInt("team_id"));
                a.setDisplayname(rs.getString("displayname"));
                a.setRole_id(rs.getInt("role_id"));
                a.setEmail(rs.getString("email"));
                cus.setClasses(c);
                cus.setTeam(t);
                cus.setAccount(a);
                cus.setTeam_leader(rs.getInt("team_leader"));
                cus.setDropout_date(rs.getDate("dropout_date"));
                cus.setUser_notes(rs.getString("user_notes"));
                cus.setOngoing_eval(rs.getString("ongoing_eval"));
                cus.setFinal_pres_eval(rs.getString("final_pres_eval"));
                cus.setFinal_topic_eval(rs.getString("final_topic_eval"));
                cus.setStatus(rs.getInt("status"));
                cuslist.add(cus);
            }
        } catch (Exception e) {
        }
        return cuslist;
    }
    public Class_User getDetail(String email){
        try {
            String sql = "SELECT * FROM class_user where `email` = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, email);
            ResultSet rs = stm.executeQuery();
            if(rs.next()){
                Class_User cu = new Class_User();
                Account a = new Account();
                Classes c = new Classes();
                Team t = new Team();
                c.setClass_id(rs.getInt("class_id"));
                cu.setClasses(c);
                t.setTeam_id(rs.getInt("team_id"));
                cu.setTeam(t);
                a.setEmail(rs.getString("email"));
                cu.setAccount(a);
                cu.setTeam_leader(rs.getInt("team_leader"));
                cu.setDropout_date(rs.getDate("dropout_date"));
                cu.setUser_notes(rs.getString("user_notes"));
                cu.setOngoing_eval(rs.getString("ongoing_eval"));
                cu.setFinal_pres_eval(rs.getString("final_pres_eval"));
                cu.setFinal_topic_eval(rs.getString("final_topic_eval"));
                cu.setStatus(rs.getInt("status"));
                return cu;
                    
            }
        } catch (SQLException ex) {
            Logger.getLogger(Class_UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
}
