package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DBContext {

	protected Connection connection;

	public DBContext() {

		try {
			String dbName = "spm_project";
			String userName = "root";
			String password = "isp";
			String url = "jdbc:mysql://localhost:3306/" + dbName;

			Class.forName("com.mysql.cj.jdbc.Driver");
			connection = DriverManager.getConnection(url, userName, password);
		} catch (ClassNotFoundException | SQLException ex) {
			Logger.getLogger(DBContext.class.getName()).log(Level.SEVERE, null, ex);
		}
	}
}
