/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import dao.DBContext;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Setting;

/**
 *
 * @author RAZER
 */
public class SettingDAO extends DBContext {

    public List<Setting> getSettingList() {
        List<Setting> settinglist = new ArrayList<>();

        try {
            String sql = "select * from Setting";
            PreparedStatement stm = connection.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Setting s = new Setting();
                s.setSetting_id(rs.getInt("setting_id"));
                s.setType_id(rs.getInt("type_id"));
                s.setSetting_title(rs.getString("setting_title"));
                s.setSetting_value(rs.getString("setting_value"));
                s.setDisplay_order(rs.getInt("display_order"));
                s.setStatus(rs.getInt("status"));
                settinglist.add(s);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return (settinglist);
    }

    public Setting getSettingdetail(int setting_id) {
        try {
            String sql = "SELECT * FROM setting where setting_id = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, setting_id);
            ResultSet rs = stm.executeQuery();
            if (rs.next()) {
                Setting s = new Setting();
                s.setSetting_id(rs.getInt("setting_id"));
                s.setType_id(rs.getInt("type_id"));
                s.setSetting_title(rs.getString("setting_title"));
                s.setSetting_value(rs.getString("setting_value"));
                s.setDisplay_order(rs.getInt("display_order"));
                s.setStatus(rs.getInt("status"));
                return s;
            }
        } catch (Exception e) {
        }
        return null;
    }
    
    public void updateSetting(Setting s){
        String sql = "UPDATE `spm_project`.`setting`\n" +
                        "SET\n" +
                        "`setting_title` = ?,\n" +
                        "`type_id` = ?,\n" +
                        "`display_order` = ?,\n" +
                        "`setting_value` = ?,\n" +
                        "`status` = ?\n" +
                        "WHERE `setting_id` = ?;";
        PreparedStatement stm = null;
        try {
            stm = connection.prepareStatement(sql);
            stm.setString(1, s.getSetting_title());
            stm.setInt(2, s.getType_id());
            stm.setInt(3, s.getDisplay_order());
            stm.setString(4, s.getSetting_value());
            stm.setInt(5, s.getStatus());
            stm.setInt(6, s.getSetting_id());
            stm.executeUpdate();
        } catch (Exception e) {
        } finally {
            if(stm != null)
                try {
                    stm.close();
            } catch (SQLException ex) {
                Logger.getLogger(SettingDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
            if(connection != null)
                try {
                    connection.close();
            } catch (SQLException ex) {
                Logger.getLogger(SettingDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    public void updatestatus(int setting_id, int status){
        String sql = "UPDATE `spm_project`.`setting`\n" +
                        "SET\n" +
                        "`status` = ?\n" +
                        "WHERE `setting_id` = ?;";
        PreparedStatement stm = null;
        try {
            stm = connection.prepareStatement(sql);
            stm.setInt(1, status);
            stm.setInt(2, setting_id);
            stm.executeUpdate();
        } catch (Exception e) {
        } finally {
            if(stm != null)
                try {
                    stm.close();
            } catch (SQLException ex) {
                Logger.getLogger(SettingDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
            if(connection != null)
                try {
                    connection.close();
            } catch (SQLException ex) {
                Logger.getLogger(SettingDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    public void insertSetting(Setting s){
        String sql = "INSERT INTO `spm_project`.`setting`\n" +
                        "(`setting_title`,\n" +
                        "`type_id`,\n" +
                        "`display_order`,\n" +
                        "`setting_value`,\n" +
                        "`status`)\n" +
                        "VALUES\n" +
                        "(?,\n" +
                        "?,\n" +
                        "?,\n" +
                        "?,\n" +
                        "?);";
        PreparedStatement stm = null;
        try {
            stm = connection.prepareStatement(sql);
            stm.setString(1, s.getSetting_title());
            stm.setInt(2, s.getType_id());
            stm.setInt(3, s.getDisplay_order());
            stm.setString(4, s.getSetting_value());
            stm.setInt(5, s.getStatus());
            stm.executeUpdate();
        } catch (Exception e) {
        } finally {
            if(stm != null)
                try {
                    stm.close();
            } catch (SQLException ex) {
                Logger.getLogger(SettingDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
            if(connection != null)
                try {
                    connection.close();
            } catch (SQLException ex) {
                Logger.getLogger(SettingDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

}
