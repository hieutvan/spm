/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Function;
import model.LOC_Evaluation;
import model.Milestone;
import model.Team;
import model.Tracking;

/**
 *
 * @author admin
 */
public class LOCEvaluationDAO extends DBContext{
    public LOC_Evaluation getLOCEval(int tracking_id){
        try {
            String sql = "select le.*, t.team_id, t.milestone_id, t.function_id,f.function_name, f.complexity_id from loc_evaluation le inner join tracking t\n" +
                            "on le.tracking_id = t.tracking_id\n" +
                            "inner join `function` f on t.function_id = f.function_id\n" +
                            "where t.tracking_id = ?";
        PreparedStatement stm = connection.prepareStatement(sql);
        stm.setInt(1, tracking_id);
            ResultSet rs = stm.executeQuery();
            while(rs.next()){
                LOC_Evaluation le = new LOC_Evaluation();
                Function f = new Function();
                Tracking t = new Tracking();
                le.setEvaluation_id(rs.getInt("evaluation_id"));
                le.setEvaluation_time(rs.getDate("evaluation_time"));
                le.setEvaluation_note(rs.getString("evaluation_note"));
                le.setComplexity_id(rs.getInt("complexity_id"));
                le.setQuality_id(rs.getInt("quality_id"));
                le.setLOC(rs.getInt("LOC"));
                t.setTracking_id(rs.getInt("tracking_id"));
                t.setTeam(new Team(rs.getInt("team_id")));
                t.setMilestone(new Milestone(rs.getInt("milestone_id")));
                f.setFunction_id(rs.getInt("function_id"));
                f.setComplexity_id(rs.getInt("complexity_id"));
                le.setTracking(t);
                return le;
            }
        } catch (Exception e) {
        }return null;
    }
    
    public void updateLocEvaluation(LOC_Evaluation le){
        String sql = "UPDATE `spm_project`.`loc_evaluation`\n" +
                        "SET\n" +
                        "`evaluation_time` = ?,\n" +
                        "`evaluation_note` = ?,\n" +
                        "`complexity_id` = ?,\n" +
                        "`quality_id` = ?,\n" +
                        "`LOC` = ?,\n" +
                        "`tracking_id` = ?\n" +
                        "WHERE `evaluation_id` = ?;";
        PreparedStatement stm = null;
        try {
            stm = connection.prepareStatement(sql);
            stm.setDate(1, le.getEvaluation_time());
            stm.setString(2, le.getEvaluation_note());
            stm.setInt(3, le.getComplexity_id());
            stm.setInt(4, le.getQuality_id());
            stm.setInt(5, le.getLOC());
            stm.setInt(6, le.getTracking().getTracking_id());
            stm.setInt(7, le.getEvaluation_id());
            stm.executeUpdate();
        } catch (Exception e) {
        } finally {
            if(stm != null)
                try {
                    stm.close();
            } catch (SQLException ex) {
                Logger.getLogger(LOCEvaluationDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
            if(connection != null)
                try {
                    connection.close();
            } catch (SQLException ex) {
                Logger.getLogger(LOCEvaluationDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    public void addLocEva(LOC_Evaluation le){
        String sql = "INSERT INTO `spm_project`.`loc_evaluation`\n" +
                            "(`evaluation_time`,\n" +
                            "`evaluation_note`,\n" +
                            "`complexity_id`,\n" +
                            "`quality_id`,\n" +
                            "`LOC`,\n" +
                            "`tracking_id`)\n" +
                            "VALUES\n" +
                            "(?,\n" +
                            "?,\n" +
                            "?,\n" +
                            "?,\n" +
                            "?,\n" +
                            "?);" ;
        PreparedStatement stm = null;
        try {
            stm = connection.prepareStatement(sql);
            stm.setDate(1, le.getEvaluation_time());
            stm.setString(2, le.getEvaluation_note());
            stm.setInt(3, le.getComplexity_id());
            stm.setInt(4, le.getQuality_id());
            stm.setInt(5, le.getLOC());
            stm.setInt(6, le.getTracking().getTracking_id());
            stm.executeUpdate();
        } catch (Exception e) {
        } finally {
            if(stm != null)
                try {
                    stm.close();
            } catch (SQLException ex) {
                Logger.getLogger(LOCEvaluationDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
            if(connection != null)
                try {
                    connection.close();
            } catch (SQLException ex) {
                Logger.getLogger(LOCEvaluationDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
