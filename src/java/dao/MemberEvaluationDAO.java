package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Account;
import model.Class_User;
import model.Classes;
import model.EvaluationCriteria;
import model.Iteration;
import model.IterationEvaluation;
import model.MemberEvaluation;
import model.Subject;
import model.Team;

public class MemberEvaluationDAO extends DBContext {
    
    public ArrayList<MemberEvaluation> getAllMemEvalByUser(String email) {
        ArrayList<MemberEvaluation> memEvalList = new ArrayList<>();
        try {
            String sql = "SELECT me.member_eval_id, me.evaluation_id, t.team_id, t.team_code, c.class_id, c.class_code, s.subject_id, s.subject_name, a.displayname, i.iteration_id, i.iteration_name, me.converted_loc, le.LOC, me.grade, me.note\n" +
                "FROM member_evaluation me inner join iteration_evaluation ie \n" +
                "on me.evaluation_id = ie.evaluation_id\n" +
                "inner join class_user cu on ie.email = cu.email\n" +
                "inner join class c on cu.class_id = c.class_id\n" +
                "inner join team t on c.class_id = t.team_id\n" +
                "inner join subject s on c.subject_id = s.subject_id\n" +
                "inner join evaluation_criteria ec on me.criteria_id = ec.criteria_id\n" +
                "inner join iteration i on ec.iteration_id = i.iteration_id\n" +
                "inner join loc_evaluation le on ie.evaluation_id = le.evaluation_id\n" +
                "inner join account a on cu.email = a.email\n" +
                "where cu.email = ?";
            
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, email);
            ResultSet rs = stm.executeQuery();
            
            while (rs.next()) {
                MemberEvaluation me = new MemberEvaluation();
                me.setMember_eval_id(rs.getInt("member_eval_id"));
                IterationEvaluation ie = new IterationEvaluation();
                ie.setEvaluation_id(rs.getInt("evaluation_id"));
                me.setEvaluation(ie);
                
                Team t = new Team();
                t.setTeam_id(rs.getInt("team_id"));
                t.setTeam_code(rs.getString("team_code"));
                
                Classes c = new Classes();
                c.setClass_id(rs.getInt("class_id"));
                c.setClass_code(rs.getString("class_code"));
                
                Subject s = new Subject();
                s.setSubject_id(rs.getInt("subject_id"));
                s.setSubject_name(rs.getString("subject_name"));
                
                c.setSubject(s);
                ie.setClasses(new Class_User(c));
                ie.setTeam(new Class_User(t));
                
                Account a = new Account();
                a.setDisplayname(rs.getString("displayname"));
                ie.setEmail(new Class_User(a));
                
                Iteration i = new Iteration();
                i.setSubject(s);
                i.setIteration_id(rs.getInt("iteration_id"));
                i.setIteration_name(rs.getString("iteration_name"));
                
                EvaluationCriteria ec = new EvaluationCriteria();
                ec.setIteration(i);
                
                me.setConverted_loc(rs.getInt("converted_loc"));
                me.setGrade(rs.getFloat("grade"));
                me.setNote(rs.getString("note"));
                
                memEvalList.add(me);
            }
        } catch (Exception e) {
        }
        
        return memEvalList;
    }
    
    public void addMemberEvaluation(MemberEvaluation me) {
        String sql = "INSERT INTO member_evaluation VALUES (DEFAULT, ?, ?, ?, ?, ?);";
        
        PreparedStatement stm = null;
        
        try {
            stm = connection.prepareStatement(sql);
            
            stm.setInt(1, me.getEvaluation().getEvaluation_id());
            stm.setInt(2, me.getCriteria().getCriteria_id());
            stm.setInt(3, me.getConverted_loc());
            stm.setFloat(4, me.getGrade());
            stm.setString(5, me.getNote());
            
            stm.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace(System.err);
            System.err.println("SQLState: " + e.getSQLState());
            System.err.println("Error Code: " + e.getErrorCode());
            System.err.println("Message: " + e.getMessage());
        } finally {
            if (stm != null)
                try {
                    stm.close();
                } catch (SQLException ex) {
                    Logger.getLogger(TeamEvaluationDAO.class.getName()).log(Level.SEVERE, null, ex);
                }
            if (connection != null)
                try {
                    connection.close();
                } catch (SQLException ex) {
                    Logger.getLogger(TeamEvaluationDAO.class.getName()).log(Level.SEVERE, null, ex);
                }
        }
    }
}