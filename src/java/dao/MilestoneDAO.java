/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Classes;
import model.Iteration;
import model.Milestone;

/**
 *
 * @author admin
 */
public class MilestoneDAO extends DBContext {

    public ArrayList<Milestone> getAllMilestone(String email) {
        ArrayList<Milestone> milestonelist = new ArrayList<>();
        try {

            String sql = "SELECT m.milestone_id, i.iteration_name, i.iteration_id, c.class_code, c.class_id, m.from_date, m.to_date, m.status FROM milestone m inner join iteration i\n" +
                        "on m.iteration_id = i.iteration_id\n" +
                        "inner join class c \n" +
                        "on m.class_id = c.class_id where c.email = ? and c.status = 2";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, email);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Milestone ms = new Milestone();
                Iteration i = new Iteration();
                Classes c = new Classes();
                ms.setMilestone_id(rs.getInt("milestone_id"));
                i.setIteration_name(rs.getString("iteration_name"));
                i.setIteration_id(rs.getInt("iteration_id"));
                ms.setIteration(i);
                c.setClass_code(rs.getString("class_code"));
                c.setClass_id(rs.getInt("class_id"));
                ms.setClasses(c);
                ms.setFrom_date(rs.getDate("from_date"));
                ms.setTo_date(rs.getDate("to_date"));
                ms.setStatus(rs.getInt("status"));
                milestonelist.add(ms);
            }
        } catch (SQLException ex) {
            Logger.getLogger(MilestoneDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return milestonelist;
    }
    public ArrayList<Milestone> getAllMilestonebyUser(String email) {
        ArrayList<Milestone> milestonelist = new ArrayList<>();
        try {

            String sql = "select m.milestone_id from milestone m inner join class c\n" +
                    "on m.class_id = c.class_id\n" +
                    "inner join class_user cu on \n" +
                    "c.class_id = cu.class_id\n" +
                    "where cu.email = ? group by m.milestone_id";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, email);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Milestone ms = new Milestone();
                ms.setMilestone_id(rs.getInt("milestone_id"));
                milestonelist.add(ms);
            }
        } catch (SQLException ex) {
            Logger.getLogger(MilestoneDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return milestonelist;
    }
        public ArrayList<Milestone> getAllMilestonebyTrainer(String email) {
        ArrayList<Milestone> milestonelist = new ArrayList<>();
        try {

            String sql = "select m.milestone_id from milestone m inner join class c\n" +
                            "on m.class_id = c.class_id\n" +
                            "where c.email = ? group by m.milestone_id";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, email);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Milestone ms = new Milestone();
                ms.setMilestone_id(rs.getInt("milestone_id"));
                milestonelist.add(ms);
            }
        } catch (SQLException ex) {
            Logger.getLogger(MilestoneDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return milestonelist;
    }
    public Milestone getMilestonebyID(int milestone_id) {
        try {
            String sql = "SELECT * FROM spm_project.milestone WHERE milestone_id = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, milestone_id);
            ResultSet rs = stm.executeQuery();
            if (rs.next()) {
                Milestone m = new Milestone();
                Iteration i = new Iteration();
                Classes c = new Classes();
                m.setMilestone_id(rs.getInt("milestone_id"));
                i.setIteration_id(rs.getInt("iteration_id"));
                m.setIteration(i);
                c.setClass_id(rs.getInt("class_id"));
                m.setClasses(c);
                m.setFrom_date(rs.getDate("from_date"));
                m.setTo_date(rs.getDate("to_date"));
                m.setStatus(rs.getInt("status"));
                return m;
            }
        } catch (Exception e) {
        }
        return null;
    }
    public void updatemilestone(Milestone m){
    String sql = "UPDATE `spm_project`.`milestone`\n" +
                    "SET\n" +
                    "`iteration_id` = ?,\n" +
                    "`class_id` = ?,\n" +
                    "`from_date` = ?,\n" +
                    "`to_date` = ?,\n" +
                    "`status` = ?\n" +
                    "WHERE `milestone_id` = ?;";
    PreparedStatement stm = null;
        try {
            stm = connection.prepareStatement(sql);
            stm.setInt(1, m.getIteration().getIteration_id());
            stm.setInt(2, m.getClasses().getClass_id());
            stm.setDate(3, m.getFrom_date());
            stm.setDate(4, m.getTo_date());
            stm.setInt(5, m.getStatus());
            stm.setInt(6, m.getMilestone_id());
            stm.executeUpdate();
        } catch (Exception e) {
        } finally {
            if(stm != null){
                try {
                    stm.close();
                } catch (SQLException ex) {
                    Logger.getLogger(MilestoneDAO.class.getName()).log(Level.SEVERE, null, ex);
                }
            }if(connection != null){
                try {
                    connection.close();
                } catch (SQLException ex) {
                    Logger.getLogger(MilestoneDAO.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
    public void addmilestone(Milestone m){
        String sql = "INSERT INTO `spm_project`.`milestone`\n" +
                    "(`iteration_id`,\n" +
                    "`class_id`,\n" +
                    "`from_date`,\n" +
                    "`to_date`,\n" +
                    "`status`)\n" +
                    "VALUES\n" +
                    "(?,\n" +
                    "?,\n" +
                    "?,\n" +
                    "?,\n" +
                    "?);";
        PreparedStatement stm = null;
        try {
            stm = connection.prepareStatement(sql);
            stm.setInt(1, m.getIteration().getIteration_id());
            stm.setInt(2, m.getClasses().getClass_id());
            stm.setDate(3, m.getFrom_date());
            stm.setDate(4, m.getTo_date());
            stm.setInt(5, m.getStatus());
            stm.executeUpdate();
        } catch (Exception e) {
        } finally {
            if(stm != null){
                try {
                    stm.close();
                } catch (SQLException ex) {
                    Logger.getLogger(MilestoneDAO.class.getName()).log(Level.SEVERE, null, ex);
                }
            }if(connection != null){
                try {
                    connection.close();
                } catch (SQLException ex) {
                    Logger.getLogger(MilestoneDAO.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
    
    public void updateStatus(int milestone_id, int status){
        String sql = "UPDATE `spm_project`.`milestone`\n" +
                        "SET\n" +
                        "`status` = ?\n" +
                        "WHERE `milestone_id` = ?;";
        PreparedStatement stm = null;
        try {
            stm = connection.prepareStatement(sql);
            stm.setInt(1, status);
            stm.setInt(2, milestone_id);
            stm.executeUpdate();
        } catch (Exception e) {
        } finally {
            if(stm != null){
                try {
                    stm.close();
                } catch (SQLException ex) {
                    Logger.getLogger(MilestoneDAO.class.getName()).log(Level.SEVERE, null, ex);
                }
            }if(connection != null){
                try {
                    connection.close();
                } catch (SQLException ex) {
                    Logger.getLogger(MilestoneDAO.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        }
}
