package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Account;
import model.Class_User;
import model.Classes;
import model.Iteration;
import model.IterationEvaluation;
import model.Team;

public class IterationEvaluationDAO extends DBContext {
    public ArrayList<IterationEvaluation> getallIterEvabyTrainer(String email) {
        ArrayList<IterationEvaluation> IterEvaList = new ArrayList<>();
        try {
            String sql = "select ie.*, i.iteration_name, a.displayname from iteration_evaluation ie inner join iteration i \n" +
                                "on ie.iteration_id = i.iteration_id\n" +
                                "inner join class_user cu on ie.class_id = cu.class_id and ie.team_id = cu.team_id and ie.email = cu.email\n" +
                                "inner join class c on cu.class_id = c.class_id\n" +
                                "inner join account a on cu.email = a.email \n" +
                                "where c.email = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, email);
            ResultSet rs = stm.executeQuery();
            while(rs.next()){
                IterationEvaluation ie = new IterationEvaluation();
                ie.setEvaluation_id(rs.getInt("evaluation_id"));
                Iteration i = new Iteration();
                i.setIteration_id(rs.getInt("iteration_id"));
                i.setIteration_name(rs.getString("iteration_name"));
                ie.setIteration(i);
                ie.setClasses(new Class_User(new Classes(rs.getInt("class_id"))));
                ie.setTeam(new Class_User(new Team(rs.getInt("team_id"))));
                Account a  = new Account();
                a.setEmail(rs.getString("email"));
                a.setDisplayname(rs.getString("displayname"));
                ie.setEmail(new Class_User(a));
                ie.setBonus(rs.getFloat("bonus"));
                ie.setGrade(rs.getFloat("grade"));
                ie.setNote(rs.getString("note"));
                IterEvaList.add(ie);
                
            }
        } catch (Exception e) {
        }
        return IterEvaList;
    }
    
    public ArrayList<IterationEvaluation> getallIterEvabyUser(String email) {
        ArrayList<IterationEvaluation> IterEvaList = new ArrayList<>();
        try {
            String sql = "select ie.*, i.iteration_name, a.displayname from iteration_evaluation ie inner join iteration i \n" +
                                "on ie.iteration_id = i.iteration_id\n" +
                                "inner join class_user cu on ie.class_id = cu.class_id and ie.team_id = cu.team_id and ie.email = cu.email\n" +
                                "inner join class c on cu.class_id = c.class_id\n" +
                                "inner join account a on cu.email = a.email\n" +
                                "where cu.email = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, email);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                IterationEvaluation ie = new IterationEvaluation();
                ie.setEvaluation_id(rs.getInt("evaluation_id"));
                Iteration i = new Iteration();
                i.setIteration_id(rs.getInt("iteration_id"));
                i.setIteration_name(rs.getString("iteration_name"));
                ie.setIteration(i);
                ie.setClasses(new Class_User(new Classes(rs.getInt("class_id"))));
                ie.setTeam(new Class_User(new Team(rs.getInt("team_id"))));
                Account a  = new Account();
                a.setEmail(rs.getString("email"));
                a.setDisplayname(rs.getString("displayname"));
                ie.setEmail(new Class_User(a));
                ie.setBonus(rs.getFloat("bonus"));
                ie.setGrade(rs.getFloat("grade"));
                ie.setNote(rs.getString("note"));
                IterEvaList.add(ie);
                
            }
        } catch (Exception e) {
        }
        return IterEvaList;
    }
        
    public IterationEvaluation getIterEvaDetail(int evaluation_id) {
        try {
            String sql = "SELECT ie.evaluation_id, ie.iteration_id, i.iteration_name,ie.class_id, ie.team_id, ie.email ,a.displayname, ie.bonus, ie.grade, ie.note \n" +
                    "FROM `iteration_evaluation` ie \n" +
                    "inner join `account` a on ie.email = a.email\n" +
                    "inner join `iteration` i on ie.iteration_id = i.iteration_id\n" +
                    "where ie.evaluation_id = ?;";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, evaluation_id);
            ResultSet rs = stm.executeQuery();
            if (rs.next()) {
                IterationEvaluation ie = new IterationEvaluation();
                ie.setEvaluation_id(rs.getInt("evaluation_id"));
                Iteration i = new Iteration();
                i.setIteration_id(rs.getInt("iteration_id"));
                i.setIteration_name(rs.getString("iteration_name"));
                ie.setIteration(i);
                Team t = new Team(rs.getInt("team_id"));
                Classes c = new Classes(rs.getInt("class_id"));
                ie.setClasses(new Class_User(c));
                ie.setTeam(new Class_User(t));
                Account a  = new Account();
                a.setEmail(rs.getString("email"));
                a.setDisplayname(rs.getString("displayname"));
                ie.setEmail(new Class_User(a));
                ie.setBonus(rs.getFloat("bonus"));
                ie.setGrade(rs.getFloat("grade"));
                ie.setNote(rs.getString("note"));
                return ie;
            }
        } catch (Exception e) {
        }
        return null;
    }

    public void updateIterEval(IterationEvaluation ie) {
                String sql = "UPDATE `spm_project`.`iteration_evaluation`\n" +
                            "SET\n" +
                            "`iteration_id` = ?,\n" +
                            "`bonus` = ?,\n" +
                            "`grade` = ?,\n" +
                            "`note` = ?\n" +
                            "WHERE `evaluation_id` = ?;";
            PreparedStatement stm = null;
        try {         
            stm = connection.prepareStatement(sql);
            stm.setInt(1, ie.getIteration().getIteration_id());
            stm.setFloat(2, ie.getBonus());
            stm.setFloat(3, ie.getGrade());
            stm.setString(4, ie.getNote());
            stm.setInt(5, ie.getEvaluation_id());
            stm.executeUpdate();
        } catch (Exception e) {
        } finally {
            if (stm != null)
                try {
                    stm.close();
            } catch (SQLException ex) {
                Logger.getLogger(IterationEvaluationDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
            if (connection != null)
                try {
                    connection.close();
            } catch (SQLException ex) {
                Logger.getLogger(IterationEvaluationDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public int addIterEval(IterationEvaluation ie) {
        try {
            String sql = "INSERT INTO `spm_project`.`iteration_evaluation`\n" +
                            "(`iteration_id`,\n" +
                            "`class_id`,\n" +
                            "`team_id`,\n" +
                            "`email`,\n" +
                            "`bonus`,\n" +
                            "`grade`,\n" +
                            "`note`)\n" +
                            "VALUES\n" +
                            "(?,\n" +
                            "?,\n" +
                            "?,\n" +
                            "?,\n" +
                            "?,\n" +
                            "?,\n" +
                            "?);";
            
            PreparedStatement stm = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            stm.setInt(1, ie.getIteration().getIteration_id());
            stm.setInt(2, ie.getClasses().getClasses().getClass_id());
            stm.setInt(3, ie.getClasses().getTeam().getTeam_id());
            stm.setString(4, ie.getClasses().getAccount().getEmail());
            stm.setFloat(5, ie.getBonus());
            stm.setFloat(6, ie.getGrade());
            stm.setString(7, ie.getNote());
            stm.executeUpdate();
            
            ResultSet rs = stm.getGeneratedKeys();
            if (rs.next()) {
                return rs.getInt(1);
            }
        } catch (SQLException e) {
            e.printStackTrace(System.err);
            System.err.println("SQLState: " + e.getSQLState());
            System.err.println("Error Code: " + e.getErrorCode());
            System.err.println("Message: " + e.getMessage());
        }
        
        return -1;
    }
}
