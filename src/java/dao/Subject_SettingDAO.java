/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Subject;
import model.Subject_Setting;

/**
 *
 * @author admin
 */
public class Subject_SettingDAO extends DBContext {

    public ArrayList<Subject_Setting> getall() {
        ArrayList<Subject_Setting> sslist = new ArrayList<>();
        try {
            String sql = "SELECT ss.setting_id, s.subject_id, s.subject_code, s.subject_name, ss.type_id, ss.setting_title, ss.setting_value, ss.display_order, ss.status \n" +
                        "FROM subject_setting ss inner join subject s\n" +
                        "on ss.subject_id = s.subject_id order by ss.display_order ASC";
            PreparedStatement stm = connection.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();
            while(rs.next()){
                Subject_Setting ss = new Subject_Setting();
                Subject s = new Subject();
                ss.setSSsetting_id(rs.getInt("setting_id"));
                s.setSubject_id(rs.getInt("subject_id"));
                s.setSubject_code(rs.getString("subject_code"));
                s.setSubject_name(rs.getString("subject_name"));
                ss.setSubject(s);
                ss.setType_id(rs.getInt("type_id"));
                ss.setSetting_title(rs.getString("setting_title"));
                ss.setSetting_value(rs.getString("setting_value"));
                ss.setDisplay_order(rs.getInt("display_order"));
                ss.setStatus(rs.getInt("status"));
                sslist.add(ss);
            }
        } catch (Exception e) {
        }
        return sslist;
    }
        public ArrayList<Subject_Setting> getallbytrainer(String email) {
        ArrayList<Subject_Setting> sslist = new ArrayList<>();
        try {
            String sql = "SELECT ss.setting_id, s.subject_id, s.subject_code, s.subject_name, ss.type_id, ss.setting_title, ss.setting_value, ss.display_order, ss.status \n" +
                            "FROM subject_setting ss inner join subject s\n" +
                            "on ss.subject_id = s.subject_id\n" +
                            "where s.email = ? order by ss.display_order ASC";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, email);
            ResultSet rs = stm.executeQuery();
            while(rs.next()){
                Subject_Setting ss = new Subject_Setting();
                Subject s = new Subject();
                ss.setSSsetting_id(rs.getInt("setting_id"));
                s.setSubject_id(rs.getInt("subject_id"));
                s.setSubject_code(rs.getString("subject_code"));
                s.setSubject_name(rs.getString("subject_name"));
                ss.setSubject(s);
                ss.setType_id(rs.getInt("type_id"));
                ss.setSetting_title(rs.getString("setting_title"));
                ss.setSetting_value(rs.getString("setting_value"));
                ss.setDisplay_order(rs.getInt("display_order"));
                ss.setStatus(rs.getInt("status"));
                sslist.add(ss);
            }
        } catch (Exception e) {
        }
        return sslist;
    }
        public void updatestatus(int setting_id, int status){
            String sql = "UPDATE `spm_project`.`subject_setting`\n" +
                        "SET\n" +
                        "`status` = ?\n" +
                        "WHERE `setting_id` = ?;";
            PreparedStatement stm = null;
            try {
                stm = connection.prepareStatement(sql);
                stm.setInt(1, status);
                stm.setInt(2, setting_id);
                stm.executeUpdate();
            } catch (Exception e) {
            } finally {
                if(stm != null)
                    try {
                        stm.close();
                } catch (SQLException ex) {
                    Logger.getLogger(Subject_SettingDAO.class.getName()).log(Level.SEVERE, null, ex);
                }
                if(connection != null)
                    try {
                        connection.close();
                } catch (SQLException ex) {
                    Logger.getLogger(Subject_SettingDAO.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        
        public ArrayList<Subject_Setting> getQuality(int subject_id){
            ArrayList<Subject_Setting> sslist = new ArrayList<>();
            try {
                            String sql = "select ss.setting_id,ss.type_id, ss.setting_title, ss.setting_value from loc_evaluation le inner join tracking t\n" +
                            "on le.tracking_id = t.tracking_id\n" +
                            "inner join milestone m on t.milestone_id = m.milestone_id\n" +
                            "inner join class c on m.class_id = c.class_id\n" +
                            "inner join subject s on c.subject_id = s.subject_id\n" +
                            "inner join subject_setting ss on s.subject_id = ss.subject_id\n" +
                            "where s.subject_id = ? group by ss.setting_id,ss.type_id, ss.setting_title, ss.setting_value";
                    PreparedStatement stm = connection.prepareStatement(sql);
                    stm.setInt(1, subject_id);
                    ResultSet rs = stm.executeQuery();
                    while(rs.next()){
                        Subject_Setting ss = new Subject_Setting();
                        ss.setSSsetting_id(rs.getInt("setting_id"));
                        ss.setSetting_title(rs.getString("setting_title"));
                        ss.setSetting_value(rs.getString("setting_value"));
                        ss.setType_id(rs.getInt("type_id"));
                        sslist.add(ss);
                    }
            } catch (Exception e) {
            }return sslist;
                    
        }
        
        public Subject_Setting getSubject_settingDetail(int setting_id){
            try {
                String sql = "select ss.*, s.subject_code, s.subject_name from subject_setting ss inner join subject s\n" +
                                "on ss.subject_id = s.subject_id\n" +
                                "inner join account a on s.email = a.email\n" +
                                "where ss.setting_id = ?";
                PreparedStatement stm = connection.prepareStatement(sql);
                stm.setInt(1, setting_id);
                ResultSet rs = stm.executeQuery();
                while(rs.next()){
                    Subject_Setting ss = new Subject_Setting();
                    Subject s = new Subject();
                    ss.setSSsetting_id(rs.getInt("setting_id"));
                    s.setSubject_id(rs.getInt("subject_id"));
                    s.setSubject_code(rs.getString("subject_code"));
                    s.setSubject_name(rs.getString("subject_name"));
                    ss.setSubject(s);
                    ss.setType_id(rs.getInt("type_id"));
                    ss.setSetting_title(rs.getString("setting_title"));
                    ss.setSetting_value(rs.getString("setting_value"));
                    ss.setDisplay_order(rs.getInt("display_order"));
                    ss.setStatus(rs.getInt("status"));
                    return ss;
                }
            } catch (Exception e) {
            }
            return null;
        }
        public void updateSubSetting(Subject_Setting ss){
            String sql = "UPDATE `spm_project`.`subject_setting`\n" +
                            "SET\n" +
                            "`subject_id` = ?,\n" +
                            "`type_id` = ?,\n" +
                            "`setting_title` = ?,\n" +
                            "`setting_value` = ?,\n" +
                            "`display_order` = ?,\n" +
                            "`status` = ?\n" +
                            "WHERE `setting_id` = ?;";
            PreparedStatement stm = null;
            try {
                stm = connection.prepareStatement(sql);
                stm.setInt(1, ss.getSubject().getSubject_id());
                stm.setInt(2, ss.getType_id());
                stm.setString(3, ss.getSetting_title());
                stm.setString(4, ss.getSetting_value());
                stm.setInt(5, ss.getDisplay_order());
                stm.setInt(6, ss.getStatus());
                stm.setInt(7, ss.getSSsetting_id());
                stm.executeUpdate();
            } catch (Exception e) {
            } finally {
                if(stm != null)
                    try {
                        stm.close();
                } catch (SQLException ex) {
                    Logger.getLogger(Subject_SettingDAO.class.getName()).log(Level.SEVERE, null, ex);
                }
                if(connection != null)
                    try {
                        connection.close();
                } catch (SQLException ex) {
                    Logger.getLogger(Subject_SettingDAO.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        
        public void addSubject_Setting(Subject_Setting ss){
            String sql = "INSERT INTO `spm_project`.`subject_setting`\n" +
                            "(`subject_id`,\n" +
                            "`type_id`,\n" +
                            "`setting_title`,\n" +
                            "`setting_value`,\n" +
                            "`display_order`,\n" +
                            "`status`)\n" +
                            "VALUES\n" +
                            "(?,\n" +
                            "?,\n" +
                            "?,\n" +
                            "?,\n" +
                            "?,\n" +
                            "?);";
            PreparedStatement stm = null;
            try {
                stm = connection.prepareStatement(sql);
                stm.setInt(1, ss.getSubject().getSubject_id());
                stm.setInt(2, ss.getType_id());
                stm.setString(3, ss.getSetting_title());
                stm.setString(4, ss.getSetting_value());
                stm.setInt(5, ss.getDisplay_order());
                stm.setInt(6, ss.getStatus());
                stm.executeUpdate();
            } catch (Exception e) {
            } finally {
                if(stm != null)
                    try {
                        stm.close();
                } catch (SQLException ex) {
                    Logger.getLogger(Subject_SettingDAO.class.getName()).log(Level.SEVERE, null, ex);
                }
                if(connection != null)
                    try {
                        connection.close();
                } catch (SQLException ex) {
                    Logger.getLogger(Subject_SettingDAO.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
}
