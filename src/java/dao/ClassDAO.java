/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Account;
import model.Classes;
import model.Subject;

/**
 *
 * @author admin
 */
public class ClassDAO extends DBContext {

    public ArrayList<Classes> getallclass() {
        ArrayList<Classes> classlist = new ArrayList<>();
        try {
            String sql = "select c.class_id, c.class_code, a.displayname,a.email, s.subject_id, s.subject_name, c.class_term, c.class_year, c.block5_class, c.status from class c \n" +
                        "                inner join subject s\n" +
                        "                on c.subject_id = s.subject_id\n" +
                        "                inner join account a\n" +
                        "                group by c.class_id";
            PreparedStatement stm = connection.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Classes c = new Classes();
                Subject s = new Subject();
                Account a = new Account();
                c.setClass_id(rs.getInt("class_id"));
                a.setDisplayname(rs.getString("displayname"));
                a.setEmail(rs.getString("email")); 
                c.setAccount(a);
                s.setSubject_name(rs.getString("subject_name"));
                s.setSubject_id(rs.getInt("subject_id"));
                c.setSubject(s);
                c.setClass_code(rs.getString("class_code"));
                c.setClass_year(rs.getInt("class_year"));
                c.setClass_term(rs.getString("class_term"));
                c.setBlock5_class(rs.getInt("block5_class"));
                c.setStatus(rs.getInt("status"));
                classlist.add(c);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ClassDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return classlist;
    }

    public ArrayList<Classes> getAllbytrainer(String email) {
        ArrayList<Classes> classlist = new ArrayList<>();
        try {
            String sql = "select c.class_id, c.class_code, a.displayname,a.email, s.subject_name,s.subject_id, c.class_term, c.class_year, c.block5_class, c.status from class c \n" +
                            "inner join subject s\n" +
                            "on c.subject_id = s.subject_id\n" +
                            "inner join account a \n" +
                            "on c.email = a.email\n" +
                            "where c.email = ?and c.status = 2";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, email);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Classes c = new Classes();
                Subject s = new Subject();
                Account a = new Account();
                c.setClass_id(rs.getInt("class_id"));
                a.setDisplayname(rs.getString("displayname"));
                a.setEmail(rs.getString("email")); 
                c.setAccount(a);
                s.setSubject_name(rs.getString("subject_name"));
                s.setSubject_id(rs.getInt("subject_id"));
                c.setSubject(s);
                c.setClass_code(rs.getString("class_code"));
                c.setClass_year(rs.getInt("class_year"));
                c.setClass_term(rs.getString("class_term"));
                c.setBlock5_class(rs.getInt("block5_class"));
                c.setStatus(rs.getInt("status"));
                classlist.add(c);
            }
        } catch (Exception e) {
        }
        return classlist;
    }

    public ArrayList<Classes> getClassbyAuthor(String email) {
        ArrayList<Classes> classlist = new ArrayList<>();
        try {
            String sql = "select c.class_id, c.class_code, a.displayname,a.email, s.subject_id, s.subject_name, c.class_term, c.class_year, c.block5_class, c.status from class c \n" +
                "inner join subject s\n" +
                "on c.subject_id = s.subject_id\n" +
                "inner join account a \n" +
                "on c.email = a.email\n" +
                "where s.email = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, email);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Classes c = new Classes();
                Subject s = new Subject();
                Account a = new Account();
                c.setClass_id(rs.getInt("class_id"));
                a.setDisplayname(rs.getString("displayname"));
                a.setEmail(rs.getString("email")); 
                c.setAccount(a);
                s.setSubject_name(rs.getString("subject_name"));
                s.setSubject_id(rs.getInt("subject_id"));
                c.setSubject(s);
                c.setClass_code(rs.getString("class_code"));
                c.setClass_year(rs.getInt("class_year"));
                c.setClass_term(rs.getString("class_term"));
                c.setBlock5_class(rs.getInt("block5_class"));
                c.setStatus(rs.getInt("status"));
                classlist.add(c);
            }
        } catch (Exception e) {
        }
        return classlist;
    }

    public Classes getclassdetail(int class_id) {
        try {
            String sql = "select * from class where class_id = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, class_id);
            ResultSet rs = stm.executeQuery();
            if (rs.next()) {
                Classes classes = new Classes();
                Subject s = new Subject();
                Account a = new Account();
                classes.setClass_id(rs.getInt("class_id"));
                classes.setClass_code(rs.getString("class_code"));
                a.setEmail(rs.getString("email"));
                classes.setAccount(a);
                s.setSubject_id(rs.getInt("subject_id"));
                classes.setSubject(s);
                classes.setClass_year(rs.getInt("class_year"));
                classes.setClass_term(rs.getString("class_term"));
                classes.setBlock5_class(rs.getInt("block5_class"));
                classes.setStatus(rs.getInt("status"));
                return classes;
            }
        } catch (SQLException ex) {
            Logger.getLogger(ClassDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public void updateClass(Classes c) {
        String sql = "UPDATE `spm_project`.`class`\n"
                + "SET\n"
                + "`class_code` = ?,\n"
                + "`email` = ?,\n"
                + "`subject_id` = ?,\n"
                + "`class_year` = ?,\n"
                + "`class_term` = ?,\n"
                + "`block5_class` = ?,\n"
                + "`status` = ?\n"
                + "WHERE `class_id` = ?;";
        PreparedStatement stm = null;
        try {
            stm = connection.prepareStatement(sql);
            stm.setString(1, c.getClass_code());
            stm.setString(2, c.getAccount().getEmail());
            stm.setInt(3, c.getSubject().getSubject_id());
            stm.setInt(4, c.getClass_year());
            stm.setString(5, c.getClass_term());
            stm.setInt(6, c.getBlock5_class());
            stm.setInt(7, c.getStatus());
            stm.setInt(8, c.getClass_id());
            stm.executeUpdate();
        } catch (Exception e) {
        } finally {
            if (stm != null) {
                try {
                    stm.close();
                } catch (SQLException ex) {
                    Logger.getLogger(ClassDAO.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    Logger.getLogger(ClassDAO.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    public void addclass(Classes c) {
        String sql = "INSERT INTO `spm_project`.`class`\n"
                + "(`class_code`,\n"
                + "`email`,\n"
                + "`subject_id`,\n"
                + "`class_year`,\n"
                + "`class_term`,\n"
                + "`block5_class`,\n"
                + "`status`)\n"
                + "VALUES\n"
                + "(?,\n"
                + "?,\n"
                + "?,\n"
                + "?,\n"
                + "?,\n"
                + "?,\n"
                + "?);";
        PreparedStatement stm = null;
        try {
            stm = connection.prepareStatement(sql);
            stm.setString(1, c.getClass_code());
            stm.setString(2, c.getAccount().getEmail());
            stm.setInt(3, c.getSubject().getSubject_id());
            stm.setInt(4, c.getClass_year());
            stm.setString(5, c.getClass_term());
            stm.setInt(6, c.getBlock5_class());
            stm.setInt(7, c.getStatus());
            stm.executeUpdate();
        } catch (Exception e) {
        } finally {
            if (stm != null) {
                try {
                    stm.close();
                } catch (SQLException ex) {
                    Logger.getLogger(ClassDAO.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    Logger.getLogger(ClassDAO.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    public void updatestatus(Classes c) {
        String sql = "UPDATE `spm_project`.`class`\n"
                + "SET\n"
                + "`status` = ?\n"
                + "WHERE `class_id` = ?;";

        PreparedStatement stm = null;
        try {
            stm = connection.prepareStatement(sql);
            stm.setInt(1, c.getStatus());
            stm.setInt(2, c.getClass_id());
            stm.executeUpdate();
        } catch (Exception e) {
        } finally {
            if (stm != null) {
                try {
                    stm.close();
                } catch (SQLException ex) {
                    Logger.getLogger(ClassDAO.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    Logger.getLogger(ClassDAO.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    public void updateBL5(Classes c) {
        String sql = "UPDATE `spm_project`.`class`\n"
                + "SET\n"
                + "`block5_class` = ?\n"
                + "WHERE `class_id` = ?;";

        PreparedStatement stm = null;
        try {
            stm = connection.prepareStatement(sql);
            stm.setInt(1, c.getBlock5_class());
            stm.setInt(2, c.getClass_id());
            stm.executeUpdate();
        } catch (Exception e) {
        } finally {
            if (stm != null) {
                try {
                    stm.close();
                } catch (SQLException ex) {
                    Logger.getLogger(ClassDAO.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    Logger.getLogger(ClassDAO.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
}
