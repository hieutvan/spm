/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Account;
import model.Feature;
import model.Function;
import model.Team;

/**
 *
 * @author admin
 */
public class FunctionDAO extends DBContext {

    public ArrayList<Function> getAllbyTeam(String email) {
        ArrayList<Function> functionlist = new ArrayList<>();
        try {
            String sql = "SELECT f.*, t.team_id, t.team_code,fe.feature_id, fe.feature_name ,\n"
                    + "a.displayname, a.email\n"
                    + "FROM `function` f inner join team t\n"
                    + "on f.team_id = t.team_id\n"
                    + "inner join features fe on f.feature_id = fe.feature_id\n"
                    + "inner join class_user c on t.team_id = c.team_id\n"
                    + "inner join account a on f.email = a.email\n"
                    + "where c.email = ? order by t.team_code";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, email);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Function function = new Function();
                Feature f = new Feature();
                Team t = new Team();
                Account a = new Account();
                function.setFunction_id(rs.getInt("function_id"));
                t.setTeam_id(rs.getInt("team_id"));
                t.setTeam_code(rs.getString("team_code"));
                function.setTeam(t);
                function.setFunction_name(rs.getString("function_name"));
                f.setFeature_id(rs.getInt("feature_id"));
                f.setFeature_name(rs.getString("feature_name"));
                function.setFeature(f);
                function.setAccess_roles(rs.getString("access_roles"));
                function.setDescription(rs.getString("description"));
                function.setComplexity_id(rs.getInt("complexity_id"));
                a.setDisplayname(rs.getString("displayname"));
                a.setEmail(rs.getString("email"));
                function.setAccount(a);
                function.setPriority(rs.getInt("priority"));
                function.setStatus(rs.getInt("status"));
                functionlist.add(function);
            }
        } catch (Exception e) {
        }
        return functionlist;
    }

    public ArrayList<Function> getAllbyTeambyTrainer(String email) {
        ArrayList<Function> functionlist = new ArrayList<>();
        try {
            String sql = "SELECT f.*, t.team_id, t.team_code, fe.feature_id, fe.feature_name, a.displayname, a.email\n"
                    + "FROM `function` f inner join team t\n"
                    + "on f.team_id = t.team_id\n"
                    + "inner join features fe on f.feature_id = fe.feature_id\n"
                    + "inner join account a on f.email = a.email\n"
                    + "inner join class c on t.class_id = c.class_id\n"
                    + "where c.email =?";

            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, email);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Function function = new Function();
                Feature f = new Feature();
                Team t = new Team();
                Account a = new Account();
                function.setFunction_id(rs.getInt("function_id"));
                t.setTeam_id(rs.getInt("team_id"));
                t.setTeam_code(rs.getString("team_code"));
                function.setTeam(t);
                function.setFunction_name(rs.getString("function_name"));
                f.setFeature_id(rs.getInt("feature_id"));
                f.setFeature_name(rs.getString("feature_name"));
                function.setFeature(f);
                function.setAccess_roles(rs.getString("access_roles"));
                function.setDescription(rs.getString("description"));
                function.setComplexity_id(rs.getInt("complexity_id"));
                a.setDisplayname(rs.getString("displayname"));
                a.setEmail(rs.getString("email"));
                function.setAccount(a);
                function.setPriority(rs.getInt("priority"));
                function.setStatus(rs.getInt("status"));
                functionlist.add(function);
            }
        } catch (Exception e) {
        }
        return functionlist;
    }
    
    public ArrayList<Function> getAllTeamandFeature(int team_id){
        ArrayList<Function> TeamandFeatureList = new ArrayList<>();
        try {
            String sql = "SELECT f.feature_id, fe.feature_name from `function` f\n" +
                            "inner join features fe on \n" +
                            "f.feature_id = fe.feature_id\n" +
                            "where f.team_id = ? group by f.feature_id";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, team_id);
            ResultSet rs = stm.executeQuery();
            while(rs.next()){
                Feature f = new Feature();
                Function function = new Function();
                f.setFeature_id(rs.getInt("feature_id"));
                f.setFeature_name(rs.getString("feature_name"));
                function.setFeature(f);
                TeamandFeatureList.add(function);
            }
        } catch (Exception e) {
        }
        return TeamandFeatureList;
    }
        public ArrayList<Function> getAllFunctionbyUser(String email){
        ArrayList<Function> FunctionList = new ArrayList<>();
        try {
            String sql = "select f.*, t.team_id, t.team_code, fe.feature_id, fe.feature_name, a.displayname, a.email from `function` f inner join team t \n" +
                                "on f.team_id = t.team_id\n" +
                                "inner join features fe on t.team_id = fe.team_id\n" +
                                "inner join class_user cu on t.team_id = cu.team_id\n" +
                                "inner join account a on cu.email = a.email\n" +
                                "where cu.email = ?\n" +
                                "group by f.function_id\n" +
                                "order by f.priority";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, email);
            ResultSet rs = stm.executeQuery();
            while(rs.next()){
                Function function = new Function();
                Feature f = new Feature();
                Account a = new Account();
                Team t = new Team();
                function.setFunction_id(rs.getInt("function_id"));
                t.setTeam_id(rs.getInt("team_id"));
                t.setTeam_code(rs.getString("team_code"));
                function.setTeam(t);
                function.setFunction_name(rs.getString("function_name"));
                f.setFeature_id(rs.getInt("feature_id"));
                f.setFeature_name(rs.getString("feature_name"));
                function.setFeature(f);
                function.setAccess_roles(rs.getString("access_roles"));
                function.setDescription(rs.getString("description"));
                function.setComplexity_id(rs.getInt("complexity_id"));
                a.setDisplayname(rs.getString("displayname"));
                a.setEmail(rs.getString("email"));
                function.setAccount(a);
                function.setPriority(rs.getInt("priority"));
                function.setStatus(rs.getInt("status"));
                FunctionList.add(function);
            }
        } catch (Exception e) {
        }
        return FunctionList;
    }
        public ArrayList<Function> getAllFunctionTrainer(String email, int team_id){
        ArrayList<Function> FunctionList = new ArrayList<>();
        try {
            String sql = "select f.function_id, f.function_name from `function` f inner join team t \n" +
                            "on f.team_id = t.team_id\n" +
                            "inner join class c on t.class_id = c.class_id\n" +
                            "where c.email = ? and t.team_id = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, email);
            stm.setInt(2, team_id);
            ResultSet rs = stm.executeQuery();
            while(rs.next()){
                Function function = new Function();
                function.setFunction_id(rs.getInt("function_id"));
                function.setFunction_name(rs.getString("function_name"));
                FunctionList.add(function);
            }
        } catch (Exception e) {
        }
        return FunctionList;
    }
        public ArrayList<Function> getAllFunctionbyTrainer(String email){
        ArrayList<Function> FunctionList = new ArrayList<>();
        try {
            String sql = "select f.function_id, f.function_name, f.complexity_id from `function` f inner join team t \n" +
                            "on f.team_id = t.team_id\n" +
                            "inner join class c on t.class_id = c.class_id\n" +
                            "where c.email = ? group by f.function_name, f.complexity_id";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, email);
            ResultSet rs = stm.executeQuery();
            while(rs.next()){
                Function function = new Function();
                function.setFunction_id(rs.getInt("function_id"));
                function.setFunction_name(rs.getString("function_name"));
                function.setComplexity_id(rs.getInt("complexity_id"));
                FunctionList.add(function);
            }
        } catch (Exception e) {
        }
        return FunctionList;
    }
    public void updateStatus(int function_id, int status) {
        String sql = "UPDATE `spm_project`.`function`\n"
                + "SET\n"
                + "`status` = ?\n"
                + "WHERE `function_id` = ?;";
        PreparedStatement stm = null;
        try {
            stm = connection.prepareStatement(sql);
            stm.setInt(1, status);
            stm.setInt(2, function_id);
            stm.executeUpdate();
        } catch (Exception e) {
        } finally {
            if (stm != null)
               try {
                stm.close();
            } catch (SQLException ex) {
                Logger.getLogger(FunctionDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
            if (connection != null)
               try {
                connection.close();
            } catch (SQLException ex) {
                Logger.getLogger(FunctionDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public Function getFunctionDetail(int function_id) {
        try {
            String sql = "SELECT f.function_id, t.team_id, t.team_code, f.function_name,fe.feature_id, fe.feature_name, f.access_roles, f.description, f.complexity_id, a.displayname,a.email, f.priority, f.status\n"
                    + "FROM `function` f inner join team t\n"
                    + "on f.team_id = t.team_id\n"
                    + "inner join features fe on f.feature_id = fe.feature_id\n"
                    + "inner join account a on f.email = a.email\n"
                    + "inner join class c on t.class_id = c.class_id\n"
                    + "where f.function_id = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, function_id);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Function function = new Function();
                Feature f = new Feature();
                Team t = new Team();
                Account a = new Account();
                function.setFunction_id(rs.getInt("function_id"));
                t.setTeam_id(rs.getInt("team_id"));
                t.setTeam_code(rs.getString("team_code"));
                function.setTeam(t);
                function.setFunction_name(rs.getString("function_name"));
                f.setFeature_id(rs.getInt("feature_id"));
                f.setFeature_name(rs.getString("feature_name"));
                function.setFeature(f);
                function.setAccess_roles(rs.getString("access_roles"));
                function.setDescription(rs.getString("description"));
                function.setComplexity_id(rs.getInt("complexity_id"));
                a.setDisplayname(rs.getString("displayname"));
                a.setEmail(rs.getString("email"));
                function.setAccount(a);
                function.setPriority(rs.getInt("priority"));
                function.setStatus(rs.getInt("status"));
                return function;
            }
        } catch (Exception e) {
        }
        return null;
    }
    
    public void updateFunction(Function f){
        String sql= "UPDATE `spm_project`.`function`\n" +
                        "SET\n" +
                        "`team_id` = ?,\n" +
                        "`function_name` = ?,\n" +
                        "`feature_id` = ?,\n" +
                        "`access_roles` = ?,\n" +
                        "`description` = ?,\n" +
                        "`complexity_id` = ?,\n" +
                        "`email` = ?,\n" +
                        "`priority` = ?,\n" +
                        "`status` = ?\n" +
                        "WHERE `function_id` = ?;";
        PreparedStatement stm = null;
        try {
            stm = connection.prepareStatement(sql);
            stm.setInt(1, f.getTeam().getTeam_id());
            stm.setString(2, f.getFunction_name());
            stm.setInt(3, f.getFeature().getFeature_id());
            stm.setString(4, f.getAccess_roles());
            stm.setString(5, f.getDescription());
            stm.setInt(6, f.getComplexity_id());
            stm.setString(7, f.getAccount().getEmail());
            stm.setInt(8, f.getPriority());
            stm.setInt(9, f.getStatus());
            stm.setInt(10, f.getFunction_id());
            stm.executeUpdate();
        } catch (Exception e) {
        } finally {
            if(stm != null){
                try {
                    stm.close();
                } catch (SQLException ex) {
                    Logger.getLogger(FunctionDAO.class.getName()).log(Level.SEVERE, null, ex);
                }
            }if(connection != null){
                try {
                    connection.close();
                } catch (SQLException ex) {
                    Logger.getLogger(FunctionDAO.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
    public void updateComplexity(int complexity_id, int function_id){
        String sql = "UPDATE `spm_project`.`function`\n" +
                        "SET\n" +
                        "`complexity_id` = ?\n" +
                        "WHERE `function_id` = ?;";
        PreparedStatement stm = null;
        try {
            stm = connection.prepareStatement(sql);
            stm.setInt(1, complexity_id);
            stm.setInt(2, function_id);
            stm.executeUpdate();
        } catch (Exception e) {
        } finally {
            if(stm != null){
                try {
                    stm.close();
                } catch (SQLException ex) {
                    Logger.getLogger(FunctionDAO.class.getName()).log(Level.SEVERE, null, ex);
                }
            }if(connection != null){
                try {
                    connection.close();
                } catch (SQLException ex) {
                    Logger.getLogger(FunctionDAO.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
    
    public ArrayList<Function> getallComplexity(){
        ArrayList<Function> complexlist = new ArrayList<>();
        try {
            String sql = "select complexity_id from `function` group by complexity_id";
            PreparedStatement stm = connection.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();
            while(rs.next()){
                Function f = new Function();
                f.setComplexity_id(rs.getInt("complexity_id"));
                complexlist.add(f);
                
            }
        } catch (Exception e) {
        }
        return complexlist;
    }
    
    public void addFunction(Function f){
        String sql = "INSERT INTO `spm_project`.`function`\n" +
                        "(`team_id`,\n" +
                        "`function_name`,\n" +
                        "`feature_id`,\n" +
                        "`access_roles`,\n" +
                        "`description`,\n" +
                        "`complexity_id`,\n" +
                        "`email`,\n" +
                        "`priority`,\n" +
                        "`status`)\n" +
                        "VALUES\n" +
                        "(?,\n" +
                        "?,\n" +
                        "?,\n" +
                        "?,\n" +
                        "?,\n" +
                        "?,\n" +
                        "?,\n" +
                        "?,\n" +
                        "?);";
                PreparedStatement stm = null;
                try {
            stm = connection.prepareStatement(sql);
            stm.setInt(1, f.getTeam().getTeam_id());
            stm.setString(2, f.getFunction_name());
            stm.setInt(3, f.getFeature().getFeature_id());
            stm.setString(4, f.getAccess_roles());
            stm.setString(5, f.getDescription());
            stm.setInt(6, f.getComplexity_id());
            stm.setString(7, f.getAccount().getEmail());
            stm.setInt(8, f.getPriority());
            stm.setInt(9, f.getStatus());
            stm.executeUpdate();
        } catch (Exception e) {
        } finally {
                    if(stm != null){
                try {
                    stm.close();
                } catch (SQLException ex) {
                    Logger.getLogger(FunctionDAO.class.getName()).log(Level.SEVERE, null, ex);
                }
            }if(connection != null){
                try {
                    connection.close();
                } catch (SQLException ex) {
                    Logger.getLogger(FunctionDAO.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
}
