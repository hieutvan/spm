/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Account;
import model.Function;
import model.Issue;
import model.Milestone;
import model.Team;

/**
 *
 * @author admin
 */
public class IssueDAO extends DBContext {

    public ArrayList<Issue> getAllIssuebyTeam(String email) {
        ArrayList<Issue> issuelist = new ArrayList<>();
        try {
            String sql = "SELECT i.issue_id, a.displayname, i.assignee_email, i.issue_title, i.description, i.gitlab_id, i.gitlab_url, i.created_at, i.due_date,t.team_id, t.team_code, m.milestone_id, f.function_id, f.function_name, i.labels,i.issue_type, i.status\n"
                    + "FROM issue i inner join account a\n"
                    + "on i.assignee_email = a.email\n"
                    + "inner join team t on i.team_id = t.team_id\n"
                    + "inner join milestone m on i.milestone_id = m.milestone_id\n"
                    + "inner join `function` f on i.function_id = f.function_id\n"
                    + "inner join class_user cu on t.team_id = cu.team_id\n"
                    + "where cu.email = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, email);
            ResultSet rs = stm.executeQuery();
            while(rs.next()){
                Issue i = new Issue();
                Team t = new Team();
                Account a = new Account();
                Milestone m = new Milestone();
                Function f = new Function();
                i.setIssue_id(rs.getInt("issue_id"));
                a.setDisplayname(rs.getString("displayname"));
                a.setEmail(rs.getString("assignee_email"));
                i.setAccount(a);
                i.setIssue_title(rs.getString("issue_title"));
                i.setDescription(rs.getString("description"));
                i.setGitlab_id(rs.getInt("gitlab_id"));
                i.setGitlab_url(rs.getString("gitlab_url"));
                i.setCreated_at(rs.getDate("created_at"));
                i.setDue_date(rs.getDate("due_date"));
                t.setTeam_id(rs.getInt("team_id"));
                t.setTeam_code(rs.getString("team_code"));
                i.setTeam(t);
                m.setMilestone_id(rs.getInt("milestone_id"));
                i.setMilestone(m);
                f.setFunction_id(rs.getInt("function_id"));
                f.setFunction_name(rs.getString("function_name"));
                i.setFunction(f);
                i.setLabels(rs.getString("labels"));
                i.setIssue_type(rs.getString("issue_type"));
                i.setStatus(rs.getInt("status"));
                issuelist.add(i);
            }
        } catch (Exception e) {
        }
        return issuelist;
    }
    
        public ArrayList<Issue> getAllIssuebyTrainer(String email) {
        ArrayList<Issue> issuelist = new ArrayList<>();
        try {
            String sql = "SELECT i.issue_id, a.displayname, i.assignee_email, i.issue_title, i.description, i.gitlab_id, i.gitlab_url, i.created_at, i.due_date,t.team_id, t.team_code, m.milestone_id, f.function_id, f.function_name, i.labels,i.issue_type, i.status\n" +
                    "FROM issue i inner join account a\n" +
                    "on i.assignee_email = a.email\n" +
                    "inner join team t on i.team_id = t.team_id\n" +
                    "inner join milestone m on i.milestone_id = m.milestone_id\n" +
                    "inner join `function` f on i.function_id = f.function_id\n" +
                    "inner join class c on t.class_id = c.class_id\n" +
                    "where c.email = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, email);
            ResultSet rs = stm.executeQuery();
            while(rs.next()){
                Issue i = new Issue();
                Team t = new Team();
                Account a = new Account();
                Milestone m = new Milestone();
                Function f = new Function();
                i.setIssue_id(rs.getInt("issue_id"));
                a.setDisplayname(rs.getString("displayname"));
                a.setEmail(rs.getString("assignee_email"));
                i.setAccount(a);
                i.setIssue_title(rs.getString("issue_title"));
                i.setDescription(rs.getString("description"));
                i.setGitlab_id(rs.getInt("gitlab_id"));
                i.setGitlab_url(rs.getString("gitlab_url"));
                i.setCreated_at(rs.getDate("created_at"));
                i.setDue_date(rs.getDate("due_date"));
                t.setTeam_id(rs.getInt("team_id"));
                t.setTeam_code(rs.getString("team_code"));
                i.setTeam(t);
                m.setMilestone_id(rs.getInt("milestone_id"));
                i.setMilestone(m);
                f.setFunction_id(rs.getInt("function_id"));
                f.setFunction_name(rs.getString("function_name"));
                i.setFunction(f);
                i.setLabels(rs.getString("labels"));
                i.setIssue_type(rs.getString("issue_type"));
                i.setStatus(rs.getInt("status"));
                issuelist.add(i);
            }
        } catch (Exception e) {
        }
        return issuelist;
    }
    public void updateStatus(int issue_id, int status){
        String sql = "UPDATE `spm_project`.`issue`\n" +
                "SET\n" +
                "`status` = ?\n" +
                "WHERE `issue_id` = ?;";
        PreparedStatement stm = null;
        try {
            stm = connection.prepareStatement(sql);
            stm.setInt(1, status);
            stm.setInt(2, issue_id);
            stm.executeUpdate();
        } catch (Exception e) {
        } finally {
            if(stm != null){
                try {
                    stm.close();
                } catch (SQLException ex) {
                    Logger.getLogger(IssueDAO.class.getName()).log(Level.SEVERE, null, ex);
                }
            }if(connection != null){
                try {
                    connection.close();
                } catch (SQLException ex) {
                    Logger.getLogger(IssueDAO.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
    public Issue getIssueDetail(int issue_id){
        try {
            String sql = "SELECT i.issue_id, a.displayname, i.assignee_email, i.issue_title, i.description, i.gitlab_id, i.gitlab_url, i.created_at, i.due_date,t.team_id, t.team_code, m.milestone_id, f.function_id, f.function_name, i.labels,i.issue_type, i.status\n" +
                        "FROM issue i inner join account a\n" +
                        "on i.assignee_email = a.email\n" +
                        "inner join team t on i.team_id = t.team_id\n" +
                        "inner join milestone m on i.milestone_id = m.milestone_id\n" +
                        "inner join `function` f on i.function_id = f.function_id\n" +
                        "where i.issue_id = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, issue_id);
            ResultSet rs = stm.executeQuery();
            while(rs.next()){
                Issue i = new Issue();
                Team t = new Team();
                Account a = new Account();
                Milestone m = new Milestone();
                Function f = new Function();
                i.setIssue_id(rs.getInt("issue_id"));
                a.setDisplayname(rs.getString("displayname"));
                a.setEmail(rs.getString("assignee_email"));
                i.setAccount(a);
                i.setIssue_title(rs.getString("issue_title"));
                i.setDescription(rs.getString("description"));
                i.setGitlab_id(rs.getInt("gitlab_id"));
                i.setGitlab_url(rs.getString("gitlab_url"));
                i.setCreated_at(rs.getDate("created_at"));
                i.setDue_date(rs.getDate("due_date"));
                t.setTeam_id(rs.getInt("team_id"));
                t.setTeam_code(rs.getString("team_code"));
                i.setTeam(t);
                m.setMilestone_id(rs.getInt("milestone_id"));
                i.setMilestone(m);
                f.setFunction_id(rs.getInt("function_id"));
                f.setFunction_name(rs.getString("function_name"));
                i.setFunction(f);
                i.setLabels(rs.getString("labels"));
                i.setIssue_type(rs.getString("issue_type"));
                i.setStatus(rs.getInt("status"));
                return i;
            }
        } catch (Exception e) {
        }
        return null;
    }
    public void addIssue(Issue i){
        String sql = "INSERT INTO `spm_project`.`issue`\n" +
                        "(`assignee_email`,\n" +
                        "`issue_title`,\n" +
                        "`description`,\n" +
                        "`gitlab_id`,\n" +
                        "`gitlab_url`,\n" +
                        "`created_at`,\n" +
                        "`due_date`,\n" +
                        "`team_id`,\n" +
                        "`milestone_id`,\n" +
                        "`function_id`,\n" +
                        "`labels`,\n" +
                        "`issue_type`,\n" +
                        "`status`)\n" +
                        "VALUES\n" +
                        "(?,\n" +
                        "?,\n" +
                        "?,\n" +
                        "?,\n" +
                        "?,\n" +
                        "?,\n" +
                        "?,\n" +
                        "?,\n" +
                        "?,\n" +
                        "?,\n" +
                        "?,\n" +
                        "?,\n" +
                        "?);";
        PreparedStatement stm = null;
        try {
            stm = connection.prepareStatement(sql);
            stm.setString(1, i.getAccount().getEmail());
            stm.setString(2, i.getIssue_title());
            stm.setString(3, i.getDescription());
            stm.setInt(4, i.getGitlab_id());
            stm.setString(5, i.getGitlab_url());
            stm.setDate(6, i.getCreated_at());
            stm.setDate(7, i.getDue_date());
            stm.setInt(8, i.getTeam().getTeam_id());
            stm.setInt(9, i.getMilestone().getMilestone_id());
            stm.setInt(10, i.getFunction().getFunction_id());
            stm.setString(11, i.getLabels());
            stm.setString(12, i.getIssue_type());
            stm.setInt(13, i.getStatus());
            stm.executeUpdate();
        } catch (Exception e) {
        } finally {
            if(stm != null){
                try {
                    stm.close();
                } catch (SQLException ex) {
                    Logger.getLogger(IssueDAO.class.getName()).log(Level.SEVERE, null, ex);
                }
            }if(connection != null){
                try {
                    connection.close();
                } catch (SQLException ex) {
                    Logger.getLogger(IssueDAO.class.getName()).log(Level.SEVERE, null, ex);
                }
        }
        }
    }
    
    public void updateIssue(Issue i){
        String sql = "UPDATE `spm_project`.`issue`\n" +
                        "SET\n" +
                        "`assignee_email` = ?,\n" +
                        "`issue_title` = ?,\n" +
                        "`description` = ?,\n" +
                        "`gitlab_id` = ?,\n" +
                        "`gitlab_url` = ?,\n" +
                        "`created_at` = ?,\n" +
                        "`due_date` = ?,\n" +
                        "`team_id` = ?,\n" +
                        "`milestone_id` = ?,\n" +
                        "`function_id` = ?,\n" +
                        "`labels` = ?,\n" +
                        "`issue_type` = ?,\n" +
                        "`status` = ?\n" +
                        "WHERE `issue_id` = ?;";
        PreparedStatement stm = null;
        try {
            stm = connection.prepareStatement(sql);
            stm.setString(1, i.getAccount().getEmail());
            stm.setString(2, i.getIssue_title());
            stm.setString(3, i.getDescription());
            stm.setInt(4, i.getGitlab_id());
            stm.setString(5, i.getGitlab_url());
            stm.setDate(6, i.getCreated_at());
            stm.setDate(7, i.getDue_date());
            stm.setInt(8, i.getTeam().getTeam_id());
            stm.setInt(9, i.getMilestone().getMilestone_id());
            stm.setInt(10, i.getFunction().getFunction_id());
            stm.setString(11, i.getLabels());
            stm.setInt(12, i.getStatus());
            stm.setString(13, i.getIssue_type());
            stm.setInt(14, i.getIssue_id());
            stm.executeUpdate();
        } catch (Exception e) {
        } finally {
            if(stm != null){
                try {
                    stm.close();
                } catch (SQLException ex) {
                    Logger.getLogger(IssueDAO.class.getName()).log(Level.SEVERE, null, ex);
                }
            }if(connection != null){
                try {
                    connection.close();
                } catch (SQLException ex) {
                    Logger.getLogger(IssueDAO.class.getName()).log(Level.SEVERE, null, ex);
                }
        }
        }
    }
    
    public ArrayList<Issue> getAllIssuetypebyUser(String email){
        ArrayList<Issue> issuetypelist = new ArrayList<>();
        try {
            String sql = "select i.issue_type from issue i inner join class_user c \n" +
                        "on i.team_id = c.team_id\n" +
                        "where c.email = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1,email);
            ResultSet rs = stm.executeQuery();
            while(rs.next()){
                Issue i = new Issue();
                i.setIssue_type(rs.getString("issue_type"));
                issuetypelist.add(i);
            }
        } catch (Exception e) {
        }
        return issuetypelist;
    }
        public ArrayList<Issue> getAllIssuetypebyTrainer(String email){
        ArrayList<Issue> issuetypelist = new ArrayList<>();
        try {
            String sql = "select i.issue_type from issue i inner join team t \n" +
                        "on i.team_id = t.team_id \n" +
                        "inner join class c on t.class_id = c.class_id\n" +
                        "where c.email = ? group by i.issue_type";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1,email);
            ResultSet rs = stm.executeQuery();
            while(rs.next()){
                Issue i = new Issue();
                i.setIssue_type(rs.getString("issue_type"));
                issuetypelist.add(i);
            }
        } catch (Exception e) {
        }
        return issuetypelist;
    }
}
