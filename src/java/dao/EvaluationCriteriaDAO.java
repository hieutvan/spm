package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.EvaluationCriteria;
import model.Iteration;
import model.Subject;

public class EvaluationCriteriaDAO extends DBContext {

    public ArrayList<EvaluationCriteria> getallcriteria() {
        ArrayList<EvaluationCriteria> eclist = new ArrayList<>();
        
        try {
            String sql = "SELECT ec.criteria_id, i.iteration_id, i.iteration_name, s.subject_code, s.subject_name, ec.evaluation_weight, ec.team_evaluation, ec.criteria_order, ec.max_loc, ec.status\n" +
                            "FROM evaluation_criteria ec inner join iteration i on \n" +
                            "ec.iteration_id = i.iteration_id\n" +
                            "inner join subject s on i.subject_id = s.subject_id\n" +
                            "order by ec.criteria_order ASC";
            PreparedStatement stm = connection.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                EvaluationCriteria ec = new EvaluationCriteria();
                Iteration i = new Iteration();
                Subject s = new Subject();
                ec.setCriteria_id(rs.getInt("criteria_id"));
                i.setIteration_id(rs.getInt("iteration_id"));
                i.setIteration_name(rs.getString("iteration_name"));
                s.setSubject_code(rs.getString("subject_code"));
                s.setSubject_name(rs.getString("subject_name"));
                i.setSubject(s);
                ec.setIteration(i);
                ec.setEvaluation_weight(rs.getInt("evaluation_weight"));
                ec.setTeam_evaluation(rs.getInt("team_evaluation"));
                ec.setCriteria_order(rs.getInt("criteria_order"));
                ec.setMax_loc(rs.getInt("max_loc"));
                ec.setStatus(rs.getInt("status"));
                eclist.add(ec);
            }
        } catch (SQLException ex) {
            Logger.getLogger(EvaluationCriteriaDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return eclist;
    }

    public void addcriteria(EvaluationCriteria ec) {
        String sql = "INSERT INTO `spm_project`.`evaluation_criteria`\n" +
                        "(`iteration_id`,\n" +
                        "`evaluation_weight`,\n" +
                        "`team_evaluation`,\n" +
                        "`criteria_order`,\n" +
                        "`max_loc`,\n" +
                        "`status`)\n" +
                        "VALUES\n" +
                        "(?,\n" +
                        "?,\n" +
                        "?,\n" +
                        "?,\n" +
                        "?,\n" +
                        "?);";
        PreparedStatement stm = null;
        try {
            stm = connection.prepareStatement(sql);
            stm.setInt(1, ec.getIteration().getIteration_id());
            stm.setInt(2, ec.getEvaluation_weight());
            stm.setInt(3, ec.getTeam_evaluation());
            stm.setInt(4, ec.getCriteria_order());
            stm.setInt(5, ec.getMax_loc());
            stm.setInt(6, ec.getStatus());
            stm.executeUpdate();
        } catch (Exception e) {
        } finally {
            if(stm != null){
                try {
                    stm.close();
                } catch (SQLException ex) {
                    Logger.getLogger(EvaluationCriteriaDAO.class.getName()).log(Level.SEVERE, null, ex);
                }
            }if(connection != null){
                try {
                    connection.close();
                } catch (SQLException ex) {
                    Logger.getLogger(EvaluationCriteriaDAO.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        
    }
    
    public EvaluationCriteria getEC(int criteria_id) {
        try {
            String sql = "SELECT ec.criteria_id, ec.iteration_id, i.iteration_name, s.subject_name, ec.evaluation_weight, ec.team_evaluation, ec.criteria_order, ec.max_loc, ec.status FROM evaluation_criteria ec inner join iteration i \n" +
                            "on ec.iteration_id = i.iteration_id\n" +
                            "inner join subject s on i.subject_id = s.subject_id\n" +
                            "where ec.criteria_id = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, criteria_id);
            ResultSet rs= stm.executeQuery();
            if(rs.next()){
                EvaluationCriteria ec = new EvaluationCriteria();
                Subject s = new Subject();
                ec.setCriteria_id(rs.getInt("criteria_id"));
                Iteration iter = new Iteration();
                iter.setIteration_id(rs.getInt("iteration_id"));
                iter.setIteration_name(rs.getString("iteration_name"));
                s.setSubject_name(rs.getString("subject_name"));
                iter.setSubject(s);
                ec.setIteration(iter);
                ec.setEvaluation_weight(rs.getInt("evaluation_weight"));
                ec.setTeam_evaluation(rs.getInt("team_evaluation"));
                ec.setCriteria_order(rs.getInt("criteria_order"));
                ec.setMax_loc(rs.getInt("max_loc"));
                ec.setStatus(rs.getInt("status"));
                return ec;
            }
        } catch (SQLException ex) {
            Logger.getLogger(EvaluationCriteriaDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public void updatecrit(EvaluationCriteria ec) {
        String sql = "UPDATE `spm_project`.`evaluation_criteria`\n" +
                "SET\n" +
                "`evaluation_weight` = ?,\n" +
                "`team_evaluation` = ?,\n" +
                "`criteria_order` = ?,\n" +
                "`max_loc` = ?,\n" +
                "`status` = ?\n" +
                "WHERE `criteria_id` =?;";
        PreparedStatement stm = null;
        try {
            stm = connection.prepareStatement(sql);
            stm.setInt(1, ec.getEvaluation_weight());
            stm.setInt(2, ec.getTeam_evaluation());
            stm.setInt(3, ec.getCriteria_order());
            stm.setInt(4, ec.getMax_loc());
            stm.setInt(5, ec.getStatus());
            stm.setInt(6, ec.getCriteria_id());
            stm.executeUpdate();
        } catch (Exception e) {
        } finally {
            if(stm != null){
                try {
                    stm.close();
                } catch (SQLException ex) {
                    Logger.getLogger(EvaluationCriteriaDAO.class.getName()).log(Level.SEVERE, null, ex);
                }
            }if(connection != null){
                try {
                    connection.close();
                } catch (SQLException ex) {
                    Logger.getLogger(EvaluationCriteriaDAO.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
    
    public ArrayList<EvaluationCriteria> getbyAuthor(String email) {
        ArrayList<EvaluationCriteria> eclist = new ArrayList<>();
        try {
            String sql = "SELECT ec.criteria_id, i.iteration_id, i.iteration_name, s.subject_code, s.subject_name, ec.evaluation_weight, ec.team_evaluation, ec.criteria_order, ec.max_loc, ec.status\n" +
                            "FROM evaluation_criteria ec inner join iteration i on \n" +
                            "ec.iteration_id = i.iteration_id\n" +
                            "inner join subject s on i.subject_id = s.subject_id\n" +
                            "where s.email = ? order by ec.criteria_order ASC\n";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, email);
            ResultSet rs = stm.executeQuery();
            while(rs.next()){   
                EvaluationCriteria ec = new EvaluationCriteria();
                Iteration i = new Iteration();
                Subject s = new Subject();
                ec.setCriteria_id(rs.getInt("criteria_id"));
                i.setIteration_id(rs.getInt("iteration_id"));
                i.setIteration_name(rs.getString("iteration_name"));
                s.setSubject_code(rs.getString("subject_code"));
                s.setSubject_name(rs.getString("subject_name"));
                i.setSubject(s);
                ec.setIteration(i);
                ec.setEvaluation_weight(rs.getInt("evaluation_weight"));
                ec.setTeam_evaluation(rs.getInt("team_evaluation"));
                ec.setCriteria_order(rs.getInt("criteria_order"));
                ec.setMax_loc(rs.getInt("max_loc"));
                ec.setStatus(rs.getInt("status"));
                eclist.add(ec);
            }
        } catch (SQLException ex) {
            Logger.getLogger(EvaluationCriteriaDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return eclist;
    }
   
    public void updateteamevaluate(EvaluationCriteria ec) {
        String sql = "UPDATE `spm_project`.`evaluation_criteria`\n" +
                    "SET\n" +
                    "`team_evaluation` = ?\n" +
                    "WHERE `criteria_id` = ?";
        PreparedStatement stm = null;
        try {
            stm = connection.prepareStatement(sql);
            stm.setInt(1, ec.getTeam_evaluation());
            stm.setInt(2, ec.getCriteria_id());
            stm.executeUpdate();
        } catch (Exception e) {
        } finally {
            if(stm != null){
                try {
                    stm.close();
                } catch (SQLException ex) {
                    Logger.getLogger(EvaluationCriteriaDAO.class.getName()).log(Level.SEVERE, null, ex);
                }
            }if(connection != null){
                try {
                    connection.close();
                } catch (SQLException ex) {
                    Logger.getLogger(EvaluationCriteriaDAO.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
    
    public void updatestatus(EvaluationCriteria ec){
        String sql = "UPDATE `spm_project`.`evaluation_criteria`\n" +
                    "SET\n" +
                    "`status` = ?\n" +
                    "WHERE `criteria_id` = ?";
        PreparedStatement stm = null;
        try {
            stm = connection.prepareStatement(sql);
            stm.setInt(1, ec.getStatus());
            stm.setInt(2, ec.getCriteria_id());
            stm.executeUpdate();
        } catch (Exception e) {
        } finally {
            if(stm != null){
                try {
                    stm.close();
                } catch (SQLException ex) {
                    Logger.getLogger(EvaluationCriteriaDAO.class.getName()).log(Level.SEVERE, null, ex);
                }
            }if(connection != null){
                try {
                    connection.close();
                } catch (SQLException ex) {
                    Logger.getLogger(EvaluationCriteriaDAO.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
   
    public int totalLoc(int iteration_id){
        try {
            String sql = "select sum(ec.max_loc) as totalLOC  from evaluation_criteria ec inner join iteration i on\n" +
                            "ec.iteration_id = i.iteration_id\n" +
                            "where i.iteration_id = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, iteration_id);
            ResultSet rs = stm.executeQuery();
            while(rs.next()){
                return rs.getInt("totalLOC");
            }
        } catch (SQLException ex) {
            Logger.getLogger(EvaluationCriteriaDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return -1;
    }
        
    public ArrayList<EvaluationCriteria> getallECwithIterbyStudent(String email){
        ArrayList<EvaluationCriteria> eclist = new ArrayList<>();
        try {
            String sql = "select i.iteration_id, i.iteration_name, ec.evaluation_weight, ec.max_loc from iteration i inner join subject s on \n" +
                            "i.subject_id = s.subject_id\n" +
                            "inner join class c on s.subject_id = c.subject_id\n" +
                            "inner join team t on c.class_id = t.class_id\n" +
                            "inner join class_user cu on t.team_id = cu.team_id\n" +
                            "inner join evaluation_criteria ec on i.iteration_id = ec.iteration_id\n" +
                            "where cu.email = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, email);
            ResultSet rs = stm.executeQuery();
            while(rs.next()){
                EvaluationCriteria ec = new EvaluationCriteria();
                Iteration i = new Iteration();
                i.setIteration_id(rs.getInt("iteration_id"));
                i.setIteration_name(rs.getString("iteration_name"));
                ec.setIteration(i);
                ec.setEvaluation_weight(rs.getInt("evaluation_weight"));
                ec.setMax_loc(rs.getInt("max_loc"));
                eclist.add(ec);
            }
        } catch (Exception e) {
        }
        return eclist;
    }

    public ArrayList<EvaluationCriteria> getallECwithIterbyTrainer(String email){
        ArrayList<EvaluationCriteria> eclist = new ArrayList<>();
        try {
            String sql = "select distinct  i.iteration_id, i.iteration_name, ec.evaluation_weight, ec.max_loc from iteration i inner join subject s\n" +
                                "on i.subject_id = s.subject_id\n" +
                                "inner join class c on s.subject_id = c.subject_id\n" +
                                "inner join evaluation_criteria ec on i.iteration_id = ec.iteration_id\n" +
                                "where c.email = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, email);
            ResultSet rs = stm.executeQuery();
            while(rs.next()){
                EvaluationCriteria ec = new EvaluationCriteria();
                Iteration i = new Iteration();
                i.setIteration_id(rs.getInt("iteration_id"));
                i.setIteration_name(rs.getString("iteration_name"));
                ec.setIteration(i);
                ec.setEvaluation_weight(rs.getInt("evaluation_weight"));
                ec.setMax_loc(rs.getInt("max_loc"));
                eclist.add(ec);
            }
        } catch (Exception e) {
        }
        return eclist;
    }

    public EvaluationCriteria getECbyIter(int iteration_id) {
        try {
            String sql = "select * from evaluation_criteria where iteration_id = ? and team_evaluation = 2";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, iteration_id);
            ResultSet rs = stm.executeQuery();
            
            if (rs.next()) {
                IterationDAO iDAO = new IterationDAO();
                
                EvaluationCriteria ec = new EvaluationCriteria(
                        rs.getInt("criteria_id"),
                        iDAO.getiteration(rs.getInt("iteration_id")),
                        rs.getInt("evaluation_weight"),
                        rs.getInt("team_evaluation"),
                        rs.getInt("criteria_order"),
                        rs.getInt("max_loc"),
                        rs.getInt("status")
                );
                
                return ec;
            }
        } catch (SQLException e) {
            e.printStackTrace(System.err);
            System.err.println("SQLState: " + e.getSQLState());
            System.err.println("Error Code: " + e.getErrorCode());
            System.err.println("Message: " + e.getMessage());
        }
        
        return null;
    }   
}
