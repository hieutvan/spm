package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Account;
import model.Feature;
import model.Function;
import model.Milestone;
import model.Team;
import model.Tracking;

public class TrackingDAO extends DBContext {
    public ArrayList<Tracking> getAllTrackingbyStudent(String email) {
        ArrayList<Tracking> trackinglist = new ArrayList<>();
        
        try {
            String sql = "SELECT tr.*, t.team_id, t.team_code, m.milestone_id, f.function_id, f.function_name,f.status, a2.displayname \n" +
                                "FROM tracking tr inner join team t on \n" +
                                "tr.team_id = t.team_id\n" +
                                "inner join milestone m on tr.milestone_id = m.milestone_id\n" +
                                "inner join `function` f on tr.function_id = f.function_id\n" +
                                "inner join class c on t.class_id = c.class_id\n" +
                                "inner join account a2 on tr.assignee_email = a2.email\n" +
                                "inner join class_user cu on t.team_id = cu.team_id\n" +
                                "where cu.email = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, email);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Tracking tracking = new Tracking();
                Team t = new Team();
                Milestone m = new Milestone();
                Function f = new Function();
                Account a1 = new Account();
                Account a2 = new Account();
                tracking.setTracking_id(rs.getInt("tracking_id"));
                t.setTeam_id(rs.getInt("team_id"));
                t.setTeam_code(rs.getString("team_code"));
                tracking.setTeam(t);
                m.setMilestone_id(rs.getInt("milestone_id"));
                tracking.setMilestone(m);
                f.setFunction_id(rs.getInt("function_id"));
                f.setFunction_name(rs.getString("function_name"));
                f.setStatus(rs.getInt("status"));
                tracking.setFunction(f);
                a1.setEmail(rs.getString("assigner_email"));
                a2.setEmail(rs.getString("assignee_email"));
                a2.setDisplayname(rs.getString("displayname"));
                tracking.setAssigner_email(a1);
                tracking.setAssignee_email(a2);
                tracking.setTracking_note(rs.getString("tracking_note"));
                tracking.setUpdates(rs.getString("updates"));
                tracking.setStatus(rs.getInt("status"));
                trackinglist.add(tracking);
            }
        } catch (SQLException ex) {
            Logger.getLogger(TrackingDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return trackinglist;
    }
    
    public ArrayList<Tracking> getAllTrackingsbyTrainer(String email) {
        ArrayList<Tracking> trackinglist = new ArrayList<>();

        try {
            String sql = "SELECT tr.tracking_id, t.team_id, t.team_code, m.milestone_id, f.function_id, f.function_name, tr.assigner_email, tr.assignee_email,a2.displayname, \n" +
                            "tr.tracking_note, tr.updates, tr.status\n" +
                            "FROM tracking tr inner join team t on \n" +
                            "tr.team_id = t.team_id\n" +
                            "inner join milestone m on tr.milestone_id = m.milestone_id\n" +
                            "inner join `function` f on tr.function_id = f.function_id\n" +
                            "inner join class c on t.class_id = c.class_id\n" +
                            "inner join account a1 on tr.assigner_email = a1.email\n" +
                            "inner join account a2 on tr.assignee_email = a2.email\n" +
                            "where c.email = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, email);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Tracking tracking = new Tracking();
                Team t = new Team();
                Milestone m = new Milestone();
                Function f = new Function();
                Account a1 = new Account();
                Account a2 = new Account();
                tracking.setTracking_id(rs.getInt("tracking_id"));
                t.setTeam_id(rs.getInt("team_id"));
                t.setTeam_code(rs.getString("team_code"));
                tracking.setTeam(t);
                m.setMilestone_id(rs.getInt("milestone_id"));
                tracking.setMilestone(m);
                f.setFunction_id(rs.getInt("function_id"));
                f.setFunction_name(rs.getString("function_name"));
                tracking.setFunction(f);
                a1.setEmail(rs.getString("assigner_email"));
                a2.setEmail(rs.getString("assignee_email"));
                a2.setDisplayname(rs.getString("displayname"));
                tracking.setAssigner_email(a1);
                tracking.setAssignee_email(a2);
                tracking.setTracking_note(rs.getString("tracking_note"));
                tracking.setUpdates(rs.getString("updates"));
                tracking.setStatus(rs.getInt("status"));
                trackinglist.add(tracking);
            }
        } catch (SQLException ex) {
            Logger.getLogger(TrackingDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return trackinglist;
    }
       
    public Tracking getTrackingDetails(int tracking_id){
        try {
            String sql = "SELECT tr.*, t.team_id, t.team_code, m.milestone_id, f.function_id, f.function_name, a1.displayname\n" +
                            "FROM tracking tr inner join team t on \n" +
                            "tr.team_id = t.team_id\n" +
                            "inner join milestone m on tr.milestone_id = m.milestone_id\n" +
                            "inner join `function` f on tr.function_id = f.function_id\n" +
                            "inner join account a1 on tr.assignee_email = a1.email\n" +
                            "where tr.tracking_id = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, tracking_id);
            ResultSet rs = stm.executeQuery();
            if(rs.next()){
            Tracking tracking = new Tracking();
            Team t = new Team();
            Milestone m = new Milestone();
            Function f = new Function();
            Account a1 = new Account();
            Account a2 = new Account();
            tracking.setTracking_id(rs.getInt("tracking_id"));
            t.setTeam_id(rs.getInt("team_id"));
            t.setTeam_code(rs.getString("team_code"));
            tracking.setTeam(t);
            m.setMilestone_id(rs.getInt("milestone_id"));
            tracking.setMilestone(m);
            f.setFunction_id(rs.getInt("function_id"));
            f.setFunction_name(rs.getString("function_name"));
            tracking.setFunction(f);
            a1.setEmail(rs.getString("assigner_email"));
            a2.setEmail(rs.getString("assignee_email"));
            a2.setDisplayname(rs.getString("displayname"));
            tracking.setAssigner_email(a1);
            tracking.setAssignee_email(a2);
            tracking.setTracking_note(rs.getString("tracking_note"));
            tracking.setUpdates(rs.getString("updates"));
            tracking.setStatus(rs.getInt("status"));
            return tracking;
            }
        } catch (Exception e) {
        }
        return null;
    }

    public void updateTracking(Tracking tr){
        String sql = "UPDATE `spm_project`.`tracking`\n" +
                        "SET\n" +
                        "`team_id` = ?,\n" +
                        "`milestone_id` = ?,\n" +
                        "`function_id` = ?,\n" +
                        "`assigner_email` = ?,\n" +
                        "`assignee_email` = ?,\n" +
                        "`tracking_note` = ?,\n" +
                        "`updates` = ?,\n" +
                        "`status` = ?\n" +
                        "WHERE `tracking_id` = ?;";
        PreparedStatement stm = null;
        try {
            stm = connection.prepareStatement(sql);
            stm.setInt(1, tr.getTeam().getTeam_id());
            stm.setInt(2, tr.getMilestone().getMilestone_id());
            stm.setInt(3, tr.getFunction().getFunction_id());
            stm.setString(4, tr.getAssigner_email().getEmail());
            stm.setString(5, tr.getAssignee_email().getEmail());
            stm.setString(6, tr.getTracking_note());
            stm.setString(7, tr.getUpdates());
            stm.setInt(8, tr.getStatus());
            stm.setInt(9, tr.getTracking_id());
            stm.executeUpdate();
        } catch (Exception e) {
        } finally {
            if(stm != null){
                try {
                    stm.close();
                } catch (SQLException ex) {
                    Logger.getLogger(TrackingDAO.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            if(connection != null){
                try {
                    connection.close();
                } catch (SQLException ex) {
                    Logger.getLogger(TrackingDAO.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    public void updateStatus(int tracking_id, int status){
        String sql = "UPDATE `spm_project`.`tracking`\n" +
                    "SET\n" +
                    "`status` = ?\n" +
                    "WHERE `tracking_id` = ?;";
        PreparedStatement stm = null;
        try {
            stm = connection.prepareStatement(sql);
            stm.setInt(1, status);
            stm.setInt(2, tracking_id);
            stm.executeUpdate();
        } catch (Exception e) {
        } finally {
            if(stm != null){
                try {
                    stm.close();
                } catch (SQLException ex) {
                    Logger.getLogger(TrackingDAO.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            if(connection != null){
                try {
                    connection.close();
                } catch (SQLException ex) {
                    Logger.getLogger(TrackingDAO.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    public Tracking getFunctionbyTracking(int tracking_id){
    try {
        String sql = "select f.*, t.tracking_id from `function` f inner join tracking t\n" +
                        "on f.function_id = t.tracking_id\n" +
                        "where t.tracking_id = ?";
        PreparedStatement stm = connection.prepareStatement(sql);
        stm.setInt(1, tracking_id);
        ResultSet rs = stm.executeQuery();
        while(rs.next()){
            Tracking t = new Tracking();
            Function f = new Function();
            f.setFunction_id(rs.getInt("function_id"));
            f.setTeam(new Team(rs.getInt("team_id")));
            f.setFunction_name(rs.getString("function_name"));
            f.setFeature(new Feature(rs.getInt("feature_id")));
            f.setAccess_roles(rs.getString("access_roles"));
            f.setDescription(rs.getString("description"));
            f.setComplexity_id(rs.getInt("complexity_id"));
            f.setAccount(new Account(rs.getString("email")));
            f.setPriority(rs.getInt("priority"));
            f.setStatus(rs.getInt("status"));
            t.setTracking_id(rs.getInt("tracking_id"));
            t.setFunction(f);
            return t;
        }
    } catch (Exception e) {
    }
    return null;
}
}
