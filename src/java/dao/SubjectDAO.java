/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Account;
import model.Subject;

/**
 *
 * @author admin
 */
public class SubjectDAO extends DBContext {

    public ArrayList<Subject> getallsubject() {
        ArrayList<Subject> subjectlist = new ArrayList<>();
        try {
            String sql = "select s.subject_id, s.subject_code, s.subject_name, s.email, a.displayname, s.status from subject s inner join account a on \n" +
                            "s.email = a.email";
            PreparedStatement stm = connection.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Subject s = new Subject();
                Account a = new Account();
                s.setSubject_id(rs.getInt("subject_id"));
                s.setSubject_code(rs.getString("subject_code"));
                s.setSubject_name(rs.getString("subject_name"));
                s.setStatus(rs.getInt("status"));
                a.setEmail(rs.getString("email"));
                a.setDisplayname(rs.getString("displayname"));
                s.setEmail(a);
                subjectlist.add(s);
            }
        } catch (Exception e) {
        }
        return subjectlist;
    }
    
        
    public ArrayList<Subject> getSubjectbyAuthor(String email){
        ArrayList<Subject> subjectlist = new ArrayList<>();
        try {
            String sql = "SELECT * FROM spm_project.subject where email = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, email);
            ResultSet rs = stm.executeQuery();
            while(rs.next()){
                Subject s = new Subject();
                Account a = new Account();
                s.setSubject_id(rs.getInt("subject_id"));
                s.setSubject_code(rs.getString("subject_code"));
                s.setSubject_name(rs.getString("subject_name"));
                s.setStatus(rs.getInt("status"));
                a.setEmail(rs.getString("email"));
                s.setEmail(a);
                subjectlist.add(s);
            }
        } catch (Exception e) {
        }
        return subjectlist;
    }
    public Subject getSubjectdetail(String code) {
        try {
            String sql = "SELECT * FROM `subject` WHERE `subject_code` = ?;";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, code);
            ResultSet rs = stm.executeQuery();
            if (rs.next()) {
                Subject s = new Subject();
                s.setSubject_id(rs.getInt("subject_id"));
                s.setSubject_code(rs.getString("subject_code"));
                s.setSubject_name(rs.getString("subject_name"));
                s.setStatus(rs.getInt("status"));
                Account a = new Account();
                a.setEmail(rs.getString("email"));
                s.setEmail(a);                
                return s;
            }
        } catch (Exception e) {
        }
        return null;
    }
    
    public Subject getSubject(int subject_id){
        try {
            String sql = "SELECT * FROM `subject` WHERE `subject_id` = ?;";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, subject_id);
            ResultSet rs = stm.executeQuery();
            if(rs.next()){
                Subject s = new Subject();
                s.setSubject_id(rs.getInt("subject_id"));
                s.setSubject_code(rs.getString("subject_code"));
                s.setSubject_name(rs.getString("subject_name"));
                s.setStatus(rs.getInt("status"));
                Account a = new Account();
                a.setEmail(rs.getString("email"));
                s.setEmail(a);                
                return s;
            }
        } catch (Exception e) {
        }
        return null;
    }
    
    public void addsubject(Subject s){
        String sql = "INSERT INTO `spm_project`.`subject`\n" +
                    "(`subject_code`,\n" +
                    "`subject_name`,\n" +
                    "`email`,\n" +
                    "`status`)\n" +
                    "VALUES\n" +
                    "(?,\n" +
                    "?,\n" +
                    "?,\n" +
                    "?);";
        
        PreparedStatement stm = null;
        try {
            stm = connection.prepareStatement(sql);
            stm.setString(1, s.getSubject_code());
            stm.setString(2, s.getSubject_name());
            stm.setString(3, s.getEmail().getEmail());
            stm.setInt(4, s.getStatus());
            stm.executeUpdate();
        } catch (Exception e) {
        }finally{
            if(stm!= null){
                try {
                    stm.close();
                } catch (SQLException ex) {
                    Logger.getLogger(SubjectDAO.class.getName()).log(Level.SEVERE, null, ex);
                }
            }if(connection != null){
                try {
                    connection.close();
                } catch (SQLException ex) {
                    Logger.getLogger(SubjectDAO.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
    
    
    public void updatesubject(Subject s){
    String sql = "UPDATE `spm_project`.`subject`\n" +
                "SET \n" +
                "`subject_code` = ?,\n" +
                "`subject_name` = ?,\n" +
                "`email` = ?,\n" +
                "`status` = ?\n"+
                "where `subject_id` = ?;";
    PreparedStatement stm = null;
        try {
            stm = connection.prepareStatement(sql);
            stm.setString(1, s.getSubject_code());
            stm.setString(2, s.getSubject_name());
            stm.setString(3, s.getEmail().getEmail());
            stm.setInt(4, s.getStatus());
            stm.setInt(5, s.getSubject_id());
            stm.executeUpdate();
        } catch (Exception e) {
        }finally{
            if(stm != null){
                try {
                    stm.close();
                } catch (SQLException ex) {
                    Logger.getLogger(SubjectDAO.class.getName()).log(Level.SEVERE, null, ex);
                }
            }if(connection != null){
                try {
                    connection.close();
                } catch (SQLException ex) {
                    Logger.getLogger(SubjectDAO.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
    
    public void deletesubject(Subject s){
    String sql = "DELETE FROM `spm_project`.`subject`\n" +
                        "WHERE `subject_code` = ?;";
    PreparedStatement stm = null;
        try {
            stm = connection.prepareStatement(sql);
            stm.setString(1, s.getSubject_code());
            stm.executeUpdate();
        } catch (Exception e) {
        }finally{
            if(stm!= null){
                try {
                    stm.close();
                } catch (SQLException ex) {
                    Logger.getLogger(SubjectDAO.class.getName()).log(Level.SEVERE, null, ex);
                }
            }if(connection != null){
                try {
                    connection.close();
                } catch (SQLException ex) {
                    Logger.getLogger(SubjectDAO.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
    }
    }
    
    public void updatesubjectstatus(int status, int subject_id){
        String sql = "UPDATE `spm_project`.`subject`\n" +
                    "SET\n" +
                    "`status` = ?\n" +
                    "WHERE `subject_id` = ?;";
        PreparedStatement stm = null;
        try {
            stm = connection.prepareStatement(sql);
            stm.setInt(1, status);
            stm.setInt(2, subject_id);
            stm.executeUpdate();
        } catch (Exception e) {
        }finally{
            if(stm!= null){
                try {
                    stm.close();
                } catch (SQLException ex) {
                    Logger.getLogger(SubjectDAO.class.getName()).log(Level.SEVERE, null, ex);
                }
            }if(connection!= null){
                try {
                    connection.close();
                } catch (SQLException ex) {
                    Logger.getLogger(SubjectDAO.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
    public Subject getsubjectbytracking(int tracking_id){
        try {
            String sql = "select s.subject_id, s.subject_code, s.subject_name from tracking t inner join milestone m \n" +
                            "on t.milestone_id = m.milestone_id\n" +
                            "inner join class c on m.class_id = c.class_id\n" +
                            "inner join subject s on c.subject_id = s.subject_id\n" +
                            "where t.tracking_id = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, tracking_id);
            ResultSet rs = stm.executeQuery();
            if(rs.next()){
                Subject s = new Subject();
                s.setSubject_id(rs.getInt("subject_id"));
                s.setSubject_code(rs.getString("subject_code"));
                s.setSubject_name(rs.getString("subject_name"));
                return s;
            }
                    
        } catch (Exception e) {
        }
        return null;
    }
    public ArrayList<Subject> getallSubjectbyClass(String email){
        ArrayList<Subject> sublist = new ArrayList<>();
        try {
            String sql = "select s.subject_id, s.subject_code, s.subject_name from class c inner join subject s \n" +
                            "on c.subject_id = s.subject_id\n" +
                            "where c.email = ? group by s.subject_id, s.subject_code, s.subject_name";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, email);
            ResultSet rs = stm.executeQuery();
            while(rs.next()){
                Subject s = new Subject();
                s.setSubject_id(rs.getInt("subject_id"));
                s.setSubject_code(rs.getString("subject_code"));
                s.setSubject_name(rs.getString("subject_name"));
                sublist.add(s);
            }
        } catch (Exception e) {
        }
        return sublist;
    }

}
