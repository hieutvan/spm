package controller;

import dao.AccountDAO;
import dao.Class_UserDAO;
import dao.EvaluationCriteriaDAO;
import dao.IterationDAO;
import dao.IterationEvaluationDAO;
import dao.LOCEvaluationDAO;
import dao.MemberEvaluationDAO;
import dao.SubjectDAO;
import dao.TeamEvaluationDAO;
import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import model.Account;
import model.Class_User;
import model.EvaluationCriteria;
import model.Iteration;
import model.IterationEvaluation;
import model.MemberEvaluation;
import model.Subject;
import model.Team;
import model.Team_Evaluation;

public class AddIterEvalController extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        Account account = (Account) request.getSession().getAttribute("account");
        SubjectDAO sjd = new SubjectDAO();
        IterationDAO itd = new IterationDAO();
        AccountDAO acd = new AccountDAO();
        if (account != null) {
            if (account.getRole_id() == 4) {
                ArrayList<Subject> sublist = sjd.getallSubjectbyClass(account.getEmail());
                ArrayList<Account> assigneelist = acd.getAllStudentbyTrainer(account.getEmail());
                String raw_sid = request.getParameter("subject_id");
                if (raw_sid == null || raw_sid.trim().length() == 0) {
                    raw_sid = "-1";
                }
                int subject_id = Integer.parseInt(raw_sid);
                ArrayList<Iteration> iterlist = itd.getallIterbySubject(account.getEmail(), subject_id);
                request.setAttribute("sublist", sublist);
                request.setAttribute("subject_id", subject_id);
                request.setAttribute("iterlist", iterlist);
                request.setAttribute("assigneelist", assigneelist);
            }
            request.getRequestDispatcher("additereval.jsp").forward(request, response);
        } else {
            response.sendRedirect("index");
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        // Add iteration evaluation
        
        int iteration_id = Integer.parseInt(request.getParameter("iteration_id"));
        Iteration iteration = new Iteration(iteration_id);
        
        String email = request.getParameter("email");
        Class_UserDAO cuDAO = new Class_UserDAO();
        Class_User cu = cuDAO.getDetail(email);
        
        String float_bonus = request.getParameter("bonus");
        String floatGrade = request.getParameter("grade");
        
        IterationEvaluation ie = new IterationEvaluation();
        
        if (float_bonus == null) {
            ie.setBonus(0);
        } else {
            ie.setBonus(Float.parseFloat(float_bonus));
        }
        
        if (floatGrade == null) {
            ie.setGrade(0);
        } else {
            ie.setGrade(Float.parseFloat(floatGrade));
        }
        
        String note = request.getParameter("note");
        
        ie.setIteration(iteration);
        ie.setClasses(cu);
        ie.setTeam(cu);
        ie.setEmail(cu);
        ie.setNote(note);
        
        IterationEvaluationDAO ieDAO = new IterationEvaluationDAO();
        TeamEvaluationDAO teDAO = new TeamEvaluationDAO();
        int newIterEvalID = ieDAO.addIterEval(ie);
        ie.setEvaluation_id(newIterEvalID);
        
        // Add team evaluation
        
        EvaluationCriteriaDAO ecDAO = new EvaluationCriteriaDAO();
        EvaluationCriteria ec = ecDAO.getECbyIter(iteration_id);
        
        int team_id = ie.getTeam().getTeam().getTeam_id();
        
        Team_Evaluation te = new Team_Evaluation();
        te.setEvaluation(ie);
        te.setCriteria(ec);
        te.setTeam(new Team(team_id));
        
        if (floatGrade == null) {
            te.setGrade(0);
        } else {
            te.setGrade(Float.parseFloat(floatGrade));
        }
        
        te.setNote(note);
        teDAO.addTeamEval(te);
        
        // Add member evaluation
        
        MemberEvaluationDAO meDAO = new MemberEvaluationDAO();
        LOCEvaluationDAO leDAO = new LOCEvaluationDAO();
        
        MemberEvaluation me = new MemberEvaluation();
        
        me.setEvaluation(ie);
        me.setCriteria(ec);
        me.setConverted_loc(0);
        
        if (floatGrade == null) {
            me.setGrade(0);
        } else {
            me.setGrade(Float.parseFloat(floatGrade));
        }
        
        me.setNote(note);
        
        meDAO.addMemberEvaluation(me);
        
        response.sendRedirect("iterevalist");
    }
}
