/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dao.TeamEvaluationDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import model.Account;
import model.EvaluationCriteria;
import model.Team;
import model.Team_Evaluation;

/**
 *
 * @author admin
 */
public class TeamEvalDetailController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        Account account = (Account) request.getSession().getAttribute("account");
        int team_eval_id = Integer.parseInt(request.getParameter("team_eval_id"));
        TeamEvaluationDAO ted = new TeamEvaluationDAO();
        if (account != null) {
            if (account.getRole_id() == 4) {
                Team_Evaluation teameval = ted.getTeamEvalByID(team_eval_id);
                request.setAttribute("teameval", teameval);
            }
            if (account.getRole_id() == 1) {
                Team_Evaluation teameval = ted.getTeamEvalByID(team_eval_id);
                request.setAttribute("teameval", teameval);
            }
            request.getRequestDispatcher("teameval_detail.jsp").forward(request, response);
        }else{
            response.sendRedirect("index");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int team_eval_id = Integer.parseInt(request.getParameter("team_eval_id"));
        int criteria_id = Integer.parseInt(request.getParameter("criteria_id"));
        int team_id = Integer.parseInt(request.getParameter("team_id"));
        float grade = Float.parseFloat(request.getParameter("grade"));
        String note = request.getParameter("note");
        
        
        Team_Evaluation te = new Team_Evaluation();
        te.setTeam_eval_id(team_eval_id);
        te.setCriteria(new EvaluationCriteria(criteria_id));
        te.setTeam(new Team(team_id));
        te.setGrade(grade);
        te.setNote(note);
        
        
        
        TeamEvaluationDAO ted = new TeamEvaluationDAO();
        ted.updateTeamEval(te);
        response.sendRedirect("teamevaldetail?team_eval_id="+team_eval_id);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
