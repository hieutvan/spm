/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import common.Export;
import dao.AccountDAO;
import dao.Class_UserDAO;
import dao.TeamDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.io.File;
import java.io.FileInputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import model.Account;
import model.Class_User;
import model.Team;

/**
 *
 * @author admin
 */
public class TeamUserListController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        Account account = (Account) request.getSession().getAttribute("account");
        Class_UserDAO cud = new Class_UserDAO();
        AccountDAO acd = new AccountDAO();
        TeamDAO td = new TeamDAO();
        if (account != null && account.getRole_id() == 4) {
            int team_id = Integer.parseInt(request.getParameter("team_id"));
            Team t = td.getTeamDetail(team_id);
            ArrayList<Class_User> tculist = cud.getAllUserbyTeam(team_id);
            request.setAttribute("team", t);
            request.setAttribute("tculist", tculist);
            request.getRequestDispatcher("teamuser_list.jsp").forward(request, response);
        } else {
            response.sendRedirect("index");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Account account = (Account) request.getSession().getAttribute("account");
        Class_UserDAO cud = new Class_UserDAO();
        if (account != null && account.getRole_id() == 4) {
            String raw_filename = request.getParameter("filename");
            int team_id = Integer.parseInt(request.getParameter("team_id"));
            System.out.println(raw_filename);
            ArrayList<Class_User> teamuserlist = cud.getAllUserbyTeam(team_id);
            String path = Export.exportclassuser(teamuserlist, raw_filename);
            response.setContentType("application/vnd.ms-excel");
            response.setHeader("Content-Disposition", "attachment;filename=" + raw_filename + ".xlsx");
            File excelFile = new File(path);
            OutputStream out = response.getOutputStream();
            FileInputStream in = new FileInputStream(excelFile);
            byte[] buffer = new byte[4096];
            int length;

            while ((length = in.read(buffer)) > 0) {
                out.write(buffer, 0, length);
            }

            in.close();
            out.flush();
        } else {
            response.sendRedirect("index");
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
