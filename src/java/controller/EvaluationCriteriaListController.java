package controller;

import dao.EvaluationCriteriaDAO;
import dao.IterationDAO;
import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import model.Account;
import model.EvaluationCriteria;
import model.Iteration;

public class EvaluationCriteriaListController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        Account account = (Account) request.getSession().getAttribute("account");
        if (account != null && account.getRole_id() != 1) {
            EvaluationCriteriaDAO ecd = new EvaluationCriteriaDAO();
            IterationDAO itd = new IterationDAO();
            //admin role can see all
            if (account.getRole_id() == 2) {
                ArrayList<EvaluationCriteria> eclist = ecd.getallcriteria();
                for (EvaluationCriteria ec : eclist) {
                    int iteration_id = ec.getIteration().getIteration_id();
                    Iteration iter = itd.getiteration(iteration_id);
                    request.setAttribute("iter", iter);
                }
                request.setAttribute("eclist", eclist);
            }
            //author role can see what they are assigned
            if (account.getRole_id() == 3 || account.getRole_id() == 4) {
                String email = account.getEmail();
                ArrayList<EvaluationCriteria> eclist = ecd.getbyAuthor(email);
                for (EvaluationCriteria ec : eclist) {
                    int iteration_id = ec.getIteration().getIteration_id();
                    Iteration iter = itd.getiteration(iteration_id);
                    request.setAttribute("iter", iter);
                }
                request.setAttribute("eclist", eclist);
            }
            request.getRequestDispatcher("eva_critlist.jsp").forward(request, response);
        } else {
            response.sendRedirect("login");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
