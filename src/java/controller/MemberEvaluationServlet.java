package controller;

import dao.MemberEvaluationDAO;
import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import model.Account;
import model.MemberEvaluation;

public class MemberEvaluationServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Account account = (Account) request.getSession().getAttribute("account");
        
        if (account != null) {
            switch (account.getRole_id()) {
                case 4:
                    MemberEvaluationDAO meDAO = new MemberEvaluationDAO();
                    ArrayList<MemberEvaluation> me = meDAO.getAllMemEvalByUser(account.getEmail());

                    request.setAttribute("evals", me);
                    request.getRequestDispatcher("memEval.jsp").forward(request, response);
                    
                    break;
                case 1:
                    break;
            }
        } else {
            response.sendRedirect("index");
        }
    }
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //
    }
}
