/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dao.AccountDAO;
import dao.SubjectDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import model.Account;
import model.Subject;

/**
 *
 * @author admin
 */
public class UpdateSubjectController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet UpdateSubjectController</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet UpdateSubjectController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Account account = (Account) request.getSession().getAttribute("account");
        if (account != null && account.getRole_id() == 2) {
            String subject_code = request.getParameter("subject_code");
            SubjectDAO sjd = new SubjectDAO();
            Subject subj = sjd.getSubjectdetail(subject_code);

            AccountDAO acd = new AccountDAO();
            ArrayList<Account> trainerlist = acd.getAllTrainer();
            request.setAttribute("trainerlist",trainerlist );
            request.setAttribute("subj", subj);
            request.getRequestDispatcher("update_subject.jsp").forward(request, response);
        } else {
            response.sendRedirect("index");
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String subject_code = request.getParameter("subject_code");
        String subject_name = request.getParameter("subject_name");
        String subject_id = request.getParameter("subject_id");
        String email = request.getParameter("email");
        int status = Integer.parseInt(request.getParameter("status"));
        Subject subject = new Subject();

        subject.setSubject_code(subject_code.toUpperCase());
        subject.setSubject_name(subject_name.toUpperCase());
        subject.setSubject_id(Integer.parseInt(subject_id));
        Account account = new Account();
        account.setEmail(email);
        subject.setEmail(account);
        subject.setStatus(status);
        SubjectDAO sjd = new SubjectDAO();
        sjd.updatesubject(subject);
        response.sendRedirect("subjectlist");

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
