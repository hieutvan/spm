package controller;

import dao.TeamEvaluationDAO;
import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import model.Account;
import model.Team_Evaluation;

public class TeamEvalListController extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        Account account = (Account) request.getSession().getAttribute("account");
        TeamEvaluationDAO ted = new TeamEvaluationDAO();
        if (account != null) {
            if (account.getRole_id() == 4) {
                ArrayList<Team_Evaluation> teamevallist = ted.getTeamEvalsByTrainer(account.getEmail());
                request.setAttribute("teamevallist", teamevallist);
            }
            if (account.getRole_id() == 1) {
                ArrayList<Team_Evaluation> teamevallist = ted.getTeamEvalsByUser(account.getEmail());
                request.setAttribute("teamevallist", teamevallist);
            }
            request.getRequestDispatcher("teameval_list.jsp").forward(request, response);
        } else {
            response.sendRedirect("index");
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
}
