/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import common.Export;
import dao.FunctionDAO;
import dao.TeamDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import model.Account;
import model.Function;
import model.Team;

/**
 *
 * @author admin
 */
public class FunctionListController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        Account account = (Account) request.getSession().getAttribute("account");

        if (account != null) {
            if (account.getRole_id() == 1) {
                FunctionDAO fud = new FunctionDAO();
                ArrayList<Function> functionlist = fud.getAllbyTeam(account.getEmail());
                request.setAttribute("functionlist", functionlist);
            }

            if (account.getRole_id() == 4) {
                FunctionDAO fud = new FunctionDAO();
                TeamDAO td = new TeamDAO();
                ArrayList<Team> teamlist = td.getAllTeambyTrainerwithfunction(account.getEmail());
                request.setAttribute("teamlist", teamlist);
                String raw_team_id = request.getParameter("team_id");
                if (raw_team_id == null || raw_team_id.trim().length() == 0) {
                    raw_team_id = "-1";
                }
                int team_id = Integer.parseInt(raw_team_id);
                System.out.println(team_id);
                ArrayList<Function> functionlist = fud.getAllbyTeambyTrainer(account.getEmail());
                System.out.println(functionlist.toString());
                request.setAttribute("team_id", team_id);
                request.setAttribute("functionlist", functionlist);
            }

            request.getRequestDispatcher("function_list.jsp").forward(request, response);
        } else {
            response.sendRedirect("index");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Account account = (Account) request.getSession().getAttribute("account");
        if (account != null) {
            if (account.getRole_id() == 4) {
                String raw_filename = request.getParameter("filename");
                FunctionDAO fud = new FunctionDAO();
                ArrayList<Function> functionlist = fud.getAllbyTeambyTrainer(account.getEmail());
                String path = Export.exportfunction(functionlist, raw_filename);
                System.out.println(raw_filename);
                response.setContentType("application/vnd.ms-excel");
                response.setHeader("Content-Disposition", "attachment;filename=" + raw_filename + ".xlsx");
                File excelFile = new File(path);
                OutputStream out = response.getOutputStream();
                FileInputStream in = new FileInputStream(excelFile);
                byte[] buffer = new byte[4096];
                int length;
                while ((length = in.read(buffer)) > 0) {
                    out.write(buffer, 0, length);
                }
                in.close();
                out.flush();
            }
            if(account.getRole_id() == 1){
                String raw_filename = request.getParameter("filename");
                FunctionDAO fud = new FunctionDAO();
                ArrayList<Function> functionlist = fud.getAllFunctionbyUser(account.getEmail());
                String path = Export.exportfunction(functionlist, raw_filename);
                System.out.println(raw_filename);
                response.setContentType("application/vnd.ms-excel");
                response.setHeader("Content-Disposition", "attachment;filename=" + raw_filename + ".xlsx");
                File excelFile = new File(path);
                OutputStream out = response.getOutputStream();
                FileInputStream in = new FileInputStream(excelFile);
                byte[] buffer = new byte[4096];
                int length;
                while ((length = in.read(buffer)) > 0) {
                    out.write(buffer, 0, length);
                }
                in.close();
                out.flush();
            }
        } else {
            response.sendRedirect("index");
        }

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
