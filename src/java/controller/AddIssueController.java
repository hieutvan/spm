/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dao.AccountDAO;
import dao.Class_UserDAO;
import dao.FunctionDAO;
import dao.IssueDAO;
import dao.MilestoneDAO;
import dao.TeamDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import model.Account;
import model.Class_User;
import model.Function;
import model.Issue;
import model.Milestone;
import model.Team;

/**
 *
 * @author admin
 */
public class AddIssueController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        Account account = (Account) request.getSession().getAttribute("account");

        if (account != null) {
            if (account.getRole_id() == 1) {
                TeamDAO td = new TeamDAO();
                MilestoneDAO md = new MilestoneDAO();
                FunctionDAO fd = new FunctionDAO();
                Class_UserDAO cud = new Class_UserDAO();
                ArrayList<Milestone> milestonelist = md.getAllMilestonebyUser(account.getEmail());
                ArrayList<Function> functionlist = fd.getAllFunctionbyUser(account.getEmail());

                Team team = td.getAllTeambyUser(account.getEmail());
                ArrayList<Class_User> teamuserlist = cud.getAllUserbyTeam(team.getTeam_id());
                request.setAttribute("teamuserlist", teamuserlist);
                request.setAttribute("milestonelist", milestonelist);
                request.setAttribute("functionlist", functionlist);
            }
            if (account.getRole_id() == 4) {
                Class_UserDAO cud = new Class_UserDAO();
                TeamDAO td = new TeamDAO();
                MilestoneDAO md = new MilestoneDAO();
                FunctionDAO fd = new FunctionDAO();
                ArrayList<Class_User> teamuserlist = cud.getAllUserbyTrainer(account.getEmail());
                ArrayList<Milestone> milestonelist = md.getAllMilestonebyTrainer(account.getEmail());
                ArrayList<Function> functionlist = fd.getAllFunctionbyTrainer(account.getEmail());
                request.setAttribute("teamuserlist", teamuserlist);
                request.setAttribute("milestonelist", milestonelist);
                request.setAttribute("functionlist", functionlist);
            }
            request.getRequestDispatcher("add_issue.jsp").forward(request, response);
        } else {
            response.sendRedirect("index");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Account account = (Account) request.getSession().getAttribute("account");
        if (account != null) {
            TeamDAO td = new TeamDAO();
            Issue issue = new Issue();
            IssueDAO isd = new IssueDAO();
            if (account.getRole_id() == 1) {
                String issue_title = request.getParameter("issue_title");
                String description = request.getParameter("descripion");
                Account a = new Account();
                String email = request.getParameter("email");
                a.setEmail(email);
                Team team = td.getAllTeambyUser(account.getEmail());
                Team t = new Team();
                int team_id = team.getTeam_id();
                t.setTeam_id(team_id);
                Milestone m = new Milestone();
                int milestone_id = Integer.parseInt(request.getParameter("milestone_id"));
                m.setMilestone_id(milestone_id);
                Function f = new Function();
                int function_id = Integer.parseInt(request.getParameter("function_id"));
                f.setFunction_id(function_id);
                int gitlab_id = Integer.parseInt(request.getParameter("gitlab_id"));
                String gitlab_URL = request.getParameter("gitlab_url");
                String labels = request.getParameter("labels");
                String raw_due_date = request.getParameter("due_date");
                Date due_date = Date.valueOf(raw_due_date);
                LocalDate raw_created_at = LocalDate.now();
                Date created_at = Date.valueOf(raw_created_at);
                int status = Integer.parseInt(request.getParameter("status"));
                issue.setAccount(a);
                issue.setIssue_title(issue_title);
                issue.setDescription(description);
                issue.setGitlab_id(gitlab_id);
                issue.setGitlab_url(gitlab_URL);
                issue.setCreated_at(created_at);
                issue.setDue_date(due_date);
                issue.setTeam(t);
                issue.setMilestone(m);
                issue.setFunction(f);
                issue.setLabels(labels);
                issue.setStatus(status);
                isd.addIssue(issue);

                response.sendRedirect("issuelist");
               
            }
            if(account.getRole_id() == 4){
                String issue_title = request.getParameter("issue_title");
                String description = request.getParameter("descripion");
                Account a = new Account();
                String email = request.getParameter("email");
                a.setEmail(email);
                Team team = td.getAllTeambyUser(email);
                Team t = new Team();
                int team_id = team.getTeam_id();
                t.setTeam_id(team_id);
                Milestone m = new Milestone();
                int milestone_id = Integer.parseInt(request.getParameter("milestone_id"));
                m.setMilestone_id(milestone_id);
                Function f = new Function();
                int function_id = Integer.parseInt(request.getParameter("function_id"));
                f.setFunction_id(function_id);
                int gitlab_id = Integer.parseInt(request.getParameter("gitlab_id"));
                String gitlab_URL = request.getParameter("gitlab_url");
                String labels = request.getParameter("labels");
                String raw_due_date = request.getParameter("due_date");
                Date due_date = Date.valueOf(raw_due_date);
                LocalDate raw_created_at = LocalDate.now();
                Date created_at = Date.valueOf(raw_created_at);
                int status = Integer.parseInt(request.getParameter("status"));
                issue.setAccount(a);
                issue.setIssue_title(issue_title);
                issue.setDescription(description);
                issue.setGitlab_id(gitlab_id);
                issue.setGitlab_url(gitlab_URL);
                issue.setCreated_at(created_at);
                issue.setDue_date(due_date);
                issue.setTeam(t);
                issue.setMilestone(m);
                issue.setFunction(f);
                issue.setLabels(labels);
                issue.setStatus(status);
                isd.addIssue(issue);

                response.sendRedirect("issuelist");
            }
        } else {
            response.sendRedirect("index");
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
