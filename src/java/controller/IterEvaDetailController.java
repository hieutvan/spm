/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dao.IterationEvaluationDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import model.Account;
import model.Iteration;
import model.IterationEvaluation;

/**
 *
 * @author admin
 */
public class IterEvaDetailController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        Account account = (Account) request.getSession().getAttribute("account");
        int evaluation_id = Integer.parseInt(request.getParameter("evaluation_id"));
        IterationEvaluationDAO ied = new IterationEvaluationDAO();
        if (account != null) {
            if (account.getRole_id() == 4) {
                IterationEvaluation iedetail = ied.getIterEvaDetail(evaluation_id);
                request.setAttribute("iedetail", iedetail);
                System.out.println(evaluation_id);
            }
            if (account.getRole_id() == 1) {
                IterationEvaluation iedetail = ied.getIterEvaDetail(evaluation_id);
                request.setAttribute("iedetail", iedetail);
            }
            request.getRequestDispatcher("itereval_detail.jsp").forward(request, response);
        } else {
            response.sendRedirect("index");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int evaluation_id = Integer.parseInt(request.getParameter("evaluation_id"));
        int iteration_id = Integer.parseInt(request.getParameter("iteration_id"));
        Iteration iteration = new Iteration(iteration_id);
        float grade = Float.parseFloat(request.getParameter("grade"));
        float bonus = Float.parseFloat(request.getParameter("bonus"));
        String note = request.getParameter("note");

        IterationEvaluation ie = new IterationEvaluation(evaluation_id, iteration, bonus, grade, note);
        IterationEvaluationDAO ied = new IterationEvaluationDAO();
        ied.updateIterEval(ie);

        response.sendRedirect("iterevadetail?evaluation_id=" + evaluation_id);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
