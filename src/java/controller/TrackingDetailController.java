package controller;

import dao.AccountDAO;
import dao.FunctionDAO;
import dao.MilestoneDAO;
import dao.TeamDAO;
import dao.TrackingDAO;
import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import model.Account;
import model.Tracking;

public class TrackingDetailController extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        Account account = (Account) request.getSession().getAttribute("account");
        TrackingDAO tDAO = new TrackingDAO();
        AccountDAO acd = new AccountDAO();
        if (account != null) {
            if (account.getRole_id() == 4) {
                int trID = Integer.parseInt(request.getParameter("tracking_id"));
                Tracking tracking = tDAO.getTrackingDetails(trID);
                int team_id = tracking.getTeam().getTeam_id();
                ArrayList<Account> assigneelist = acd.getAllAssigneebyTrainer(account.getEmail(), team_id);
                request.setAttribute("tracking", tracking);
                request.setAttribute("peopleList", assigneelist);
            }
            if (account.getRole_id() == 1) {
                int trID = Integer.parseInt(request.getParameter("tracking_id"));
                Tracking tracking = tDAO.getTrackingDetails(trID);
                ArrayList<Account> assigneelist = acd.getAllAssigneebyAssingee(account.getEmail());
                request.setAttribute("tracking", tracking);
                request.setAttribute("peopleList", assigneelist);
            }
            request.getRequestDispatcher("tracking_detail.jsp").forward(request, response);
        } else {
            response.sendRedirect("index");
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int trackingID = Integer.parseInt(request.getParameter("tracking_id"));
        int teamID = Integer.parseInt(request.getParameter("team_id"));
        int milestoneID = Integer.parseInt(request.getParameter("milestone_id"));
        int funcID = Integer.parseInt(request.getParameter("function_id"));
        String assignerEmail = request.getParameter("assigner_email");
        String assigneeEmail = request.getParameter("assignee_email");
        String trackingNotes = request.getParameter("tracking_notes");
        String updates = request.getParameter("updates");
        int status = Integer.parseInt(request.getParameter("status"));
        
        TrackingDAO trDAO = new TrackingDAO();
        TeamDAO tDAO = new TeamDAO();
        MilestoneDAO mDAO = new MilestoneDAO();
        FunctionDAO fDAO = new FunctionDAO();
        AccountDAO aDAO = new AccountDAO();
        
        Tracking tr = new Tracking(trackingID,
                tDAO.getTeamDetail(teamID),
                mDAO.getMilestonebyID(milestoneID),
                fDAO.getFunctionDetail(funcID),
                aDAO.getAccount(assignerEmail),
                aDAO.getAccount(assigneeEmail),
                trackingNotes,
                updates,
                status);

        trDAO.updateTracking(tr);
        response.sendRedirect("trackinglist");           
    }
}
