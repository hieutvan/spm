/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dao.AccountDAO;
import dao.ClassDAO;
import dao.SubjectDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import model.Account;
import model.Classes;
import model.Subject;

/**
 *
 * @author admin
 */
public class AddClassController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        Account account = (Account) request.getSession().getAttribute("account");
        SubjectDAO sjd = new SubjectDAO();
        AccountDAO acd = new AccountDAO();
        if (account != null) {
            if (account.getRole_id() == 2) {
                ArrayList<Subject> sublist = sjd.getallsubject();
                ArrayList<Account> acclist = acd.getAllTrainer();
                request.setAttribute("sub", sublist);
                request.setAttribute("acc", acclist);
            }
            if (account.getRole_id() == 3) {
                ArrayList<Subject> sublist = sjd.getSubjectbyAuthor(account.getEmail());
                ArrayList<Account> acclist = acd.getAllTrainer();
                request.setAttribute("sub", sublist);
                request.setAttribute("acc", acclist);
            }
            request.getRequestDispatcher("add_class.jsp").forward(request, response);
        } else {
            response.sendRedirect("index");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String class_code = request.getParameter("class_code");
        int subject_id = Integer.parseInt(request.getParameter("subject_id"));
        String email = request.getParameter("email");
        int class_year = Integer.parseInt(request.getParameter("class_year"));
        String class_term = request.getParameter("class_term");
        int block5_class = Integer.parseInt(request.getParameter("block5_class"));
        int status = Integer.parseInt(request.getParameter("status"));

        Classes classes = new Classes();
        classes.setClass_code(class_code);
        Account account = new Account();
        account.setEmail(email);
        classes.setAccount(account);
        Subject subject = new Subject();
        subject.setSubject_id(subject_id);
        classes.setSubject(subject);
        classes.setClass_year(class_year);
        classes.setClass_term(class_term);
        classes.setBlock5_class(block5_class);
        classes.setStatus(status);

        ClassDAO cld = new ClassDAO();
        cld.addclass(classes);
        response.sendRedirect("classlist");

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
