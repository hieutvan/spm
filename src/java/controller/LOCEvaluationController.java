package controller;

import dao.FunctionDAO;
import dao.LOCEvaluationDAO;
import dao.SubjectDAO;
import dao.Subject_SettingDAO;
import dao.TrackingDAO;
import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import model.Account;
import model.Function;
import model.LOC_Evaluation;
import model.Subject;
import model.Subject_Setting;
import model.Tracking;

public class LOCEvaluationController extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        Account account = (Account) request.getSession().getAttribute("account");
        LOCEvaluationDAO led = new LOCEvaluationDAO();
        TrackingDAO trd = new TrackingDAO();
        FunctionDAO fd = new FunctionDAO();
        SubjectDAO sd = new SubjectDAO();
        Subject_SettingDAO ssd = new Subject_SettingDAO();
        if (account != null) {
            if (account.getRole_id() == 4) {
                int tracking_id = Integer.parseInt(request.getParameter("tracking_id"));
                LOC_Evaluation loceva = led.getLOCEval(tracking_id);
                Tracking track = trd.getFunctionbyTracking(tracking_id);
                Subject subject = sd.getsubjectbytracking(tracking_id);
                ArrayList<Subject_Setting> sslist = ssd.getQuality(subject.getSubject_id());
                ArrayList<Function> complexlist = fd.getallComplexity();
                System.out.println(complexlist.toString());
                request.setAttribute("sslist", sslist);
                request.setAttribute("subject", subject);
                request.setAttribute("loceva", loceva);
                request.setAttribute("track", track);
                request.setAttribute("complexlist", complexlist);
                System.out.println(complexlist.toString());
                System.out.println(loceva);
            }
            if (account.getRole_id() == 1) {
                int tracking_id = Integer.parseInt(request.getParameter("tracking_id"));
                LOC_Evaluation loceva = led.getLOCEval(tracking_id);
                Tracking track = trd.getFunctionbyTracking(tracking_id);
                Subject subject = sd.getsubjectbytracking(tracking_id);
                ArrayList<Subject_Setting> sslist = ssd.getQuality(subject.getSubject_id());
                ArrayList<Function> complexlist = fd.getallComplexity();
                System.out.println(complexlist.toString());
                request.setAttribute("sslist", sslist);
                request.setAttribute("subject", subject);
                request.setAttribute("loceva", loceva);
                request.setAttribute("track", track);
                request.setAttribute("complexlist", complexlist);
            }
            request.getRequestDispatcher("Loc_Evaluation.jsp").forward(request, response);
        } else {
            response.sendRedirect("index");
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int evaluation_id = Integer.parseInt(request.getParameter("evaluation_id"));
        LocalDate raw_evaluation_time = LocalDate.now();
        Date evaluation_time = Date.valueOf(raw_evaluation_time);
        String evaluation_note = request.getParameter("evaluation_note");
        int complex_value = Integer.parseInt(request.getParameter("complex_value"));
        int quality_value = Integer.parseInt(request.getParameter("quality_value"));

        int complexity_id = 0;
        switch (complex_value) {
            case 60:
                complexity_id = 1;
                break;
            case 120:
                complexity_id = 2;
                break;
            case 240:
                complexity_id = 3;
                break;
            default:
                break;
        }
        
        int quality_id = 0;
        switch (quality_value) {
            case 100:
                quality_id = 1;
                break;
            case 75:
                quality_id = 2;
                break;
            case 50:
                quality_id = 3;
                break;
            case 0:
                quality_id = 4;
                break;
            default:
                break;
        }
        
        int LOC = Integer.parseInt(request.getParameter("LOC"));
        int function_id = Integer.parseInt(request.getParameter("function_id"));
        int tracking_id = Integer.parseInt(request.getParameter("tracking_id"));

        LOC_Evaluation le = new LOC_Evaluation();
        LOCEvaluationDAO led = new LOCEvaluationDAO();
        le.setEvaluation_id(evaluation_id);
        le.setEvaluation_time(evaluation_time);
        le.setEvaluation_note(evaluation_note);
        le.setComplexity_id(complexity_id);
        le.setQuality_id(quality_id);
        le.setLOC(LOC);
        System.out.println(LOC);
        Tracking track = new Tracking();
        track.setTracking_id(tracking_id);
        le.setTracking(track);
        led.updateLocEvaluation(le);

        Function func = new Function();
        FunctionDAO fd = new FunctionDAO();
        func.setFunction_id(function_id);
        func.setComplexity_id(complexity_id);
        fd.updateComplexity(complexity_id, function_id);
        response.sendRedirect("locevafunction?tracking_id=" + tracking_id);
    }
}
