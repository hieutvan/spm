/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dao.SubjectDAO;
import dao.Subject_SettingDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import model.Account;
import model.Subject;
import model.Subject_Setting;

/**
 *
 * @author admin
 */
public class AddSubjectSettingController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        Account account = (Account) request.getSession().getAttribute("account");
        SubjectDAO sjd = new SubjectDAO();
        if (account != null) {
            if (account.getRole_id() == 2) {
                ArrayList<Subject> sublist = sjd.getallsubject();
                request.setAttribute("sublist", sublist);
            }
            if (account.getRole_id() == 3) {
                ArrayList<Subject> sublist = sjd.getSubjectbyAuthor(account.getEmail());
                request.setAttribute("sublist", sublist);
            }
            request.getRequestDispatcher("add_subjectsetting.jsp").forward(request, response);
        }else{
            response.sendRedirect("index");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int subject_id = Integer.parseInt(request.getParameter("subject_id"));
        int type_id = Integer.parseInt(request.getParameter("type_id"));
        String setting_title = request.getParameter("setting_title");
        String setting_value = request.getParameter("setting_value");
        int display_order = Integer.parseInt(request.getParameter("display_order"));
        int status = Integer.parseInt(request.getParameter("status"));
        
        Subject_Setting ss = new Subject_Setting();
        ss.setSubject(new Subject(subject_id));
        ss.setType_id(type_id);
        ss.setSetting_title(setting_title);
        ss.setSetting_value(setting_value);
        ss.setDisplay_order(display_order);
        ss.setStatus(status);
        
        Subject_SettingDAO ssd = new Subject_SettingDAO();
        ssd.addSubject_Setting(ss);
        response.sendRedirect("subjectsettinglist");
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
