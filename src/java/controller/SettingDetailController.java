/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dao.SettingDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import model.Account;
import model.Setting;

/**
 *
 * @author admin
 */
public class SettingDetailController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        Account account = (Account) request.getSession().getAttribute("account");
        SettingDAO std = new SettingDAO();
        if (account != null && account.getRole_id() == 2) {
         int setting_id = Integer.parseInt(request.getParameter("setting_id"));
            Setting setting = std.getSettingdetail(setting_id);
            request.setAttribute("setting", setting);
            request.getRequestDispatcher("setting_detail.jsp").forward(request, response);
        }else{
            response.sendRedirect("index");
                    
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String raw_setting_id = request.getParameter("setting_id");
        int setting_id = Integer.parseInt(raw_setting_id);
        String setting_title = request.getParameter("setting_title");
        String setting_value = request.getParameter("setting_value");
        int order = Integer.parseInt(request.getParameter("display_order"));
        int type = Integer.parseInt(request.getParameter("type_id"));
        int status = Integer.parseInt(request.getParameter("status"));
        
        Setting s = new Setting(setting_title, setting_id, type, order, setting_value, status);
        SettingDAO std = new SettingDAO();
        std.updateSetting(s);
        response.sendRedirect("settingdetail?setting_id="+setting_id);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
