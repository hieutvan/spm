/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dao.AccountDAO;
import dao.ClassDAO;
import dao.Class_UserDAO;
import dao.TeamDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import model.Account;
import model.Class_User;
import model.Classes;
import model.Team;

/**
 *
 * @author admin
 */
public class ClassUserDetailController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        Account account = (Account) request.getSession().getAttribute("account");
        if (account != null && account.getRole_id() == 4) {
            String email = request.getParameter("email");
            Class_UserDAO cud = new Class_UserDAO();
            ClassDAO cld = new ClassDAO();
            TeamDAO td = new TeamDAO();
            AccountDAO acd = new AccountDAO();
            Class_User cu = cud.getDetail(email);
            int class_id = cu.getClasses().getClass_id();
            Classes c = cld.getclassdetail(class_id);
            int team_id = cu.getTeam().getTeam_id();
            Team t = td.getTeamDetail(team_id);
            String useremail = cu.getAccount().getEmail();
            Account a = acd.getAccount(useremail);
            request.setAttribute("account", a);
            request.setAttribute("classes", c);
            request.setAttribute("team", t);
            request.setAttribute("user", cu);
            request.getRequestDispatcher("classuser_detail.jsp").forward(request, response);
        }else{
            response.sendRedirect("index");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
