package controller;

import common.EmailController;
import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import model.Account;
import dao.AccountDAO;
import jakarta.mail.MessagingException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class RegisterController extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.getRequestDispatcher("register.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String displayname = request.getParameter("name");
		String user = request.getParameter("username");
		String pass = request.getParameter("password");
		String email = request.getParameter("email");
		String repassword = request.getParameter("repassword");
		String phonenumber = request.getParameter("phonenumber");
                int role_id = 1;
		AccountDAO accdb = new AccountDAO();
		Account acc = new Account();

		if (!pass.equals(repassword)) {
			request.getRequestDispatcher("register.jsp").forward(request, response);
		} else {
			acc.setDisplayname(displayname);
			acc.setUsername(user);
			acc.setPassword(pass);
			acc.setEmail(email);
			acc.setPhonenumber(phonenumber);
                        acc.setRole_id(role_id);
			accdb.insertAccount(acc);
			HttpSession session = request.getSession();
                    try {
                        EmailController.sendMail(email, user, pass);
                    } catch (MessagingException ex) {
                        Logger.getLogger(RegisterController.class.getName()).log(Level.SEVERE, null, ex);
                    }
			session.setAttribute("account", acc);
                        
			response.sendRedirect("index");
		}
	}
}
