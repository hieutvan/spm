/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dao.IterationDAO;
import dao.SubjectDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import model.Account;
import model.Iteration;
import model.Subject;

/**
 *
 * @author admin
 */
public class UpdateIterController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet UpdateIterController</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet UpdateIterController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Account account = (Account) request.getSession().getAttribute("account");
        if(account != null && account.getRole_id() != 1){
            int iteration_id = Integer.parseInt(request.getParameter("iteration_id"));
            IterationDAO itd = new IterationDAO();
            SubjectDAO sjd = new SubjectDAO();
            Iteration iter = itd.getiteration(iteration_id);
            int subject_id = iter.getSubject().getSubject_id();
            Subject subj = sjd.getSubject(subject_id);
            request.setAttribute("subject", subj);
            request.setAttribute("iter", iter);
            
            request.getRequestDispatcher("update_iter.jsp").forward(request, response);
        }else{
            response.sendRedirect("login");
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int iteration_id = Integer.parseInt(request.getParameter("iteration_id"));
        String iteration_name = request.getParameter("iteration_name");
        int duration = Integer.parseInt(request.getParameter("duration"));
        int status = Integer.parseInt(request.getParameter("status"));
        int subject_id = Integer.parseInt(request.getParameter("subject_id"));
        
        
        
        Iteration iter = new Iteration();
        iter.setIteration_id(iteration_id);
        iter.setIteration_name(iteration_name);
        iter.setDuration(duration);
        iter.setStatus(status);
        IterationDAO itd = new IterationDAO();
        itd.updateiteration(iter);
        response.sendRedirect("iterationlist?subject_id="+subject_id);
        
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
