/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dao.EvaluationCriteriaDAO;
import dao.IterationDAO;
import dao.SubjectDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import model.Account;
import model.EvaluationCriteria;
import model.Iteration;
import model.Subject;

/**
 *
 * @author admin
 */
public class AddCriteriaController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet AddCriteriaController</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet AddCriteriaController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Account account = (Account) request.getSession().getAttribute("account");
        if (account != null && account.getRole_id() != 1 && account.getRole_id() != 4) {
            IterationDAO itd = new IterationDAO();
            SubjectDAO sjd = new SubjectDAO();
            ArrayList<Iteration> iterlist = itd.getall();
            request.setAttribute("iterlist", iterlist);
            request.getRequestDispatcher("add_criteria.jsp").forward(request, response);
        } else {
            response.sendRedirect("index");
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int iteration_id = Integer.parseInt(request.getParameter("iteration_id"));
        int evaluation_weight = Integer.parseInt(request.getParameter("evaluation_weight"));
        int team_evaluation = Integer.parseInt(request.getParameter("team_evaluation"));
        int criteria_order = Integer.parseInt(request.getParameter("criteria_order"));
        int max_loc = Integer.parseInt(request.getParameter("max_loc"));
        int status = Integer.parseInt(request.getParameter("status"));

        if (evaluation_weight >= 100) {
            request.setAttribute("error", "evaluation weight need to be smaller than 100");
            request.getRequestDispatcher("add_criteria.jsp").forward(request, response);
        } else {
            EvaluationCriteria ec = new EvaluationCriteria();
            Iteration iter = new Iteration();
            iter.setIteration_id(iteration_id);
            ec.setIteration(iter);
            ec.setEvaluation_weight(evaluation_weight);
            ec.setTeam_evaluation(team_evaluation);
            ec.setCriteria_order(criteria_order);
            ec.setMax_loc(max_loc);
            ec.setStatus(status);

            EvaluationCriteriaDAO ecd = new EvaluationCriteriaDAO();

            ecd.addcriteria(ec);

            response.sendRedirect("evacriterialist");
        }

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
