/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dao.EvaluationCriteriaDAO;
import dao.IterationDAO;
import dao.SubjectDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import model.Account;
import model.EvaluationCriteria;
import model.Iteration;
import model.Subject;

/**
 *
 * @author admin
 */
public class UpdateCritController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet UpdateCritController</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet UpdateCritController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        Account account = (Account) request.getSession().getAttribute("account");
        if (account != null && account.getRole_id() != 1 && account.getRole_id() != 4) {
            int criteria_id = Integer.parseInt(request.getParameter("criteria_id"));
            EvaluationCriteriaDAO ecd = new EvaluationCriteriaDAO();
            EvaluationCriteria evacrit = ecd.getEC(criteria_id);
            int iteration_id = evacrit.getIteration().getIteration_id();
            System.out.println(iteration_id);
            int total = ecd.totalLoc(iteration_id);
            System.out.println(total);
            request.setAttribute("total", total);
            request.setAttribute("evacrit", evacrit);
            request.getRequestDispatcher("update_crit.jsp").forward(request, response);
        } else {
            response.sendRedirect("login");
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int criteria_id = Integer.parseInt(request.getParameter("criteria_id"));
        int evaluation_weight = Integer.parseInt(request.getParameter("evaluation_weight"));
        int team_evaluation = Integer.parseInt(request.getParameter("team_evaluation"));
        int criteria_order = Integer.parseInt(request.getParameter("criteria_order"));
        int max_loc = Integer.parseInt(request.getParameter("max_loc"));
        int status = Integer.parseInt(request.getParameter("status"));

        if (evaluation_weight >= 100) {
            request.setAttribute("error", "evaluation weight need to be smaller than 100");
            request.getRequestDispatcher("add_criteria.jsp").forward(request, response);
            response.sendRedirect("evacriterialist");
            
        } else {
            EvaluationCriteria ec = new EvaluationCriteria();
            ec.setCriteria_id(criteria_id);
            ec.setCriteria_order(criteria_order);
            ec.setEvaluation_weight(evaluation_weight);
            ec.setTeam_evaluation(team_evaluation);
            ec.setMax_loc(max_loc);
            ec.setStatus(status);

            EvaluationCriteriaDAO ecd = new EvaluationCriteriaDAO();
            ecd.updatecrit(ec);
            response.sendRedirect("evacriterialist");

        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
