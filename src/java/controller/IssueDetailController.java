/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dao.FunctionDAO;
import dao.IssueDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.sql.Date;
import java.util.ArrayList;
import model.Account;
import model.Function;
import model.Issue;
import model.Milestone;
import model.Team;

/**
 *
 * @author admin
 */
public class IssueDetailController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        Account account = (Account) request.getSession().getAttribute("account");
        IssueDAO isd = new IssueDAO();
        FunctionDAO fd = new FunctionDAO();
        if (account != null) {
            int issue_id = Integer.parseInt(request.getParameter("issue_id"));
            Issue issue = isd.getIssueDetail(issue_id);
            if (account.getRole_id() == 1) {
                ArrayList<Function> functionlist = fd.getAllFunctionbyUser(account.getEmail());
                ArrayList<Issue> issuetypelist = isd.getAllIssuetypebyUser(account.getEmail());
                
                System.out.println(functionlist.toString());
                request.setAttribute("functionlist", functionlist);
                request.setAttribute("issuetypelist", issuetypelist);
            }
            if (account.getRole_id() == 4) {
                int team_id = issue.getTeam().getTeam_id();
                ArrayList<Function> functionlist = fd.getAllFunctionTrainer(account.getEmail(), team_id);
                ArrayList<Issue> issuetypelist = isd.getAllIssuetypebyTrainer(account.getEmail());
                request.setAttribute("functionlist", functionlist);
                request.setAttribute("issuetypelist", issuetypelist);
            }
            request.setAttribute("issue", issue);
            request.getRequestDispatcher("issue_detail.jsp").forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        int issue_id = Integer.parseInt(request.getParameter("issue_id"));
        String issue_title = request.getParameter("issue_title");
        String description = request.getParameter("description");
        String email = request.getParameter("email");
        Account a = new Account();
        a.setEmail(email);
        int gitlab_id = Integer.parseInt(request.getParameter("gitlab_id"));
        String gitlab_URL = request.getParameter("gitlab_url");
        String raw_created_at = request.getParameter("created_at");
        String raw_due_date = request.getParameter("due_date");
        Date created_at = Date.valueOf(raw_created_at);
        Date due_date = Date.valueOf(raw_due_date);
        int team_id = Integer.parseInt(request.getParameter("team_id"));
        Team t = new Team();
        t.setTeam_id(team_id);
        int milestone_id = Integer.parseInt(request.getParameter("milestone_id"));
        Milestone m = new Milestone();
        m.setMilestone_id(milestone_id);
        int function_id = Integer.parseInt(request.getParameter("function_id"));
        Function f = new Function();
        f.setFunction_id(function_id);
        int status = Integer.parseInt(request.getParameter("status"));
        String labels = request.getParameter("labels");
        String issue_type = request.getParameter("issue_type");
                
        
        IssueDAO isd = new IssueDAO();
        Issue issue = new Issue();
        issue.setIssue_id(issue_id);
        issue.setAccount(a);
        issue.setIssue_title(issue_title);
        issue.setDescription(description);
        issue.setGitlab_id(gitlab_id);
        issue.setGitlab_url(gitlab_URL);
        issue.setCreated_at(created_at);
        issue.setDue_date(due_date);
        issue.setTeam(t);
        issue.setMilestone(m);
        issue.setFunction(f);
        issue.setLabels(labels);
        issue.setIssue_type(issue_type);
        issue.setStatus(status);
        isd.updateIssue(issue);
        response.sendRedirect("issuedetail?issue_id="+issue_id);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
