package controller;

import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import model.Account;

public abstract class BaseAuthController extends HttpServlet {

	private boolean isAutheticated(HttpServletRequest request) {
		Account account = (Account) request.getSession().getAttribute("account");
		if (account != null && account.getRole_id() != 1) {
			return account != null;
		} else {
			return account == null;
		}
	}

	protected abstract void processGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException;

	protected abstract void processPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		if (isAutheticated(request)) {
			processGet(request, response);
		} else {
			response.sendRedirect("login");
		}
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		if (isAutheticated(request)) {

			processPost(request, response);
		} else {
			response.sendRedirect("login");
		}
	}
}
