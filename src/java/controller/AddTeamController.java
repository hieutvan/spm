/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dao.ClassDAO;
import dao.TeamDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import model.Account;
import model.Classes;
import model.Team;

/**
 *
 * @author admin
 */
public class AddTeamController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        Account account = (Account) request.getSession().getAttribute("account");
        TeamDAO td = new TeamDAO();
        ClassDAO cld = new ClassDAO();
        if (account != null && account.getRole_id() == 4) {
            ArrayList<Classes> classlist = cld.getAllbytrainer(account.getEmail());
            request.setAttribute("classlist", classlist);
            request.getRequestDispatcher("add_team.jsp").forward(request, response);
        } else {
            response.sendRedirect("index");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int class_id = Integer.parseInt(request.getParameter("class_id"));
        String topic_code = request.getParameter("topic_code");
        String team_code = request.getParameter("team_code");
        String topic_name = request.getParameter("topic_name");
        String gitla_url = request.getParameter("gitlab_url");
        int status = Integer.parseInt(request.getParameter("status"));

        Team t = new Team();
        Classes c = new Classes();
        c.setClass_id(class_id);
        t.setTeam_code(team_code);
        t.setClasses(c);
        t.setTopic_code(topic_code);
        t.setTopic_name(topic_name);
        t.setGitlab_url(gitla_url);
        t.setStatus(status);

        TeamDAO td = new TeamDAO();
        td.addTeam(t);
        response.sendRedirect("teamlist");
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
