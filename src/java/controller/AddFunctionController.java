/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dao.AccountDAO;
import dao.FeatureDAO;
import dao.FunctionDAO;
import dao.TeamDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import model.Account;
import model.Feature;
import model.Function;
import model.Team;

/**
 *
 * @author admin
 */
public class AddFunctionController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        Account account = (Account) request.getSession().getAttribute("account");
        FeatureDAO fed = new FeatureDAO();
        AccountDAO acd = new AccountDAO();
        if (account != null) {
            if (account.getRole_id() == 1) {
                ArrayList<Feature> featurelist = fed.getallfeaturebyteam(account.getEmail());
                request.setAttribute("featurelist", featurelist);
            }
            if (account.getRole_id() == 4) {
                ArrayList<Feature> featurelist = fed.getAllFeaturebyTrainer(account.getEmail());
                ArrayList<Account> accountlist = acd.getAllStudentbyTrainer(account.getEmail());
                request.setAttribute("featurelist", featurelist);
                request.setAttribute("accountlist", accountlist);
            }
            request.getRequestDispatcher("add_function.jsp").forward(request, response);
        } else {
            response.sendRedirect("index");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Account account = (Account) request.getSession().getAttribute("account");
        if (account.getRole_id() == 1) {
            TeamDAO td = new TeamDAO();
            Team t = td.getAllTeambyUser(account.getEmail());
            int team_id = t.getTeam_id();
            String function_name = request.getParameter("function_name");
            int feature_id = Integer.parseInt(request.getParameter("feature_id"));
            String access_roles = request.getParameter("access_roles");
            String description = request.getParameter("description");
            int complexity_id = Integer.parseInt(request.getParameter("complexity_id"));
            String email = account.getEmail();
            int priority = Integer.parseInt(request.getParameter("priority"));
            int status = Integer.parseInt(request.getParameter("status"));
            FunctionDAO fd = new FunctionDAO();
            Function function = new Function();
            function.setTeam(t);
            function.setFunction_name(function_name);
            function.setFeature(new Feature(feature_id));
            function.setAccess_roles(access_roles);
            function.setDescription(description);
            function.setComplexity_id(complexity_id);
            function.setAccount(account);
            function.setPriority(priority);
            function.setStatus(status);
            fd.addFunction(function);
        }
        if (account.getRole_id() == 4) {
            TeamDAO td = new TeamDAO();
            String function_name = request.getParameter("function_name");
            int feature_id = Integer.parseInt(request.getParameter("feature_id"));
            String access_roles = request.getParameter("access_roles");
            String description = request.getParameter("description");
            int complexity_id = Integer.parseInt(request.getParameter("complexity_id"));
            String email = request.getParameter("email");
            Team t = td.getAllTeambyUser(account.getEmail());
            int team_id = t.getTeam_id();
            int priority = Integer.parseInt(request.getParameter("priority"));
            int status = Integer.parseInt(request.getParameter("status"));
            FunctionDAO fd = new FunctionDAO();
            Function function = new Function();
            function.setTeam(t);
            function.setFunction_name(function_name);
            function.setFeature(new Feature(feature_id));
            function.setAccess_roles(access_roles);
            function.setDescription(description);
            function.setComplexity_id(complexity_id);
            function.setAccount(account);
            function.setPriority(priority);
            function.setStatus(status);
            fd.addFunction(function);
            fd.addFunction(function);
        }
        response.sendRedirect("functionlist");
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
