/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dao.FunctionDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import model.Account;
import model.Feature;
import model.Function;
import model.Team;

/**
 *
 * @author admin
 */
public class FunctionDetailController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        Account account = (Account) request.getSession().getAttribute("account");
        FunctionDAO fud = new FunctionDAO();
        if (account != null) {
            if (account.getRole_id() == 1) {
                int function_id = Integer.parseInt(request.getParameter("function_id"));
                Function function = fud.getFunctionDetail(function_id);
                request.setAttribute("function", function);
            }
            if (account.getRole_id() == 4) {

                int function_id = Integer.parseInt(request.getParameter("function_id"));

                Function function = fud.getFunctionDetail(function_id);
                ArrayList<Function> TandFlist = fud.getAllTeamandFeature(function.getTeam().getTeam_id());
                request.setAttribute("function", function);
                request.setAttribute("list", TandFlist);

            }
            request.getRequestDispatcher("function_detail.jsp").forward(request, response);
        } else {
            response.sendRedirect("index");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Account account = (Account) request.getSession().getAttribute("account");
        FunctionDAO fd = new FunctionDAO();
        if (account != null) {
            int function_id = Integer.parseInt(request.getParameter("function_id"));
            int team_id = Integer.parseInt(request.getParameter("team_id"));
            String function_name = request.getParameter("function_name");
            int feature_id = Integer.parseInt(request.getParameter("feature_id"));
            String access_roles = request.getParameter("access_roles");
            String description = request.getParameter("description");
            int complexity_id = Integer.parseInt(request.getParameter("complexity_id"));
            String email = request.getParameter("email");
            int priority = Integer.parseInt(request.getParameter("priority"));
            int status = Integer.parseInt(request.getParameter("status"));
            
            Team t = new Team();
            t.setTeam_id(team_id);
            Feature f = new Feature();
            f.setFeature_id(feature_id);
            Account a = new Account();
            a.setEmail(email);
            Function function = new Function(function_id, t, function_name, f, access_roles, description, complexity_id, a, priority, status);
            fd.updateFunction(function);
            response.sendRedirect("functionlist");
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
