/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dao.FunctionDAO;
import dao.LOCEvaluationDAO;
import dao.SubjectDAO;
import dao.Subject_SettingDAO;
import dao.TrackingDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import model.Account;
import model.Function;
import model.LOC_Evaluation;
import model.Subject;
import model.Subject_Setting;
import model.Tracking;

/**
 *
 * @author admin
 */
public class AddLocEvaController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        Account account = (Account) request.getSession().getAttribute("account");
        TrackingDAO trd = new TrackingDAO();
        SubjectDAO sd = new SubjectDAO();
        Subject_SettingDAO ssd = new Subject_SettingDAO();
        FunctionDAO fd = new FunctionDAO();
        if (account != null) {
            if (account.getRole_id() == 4) {
                int tracking_id = Integer.parseInt(request.getParameter("tracking_id"));
                Tracking track = trd.getFunctionbyTracking(tracking_id);
                Subject subject = sd.getsubjectbytracking(tracking_id);
                ArrayList<Subject_Setting> sslist = ssd.getQuality(subject.getSubject_id());
                ArrayList<Function> complexlist = fd.getallComplexity();
                LocalDate raw_eva_at = LocalDate.now();
                Date eva_at = Date.valueOf(raw_eva_at);
                request.setAttribute("tracking_id", tracking_id);
                request.setAttribute("sslist", sslist);
                request.setAttribute("subject", subject);
                request.setAttribute("track", track);
                request.setAttribute("complexlist", complexlist);
                request.setAttribute("eva_at", eva_at);
            }
            request.getRequestDispatcher("add_Loc_Eva.jsp").forward(request, response);
        }else{
            response.sendRedirect("index");
        }
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        LocalDate raw_evaluation_time = LocalDate.now();
        Date evaluation_time = Date.valueOf(raw_evaluation_time);
        String evaluation_note = request.getParameter("evaluation_note");
        int complex_value = Integer.parseInt(request.getParameter("complex_value"));
        int quality_value = Integer.parseInt(request.getParameter("quality_value"));
        int complexity_id = 0;
        if (complex_value == 60) {
            complexity_id = 1;
        } else if (complex_value == 120) {
            complexity_id = 2;
        } else if (complex_value == 240) {
            complexity_id = 3;
        }
        int quality_id = 0;
        if (quality_value == 100) {
            quality_id = 1;
        } else if (quality_value == 75) {
            quality_id = 2;
        } else if (quality_value == 50) {
            quality_id = 3;
        } else if (quality_value == 0) {
            quality_id = 4;
        }
        int LOC = Integer.parseInt(request.getParameter("LOC"));
        int function_id = Integer.parseInt(request.getParameter("function_id"));
        int tracking_id = Integer.parseInt(request.getParameter("tracking_id"));
        LOC_Evaluation le = new LOC_Evaluation();
        LOCEvaluationDAO led = new LOCEvaluationDAO();
        le.setEvaluation_time(evaluation_time);
        le.setEvaluation_note(evaluation_note);
        le.setComplexity_id(complexity_id);
        le.setQuality_id(quality_id);
        le.setLOC(LOC);
        Tracking track = new Tracking();
        track.setTracking_id(tracking_id);
        le.setTracking(track);

        led.addLocEva(le);
        Function func = new Function();
        FunctionDAO fd = new FunctionDAO();
        func.setFunction_id(function_id);
        func.setComplexity_id(complexity_id);
        fd.updateComplexity(complexity_id, function_id);
        response.sendRedirect("locevafunction?tracking_id=" + tracking_id);

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
