/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dao.ClassDAO;
import dao.IterationDAO;
import dao.MilestoneDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.sql.Date;
import java.util.ArrayList;
import model.Account;
import model.Classes;
import model.Iteration;
import model.Milestone;

/**
 *
 * @author admin
 */
public class MilestoneDetailController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        Account account = (Account) request.getSession().getAttribute("account");
        MilestoneDAO msd = new MilestoneDAO();
        IterationDAO itd = new IterationDAO();
        ClassDAO cld = new ClassDAO();
        if (account != null && account.getRole_id() == 4) {
            int milestone_id = Integer.parseInt(request.getParameter("milestone_id"));
            Milestone milestone = msd.getMilestonebyID(milestone_id);
            ArrayList<Iteration> iterlist = itd.getall();
            ArrayList<Classes> classlist = cld.getallclass();
            
            request.setAttribute("milestone", milestone);
            request.setAttribute("classlist", classlist);
            request.setAttribute("iterlist", iterlist);
            request.getRequestDispatcher("milestone_detail.jsp").forward(request, response);
        } else {
            response.sendRedirect("index");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int milestone_id = Integer.parseInt(request.getParameter("milestone_id"));
        int iteration_id = Integer.parseInt(request.getParameter("iteration_id"));
        int class_id = Integer.parseInt(request.getParameter("class_id"));
        String raw_from_date = request.getParameter("from_date");
        String raw_to_date = request.getParameter("to_date");
        int status = Integer.parseInt(request.getParameter("status"));
        
        Date from_date = Date.valueOf(raw_from_date);
        Date to_date = Date.valueOf(raw_to_date);
        
        
        Milestone m = new Milestone();
        Classes c = new Classes();
        Iteration i = new Iteration();
        m.setMilestone_id(milestone_id);
        i.setIteration_id(iteration_id);
        m.setIteration(i);
        c.setClass_id(class_id);
        m.setClasses(c);
        m.setFrom_date(from_date);
        m.setTo_date(to_date);
        m.setStatus(status);
        
        
        MilestoneDAO mld = new MilestoneDAO();
        mld.updatemilestone(m);
        response.sendRedirect("milestonelist");

        
                
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
